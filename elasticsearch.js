const { Client } = require('@elastic/elasticsearch');
const database = require('./src/configs/database.config')
const http = require('http');

var client = new Client({
  node: database.elasticsearchHost,
  auth: {
    username: 'elastic',
    password: '123456',
  },
});

exports.insertData = async (data, index) => {
  const id = data._id;
  const newData = JSON.stringify(data);
  console.log(newData);
  let exists = await client.search({
    index,
    body: {
      query: {
        terms: {
          _id: [ id ] 
        }
      },
    },
  });
  let count = exists.body.hits.total.value;
  if (count) {
    return
  }
  console.log(id);
  const post_options = {
    host: database.elasticsearchSyncHost,
    port: '9200',
    path: `/${index}/_doc/${id}`,
    method: 'POST',
    auth: {
      username: 'elastic',
      password: '123456',
    },
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(newData)
    }
  }
  const post_req = http.request(post_options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        console.log('Response: ' + chunk);
    });
  });

// post the data
  await post_req.write(newData);
  await post_req.end();
}
exports.deleteByQuery = async (index, code) => {
  try {
    const a = await client.deleteByQuery({
      index: index,
      body: {
        query: {
          match: {
            rel_id: code
          }
        }
      }
    });
    console.log(a);
  } catch (e) {
    console.log(e.meta);
    return false;
  }
};
