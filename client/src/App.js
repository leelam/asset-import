import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import socketIOClient from 'socket.io-client';
import { Form, Select, Input, Progress, Button } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';

import './index.css';
const { Option } = Select;

axios.defaults.timeout = 50000;
const styleForm = {
  labelCol: { span: 4 },
  wrapperCol: { span: 4 },
};
const socket = socketIOClient('http://localhost:9009');

function ProcessImport({ process, length }) {
  return (
    <React.Fragment>
      <p>{`Đã import thành công ${process ? process : 0}/${length}`}</p>
      <Progress
        strokeLinecap="square"
        percent={+((process || 0) / length).toFixed(2) * 100}
      />
    </React.Fragment>
  );
}
class App extends Component {
  constructor(props) {
    super(props);
    socket.on('change-process', (data) =>
      this.setState({
        process: data,
      })
    );
    socket.on('send-length', (data) =>
      this.setState({
        length: data,
      })
    );
    socket.on('send-list-sheet', (data, fn) => {
      this.setState({
        sheets: data,
        callBackSheetSelect: fn,
      });
    });
    socket.on('send-error', (data) =>
      this.setState({
        errorImport: data,
      })
    );
    socket.on('send-import-detail', (data) => {
      console.log(data);
      this.setState({
        detailImport: data,
      });
    });
    socket.on('send-error', (data) =>
      this.setState({
        errorImport: data,
      })
    );
  }
  state = {
    sheets: [],
    importKmlFile: false,
    socket: {},
    process: '',
    fileName: '',
    length: 0,
    detailImport: '',
    errorImport: '',
  };
  componentDidMount() {
    this.setState({
      socket,
    });
  }
  componentWillUnmount() {
    socket.off('some event');
  }
  fetchSheet = async (value) => {
    const checkKml =
      this.state.fileName.substring(
        this.state.fileName.length - 3,
        this.state.fileName.length
      ) === 'kml';

    if (checkKml) {
      const test = await axios.get(
        `http://localhost:9009/api/v1/import_KML/kmlFile?id=${this.state.socket.id}&fileName=${this.state.fileName}&model=${value}`
      );
      return;
    }
    if (value === 'json') {
      const test = await axios.get(
        `http://localhost:9009/api/v1/import_exel/excel-json?id=${this.state.socket.id}&fileName=${this.state.fileName}&model=${value}`
      );
      return;
    }

    const test = await axios.get(
      `http://localhost:9009/api/v1/import_exel/update-product?id=${this.state.socket.id}&fileName=${this.state.fileName}&model=${value}`
    );
  };
  emitEventSelectSheet = async (value) => {
    this.state.callBackSheetSelect(value);
    this.setState({
      process: 'Đang import',
    });
  };
  render() {
    const { sheets } = this.state;
    const listSheet = sheets.length ? (
      <React.Fragment>
        <Form.Item label="Chọn sheet cần import">
          <Select onChange={(value) => this.emitEventSelectSheet(value)}>
            {sheets.map((item) => (
              <Option key={item} value={item}>
                {item}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </React.Fragment>
    ) : null;
    return (
      <div className="App">
        <h1>{this.state.process}</h1>
        <Form {...styleForm}>
          <Form.Item label="Nhập đường dẫn file">
            <Input
              type="file"
              onChange={(event, value) => {
                this.setState({
                  fileName: event.target.files[0].name,
                });
              }}
            />
          </Form.Item>
          <Form.Item label="Chọn danh mục cần import">
            <Select onChange={(value) => this.fetchSheet(value)}>
              <Option key={1} value="product">
                Bất động sản
              </Option>
              <Option key={2} value="project">
                Dự án
              </Option>
              <Option key={3} value="assets_planning">
                Quy hoạch
              </Option>
              <Option key={4} value="assets_utility_demo_1">
                Tiện ích
              </Option>
              <Option key={5} value="res_company">
                Danh ba
              </Option>
              <Option key={6} value="res_road">
                Con duong
              </Option>
              <Option key={7} value="res_road_category">
                Toan duong
              </Option>
              <Option key={8} value="res_side">
                Vùng miền
              </Option>
              <Option key={9} value="json">
                Excel to json
              </Option>
              <Option key={10} value="res_ward">
                Phường/xã
              </Option>
              <Option key={11} value="res_district">
                Quận/huyện
              </Option>
              <Option key={12} value="res_province">
                Tỉnh/thành
              </Option>
              <Option key={13} value="res_polygon">
                res_polygon
              </Option>
            </Select>
          </Form.Item>

          {listSheet}
        </Form>
        {this.state.errorImport}
        <div style={{ width: '50%', margin: 'auto' }}>
          {this.state.detailImport && (
            <ProcessImport
              process={this.state.detailImport}
              length={this.state.length}
            />
          )}
        </div>
      </div>
    );
  }
}

export default App;
