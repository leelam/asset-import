const mongoose = require('mongoose');
const asset_point_model = require('../models/asset_point.model');
const asset_point_level_model = require('../models/asset_point_level.model');
const user_activity_model = require('../models/user_activity.model');
const user_activity_category_model = require('../models/user_activity_category.model');
const product_model = require('../models/product.model');
const product_type_model = require('../models/product_type.model');
const post_model = require('../models/post.model');
const authBl = require('../controllers/auth.controller');

const GetProductTypeId = async (listCode) => {
  try {
    const list = await product_type_model.find({ code: { $in: listCode } });
    return list;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const GetAllPost = async () => {
    try {
        const list = await post_model.find();
        return list;
    } catch (error) {
        console.log(error);
        throw new Error(error);
    }
}

const GetListProductSnR = async () => {
  try {
    const typeId = await GetProductTypeId(['SELL', 'RENT']);
    await typeId.map((item) => {
      return mongoose.Types.ObjectId(item._id);
    });
    const list = await product_model.find({ type_id: { $in: typeId } });
    return list;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const LIST_MODEL_NAME = ['comment', 'user_contact'];

const activityCategory = async (user_type, type) => {
  try {
    const result = await user_activity_category_model.findOne({
      code: type,
      user_type: user_type,
    });
    return result
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const getPointByUserId = async (user_id) => {
  try {
    let point = await asset_point_model.findOne({ rel_id: user_id });
    const check_user = await authBl.checkUserType(user_id);
    if (!point) {
      if (check_user !== 'USER') {
        point = await new asset_point_model({
          rel_id: user_id,
          referendum_point: 5,
        }).save();
      } else {
        point = await new asset_point_model({
          rel_id: user_id,
        }).save();
      }
    }
    const point_level = await asset_point_level_model.findOne({
      min_point: { $lte: point.total_point },
      max_point: { $gte: point.total_point },
    });
    return { point, point_level };
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const addPoint = async (data) => {
  try {
    const { user_id, user_type, type } = data; 
    const check = await activityCategory(user_type, type);
    if (!check) {
      return true;
    }
    let check_activity = await user_activity_model.findOne({
      user_id,
      category: check._id,
      rel_table: data.rel_table,
    });
    if (LIST_MODEL_NAME.indexOf(data.modelName) !== -1) {
      check_activity = await user_activity_model.findOne({
        user_id,
        category: check._id,
        obj_table: data.obj_table,
      });
    }
    if (check_activity) {
      return true;
    }
    const get_point = await getPointByUserId(user_id);
    const old_level = get_point.point_level;
    let user_point = get_point.point;
    const {
      referendum_point,
      interactive_point,
      contribute_point,
      achievements_point,
    } = user_point;
    const updateField = check.group_point;
    const newPoint = eval(check.recipe_point);
    if (newPoint !== 0) {
      eval(check.update_field);
      await user_point.save();
      await addActivity(
        user_id,
        check._id,
        check.point,
        data.text || '',
        data.modelName,
        data.rel_table,
        data.obj_table,
      );
    }
    const new_level = await asset_point_level_model.findOne({
      min_point: { $lte: user_point.total_point },
      max_point: { $gte: user_point.total_point },
    });
    if (old_level.code === new_level.code) {
      return true;
    }
    await addPoint({ user_id, user_type, type: 'UPGRADE' });
    return true;
  } catch (error) {
    throw new Error(error);
  }
};

const addActivity = async (
  user_id,
  category,
  point,
  text,
  collection_name,
  rel_table,
  obj_table,
) => {
  try {
    const activity = await new user_activity_model({
      user_id,
      category,
      point,
      text,
      collection_name,
      rel_table,
      obj_table,
    }).save();
    return activity;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

module.exports = {
  GetListProductSnR,
  activityCategory,
  getPointByUserId,
  addPoint,
  GetAllPost,
};
