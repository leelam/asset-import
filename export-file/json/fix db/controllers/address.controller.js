var cityModel = require('../models/res_province.model');
var provinceModel = require('../models/res_district.model');
var wardModel = require('../models/res_ward.model');
const res_addressModel = require('../models/res_address.model');
const res_search_stringModel = require('../models/res_search_string.model');
const change_alias = require('../helpers/change_alias');
const { get } = require('lodash');
var mongoose = require('mongoose');
const HCM_ID = "5dc0cfc6bf871b3a3c6296bd";
exports.Province = async () => {
	let province = await cityModel.find({}).sort({sequence: -1});
	let otherProvince = [], priorityProvince = [], result = [];
	province.map(item => {
		if(!item.sequence) otherProvince.push(item);
		else priorityProvince.push(item);
	})
	otherProvince.sort((a, b) => (a.name.localeCompare(b.name)))
	result = [...priorityProvince, ...otherProvince];
  return result;
};

exports.District = async city_id => {
	const validId = mongoose.Types.ObjectId.isValid(city_id);
	if(validId) {
		let province = await provinceModel.find({ province_id: mongoose.Types.ObjectId(city_id) }).sort({ name: 1 });
		if(city_id === HCM_ID) {
			let provinceNumber = [];
			province.map((item, index) => {
				if(item.name.includes("Quận") && item.name.length <= 7) {
					provinceNumber.push(item)
				}
			})
			const arrSplice = provinceNumber.splice(1, 3);
			const result = provinceNumber.concat(arrSplice);
			province.map((item, index) => {
				if(item.name.includes("Quận") && item.name.length <= 7) {
					province.splice(index, 1, result[index - 5])
				}
			})
		}
		return province;
	} else {
		return [];
	}
};

exports.Ward = async province_id => {
	const validId = mongoose.Types.ObjectId.isValid(province_id);
	if(validId) {
		let ward = await wardModel.find({ district_id: mongoose.Types.ObjectId(province_id) })
		ward.sort((a, b) => (a.name.localeCompare(b.name)));	//sort by name
		let wardNumber = [];
		//find ward with number
		ward.map((item, index) => {
			if(item.name.includes("Phường") && item.name.length <= 9) {
				//get number in ward and parse to number type
				const numberStr = item.name.slice(7);
				const number = parseInt(numberStr);
				item.name = number;
				wardNumber.push(item)
			}
		})
		if(wardNumber.length > 0) {
			wardNumber.sort((a, b) => a.name - b.name);		//sort by number
			wardNumber.forEach(item => item.name = `Phường ${item.name}`)		//change to string
			//replace after sort
			ward.map((item, index) => {
				if(item.name.includes("Phường") && item.name.length <= 9) {
					ward.splice(index, 1, wardNumber[index])
				}
			})
		}
		return ward;
	} else {
		return [];
	}
};

exports.getAddress = async (_id) => {
  try {
    const address = await wardModel.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(_id),
        },
      },
      {
        $lookup: {
          from: 'res_district',
          localField: 'district_id',
          foreignField: '_id',
          as: 'district',
        },
      },
      {
        $unwind: '$district',
      },
      {
        $lookup: {
          from: 'res_province',
          localField: 'district.province_id',
          foreignField: '_id',
          as: 'province',
        },
      },
      {
        $unwind: '$province',
      },
      {
        $addFields: {
          ward: {
            _id: '$_id',
            name: '$name',
            code: '$code',
          },
        },
      },
      {
        $project: {
          'province._id': 1,
          'province.code': 1,
          'province.name': 1,
          'district.name': 1,
          'district.code': 1,
          'district._id': 1,
          ward: 1,
        },
      },
    ]);
    if (!address.length)
      return {
        status: false,
        message: 'Không tìm thấy địa chỉ',
      };
    return {
      status: true,
      data: address[0],
    };
  } catch (e) {
    console.log(e);
    return {
      status: false,
      message: 'Server lỗi',
    };
  }
};

exports.mergeAddress = async () => {
  try {
    const province = await cityModel.aggregate([
      {
        $lookup: {
          from: 'res_polygon',
          let: { idProvince: '$_id' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$rel_id', '$$idProvince'],
                },
              },
            },
          ],
          as: 'polygon',
        },
      },
    ]);
    const result = await Promise.all(
      province.map(async (item) => {
        const check = await res_addressModel.findOne({ code: item.code });
        if (!check) {
          const newAddressC = new res_addressModel({
            ...province,
            address_name: item.name,
            type: 'C',
            parent_id: null,
            points: item.points,
            code: item.code,
            reaction: [],
            polygon_id: item.polygon.map((ele) => ele._id),
          });
          await newAddressC.save();
          const district = await provinceModel.aggregate([
            {
              $match: {
                province_id: mongoose.Types.ObjectId(item._id),
              },
            },
            {
              $lookup: {
                from: 'res_polygon',
                let: { idProvince: '$_id' },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $eq: ['$rel_id', '$$idProvince'],
                      },
                    },
                  },
                ],
                as: 'polygon',
              },
            },
          ]);
          const saveDistrict = await Promise.all(
            district.map(async (item) => {
              const newAddressP = new res_addressModel({
                ...item,
                address_name: item.name,
                type: 'D',
                points: item.points,
                code: item.code,
                parent_id: newAddressC._id,
                reaction: [],
                polygon_id: item.polygon.map((ele) => ele._id),
              });
              await newAddressP.save();
              const ward = await wardModel.aggregate([
                {
                  $match: {
                    district_id: mongoose.Types.ObjectId(item._id),
                  },
                },
                {
                  $lookup: {
                    from: 'res_polygon',
                    let: { idProvince: '$_id' },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $eq: ['$rel_id', '$$idProvince'],
                          },
                        },
                      },
                    ],
                    as: 'polygon',
                  },
                },
              ]);
              const saveWard = await Promise.all(
                ward.map(async (item) => {
                  const newAddressW = new res_addressModel({
                    ...item,
                    address_name: item.name,
                    type: 'W',
                    points: item.points,
                    parent_id: newAddressP._id,
                    code: item.code,
                    reaction: [],
                    polygon_id: item.polygon.map((ele) => ele._id),
                  });
                  await newAddressW.save();
                })
              );
            })
          );
        }
      })
    );
    return result;
  } catch (e) {
    console.log(e);
    return false;
  }
};

exports.addSearchString = async () => {
  const address = await res_addressModel.aggregate([
    {
      $match: {
        status: true,
      },
    },
    {
      $lookup: {
        from: 'res_address_new',
        localField: 'parent_id',
        foreignField: '_id',
        as: 'parentDetail',
      },
    },
    {
      $unwind: {
        path: '$parentDetail',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'res_address_new',
        localField: 'parentDetail.parent_id',
        foreignField: '_id',
        as: 'GrandParentDetail',
      },
    },
    {
      $unwind: {
        path: '$GrandParentDetail',
        preserveNullAndEmptyArrays: true,
      },
    },
  ]);
  const weight = {
    C: 1,
    D: 0.5,
    W: 0.3,
  };
  const result = await Promise.all(
    address.map((item) => {
      const lowerCaseStringName = item.address_name.toLowerCase();
      const noneAccentName = change_alias(item.address_name.toLowerCase());
      const weightSearch =
        item.code === '79' || item.code === '01' ? 2 : weight[item.type];

      const subTitle = `${get(item, 'parentDetail.address_name', '')} ${get(
        item,
        'GrandParentDetail.address_name',
        ''
      )}`;
      const newSearch = new res_search_stringModel({
        name: 'res_address',
        rel_id: item._id,
        weight: weightSearch,
        sub_title: subTitle,
        title: item.address_name,
        refPath: 'res_address_new',
        search: `${lowerCaseStringName} ${noneAccentName}`,
      });
      return newSearch.save();
    })
  );
  return result;
};

exports.addDistrict = async () => {
  const district = await provinceModel.aggregate([
    {
      $match: {
        code: '7902',
      },
    },
    {
      $lookup: {
        from: 'res_polygon',
        let: { idProvince: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$rel_id', '$$idProvince'],
              },
            },
          },
        ],
        as: 'polygon',
      },
    },
  ]);
  const newDistrict = new res_addressModel({
    ...district[0],
    address_name: district[0].name,
    type: 'D',
    points: district[0].points,
    code: district[0].code,
    parent_id: '5dc100149bae9d046cfc645e',
    reaction: [],
    polygon_id: district[0].polygon.map((ele) => ele._id),
  });
  await newDistrict.save();
  const resWard = await wardModel.aggregate([
    {
      $match: {
        district_id: mongoose.Types.ObjectId(district[0]._id),
      },
    },
    {
      $lookup: {
        from: 'res_polygon',
        let: { idProvince: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$rel_id', '$$idProvince'],
              },
            },
          },
        ],
        as: 'polygon',
      },
    },
  ]);
  const result = await Promise.all(
    resWard.map(async (item) => {
      const newWard = new res_addressModel({
        ...item,
        address_name: item.name,
        type: 'W',
        points: item.points,
        code: item.code,
        parent_id: newDistrict._id,
        reaction: [],
        polygon_id: item.polygon.map((ele) => ele._id),
      });
      await newWard.save();
    })
  );
  return true;
};
