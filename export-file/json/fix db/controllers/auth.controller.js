const mongoose = require('mongoose');
const res_user_companyModel = require("../models/res_user_company.model");

// lamtun check type user
exports.checkUserType = async (user_id) => {
    try {
      const userCompany = await res_user_companyModel.aggregate([
        {
          $match: {
            user_id: mongoose.Types.ObjectId(user_id),
          }
        },
        {
          $lookup: {
            from: 'res_user_group',
            localField: 'group_id',
            foreignField: '_id',
            as: 'user_group'
          }
        },
        {
          $unwind: {
            path: '$user_group',
            preserveNullAndEmptyArrays: true
          }
        },
      ])
      let user_type;
      if (!userCompany.length) {  
        user_type = 'USER';
        return user_type;
      }
      user_type = userCompany[0].user_group.code;
      return user_type; 
    } catch (error) {
      throw new Error(error)
    }
  }