require('dotenv').config();
var express = require('express');
var cors = require('cors');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var http = require('http');
const server = http.createServer(app);
console.log(
  `======================= ${process.env.NODE_ENV} =======================`
);
// Configuring the database
const { database } = require('./configs/index');
require('./models/res_company.model')
const mongoose = require('mongoose');
console.log(database);
mongoose.Promise = global.Promise;
// Connecting to the database
mongoose
  .connect(database.mongoUrl, {
    useCreateIndex: true,
    useNewUrlParser: true,
    auth: { authSource: 'admin' },
    user: database.MONGOUSER,
    pass: database.MONGOPASSWORD,
  })
  .then(async () => {
    const allModel = mongoose.modelNames();

    allModel.forEach((model) => {
      const modelInstance = mongoose.model(model);
      const save = modelInstance.prototype.save;
      modelInstance.prototype.save = async function () {
        return await save.apply(this, arguments);
      };
    });
    const count = await mongoose.model('res_company').find().count();
    console.log(count);
    
    const nin = [];
    for (let index = 0; index < count; index++) {
      let item;
      if (!index) {
        item = await mongoose.model('res_company').findOne();
        await item.save();
        nin.push(item._id);
      }
      item = await mongoose.model('res_company').findOne({_id: {$nin: nin}});
      if (!item) {
        return true
      }
      await item.save();
      console.log(item);
      nin.push(item._id);
    }
  })
  .catch((err) => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
  });

var whitelist =
  process.env.NODE_ENV === 'production'
    ? ['https://asset.vn', 'https://www.asset.vn']
    : [
        'http://localhost:3000',
        'http://localhost:4000',
        'http://localhost:4001',
        'http://192.168.1.18:4000',
        'http://192.168.1.20:4000',
        'http://192.168.1.:4000',
        'http://192.168.1.43:4000',
        'http://192.168.1.85:4000',
        'http://192.168.1.52:4000',
        'http://192.168.1.240:9001',
        'http://192.168.1.94:4000',
        'https://test.asset.vn',
        'http://192.168.1.240:9000',
        'http://192.168.1.240:4100',
      ];
var corsOptions = {
  origin: whitelist,
  optionsSuccessStatus: 200,
  credentials: true,
};
app.use(cookieParser());
app.use(cors(corsOptions));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});
app.use(express.static(path.join(__dirname, '../resources')));
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true,
  })
);
app.get('/', (req, res) => res.json({ message: 'hello api page' }));

server.listen(process.env.PORT, () => {
  console.log('Site running on port ' + process.env.PORT);
});

