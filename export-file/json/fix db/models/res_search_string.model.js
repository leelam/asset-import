const mongoose = require('mongoose');
const { Schema } = mongoose;
const mongoosastic = require('mongoosastic');
const { database } = require('../configs/index');
const weight = {
  product: 0.5,
  project: 0.5,
  res_address: 1,
  post: 0.2,
  company: 0.1,
  resource: 0.1,
  res_road: '0.1',
  ask: 0.1,
};

var Res_search_stringSchema = mongoose.Schema(
  {
    name: {
      type: String,
      re: true,
      es_indexed: true,
      enum: [
        'res_address',
        'project',
        'post',
        'res_road',
        'ask',
        'resource',
        'company',
        'product',
        'guide',
      ],
    },
    status: {
      type: Boolean,
      default: true,
      es_indexed: true,
      es_type: 'boolean',
    },
    rel_id: {
      type: Schema.Types.ObjectId,
      es_indexed: true,
      unique: true,
      refPath: 'refPath',
    },
    weight: {
      es_indexed: true,
      es_type: 'float',
      type: Number,
      default: 0,
    },
    refPath: {
      type: String,
      require: true,
      es_indexed: true,
      enum: [
        'res_address_new',
        'project',
        'post',
        'res_road',
        'ask',
        'res_resource',
        'res_company',
        'product',
        'guide',
      ],
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_ward',
      es_indexed: true,
      es_type: 'keyword',
    },
    district: {
      type: Schema.Types.ObjectId,
      ref: 'res_district',
      es_indexed: true,
      es_type: 'keyword',
    },
    province: {
      type: Schema.Types.ObjectId,
      ref: 'res_province',
      es_indexed: true,
      es_type: 'keyword',
    },
    title: {
      type: String,
      es_indexed: true,
      es_type: 'text',
    },
    sub_title: {
      type: String,
      es_indexed: true,
      es_type: 'text',
    },
    search: {
      type: String,
      es_indexed: true,
      es_type: 'text',
      es_analyzer: 'whitespace',
      es_search_analyzer: 'whitespace',
    },
    rating: Number,
    import: {
      type: Boolean,
      es_indexed: true,
      es_type: 'boolean',
    },
  },
  {
    collection: 'res_search_string',
  }
);
Res_search_stringSchema.set('toObject', { virtuals: true, minimize: true });
Res_search_stringSchema.set('toJSON', { virtuals: true, minimize: true });
Res_search_stringSchema.index({ search: 'text' }, { default_language: 'none' });
Res_search_stringSchema.index({ search: 'text', rating: 1 });
Res_search_stringSchema.index({ refPath: 'text' });
Res_search_stringSchema.index({ search: 'text', name: 1 });
Res_search_stringSchema.pre('find', function () {
  this.where({ status: true });
});
Res_search_stringSchema.pre('save', async function (next) {
  await this.populate('relDetail').execPopulate();
  if (this.weight !== 0 && !this.weight) {
    this.weight = weight[this.name];
  }
  if (!this.rel_id) {
    this.status = false;
  }
  next();
});
Res_search_stringSchema.post('save', async function (doc, next) {
  if (doc.rel_id && typeof doc.rel_id === 'object') {
    doc.rel_id = doc.rel_id._id;
  }
  if (doc.rel_id)
    doc.on('es-indexed', function (err, res) {
      if (err) throw err;
    });
  next();
});

Res_search_stringSchema.virtual('relDetail', {
  ref: function () {
    return this.refPath;
  },
  localField: 'rel_id',
  foreignField: '_id',
  justOne: true,
});

Res_search_stringSchema.plugin(mongoosastic, {
  host: database.elasticsearchSyncHost,
  port: 9200,
  index: 'test',
  auth: 'elastic:123456',
  populate: [{ path: 'relDetail', select: 'district province ward_id' }],
});

const resSearchStringModel = mongoose.model(
  'res_search_string',
  Res_search_stringSchema
);

resSearchStringModel.createMapping((err, mapping) => {
  console.log('Product mapping created');
});

// let stream = resSearchStringModel.synchronize({
//   name: 'company',
//   }),
//   count = 0;

// stream.on('data', function (err, doc) {
//   count++;
//   console.log(count);
// });

// stream.on('close', function () {
//   console.log(`${count} is added`);
// });

// stream.on('error', function (err) {
//   console.log(err);
// });

module.exports = resSearchStringModel;
