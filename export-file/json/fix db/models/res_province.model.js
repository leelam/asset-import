const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');
const { database } = require('../configs/index');

var resProvinceSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: new mongoose.Types.ObjectId(),
    },
    create_date: {
      type: Date,
      default: Date.now(),
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: new mongoose.Types.ObjectId(),
    },
    write_date: {
      type: Date,
      default: Date.now(),
    },
    company_id: {
      type: String,
      default: 'ROOT',
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    province_id: { type: Schema.Types.ObjectId, ref: 'res_province' },
    name: { type: String, required: true, es_indexed: true },
    code: {
      type: String,
      required: true,
      es_indexed: true,
      es_type: 'keyword',
    },
    sequence: Number,
    old_id: String,
    points: {
      type: Object,
      es_indexed: true,
      es_type: 'geo_point',
    },
  },
  { collection: 'res_province' }
);

resProvinceSchema.plugin(mongoosastic, {
  host: database.elasticsearchSyncHost,
  port: 9200,
  index: 'res_province',
});
resProvinceSchema.pre('find', function () {
  this.where({ status: { $ne: false } });
});
const resProvinceModel = mongoose.model('res_province', resProvinceSchema);

// resProvinceModel.createMapping(() => {
//   console.log('Product mapping created');
// });
// let stream = resProvinceModel.synchronize({}),
//   count = 0;

// stream.on('data', function (err, doc) {
//   count++;
//   // console.log(count);
//   console.log(count);
// });
// stream.on('close', function () {
//   console.log(count);
// });
// stream.on('error', function (err) {
//   console.log(err);
// });

module.exports = resProvinceModel;
