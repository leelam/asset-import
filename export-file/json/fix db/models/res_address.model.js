const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { database } = require('../configs/index');
const mongoosastic = require('mongoosastic');

var ResAddressSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      ref: 'res_user',
    },
    create_date: {
      type: Date,
    },
    write_uid: {
      type: String,
    },
    write_date: {
      type: Date,
    },
    company_id: {
      type: String,
      required: true,
      default: 'ROOT',
    },
    status: {
      type: Boolean,
      default: true,
    },
    delete_id: String,
    address_name: String,
    type: String,
    parent_id: {
      type: Schema.Types.ObjectId,
      require: true,
      ref: 'res_address_new',
    },
    code: {
      type: String,
      unique: true,
      es_indexed: true,
      es_type: 'keyword',
    },
    country_id: Number,
    reaction: [
      { type: Schema.Types.ObjectId, ref: 'res_reaction', default: [] },
    ],
    points: {
      type: Object,
      es_indexed: true,
      es_type: 'geo_point',
    },
    polygon_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_polygon',
      },
    ],
    polygons: {
      type: Object,
      es_type: 'geo_shape',
      es_indexed: true,
    },
  },
  { collection: 'res_address_new' }
);

ResAddressSchema.pre('save', async function (next) {
  this.create_uid = new mongoose.Types.ObjectId('5d9c3342cc1f7b3f4c8e79dd');
  next();
});
ResAddressSchema.post('save', async function (doc, next) {
  doc.on('es-indexed', function (err, res) {
    if (err) throw err;
    next();
  });
  next();
});
ResAddressSchema.plugin(mongoosastic, {
  host: database.elasticsearchSyncHost,
  port: 9200,
  auth: 'elastic:123456',
  index: 'res_address_new',
  populate: [{ path: 'polygon_id', select: 'polygons' }],
});

let addressModel = mongoose.model('res_address_new', ResAddressSchema);

addressModel.createMapping(() => {
  console.log('Address mapping created');
});

// let stream = addressModel.synchronize({
//     type: 'C',
//   }),
//   count = 0;

// stream.on('data', function (err, doc) {
//   count++;
//   // console.log(count);
//   console.log(count);
// });
// stream.on('close', function () {
//   console.log(count);
// });
// stream.on('error', function (err) {
//   console.log(err);
// });

module.exports = addressModel;
