const mongoose = require('mongoose');
// const diff = require('deep-diff')
const Schema = mongoose.Schema;

var UserCompanySchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    user_id: {
      type: Schema.Types.ObjectId,
      required: [true, 'user_id is required'],
      ref: 'res_user'
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    company_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_company',
      required: [true, 'company_id is required']
    },
    group_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_user_group',
        required: [true, 'group_id is required']
      }
    ],

    position: { type: String, default: '' }
  },
  { collection: 'res_user_company' }
);
UserCompanySchema.pre('find', function() {
  if (!this.getQuery() && !this.getQuery().status) this.where({ status: true });
});
UserCompanySchema.pre('countDocuments', function() {
  this.where({ status: true });
});
UserCompanySchema.pre('aggregate', function() {
  this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
UserCompanySchema.virtual('user', {
  type: 'ObjectId',
  ref: 'res_user',
  localField: 'user_id',
  foreignField: '_id',
  justOne: true
});
UserCompanySchema.virtual('group', {
  type: 'ObjectId',
  ref: 'res_user_group',
  localField: 'group_id',
  foreignField: '_id',
  justOne: false
});
UserCompanySchema.virtual('company', {
  type: 'ObjectId',
  ref: 'res_company',
  localField: 'company_id',
  foreignField: '_id',
  justOne: true
});

UserCompanySchema.set('toObject', { virtuals: true, minimize: true });
UserCompanySchema.set('toJSON', { virtuals: true, minimize: true });
UserCompanySchema.index({ company_id: 1, user_id: 1 }, { unique: true });
module.exports = mongoose.model('res_user_company', UserCompanySchema);
