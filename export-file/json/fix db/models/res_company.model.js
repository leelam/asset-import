const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const change_alias = require('../helpers/change_alias');
const { getAddress } = require('../controllers/address.controller');

const contactSchema = new Schema(
  {
    create_uid: { type: Schema.Types.ObjectId, ref: 'res_user' },
    create_date: {
      type: Date,
      default: Date.now,
      es_indexed: true,
      es_type: 'date',
    },
    write_uid: { type: Schema.Types.ObjectId, ref: 'res_user' },
    write_date: {
      type: Date,
      default: Date.now,
    },
    company_name: String,
    company_id: {
      type: String,
    },
    status: Boolean,
    delete_id: String,
    name: {
      type: String,
    },
    address: {
      type: String,
      es_indexed: true,
    },
    phone: {
      type: Number,
      es_indexed: true,
    },
    code: {
      type: String,
    },
    country_code: {
      type: String,
      required: true,
      es_indexed: true,
      default: 84,
    },
    category_id: [{ type: Schema.Types.ObjectId, ref: 'res_company_category' }],
    total_views: {
      type: Number,
      default: 0,
    },
    display_name: {
      type: String,
      default: function () {
        return this.company_name;
      },
    },
    parent_id: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: 'res_company',
    },
    tel: {
      es_indexed: true,
      type: String,
    },
    fax: {
      es_indexed: true,
      type: String,
    },
    email: {
      es_indexed: true,
      type: String,
    },
    website: {
      es_indexed: true,
      type: String,
    },
    website: {
      es_indexed: true,
      type: String,
    },
    corporate_tax_code: {
      type: String,
    },
    reaction: {
      type: Array,
      es_indexed: true,
      default: [],
      ref: 'res_reaction',
    },
    curency: {
      type: String,
    },
    logo: {
      type: String,
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_ward',
    },
    search: {
      es_indexed: true,
      type: String,
    },
    working_area: {
      type: Schema.Types.ObjectId,
    },
    description: {
      type: String,
      es_indexed: true,
    },
    sequence: {
      type: Number,
      default: 0,
    },
    is_atoz: {
      type: Boolean,
      default: false,
    },
    interest_rate: String,
    rate_unit_id: String,
    rating_avg: {
      type: Number,
      default: 0,
    },
  },
  { collection: 'res_company' }
);

contactSchema.pre('save', async function (next) {
  const location = await getAddress(this.ward_id);
  let ward, district, province;
  if (location.data) {
    ward = location.data.ward.name;
    district = location.data.district.name;
    province = location.data.province.name;
  }
  const lowerCaseStringName = this.company_name && this.company_name.toLowerCase();
  const noneAccentName = this.company_name && change_alias(this.company_name.toLowerCase());
  const lowerCaseStringAddress = this.address && this.address.toLowerCase();
  const noneAccentAddress = this.address && change_alias(this.address.toLowerCase());
  const country_codePhone = `${this.country_code}${this.phone} 0${this.phone}`;
  const noneDisplayName= this.display_name && change_alias(this.display_name.toLowerCase());
  const lowerDisplayName = this.display_name && this.display_name.toLowerCase();

  this.search = `${noneDisplayName} ${lowerDisplayName} ${lowerCaseStringName} ${noneAccentName} ${lowerCaseStringAddress} ${noneAccentAddress} ${
    ward ? ward.toLowerCase() : ''
  } ${district ? district.toLowerCase() : ''} ${
    province ? province.toLowerCase() : ''
  } ${change_alias(ward)} ${district ? change_alias(
    district.toLowerCase()
  ): ''} ${province ? change_alias(province.toLowerCase()): ''}`;
	const data = {
		rel_id: this._id,
		name: 'company',
		refPath: 'res_company',
		weight: 0.1,
		search: this.search
	}
	const resSearchString = await mongoose.model('res_search_string').findOne({rel_id: this._id})
	if(resSearchString){
        resSearchString.search = this.search;
        resSearchString.save()
        console.log(resSearchString);
        
	}
	else {
		const newRessearchString = new mongoose.model('res_search_string')(data)
		newRessearchString.save()
	}
  next();
});

contactSchema.index({ search: 1 });
contactSchema.index({ total_views: 1 });
contactSchema.index({ rating_avg: 1 });
contactSchema.pre('find', function () {
  this.where({ status: true });
});
contactSchema.pre('aggregate', function () {
  // Add a $match state to the beginning of each pipeline.
  this.pipeline().unshift({ $match: { status: true } });
});
contactSchema.pre('countDocuments', function () {
  this.where({ status: true });
});
module.exports = mongoose.model('res_company', contactSchema);
