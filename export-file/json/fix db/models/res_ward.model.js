const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResWardSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: new mongoose.Types.ObjectId()
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: new mongoose.Types.ObjectId()
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: 'ROOT'
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    name: { type: String, required: true },
    code: { type: String, required: false },
    district_id: { type: Schema.Types.ObjectId, ref: 'res_district' },
    sequence: Number,
    old_id: String,
    points: Object
  },
  { collection: 'res_ward' }
);
ResWardSchema.pre('find', function() {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_ward', ResWardSchema);
