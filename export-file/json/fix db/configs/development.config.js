module.exports = {
    database: {
      mongoUrl: 'mongodb://192.168.1.254/asset_dev',
      elasticsearchHost: 'http://localhost:9200',
      elasticsearchSyncHost: 'localhost',
      MONGOUSER: 'admin',
      MONGOPASSWORD: '9V94cJ7BMKbeVHfZ',
    },
    auth: {
      accessTokenApp: '715804619171328|Q4PWMYsOK0DSRb1byKOMkLjXhrY',
      // accessTokenApp: 'EAAPpSCq16kMBALhqyXQ8KSMWhtig08EYP2RqUDnIZC7dCWOie98xCsddF5qWABpqztNmYgVkaEIAPEcJ1TLkd82xOZCv4xxt6tZCXoxy5zbscDHpdBV774OiI6BS3TASFtwtQgb0cyXFF4t44T4GDQc3bFXEHVKL7yaAsqrmxTWKOUHrZCnA0ysvmUnZBZAFzTH2PnDycuIysVPLgHZCcyg',
      urlFb: 'https://graph.facebook.com/debug_token',
      googleClientId:
        '483338159884-k54p8hjscd9rc6k92rbet59q4s89pbgr.apps.googleusercontent.com',
      googleApiKey: 'AIzaSyDzHPj1agL0j5AJYS02KTgStjGpQPgK_AU',
      invisibleRecaptchaSecretKey: '6LcDMsUUAAAAAP6Dc0Hr9v4t53hCXTElbu9Fxk0P',
      checkboxRecaptSecretKey: '6Lf8McUUAAAAADoTeIMf8lYfP-IKW965imujEYbS',
    },
    host: {
      PUBLIC: `http://192.168.1.20:${process.env.PORT}`,
      ADMIN_API_HOST: 'http://localhost:9002/api/v1',
      GRPC_HOST_SERVER: 'localhost:50002',
      GRPC_HOST_CLIENT: 'localhost:50001',
    },
    secretKeyAES: {
      key: 'D9048B2CA883DF19',
      pre: 'A',
      after: 'C5',
    },
    redisConfig: {
      host: 'localhost',
      port: '6379',
    },
    CURRENT_HOST: 'http://localhost:4000',
  };
  