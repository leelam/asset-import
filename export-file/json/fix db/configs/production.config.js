module.exports = {
    database: {
      mongoUrl: 'mongodb://171.244.38.166/asset',
      host: 'https://api.asset.vn',
      elasticsearchHost: 'http://171.244.38.166:9200',
      elasticsearchSyncHost: '171.244.38.166',
      MONGOUSER: 'prod',
      MONGOPASSWORD: process.env.DB_PASSWORD,
    },
    auth: {
      accessTokenApp: '715804619171328|Q4PWMYsOK0DSRb1byKOMkLjXhrY',
      // accessTokenApp: 'EAAPpSCq16kMBADtciEsSvlJS5MVP9c9O7ROs2eQoAb6UyVPZCBLaFI9gJzJlTT9NrVXfiyay7innrIT9ZCR0rxzLXJ5013lofYfqwZAn119C20IekfUkcN4GfZBm5gGuJoj4tAaznZAZAQg06dwtkXZCqXIatHUFQxEQN1hgZA4usoqD6lS9CUwvyXiJM3EncEzdIZCbB6nt1MV2PMOpt6vDP',
      urlFb: 'https://graph.facebook.com/debug_token',
      googleClientId:
        '483338159884-k54p8hjscd9rc6k92rbet59q4s89pbgr.apps.googleusercontent.com',
      googleApiKey: 'AIzaSyDzHPj1agL0j5AJYS02KTgStjGpQPgK_AU',
      invisibleRecaptchaSecretKey: '6LcDMsUUAAAAAP6Dc0Hr9v4t53hCXTElbu9Fxk0P',
      checkboxRecaptSecretKey: '6Lf8McUUAAAAADoTeIMf8lYfP-IKW965imujEYbS',
    },
    grpc: {
      // HOST: 'localhost:50001',
      HOST: 'localhost:50001',
    },
    host: {
      PUBLIC: `https://api.asset.vn`,
      ADMIN_API_HOST: 'https://image.asset.vn/api/v1',
      GRPC_HOST_SERVER: '0.0.0.0:50001',
      GRPC_HOST_CLIENT: '14.241.251.17:50001',
    },
    secretKeyAES: {
      key: 'D9048B2CA883DF19',
      pre: 'A',
      after: 'C5',
    },
    redisConfig: {
      host: 'localhost',
      port: '6379',
    },
    CURRENT_HOST: "https://asset.vn",
  };
  