var mongoose = require('mongoose')
var Schema = mongoose.Schema
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");

var ResPolygonSchema = new Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    collection_name: {
        type: String,
        required: true
    },
    polygons: {
        type: Object,
        // required: true
    },
    rel_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    code: {
        type: String
    }
}, {collection: 'res_polygon'})

module.exports = mongoose.model('res_polygon', ResPolygonSchema)