var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const { getAddress } = require('../controllers/address.controller');
const mongoosastic = require('mongoosastic');
const { deleteByQuery, insertData } = require('../../elasticsearch');
const resSearchStringModel = require('../models/res_search_string.model');
const changeAlias = require('../helpers/change_alias');
const weight = {
  SELL: 0.5,
  NO_TRADE: 0,
  RENT: 0.5,
  SOLD: 0.2,
  ARENT: 0.2,
};

var ProductSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: mongoose.Types.ObjectId('5dbb9fbbebd384b5e4ecc368'),
      ref: 'res_user',
    },
    polygons: {
      type: Object,
      es_indexed: true,
      es_type: 'geo_shape',
    },
    points: {
      type: Object,
      es_indexed: true,
      es_type: 'geo_point',
    },
    code: {
      type: String,
      unique: true,
      es_indexed: true,
      es_type: 'keyword',
    },
    a_value_rate: {
      type: Number,
      es_indexed: true,
      es_type: 'half_float',
    },
    create_date: {
      type: Date,
      default: Date.now,
      es_indexed: true,
      es_type: 'date',
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      ref: 'res_user',
    },
    land_area_unit_id: {
      type: String,
    },
    write_date: {
      type: Date,
      default: Date.now,
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      require: [true, 'Thiếu phường xã'],
      es_indexed: true,
      es_type: 'keyword',
      ref: 'res_ward',
    },
    district: {
      type: Schema.Types.ObjectId,
      require: [true, 'Thiếu quận huyện'],
      ref: 'res_district',
    },
    direction: {
      type: String,
      require: [true, 'Thiếu hướng'],
      es_indexed: true,
    },
    image: {
      type: String,
      es_indexed: true,
      // require: [true, 'Thiếu hình đại diện']
    },
    document: [
      {
        type: String,
      },
    ],
    sub_image: Array,
    province: {
      type: Schema.Types.ObjectId,
      require: [true, 'Thiếu tinh thanh pho'],
      ref: 'res_province',
    },
    company_id: {
      type: String,
      // required: true,
      default: 'ROOT',
    },
    investor_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_company',
        require: [true, 'Thiếu chủ đầu tư'],
        //   type: String
      },
    ],
    status: {
      type: Boolean,
      default: true,
    },
    delete_id: {
      type: Schema.Types.ObjectId,
      default: null,
      // require: true,
      ref: 'product_category',
    },
    display_photo: {
      type: String,
    },
    redbook_number: String,
    detail_photo: {
      type: String,
    },
    video: {
      type: String,
    },
    view_3d: String,
    sale_price_unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị giá'],
      enum: [
        'triệu/căn',
        'triệu/tháng',
        'tỷ/căn',
        'tỷ/tháng',
        'tỷ/tháng',
        'triệu/m2',
        'tỷ/m2',
      ],
    },
    bancon_direction: String,
    sale_price: {
      type: Number,
      // require: [true, 'Thiếu giá'],
      es_indexed: true,
    },
    width: {
      type: Number,
      es_indexed: true,
      // require: [true, 'Thiếu chiều rộng']
    },
    width_unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị chiều rộng']
    },
    length: {
      type: Number,
      // require: [true, 'Thiếu chiều cao']
    },
    name: {
      type: String,
      es_indexed: true,
      // required: [true, 'Thiếu tên bất động sản']
    },
    parent_id: {
      type: Schema.Types.ObjectId,
      ref: 'product',
    },
    category_id: {
      // type: String
      type: Schema.Types.ObjectId,
      // require: true,
      ref: 'product_category',
      es_indexed: true,
    },
    display_photo: {
      type: String,
    },
    detail_photo: {
      type: String,
    },
    video: {
      type: String,
    },
    view_3d: String,
    sale_price_unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị giá'],
    },
    sale_price: {
      type: Number,
      // require: [true, 'Thiếu giá']
    },
    width: {
      type: Number,
      // require: [true, 'Thiếu chiều rộng']
    },
    length_unit_id: {
      type: String,
      default: 'm',
      // require: [true, 'Thiếu đơn vị chiều dài']
    },
    width_unit_id: {
      type: String,
      default: 'm',
      // require: [true, 'Thiếu đơn vị chiều rộng']
    },
    height: {
      type: Number,
      // require: [true, 'Thiếu chiều dài']
    },
    length: {
      type: Number,
      es_indexed: true,
      // require: [true, 'Thiếu chiều cao']
    },
    unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị chiều dài'],
      //ref: 'product_unit'
    },
    location: {
      type: String,
      // require: [true, 'Thiếu vị trí']
    },
    handover_time: {
      type: Date,
      default: null,
      // require: [true, 'Thiếu thời gian bàn giao']
    },
    cons_status: {
      type: Schema.Types.ObjectId,
      ref: 'project_status',
      // require: [true, 'Thiếu tình trạng xây dựng'],
    },
    legal: {
      type: String,
      // require: [true, 'Thiếu tình trạng pháp lý']
    },
    land_area: {
      type: Number,
      es_indexed: true,
      // require: [true, 'Thiếu điện tích đất']
    },
    land_type: {
      type: Schema.Types.ObjectId,
      require: [true, 'Thiếu loại đất'],
      ref: 'assets_planning_category',
    },

    type_id: {
      type: Schema.Types.ObjectId,
      default: '5d6e57310b4d9fd73db02be9',
      es_indexed: true,
      ref: 'product_type',
    },
    a_value: {
      type: Number,
      // require: [true, 'Thiếu định giá bán']
    },
    bedroom_unit_id: String,
    bathroom_unit_id: String,
    a_value_unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị định giá bán'],
      ref: 'product_unit',
    },
    a_rent: {
      type: Number,
      // require: [true, 'Thiếu định giá thuê']
    },
    a_rent_unit_id: {
      type: String,
      // require: [true, 'Thiếu đơn vị định giá thuê'],
      ref: 'product_unit',
    },
    feature: {
      type: String,
      es_indexed: true,
      require: [true, 'Thiếu đặc điểm bất động sản'],
    },
    access_road: {
      type: String,
      // require: [true, 'Thiếu đường tiếp cận']
    },
    address: {
      type: String,
      es_indexed: true,
      require: [true, 'Thiếu số nhà, tên đường'],
    },
    block: {
      type: String,
    },
    project_id: {
      type: Schema.Types.ObjectId,
      ref: 'project',
    },
    bedroom: {
      type: Number,
      es_indexed: true,
      es_type: 'short',
      // require: [true, 'Thiếu số phòng ngủ']
    },
    bathroom: {
      type: Number,
      es_indexed: true,
      es_type: 'short',
      // require: [true, 'Thiếu số phòng tắm']
    },
    reaction: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_reaction',
        default: [],
      },
    ],
    asset_rating: Number,
    rating: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_rating',
      },
    ],
    description: {
      type: String,
    },
    cons_design_info_id: {
      type: Schema.Types.ObjectId,
      // require: [true, 'Thiếu thông tin thiết kế và xây dựng']
    },
    legal_info_id: {
      type: Schema.Types.ObjectId,
      // require: [true, 'Thiếu thông tin pháp lý'],
      ref: 'legal_info',
    },
    landlord_info_id: {
      type: Schema.Types.ObjectId,
      // require: [true, 'Thiếu thông tin chủ đất'],
      ref: 'product_landlord_info',
    },
    rating_avg: Number,
    number_of_floor: {
      type: Number,
      es_type: 'short',
      es_indexed: true,
    },
    floor_area: Number,
    number_of_floor_unit_id: String,
    floor_area_unit_id: {
      type: String,
      require: [true, 'Thiếu floor_area_unit_id'],
    },
    number_of_sheet: {
      type: Number,
      es_indexed: true,
    },
    number_of_parcel: {
      type: String,
      es_indexed: true,
    },
    planning_area: Number,
    planning_area_unit_id: String,
    lost_area: Number,
    lost_area_unit_id: String,
    search: {
      type: String,
      es_indexed: true,
    },
    FID: {
      type: String,
      es_indexed: true,
    },
    import: {
      type: Boolean,
      es_indexed: true,
      es_type: 'boolean',
    },
  },
  {
    collection: 'product',
  }
);
// ProductSchema.index({type_id:1,code:'text',points:'2d'})
ProductSchema.index({ code: 1, FID: 1 }, { unique: true });
ProductSchema.pre('find', function () {
  this.where({
    status: {
      $ne: false,
    },
  });
});

ProductSchema.plugin(mongoosastic, {
  host: '192.168.1.94',
  port: 9200,
  index: 'product-test',
});

ProductSchema.pre('countDocuments', function () {
  this.where({
    status: {
      $ne: false,
    },
  });
});
ProductSchema.pre('aggregate', function () {
  this.pipeline().unshift({
    $match: {
      status: {
        $ne: false,
      },
    },
  });
});

ProductSchema.pre('save', async function (next) {
  this.isAdd = this.isNew;
  const location = await getAddress(this.ward_id);
  const doc = new this.constructor(this);
  await doc.populate('type_id').populate('category_id').execPopulate();
  let ward, district, province;
  if (location.data) {
    ward = location.data.ward;
    district = location.data.district;
    province = location.data.province;
  }
  this.province = province._id;
  this.district = district._id;
  const addressDetail = `${ward ? ward.name + ' ' : ''}${
    district ? district.name + ' ' : ''
  }${province ? province.name + ' ' : ''}`;
  const search = `${doc
    .toObject()
    .category_id.category_name.toLocaleLowerCase()} ${changeAlias(
    doc.toObject().category_id.category_name.toLocaleLowerCase()
  )} ${addressDetail.toLocaleLowerCase()} ${changeAlias(
    addressDetail.toLocaleLowerCase()
  )}`;
  this.search = search;
  this.import = true;
  // this.on('es-indexed', function(err, res) {
  //   console.log('vo day change');
  //   if (err) throw err;
  //   /* Document is indexed */
  //   // console.log('Document is indexed in elasticsearch')
  // });
  next();
});

ProductSchema.post('save', async function (doc, next) {
  try {
    console.log(doc._doc.code);
    const data = {
      ...doc._doc,
      _id: doc._id,
      points: doc._doc.points ? doc._doc.points.coordinates : '',
    }
    console.log(doc._doc.points);
    await insertData(data, 'product')
    const location = await getAddress(doc.ward_id);
    await doc.populate('type_id').populate('category_id').execPopulate();
    let ward, district, province;
    if (location.data) {
      ward = location.data.ward;
      district = location.data.district;
      province = location.data.province;
    }
    const sub_title = `${doc.category_id.category_name} ${
      ward ? ward.name + ' ' : ''
    }${district ? district.name + ' ' : ''}${
      province ? province.name + ' ' : ''
    }`;
    const weightProduct = weight[doc.toObject().type_id.code];
    if (this.isAdd) {
      const newSearch = new resSearchStringModel({
        name: 'product',
        rel_id: doc._id,
        refPath: 'product',
        weight: weightProduct || 0,
        title: doc.name,
        sub_title,
        import: true,
        search: `${sub_title.toLocaleLowerCase()} ${changeAlias(
          sub_title.toLocaleLowerCase()
        )} ${doc.number_of_sheet ? `@${doc.number_of_sheet}` : ''}${
          doc.number_of_parcel ? `@${doc.number_of_parcel}` : ''
        }`,
      });
      await newSearch.save();
    }
    
    // this.on('es-indexed', function (err, res) {
    //   if (err) throw err;
    //   /* Document is indexed */
    //   // console.log('Document is indexed in elasticsearch')
    // });
    next();
  } catch (e) {
    console.log(e);
    next();
  }
});

let ProductModel = mongoose.model('product', ProductSchema);
ProductModel.createMapping((err, mapping) => {
  console.log('Product mapping created');
});
module.exports = ProductModel;
