var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ProductLegalSchema = new Schema({
    create_uid: { type: String },
    create_date: {
        type: Date
    },
    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String
    },
    status: Boolean,
    delete_id: String,

    product_id: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
    investment_approval: String,
    investment_approval_link: String,
    planning_approval_1_500: String,
    planning_approval_1_500_link: String,
    investment_project_approval: String,
    investment_project_approval_link: String,
    deposit: String,
    deposit_link: String,
    land_allocation: String,
    land_allocation_link: String,
    change_land_type: String,
    change_land_type_link: String,
    investor_rebook_licensing: String,
    investor_rebook_licensing_link: String,
    basic_design_approval: String,
    basic_design_approval_link: String,
    technical_drawings_approval: String,
    technical_drawings_approval_link: String,
    construction_permit: String,
    construction_permit_link: String,
    raise_funds_condition: String,
    raise_funds_condition_link: String,
    basement_foundation_acceptance: String,
    basement_foundation_acceptance_link: String,
    construction_acceptance: String,
    construction_acceptance_link: String,
    pccc_acceptance: String,
    pccc_acceptance_link: String,
    house_allocation: String,
    house_allocation_link: String,
    rebbook_licening: String,
    rebbook_licening_link: String,
}, {collection: 'product_legal'})

module.exports = mongoose.model('product_legal', ProductLegalSchema)