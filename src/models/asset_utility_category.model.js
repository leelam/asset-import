const mongoose = require('mongoose'); 
const Schema = mongoose.Schema
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
var UtilityCategorySchema = mongoose.Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    
    name:String,
    code: String,
    sequence:Number,
    marker_url: String,
    icon_url:String,
    english_name: String
},
    { collection: 'assets_utility_category' }
);
module.exports = mongoose.model('assets_utility_category', UtilityCategorySchema);