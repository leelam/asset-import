const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var newId = new mongoose.mongo.ObjectId('5d679819325ab70ab0157ce5');
const res_SearchStringModel = require('../models/res_search_string.model');
const { getAddress } = require('../controllers/address.controller');
const change_alias = require('../helpers/change_alias');
const { isDate } = require('lodash');
const moment = require('moment');
var ProjectSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: '5dbb9d3b65bb02ebb0ec7fb3',
      ref: 'res_user',
    },
    create_date: {
      type: Date,
      default: Date.now,
    },
    documents: [
      {
        type: String,
      },
    ],
    write_uid: {
      type: Schema.Types.ObjectId,
      default: '5dbb9d3b65bb02ebb0ec7fb3',
    },
    write_date: {
      type: Date,
      default: Date.now,
      set() {
        return Date.now();
      },
    },
    company_id: {
      type: String,
      default: 'ROOT',
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    code: {
      type: String,
      unique: true,
    },
    parent_id: {
      type: Schema.Types.ObjectId,
      ref: 'project',
    },
    points: Object,
    polygons: Object,
    ward_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_ward',
    },
    address: String,
    category_id: {
      type: Schema.Types.ObjectId,
      ref: 'project_category',
      required: [true, 'Missing category_id'],
    },
    name: { type: String, required: [true, 'Missing name'] },
    apartment_project: Number,
    apartment_project_unit_id: String,
    urban_project: Number,
    urban_project_unit_id: String,
    resort_project: Number,
    resort_project_unit_it: String,
    ch: Number,
    ch_unit_id: String,
    sh: Number,
    sh_unit_id: String,
    oft: Number,
    oft_unit_id: String,
    tmdv: Number,
    tmdv_unit_id: String,
    hotel_room: Number,
    hotel_room_unit_id: String,
    villa: Number,
    villa_unit_id: String,
    private_house: Number,
    private_house_unit_id: String,
    row_house: Number,
    row_house_unit_id: String,
    lands: Number,
    lands_unit_id: String,
    condotel: Number,
    condotel_unit_id: String,
    bungalow: Number,
    bungalow_unit_id: String,
    sale_unit_price: Number,
    sale_price_unit_id: String,
    management_fee: Number,
    management_fee_unit_id: String,
    land_area: Number,
    land_area_unit_id: String,
    location: String,
    tower: Number,
    land_type: Schema.Types.ObjectId,
    tower_unit_id: String,
    investor_id: [{ type: Schema.Types.ObjectId, default: null }],
    contractor: String,
    designer: String,
    manager: String,
    handover_time: {
      type: Date,
    },
    cons_status: Schema.Types.ObjectId,
    sale_status: Schema.Types.ObjectId,
    legal: String,
    planing_category_id: {
      type: Schema.Types.ObjectId,
      ref: 'assets_planning_category',
    },
    deposit: Number,
    deposit_unit_id: String,
    monthly_payment: Number,
    payment_unit_id: String,
    a_value: Number,
    a_value_unit_id: String,
    a_value_rate: Number,
    a_rent: Number,
    a_rent_unit_id: String,
    feature: String,
    access_road: String,
    floor: Number,
    floor_unit_id: String,
    block: Number,
    block_unit_id: String,
    description: { type: String, default: '' },
    cons_status: {
      type: Schema.Types.ObjectId,
      ref: 'project_status',
      default: '5db66cb206d6a109b1513792',
      required: [true, 'Missing status_id'],
    },
    reaction: [
      { type: Schema.Types.ObjectId, ref: 'res_reaction', default: [] },
    ],
    asset_rating: { type: Number, default: 0 },
    image: { type: String },
    sub_image: [{ type: String, default: [] }],
    legal_info_id: {
      type: Schema.Types.ObjectId,
      ref: 'project_legal_info',
      require: [true, 'Missing legal info'],
    },
    contractor_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_company',
      },
    ],
    designer_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_company',
      },
    ],
    investor_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_company',
      },
    ],
    manager_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_company',
      },
    ],
    cons_design_info_id: {
      type: Schema.Types.ObjectId,
      ref: 'project_cons_design_info',
      require: [true, 'Missing cons_design'],
    },
    search: String,
    assset_rating: Number,
    comme_project: Number,
    comme_project_unit_id: String,
    legal_name: String,
    hotel_project: Number,
    hotel_project_unit_id: String,
    mine: Number,
    mine_unit_id: String,
    cemet: Number,
    cemet_unit_id: String,
    farm: Number,
    farm_unit_id: String,
    harbo: Number,
    harbo_unit_id: String,
    indu: Number,
    indu_unit_id: String,
    bindu: Number,
    bindu_unit_id: String,
    edme: Number,
    edme_init_id: String,
    bedme: Number,
    bedme_unit_id: String,
    resort_project_unit_id: String,
    edme_unit_id: String,
    hotel: Number,
    hotel_unit_id: String,
    resvl: Number,
    resvl_unit_id: String,
    small: Number,
    small_unit_id: String,
    office: Number,
    office_unit_id: String,
    park: Number,
    park_unit_id: String,
    deno: Number,
    deno_unit_id: String,
    bank_guarantee: String,
    floor_min: Number,
    floor_min_unit_id: String,
    number_of_floor: Number,
    model_house_address: Number,
    contact: String,
    tel: String,
    work_time: Date,
    max_people: Number,
    note: String,
    import: {
      type: Boolean,
      default: true,
    },
  },
  { collection: 'project' }
);
ProjectSchema.index(
  {
    name: 'text',
    location: 'text',
  },
  {
    // default_language: "english",
    name: 'searchIndex',
    weights: {
      keyword: 10,
      location: 2,
    },
  }
);

ProjectSchema.post('save', async function (doc, next) {
  const location = await getAddress(doc.ward_id);
  let ward, district, province;
  if (location.data) {
    ward = location.data.ward;
    district = location.data.district;
    province = location.data.province;
  }
  const sub_title = `${ward ? ward.name + ' ' : ''}${
    district ? district.name + ' ' : ''
  }${province ? province.name + ' ' : ''}`;
  const newSearch = new res_SearchStringModel({
    name: 'project',
    rel_id: doc._id,
    title: doc.name,
    sub_title,
    search: `${change_alias(doc.name.toLowerCase())} ${doc.name.toLowerCase()}`,
    refPath: 'project',
    weight: '0.5',
  });
  await newSearch.save({
    name: 'project',
    rel_id: doc._id,
    title: doc.name,
    sub_title,
    search: `${change_alias(doc.name.toLowerCase())} ${doc.name.toLowerCase()}`,
    refPath: 'project',
    weight: '0.5',
  });
  // console.log(newSearch);
  next();
});

// ProjectSchema.pre("find", function() {
//   this.where({ status: { $ne: false } });
// });
// ProjectSchema.pre("countDocuments", function() {
//   this.where({ status: { $ne: false } });
// });
// ProjectSchema.pre("aggregate", function() {
//   this.pipeline().unshift({$match:{ status: { $ne: false } }});
// });
module.exports = mongoose.model('project', ProjectSchema);
