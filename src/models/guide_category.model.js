var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
var GuideCategorySchema = mongoose.Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: newId,
        ref: "res_user"
      },
      create_date: {
        type: Date,
        default: Date.now()
      },
      write_uid: {
        type: Schema.Types.ObjectId,
        default: newId
      },
      write_date: {
        type: Date,
        default: Date.now()
      },
      company_id: {
        type: String,
        default: "ROOT"
      },
      status: { type: Boolean, default: true },

    parent_id: {type:Schema.Types.ObjectId,ref:'guide_category',default:null},
    name: {
       type: String, 
       required: true
    },
    url_name: String,
    sequence:{type:Number,default:0},
    description:{type:String,default:''}
}, {collection: 'guide_category'})
GuideCategorySchema.pre("find", function() {
    this.where({ status: { $ne: false } });
  });
  GuideCategorySchema.pre("countDocuments", function() {
    this.where({ status: { $ne: false } });
  });
  GuideCategorySchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
  });
module.exports = mongoose.model('guide_category', GuideCategorySchema)