const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var UserGroupSchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: null
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: null
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        group_name: {
            type: String,
            required: [true, "group_name is required"]
        },
        permission_id:[{type:Schema.Types.ObjectId,ref:'res_permission',default:[]}],
        module_id:[{type:Schema.Types.ObjectId,ref:'res_module',default:[]}],
        code: String
    },
    { collection: "res_user_group" }
);

UserGroupSchema.pre("find", function() {
    this.where({ status: {$ne:false} });
});
UserGroupSchema.pre("countDocuments", function() {
    this.where({ status: {$ne:false} });
});
UserGroupSchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
UserGroupSchema.virtual("permission", {
    type: "ObjectId",
    ref: "res_permission",
    localField: "permission_id",
    foreignField: "_id",
    justOne: false
  });
  UserGroupSchema.virtual("module", {
    type: "ObjectId",
    ref: "res_module",
    localField: "module_id",
    foreignField: "_id",
    justOne: false
  });
  UserGroupSchema.set("toObject", { virtuals: true, minimize: true });
  UserGroupSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("res_user_group", UserGroupSchema);
