const mongoose = require('mongoose');
var resRoadSchema = mongoose.Schema(
  {
    Tinh_Thanh: String,
    points: Object,
    polygons: Object,
  },
  { collection: 'res_side' }
);

module.exports = mongoose.model('res_side', resRoadSchema);
