const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResTimeZoneSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: null
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  name: { type: String, required: true },
  rounding: { type: Number, required: true },
  symbol_left: { type: String,default:'' },
  symbol_right: { type: String,default:'' },
  decimal_separator: { type: String,default:'' },
  grouping_separator: { type: String,default:'' },
  decimal_places: { type: Number,default:0 },
  rate: { type: Number,default:0 },
},
  { collection: 'res_currency' }
);
ResTimeZoneSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_currency', ResTimeZoneSchema);