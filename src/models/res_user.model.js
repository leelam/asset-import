const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ResUserSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: null
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_uid: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: "res_user"
      },
    first_name: {
      type: String,
      required: true,
      min: 1
    },
    last_name: {
      type: String,
      required: true,
      min: 1
    },
    gender:{type:String,
    enum:["MALE","FEMALE","UNKNOWN"]},
    email: {
      type: String,
      default: "",
      min: 6
    },
    description:String,
    password: {
      type: String
    },
    partner_id: {
      type: String,
      default: ""
    },
    lang_id: {
      type: Schema.Types.ObjectId,
      default: null,
      ref:'res_rang'
    },
    gmt_offset: {
      type: Schema.Types.ObjectId,
      ref:'res_time_zone',
      default: null
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      ref:'res_ward',
      default: null
    },
    display_name:String,
    address:String,
    birthday: {type:Date,default:null},
    country_id:{type:Schema.Types.ObjectId,
    ref:'res_country',default:null},
    currency_id:{type:Schema.Types.ObjectId,
      ref:'res_currency',default:null},
    date_format: {
      type: String,
      default: ""
    },
    time_format: {
      type: String,
      default: ""
    },
    thousands_sep: {
      type: String,
      default: ""
    },
    decimal_point: {
      type: String,
      default: ""
    },
    avatar: {
      type: String,
      default: ""
    },
    type: {
      type: String,
      default: ""
    },
    inactive: {
      type: Boolean,
      default: false
    },
    renew_password_id: {
      type: String,
      default: ""
    },
    online: {
      type: Number,
      default: 0
    },
    user_referral_code: {
      type: String,
      default: ""
    },
    save_product: {
      type: Array,
      default: []
    },
    save_project: {
      type: Array,
      default: []
    },
    referral_code: {
      type: String,
      default: ""
    },
    phone: {
      type: String,
      min: 8
    },
    fb_id: {
      type: String,
      default: null
    },
    google_id: {
      type: String,
      default: null
    },
    list_referral_user: []
  },

  { collection: "res_user" }
);
ResUserSchema.virtual("ward", {
  type: "ObjectId",
  ref: "res_ward",
  localField: "ward_id",
  foreignField: "_id",
  justOne: true
});
ResUserSchema.virtual("time_zone", {
  type: "ObjectId",
  ref: "res_time_zone",
  localField: "gmt_offset",
  foreignField: "_id",
  justOne: true
});
ResUserSchema.virtual("country", {
  type: "ObjectId",
  ref: "res_country",
  localField: "country_id",
  foreignField: "_id",
  justOne: true
});
ResUserSchema.virtual("language", {
  type: "ObjectId",
  ref: "res_lang",
  localField: "lang_id",
  foreignField: "_id",
  justOne: true
});
module.exports = mongoose.model("res_user", ResUserSchema);
