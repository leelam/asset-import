const mongoose = require('mongoose')

const { Schema } = mongoose;

const productProcessSchema = new Schema({
    create_uid: { type: Schema.Types.ObjectId,ref:'res_user' },
    create_date: {
      type: Date,
      default: Date.now
    },
    write_uid: { type: Schema.Types.ObjectId,ref:'res_user' },
    write_date: {
      type: Date,
      default: Date.now
    },
    company_id: {
      type: String,
    },
    status: {
      type: Boolean,
      required: true
    },
    delete_id: String,
    rel_id: {
        type: Schema.Types.ObjectId,
        ref: 'product'
    },
    group_approve: {
         type: Schema.Types.ObjectId
    },
    user_approved: {
        type: Schema.Types.ObjectId,
        ref: 'res_user'
    },
    status_approve: {
        type: String,
        enum: ['APPROVED', 'REJECTED', 'PENDING']
    },
    feedback: String
},
    { collection: 'asset_approve_process'}
)

module.exports = mongoose.model('asset_approve_process', productProcessSchema)