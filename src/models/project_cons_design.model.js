const mongoose = require('mongoose')
const Schema = mongoose.Schema

var ProjectConsDesignInfoSchema = new Schema({
    create_uid: { type: String },
    create_date: {
        type: Date,
        default: Date.now
    },
    write_uid: {
        type: String
    },
    write_date: {
        type: Date,
        default: Date.now
    },
    company_id: {
        type: String,
        // required: true
    },
    status: Boolean,
    delete_id: String,
    general_info_of_cons: {
        type:Object
    },
    material_structure: {
        type:Object
    },
    int_finishing: {
       type:Object
    }
}, {collection: 'project_cons_design'})

module.exports = mongoose.model('project_cons_design', ProjectConsDesignInfoSchema);