const mongoose = require('mongoose'); 
var Schema = mongoose.Schema
var PlanningSchema = mongoose.Schema({
    create_uid: { type: String },

    create_date: {
        type: Date
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String,
    },
    status: Boolean,
    delete_id: String,

    code: String,
    points: Object,
    polygons: Object,
    ward_id: {
        type: Schema.Types.ObjectId,
        ref: 'res_ward'
    },
    refernce: String,
    lot_symbol: String,
    area: Number,
    area_unit_id: String,
    planning_category_id: {
        type: Schema.Types.ObjectId,
        ref: 'assets_planning_category'
    },
    people: Number,
    people_unit_id: String,
    floors: Number,
    floors_unit_id: String,
    max_rate: Number,
    max_rate_unit_id: String,
    far: Number,
    far_unit_id: String,
    setback_space: Number,
    setback_space_unit_id: String,
    import: {
        type: Boolean,
        default: true
    },
    FID: String
},
    { collection: 'assets_planning' }
);
module.exports = mongoose.model('assets_planning', PlanningSchema);