const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ProductCategorySchema = mongoose.Schema({
    // _id: { type: String, required: true },
    create_uid: { type: String },

    create_date: {
        type: Date
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String,
        default: null
    },
    status: Boolean,
    delete_id: String,
    category_name: String,
    type: String,
    code: String,
    type_id: String,
    start_date: String,
    end_date: String,
    contact_name: String,
    tel: String,
    email: String,
    description: String,
    sequence: String,
    keyword: String,
    attribute: [{ type: Schema.Types.ObjectId, ref: 'res_attributes' }]
},
    { collection: 'product_category' }
);
ProductCategorySchema.pre("find", function () {
    this.where({ status: { $ne: false } });
});
ProductCategorySchema.pre("countDocuments", function () {
    this.where({ status: { $ne: false } });
});
ProductCategorySchema.pre("aggregate", function () {
    this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
module.exports = mongoose.model('product_category', ProductCategorySchema);