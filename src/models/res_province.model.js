const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var ResCitySchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: "ROOT"
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },

  name: { type: String },
  code: { type: String, required: true, unique: true },
  points: Object,
  type: String,
  reaction: [{
    type: Schema.Types.ObjectId,
    default: []
  }],
  country_id: {
    type: String,
    default: '243'
  },
  // polygons: {
  //   type: Object,
  // },
},
  { collection: 'res_province' }
);
ResCitySchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_province', ResCitySchema);