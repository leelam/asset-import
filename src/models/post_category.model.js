const mongoose = require("mongoose");
var AutoIncrement = require("mongoose-sequence")(mongoose);
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var PostCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name: {
            type: String,
            required: [true, "name is require"]
        },
        description: { type: String, default: "" },
        url_name: { type: String, required: [true, "url_name is require"] },
        sequence: { type: Number },
        type: String,
        parent_id: { type: Schema.Types.ObjectId, ref: "post_category" },
        keyword: { type: String, default: "" },
        tag: { type: String, default: "" },
        publish: { type: Boolean, default: true }
    },
    { collection: "post_category" }
);
PostCategorySchema.plugin(AutoIncrement, {
    id: "post_category_sequence",
    inc_field: "sequence"
});
PostCategorySchema.pre("find", function() {
    this.where({ status: true });
});
PostCategorySchema.pre("countDocuments", function() {
    this.where({ status: true });
});
PostCategorySchema.pre("aggregate", function() {
    this.match({ status: true });
});
module.exports = mongoose.model("post_category", PostCategorySchema);
