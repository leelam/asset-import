const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var NotificationCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        sequence:{type:String,default:0},
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name: {
            type: String,
            required: [true, "name is require"]
        },
        code: {
            type: String,
            required: [true, "code is require"]
        },
        description: { type: String, default: "" },
        parent_id: { type: Schema.Types.ObjectId, ref: "post_category" ,default:null},
    },
    { collection: "notification_category" }
);
NotificationCategorySchema.pre("find", function() {
    this.where({ status: true });
});
NotificationCategorySchema.pre("countDocuments", function() {
    this.where({ status: true });
});
NotificationCategorySchema.pre("aggregate", function() {
    this.match({ status: true });
});
module.exports = mongoose.model("notification_category", NotificationCategorySchema);
