const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ProductSentSchema = mongoose.Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    create_uid: {
        type: Schema.Types.ObjectId,
        ref: 'res_user'
    },
    polygons: Object,
    points: Object,
    code: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        ref: 'res_user'
    },
    write_date: {
        type: Date,
        default: Date.now
    },
    ward_id: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu vị trí'],
        ref: 'res_ward'
    },
    district: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu vị trí'],
        ref: 'res_district'
    },
    direction: {
        type: String,
        require: [true, 'Thiếu hướng']
    },
    image: {
        type: String,
        require: [true, 'Thiếu hình đại diện']
    },
    document: [{
        type: String
    }],
    sub_image: Array,
    province: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu vị trí'],
        ref: 'res_province'
    },
    company_id: {
        type: String,
        required: true,
        default: 'ROOT'
    },
    investor_id: {
        type: Schema.Types.ObjectId,
    },
    status: {
        type: Boolean,
        default: true
    },
    delete_id: String,
    code: String,
    name: {
        type: String,
        required: [true, 'Thiếu tên bất động sản']
    },
    parent_id: String,
    category_id: {
        // type: String
        type: Schema.Types.ObjectId,
        require: true,
        ref: 'product_category'
    },
    display_photo: {
        type: String
    },
    detail_photo: {
        type: String
    },
    video: {
        type: String
    },
    view_3d: String,
    sale_price_unit_id: {
        type: String,
        require: [true, 'Thiếu đơn vị giá'],
    },
    sale_price: {
        type: Number,
        require: [true, 'Thiếu giá']
    },
    width: {
        type: Number,
        require: [true, 'Thiếu chiều rộng']
    },
    length_unit_id: {
        type: String,
        default: 'm',
        require: [true, 'Thiếu đơn vị chiều dài']
    },
    width_unit_id: {
        type: String,
        default: 'm',
        require: [true, 'Thiếu đơn vị chiều rộng']
    },
    height: {
        type: Number,
        require: [true, 'Thiếu chiều dài']
    },
    length: {
        type: Number,
        require: [true, 'Thiếu chiều cao']
    },
    unit_id: {
        type: String,
        require: [true, 'Thiếu đơn vị chiều dài'],
        //ref: 'product_unit'
    },
    land_area_unit_id: {
        type: String,
    },
    location: {
        type: String,
        require: [true, 'Thiếu vị trí']
    },
    handover_time: {
        type: Date,
        require: [true, 'Thiếu thời gian bàn giao']
    },
    cons_status: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu tình trạng xây dựng'],
    },
    legal: {
        type: String,
        require: [true, 'Thiếu tình trạng pháp lý']
    },
    land_area: {
        type: Number,
        require: [true, 'Thiếu điện tích đất']
    },
    land_type: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu loại đất'],
        ref: 'assets_planning_category'
    },
    type_id: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu loại giao dịch'],
    },
    bedroom_unit_id: String,
    bathroom_unit_id: String,
    a_value: {
        type: Number,
        require: [true, 'Thiếu định giá bán']
    },
    a_value_unit_id: {
        type: String,
        require: [true, 'Thiếu đơn vị định giá bán'],
        ref: 'product_unit'
    },
    a_rent: {
        type: Number,
        require: [true, 'Thiếu định giá thuê']
    },
    a_rent_unit_id: {
        type: String,
        require: [true, 'Thiếu đơn vị định giá thuê'],
        ref: 'product_unit'
    },
    feature: {
        type: String,
        require: [true, 'Thiếu đặc điểm bất động sản']
    },
    access_road: {
        type: String,
        require: [true, 'Thiếu đường tiếp cận']
    },
    address: {
        type: String,
        require: [true, 'Thiếu số nhà, tên đường']
    },
    block: {
        type: String,
    },
    project_id: {
        type: Schema.Types.ObjectId
    },
    bedroom: {
        type: Number,
        require: [true, 'Thiếu số phòng ngủ']
    },
    bathroom: {
        type: Number,
        require: [true, 'Thiếu số phòng tắm']
    },
    reaction: [{
        type: Schema.Types.ObjectId,
        ref: 'res_reaction',
        default: []
    }],
    asset_rating: Number,
    rating: [{
        type: Schema.Types.ObjectId,
        ref: 'res_rating'
    }],
    description: {
        type: String
    },
    cons_design_info_id: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu thông tin thiết kế và xây dựng']
    },
    legal_info_id: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu thông tin pháp lý'],
        ref: 'legal_info'
    },
    landlord_info_id: {
        type: Schema.Types.ObjectId,
        require: [true, 'Thiếu thông tin chủ đất'],
        ref: 'product_landlord_info'
    },
    rating_avg: Number,
    process_approve: {
        type: Object,
        default: 'AG2',
        enum: ['AG2', 'AG1', 'DT', 'VM', 'DTVM', 'RM']
    },
    status_approve: {
        type: String,
        default: 'PENDING',
        enum: ['PENDING', 'REJECTED', 'APPROVED']
    },
    number_of_floor: Number,
    floor_area: Number,
    number_of_floor_unit_id: String,
    floor_area_unit_id: String,
    number_of_sheet: Number,
    number_of_parcel: Number,
    oldId: Schema.Types.ObjectId,
    type: String
}, {
    collection: 'product_sent'
});
ProductSentSchema.pre("find", function () {
    this.where({
        status: {
            $ne: false
        }
    });
});
module.exports = mongoose.model('product_sent', ProductSentSchema);
