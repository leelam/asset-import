var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductLandlordInfoSchema = new Schema(
  {
    create_uid: { type: Schema.Types.ObjectId },
    create_date: {
      type: Date,
    },
    write_uid: {
      type: String,
    },
    write_date: {
      type: Date,
    },
    company_id: {
      type: String,
    },
    status: Boolean,
    delete_id: String,
    product_id: {
      type: Schema.Types.ObjectId,
      ref: 'product',
    },
    full_name: {
      type: String,
    },
    gender: {
      type: String,
    },
    date_of_birth: {
      type: Date,
    },
    address: {
      type: String,
    },
    tel: {
      type: String,
    },
    email: {
      type: String,
    },
    owner: {
      type: String,
    },
    note: {
      type: String,
    },
  },
  { collection: 'product_landlord_info' }
);

ProductLandlordInfoSchema.pre('find', function () {
  this.where({ status: { $ne: false } });
});
ProductLandlordInfoSchema.pre('countDocuments', function () {
  this.where({ status: { $ne: false } });
});
ProductLandlordInfoSchema.pre('aggregate', function () {
  this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
module.exports = mongoose.model(
  'product_landlord_info',
  ProductLandlordInfoSchema
);
