const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var RecruitCategorySchema = mongoose.Schema(
    {
        id:String,
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name:{type:String,required:[true,'Missing name']},
        parent_id: { type: Schema.Types.ObjectId, ref:'recruit_category',default: null },
        category_code:String,
        sequence:{type:Number,default:0},
        keyword:String,
        description:String

    },
    { collection: "recruit_category" }
);

RecruitCategorySchema.pre("countDocuments", function() {
    this.where({ status: {$ne:false} });
});
RecruitCategorySchema.pre("find", function() {
    this.where({ status: {$ne:false} });
});
module.exports = mongoose.model("recruit_category", RecruitCategorySchema);
