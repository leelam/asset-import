const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResAddressSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      ref: 'res_user'
    },
    create_date: {
      type: Date
    },
    write_uid: {
      type: String
    },
    write_date: {
      type: Date
    },
    company_id: {
      type: String,
      required: true,
      default: 'ROOT'
    },
    status: {
      type: Boolean,
      default: true
    },
    delete_id: String,
    address_name: String,
    type: String,
    parent_id: {
      type: Schema.Types.ObjectId,
      require: true,
      ref: 'res_address_new'
    },
    code: {
      type: String,
      unique: true
    },
    country_id: Number,
    reaction: [
      { type: Schema.Types.ObjectId, ref: 'res_reaction', default: [] }
    ],
    points: Object,
    polygon_id: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_polygon'
      }
    ]
  },
  { collection: 'res_address_new' }
);

ResAddressSchema.pre('save', function(next) {
  this.create_uid = new mongoose.Types.ObjectId('5d9c3342cc1f7b3f4c8e79dd');
  next();
});
module.exports = mongoose.model('res_address_new', ResAddressSchema);
