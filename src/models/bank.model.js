const mongoose = require('mongoose'); 
var Schema = mongoose.Schema
var PlanningSchema = mongoose.Schema({
    create_uid: { type: String },

    create_date: {
        type: Date
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String,
    },
    status: Boolean,
    delete_id: String,

    code: String,
    company_name: String,
    foreign_name: String,
    display_name: String,
    preferential_rate: Number,
    interest_rate: Number,
    rate_unit_id: String
},
    { collection: 'bank' }
);
module.exports = mongoose.model('bank', PlanningSchema);