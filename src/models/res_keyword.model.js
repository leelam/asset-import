const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;
var KeywordSchema = mongoose.Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: null
      },
      create_date: {
        type: Date,
        default: Date.now()
      },
    country_id:{type:Schema.Types.ObjectId,ref:'res_country',default:null},
    content: String,
    sub_title: String,
    search: { type: String, lowercase: true },
    rating: { type: Number, default: 0 }
},
    { collection: 'res_keyword' }
);
module.exports = mongoose.model('res_keyword', KeywordSchema);