const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
var RecruitSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: newId,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: newId
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: {type:Boolean,default:true},
    delete_id: String,
    category_id:[ {
      type: Schema.Types.ObjectId,
      ref: "recruit_category",
      default: []
    }],
    name: { type: String, required: true },
    sequence: { type: Number, default: 0 },
    content: { type: String, default: "" },
    url_name: { type: String, default: "" },
    keyword: { type: String, default: "" },
    description: { type: String, default: "" },
    image_url:{type:String,default:''},
  },
  { collection: "recruit" }
);

RecruitSchema.pre("find", function() {
  this.where({ status: true });
});
RecruitSchema.pre("countDocuments", function() {
  this.where({ status: true });
});
RecruitSchema.virtual("category", {
  type: "ObjectId",
  ref: "recruit_category",
  localField: "category_id",
  foreignField: "_id",
  justOne: true
});

module.exports = mongoose.model("recruit", RecruitSchema);
