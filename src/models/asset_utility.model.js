const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var UtilitySchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
    },
    create_date: {
      type: Date,
      default: Date.now,
    },
    write_uid: {
      type: Schema.Types.ObjectId,
    },
    write_date: {
      type: Date,
      default: Date.now,
    },
    company_id: {
      type: String,
      default: 'ROOT',
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    category_id: {
      type: Schema.Types.ObjectId,
      ref: 'assets_utility_category',
    },
    name: String,
    points: Object,
    code: {
      type: String,
      unique: true,
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_ward',
    },
    avatar: {
      type: String,
      set(value) {
        return `images\\Utility\\${value}`;
      },
    },
    tel: String,
    website: String,
    address: String,
    country_code: String,
    import: {
      type: Boolean,
    },
  },
  { collection: 'assets_utility_demo_1' }
);
module.exports = mongoose.model('assets_utility_demo_1', UtilitySchema);
