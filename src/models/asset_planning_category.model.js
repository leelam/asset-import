const mongoose = require('mongoose'); 
var PlanningCategorySchema = mongoose.Schema({
    create_uid: { type: String },

    create_date: {
        type: Date
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String,
    },
    status: Number,
    delete_id: String,

    
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    red: {
        type: String
    },
    green: String,
    blue: String,
    import: Boolean
},
    { collection: 'assets_planning_category' }
);
module.exports = mongoose.model('assets_planning_category', PlanningCategorySchema);