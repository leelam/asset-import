const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var UserPermissionSchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: null
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: null
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name: {
            type: String,
            required: [true, "name is required"]
        },
        url:{type:String,required:[true,'url is required']},
        method:{type:String,required:[true,'method is required']}
    },
    { collection: "res_permission" }
);

UserPermissionSchema.pre("find", function() {
    this.where({ status: true });
});
UserPermissionSchema.pre("countDocuments", function() {
    this.where({ status: true });
});
UserPermissionSchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});

module.exports = mongoose.model("res_permission", UserPermissionSchema);
