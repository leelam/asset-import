const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId('5dbb9fbbebd384b5e4ecc368');
const Schema = mongoose.Schema;
const changeAlias = require('../helpers/change_alias');
const resSearchStringModel = require('../models/res_search_string.model');
var ResRoadModel = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: '5dbb9d3b65bb02ebb0ec7fb3',
      ref: 'res_user',
    },
    create_date: {
      type: Date,
      default: Date.now,
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: '5dbb9d3b65bb02ebb0ec7fb3',
    },
    write_date: {
      type: Date,
      set() {
        return Date.now();
      },
      default: Date.now,
    },
    company_id: {
      type: String,
      default: 'ROOT',
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    code: {
      type: String,
      unique: true,
    },
    name: {
      type: String,
      required: true,
    },
    district_id: [
      {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'res_district',
      },
    ],
    points: {
      type: Object,
      required: true,
    },
    polygons: {
      type: Object,
      required: true,
    },
    from_to: String,
    category_id: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'res_road_category',
    },
    level: Number,
    planning_category_id: {
      type: Schema.Types.ObjectId,
      ref: 'assets_planning_category',
    },
    location: String,
    exist: String,
    exist_unit_id: String,
    road_width: Number,
    width_unit_id: String,
    original_price: Number,
    price_unit_id: String,
    adj_factor: {
      type: Number,
      set(value) {
        if (isNaN(value)) return 0;
      },
    },
    factor_unit_id: String,
    refernce: String,
    a_value: Number,
    a_value_unit_id: String,
    nussiness_suggestions: String,
    description: String,
    import: {
      type: Boolean,
      default: true,
    },
    reaction: [
      {
        type: Schema.Types.ObjectId,
        default: [],
      },
    ],
    FID: {
      type: String,
      required: true,
      unique: true,
    },
    length: Number,
    length_unit_id: String,
    cross_ward: [
      {
        type: Schema.Types.ObjectId,
        ref: 'res_ward',
        require: false,
      },
    ],
  },
  { collection: 'res_road' }
);
ResRoadModel.pre('find', function () {
  this.where({ status: { $ne: false } });
});
ResRoadModel.post('save', async function (doc, next) {
  const title = doc.name;
  let location = doc.location.split('-');
  data = {
    sub_title: `${doc.from_to}-${location[1]}`,
    title,
    search: `${changeAlias(
      title.toLocaleLowerCase()
    )} ${title.toLocaleLowerCase()}`,
    name: 'res_road',
    status: true,
    refPath: 'res_road',
    rel_id: doc._id,
    weight: 0.1,
  };
  const newSearch = new resSearchStringModel(data);
  await newSearch.save(data);
  return next();
});
module.exports = mongoose.model('res_road', ResRoadModel);
