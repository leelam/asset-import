const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ReportModel = mongoose.Schema(
  {
    id: String,
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: 'res_user'
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: null
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    approved_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: 'res_user'
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    // name: { type: String, required: [true, "Missing name"] },
    category_id: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "report_category"
    },
    sequence: { type: Number, default: 0 },
    keyword: String,
    answer: String,
    whoAreYou: String,
    approve:{
      type:String,
      enum: ['PENDING', 'APPROVED'],
      default:'PENDING'
    }
  },
  { collection: "report" }
);

ReportModel.pre("find", function() {
  this.where({ status: { $ne: false } });
});
ReportModel.pre("countDocuments", function() {
  this.where({ status: { $ne: false } });
});
ReportModel.pre("aggregate", function() {
  this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
ReportModel.virtual("category", {
  type: "ObjectId",
  ref: "ask_category",
  localField: "category_id",
  foreignField: "_id",
  justOne: false
});
module.exports = mongoose.model("report", ReportModel);
