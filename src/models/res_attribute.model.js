const mongoose = require('mongoose'); 
var ResAttributeSchema = mongoose.Schema({
    create_uid: { type: String },

    create_date: {
        type: Date,
        default:Date.now()
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String
    },
    status: Boolean,
    delete_id: String,

    name: { type: String, required: true },
    code:{type:String,required:true},
    sequence: Number,
   
},
    { collection: 'res_attributes' }
);
module.exports = mongoose.model('res_attributes', ResAttributeSchema);