var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ResResourceSchema = new Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: null,
        ref:'res_user'
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: null,
        ref:'res_user'
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null,ref:'res_user' },

    name: {
        type: String,
        required: true
    },
    sequence:{
        type:Number,
        default:0
    },
    description: {
        type: String,
        required: true
    },
    category_id: [{
        type: Schema.Types.ObjectId,
        required: true
    }],
    url_name: {
        type: String,
        required: true
    },
    document_url: {
        type: String,
        required: true
    },
    reaction: [{
        type: Schema.Types.ObjectId,
        default:[]
    }]
}, { collection: 'res_resource' })

ResResourceSchema.pre("find", function () {
    this.where({ status: true });
});
ResResourceSchema.pre("countDocuments", function () {
    this.where({ status: true });
});
ResResourceSchema.pre("aggregate", function () {
    this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
ResResourceSchema.virtual("user", {
    type: "ObjectId",
    ref: "res_user",
    localField: "create_uid",
    foreignField: "_id",
    justOne: true
  });
  ResResourceSchema.set("toObject", { virtuals: true, minimize: true });
  ResResourceSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model('res_resource', ResResourceSchema)