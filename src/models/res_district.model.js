const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var ResProvinceSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: "ROOT"
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  // polygons: {
  //   type: Object,
  // },
  name: { type: String, required: true },
  code: { type: String, required: true, unique: true },
  main_code: { type: String},
  province_id: { type: Schema.Types.ObjectId, ref: 'res_province' },
  sequence: Number,
  points: Object,
  type: String,
  reaction: [{
    type: Schema.Types.ObjectId,
    default: []
  }]
},
  { collection: 'res_district' }
);
ResProvinceSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});

ResProvinceSchema.pre("save", function () {
  console.log(this);
});
ResProvinceSchema.virtual("province", {
  type: "ObjectId",
  ref: "res_province",
  localField: "province_id",
  foreignField: "_id",
  justOne: true
});
module.exports = mongoose.model('res_district', ResProvinceSchema);