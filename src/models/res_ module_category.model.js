const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ModuleCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null,
            ref:'res_user'
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null,
            ref:'res_user'
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name: {
            type: String,
            required: [true, "name is required"]
        },
        description: { type: String, default: "" },
        url_name: { type: String, required: [true, "url_name is required"] },
        code:{type:String,required:[true,'code is required'],index: { unique: true }},
        sequence: { type: Number, default:0},
        type: String,
        parent_id: { type: Schema.Types.ObjectId, ref: "res_module_category" },
        keyword: { type: String, default: "" },
    },
    { collection: "res_module_category" }
);
ModuleCategorySchema.pre("find", function() {
    this.where({ status: true });
});
ModuleCategorySchema.pre("countDocuments", function() {
    this.where({ status: true });
});
ModuleCategorySchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
module.exports = mongoose.model("res_module_category", ModuleCategorySchema);
