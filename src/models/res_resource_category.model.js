var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
var ResResourceCategorySchema = new Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: newId
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    parent_id: {
        type: Schema.Types.ObjectId,
        default: null
    },
    name: {
        type: String,
        required: true
    },
    sequence:{type:Number,default:0}
}, { collection: 'res_resource_category' })

ResResourceCategorySchema.pre("find", function () {
    this.where({ status: true });
});
ResResourceCategorySchema.pre("countDocuments", function () {
    this.where({ status: true });
});
ResResourceCategorySchema.pre("aggregate", function () {
    this.pipeline().unshift({ $match: { status: { $ne: false } } });
});

module.exports = mongoose.model('res_resource_category', ResResourceCategorySchema)