const mongoose = require("mongoose");
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var ProjectCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        category_name:{type:String,required:[true,'Missing category_name']},
        parent_id: { type: Schema.Types.ObjectId, ref:'project_category',default: null },
        category_code:String,
        sequence:Number,
        keyword:String,
        description:String

    },
    { collection: "project_category" }
);

ProjectCategorySchema.pre("find", function () {
    this.where({ status: { $ne: false } });
});
ProjectCategorySchema.pre("countDocuments", function () {
    this.where({ status: { $ne: false } });
});
ProjectCategorySchema.pre("aggregate", function () {
    this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
module.exports = mongoose.model("project_category", ProjectCategorySchema);
