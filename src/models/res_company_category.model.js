const mongoose = require("mongoose");
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var CompanyCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },

        
        name: {
            type: String,
            required: [true, "name is required"]
        },
        description: { type: String, default: "" },
        url_name: { type: String },
        code:{type:String,required:[true,'code is required'],index: { unique: true }},
        sequence: { type: Number, },
        type: String,
        parent_id: { type: Schema.Types.ObjectId, ref: "res_company_category" },
        keyword: { type: String, default: "" },
    },
    { collection: "res_company_category" }
);
CompanyCategorySchema.pre("find", function() {
    this.where({ status: true });
});
CompanyCategorySchema.pre("countDocuments", function() {
    this.where({ status: true });
});
CompanyCategorySchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
module.exports = mongoose.model("res_company_category", CompanyCategorySchema);
