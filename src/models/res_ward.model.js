const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var ResWardSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: "ROOT"
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },

  name: { type: String, required: true },
  code: { type: String, required: true },
  district_id: { type: Schema.Types.ObjectId, ref: 'res_district' },
  sequence: Number,
  type: String,
  points: Object,
  // polygons: {
  //   type: Object,
  // },
  reaction: [{
    type: Schema.Types.ObjectId,
    default: []
  }]

},
  { collection: 'res_ward' }
);
ResWardSchema.virtual("district", {
  type: "ObjectId",
  ref: "res_district",
  localField: "district_id",
  foreignField: "_id",
  justOne: true
});
ResWardSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_ward', ResWardSchema);