const mongoose = require("mongoose");
var AutoIncrement = require("mongoose-sequence")(mongoose);
const Schema = mongoose.Schema;
var PostSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: null
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    name: { type: String, required: true },
    category_id: [{ type: Schema.Types.ObjectId,ref:'post_category' }],
    description: { type: String, default: null },
    url_name: { type: String, required: true },
    sequence: { type: Number, index: { unique: true } },
    photo: String,
    receipt_no: Number,
    receipt_date: Date,
    keyword: String,
    content: String,
    flash:{type:Number,default:0},
    image_url:String,
    reaction: [{ type: Schema.Types.ObjectId, ref: "res_react" ,default:[]}],
    comment: [{ type: Schema.Types.ObjectId, ref: "comment" }],
    publish: { type: Boolean, default: true },
    publish_date: {type:Date,default:Date.now()}
  },
  { collection: "post" }
);

PostSchema.pre("find", function() {
  this.where({ status: true });
});
PostSchema.pre("countDocuments", function() {
  this.where({ status: true });
});
PostSchema.virtual("category", {
  type: "ObjectId",
  ref: "post_category",
  localField: "category_id",
  foreignField: "_id",
  justOne: false
});
PostSchema.plugin(AutoIncrement, {
  id: "post_sequence",
  inc_field: "sequence"
});
PostSchema.set("toObject", { virtuals: true, minimize: true });
PostSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("post", PostSchema);
