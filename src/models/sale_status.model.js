const mongoose = require('mongoose')
const {Schema} = mongoose
const saleStatusSchema = new Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: "res_user"
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: null
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_uid: {
          type: Schema.Types.ObjectId,
          default: null,
          ref: "res_user"
    },
    name: String,
}, {collection: 'sale_status'})

module.exports = mongoose.model('sale_status', saleStatusSchema)