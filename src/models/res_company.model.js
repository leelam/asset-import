const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId('5d679819325ab70ab0157ce5');
const Schema = mongoose.Schema;
const change_alias = require('../helpers/change_alias');
const resSearchStringModel = require('../models/res_search_string.model');
const { getAddress } = require('../controllers/address.controller');

var CompanySchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: newId,
    },
    create_date: {
      type: Date,
      default: Date.now(),
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: newId,
    },
    write_date: {
      type: Date,
      default: Date.now(),
    },
    company_id: {
      type: String,
      default: 'ROOT',
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    logo: { type: String, default: '' },
    banner: { type: String, default: '' },
    code: {
      type: String,
      default: '',
      unique: true,
    },
    address: {
      type: String,
    },
    category_id: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'res_company_category',
    },
    parent_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_company',
      require: false,
    },
    tel: {
      type: String,
      default: '',
    },
    hotline: {
      type: String,
      default: '',
    },
    email: {
      type: String,
      default: '',
    },
    website: {
      type: String,
      default: '',
    },
    address1: {
      type: String,
      default: '',
    },
    ward_id: {
      type: Schema.Types.ObjectId,
      ref: 'res_ward',
    },
    company_name: {
      type: String,
      default: '',
    },
    display_name: {
      type: String,
      default: '',
    },
    founded_year: {
      type: Number,
      default: null,
    },
    legal_representative: {
      type: String,
      default: '',
    },
    company_type: {
      type: String,
      default: '',
    },
    tax_code: {
      type: String,
      default: '',
    },
    foreign_name: String,
    preferential_rate: String,
    bank_account: {
      type: String,
      default: '',
    },
    bank_name: {
      type: String,
      default: '',
    },
    interest_rate: Number,
    bank_branch: {
      type: String,
      default: '',
    },
    description: {
      type: String,
    },
    search: { type: String, default: '' },
    points: Object,
    icon: { type: String, default: '' },
    logo: { type: String, default: '' },
    working_area: {
      type: Schema.Types.ObjectId,
      default: null,
      refPath: 'working_area_collection_name',
    },
    working_area_collection_name: {
      type: String,
      enum: ['res_ward', 'res_district', 'res_province'],
    },
    main_business: String,
    operating_status: String,
    company_code: String,
    bank_id: {
      type: Schema.Types.ObjectId,
      require: false,
      ref: 'res_company',
    },
  },
  { collection: 'res_company' }
);
CompanySchema.virtual('category', {
  type: 'ObjectId',
  ref: 'res_company_category',
  localField: 'category_id',
  foreignField: '_id',
  justOne: true,
});
CompanySchema.virtual('partner', {
  type: 'ObjectId',
  ref: 'res_partner',
  localField: 'partner_id',
  foreignField: '_id',
  justOne: false,
});

CompanySchema.index({
  company_name: 'text',
});

CompanySchema.pre('save', async function (next) {
  if (!this.ward_id) {
    console.log(this.code);
    return next();
  }
  const location = await getAddress(this.ward_id);
  let ward, district, province;
  if (location.data) {
    ward = location.data.ward.name;
    district = location.data.district.name;
    province = location.data.province.name;
  }
  console.log(this.ward_id);
  const lowerCaseStringName = this.company_name.toLowerCase();
  const noneAccentName = change_alias(this.company_name.toLowerCase());
  const lowerCaseStringAddress = this.address ? this.address.toLowerCase() : '';
  const noneAccentAddress = this.address
    ? change_alias(this.address.toLowerCase())
    : '';
  const country_codePhone = `${this.country_code}${this.tel} 0${this.tel}`;

  this.search = `${lowerCaseStringName} ${noneAccentName} ${lowerCaseStringAddress} ${noneAccentAddress} +${country_codePhone} ${
    ward ? ward.toLowerCase() : ''
  } ${district ? district.toLowerCase() : ''} ${
    province ? province.toLowerCase() : ''
  } ${change_alias(ward)} ${change_alias(
    district.toLowerCase()
  )} ${change_alias(province.toLowerCase())}`;
  next();
});

CompanySchema.post('save', async function (doc, next) {
  try {
    const checkRes = await resSearchStringModel.findOne({ rel_id: doc._id });
    const data = {
      rel_id: doc._id,
      refPath: 'res_company',
      name: 'company',
      search: doc.search,
      title: doc.company_name,
      status: true,
      weight: 0.1,
    };
    if (!checkRes) {
      const newSearch = new resSearchStringModel(data);
      await newSearch.save(data);
      return next();
    }
    checkRes.search = doc.search;
    console.log(checkRes);
    await checkRes.save(data);
  } catch (e) {
    console.log(e);
    next();
  }
});
// CompanyCategorySchema.pre("find", function() {
//     this.where({ status: true });
// });
// CompanyCategorySchema.pre("countDocuments", function() {
//     this.where({ status: true });
// });
// CompanyCategorySchema.pre("aggregate", function() {
//     this.pipeline().unshift({$match:{ status: { $ne: false } }});
// });
CompanySchema.set('toObject', { virtuals: true, minimize: true });
CompanySchema.set('toJSON', { virtuals: true, minimize: true });
module.exports = mongoose.model('res_company', CompanySchema);
