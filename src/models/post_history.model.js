const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var PostHistorySchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    type: { type: String, enum: ["INSERT", "UPDATE", "DELETE", "REVERT"], required: [true, 'type is required'] },
    collection_name: String,
    rel_id: { type: Schema.Types.ObjectId, ref: 'post' },
    version: { type: Number, required: [true, 'version is required'] },
    old_version: Object,
    new_version: Object
  },
  { collection: "post_history" }
);
PostHistorySchema.virtual("create_user", {
  type: "ObjectId",
  ref: "res_user",
  localField: "create_id",
  foreignField: "_id",
  justOne: true
});
PostHistorySchema.set("toObject", { virtuals: true, minimize: true });
PostHistorySchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("post_history", PostHistorySchema);
