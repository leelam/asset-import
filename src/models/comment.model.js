const mongoose = require("mongoose");
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var today = new Date();
var CommentSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: newId
    },
    create_date: {
      type: Date,
      default: today
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: newId
    },
    write_date: {
      type: Date,
      default: today
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    title: { type: String, required: [true, "Title is required"] },
    reaction:[{type:Schema.Types.ObjectId,ref:'res_reaction', default:[]}],
    parent_id: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "comment"
    },
    children: [
      {
        type: Schema.Types.ObjectId,
        default: null,
        ref: "comment"
      }
    ],
    path: {
      type: String,
      require: true,
      enum: [
        "assets_planning",
        "product",
        "post",
        "project",
        "res_road",
      ]
    },
    rel_id: {
      type: Schema.Types.ObjectId,
      refPath: "path"
    },
  },
  { collection: "comment" }
);
CommentSchema.pre("find", function() {
  this.where({ status: true });
});
module.exports = mongoose.model("comment", CommentSchema);
