var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    create_data: {
        type: Date,
        default: Date.now
    }
}, {collection: 'account_admin'});

module.exports = mongoose.model('user', userSchema);