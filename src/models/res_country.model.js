const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResTimeZoneSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: null
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  name: { type: String, required: true },
  population: { type: Number, required: true },
  area: { type: Number },
  pop_density: { type:Number },
  coastline: { type: Number },
  net_migration: { type: Number },
  gdp: { type: Number },
  literacy: { type: Number },
  phone: { type: Number },
  arable: { type: Number },
  crops: { type: Number },
  other: { type: Number },
  climate: { type: Number },
  birthrate: { type: Number },
  deathrate: { type: Number },
  industry: { type: Number },
  service: { type: Number },
  dialing: { type: String,default:'' },
},
  { collection: 'res_country' }
);
ResTimeZoneSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_country', ResTimeZoneSchema);