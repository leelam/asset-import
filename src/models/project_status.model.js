const mongoose = require("mongoose");
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var ProjectStatusSchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        status_name: {
            type: String,
            required: [true, "status_name is require"]
        },
        code: {type:String,default:''},
        order_by: { type: Number },
        percentage:{ type: Number },
        image_url: { type: String,default:''},
        sequence: { type: Number,default:0 },
        parent_id: { type: Schema.Types.ObjectId, ref: "project_status" ,default:null},
    },
    { collection: "project_status" }
);
ProjectStatusSchema.pre("find", function() {
    this.where({ status:{$ne:false} });
});

ProjectStatusSchema.pre("countDocuments", function() {
    this.where({ status:{$ne:false}});
});
ProjectStatusSchema.pre("aggregate", function() {
    this.match({ status:{$ne:false}});
});
module.exports = mongoose.model("project_status", ProjectStatusSchema);
