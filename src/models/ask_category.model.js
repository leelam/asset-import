const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var AskCategorySchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: null
    },
    write_date: {
      type: Date,
      default:  null
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    parent_id: {
      type: Schema.Types.ObjectId,
      ref: "ask_category",
      default: null
    },
    name: { type: String, required: [true,'name is required'] },
    code:{type:String,required:["code is required"]},
    sequence: {type:Number,default:0},
    url_name: String
  },
  { collection: "ask_category" }
);
AskCategorySchema.pre("find", function() {
  this.where({ status: { $ne: false } });
});
AskCategorySchema.pre("countDocuments", function() {
  this.where({ status: { $ne: false } });
});
AskCategorySchema.pre("aggregate", function() {
  this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
module.exports = mongoose.model("ask_category", AskCategorySchema);
