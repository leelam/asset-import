const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ModuleSchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: null
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: null
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        url: {
            type: String,
        },
        method: {
            type: String,
        },
        code: {
            type: String,
        },
    },
    { collection: "res_module" }
);

ModuleSchema.pre("find", function() {
    this.where({ status: {$ne:false} });
});
ModuleSchema.pre("countDocuments", function() {
    this.where({ status: {$ne:false} });
});
ModuleSchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
ModuleSchema.virtual("category", {
    type: "ObjectId",
    ref: "res_module_category",
    localField: "category_id",
    foreignField: "_id",
    justOne: true
  });

  ModuleSchema.set("toObject", { virtuals: true, minimize: true });
  ModuleSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("res_module", ModuleSchema);
