const mongoose = require('mongoose');

const { Schema } = mongoose;

const shapeObj = {
  status: {
    type: String,
    enum: ['Có', 'Chưa rõ', 'Không có', 'Đang cập nhật', 'Chưa có'],
  },
  file: String,
};
const projectLegalInfoSchema = new Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      ref: 'res_user',
      default: '5dbb9d3b65bb02ebb0ec7fb3',
    },
    create_date: {
      type: Date,
      default: Date.now,
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      ref: 'res_user',
      default: '5dbb9d3b65bb02ebb0ec7fb3',
    },
    write_date: {
      type: Date,
      default: Date.now,
      set() {
        return Date.now();
      },
    },
    company_id: {
      type: String,
    },
    status: {
      type: Boolean,
      default: true,
    },
    delete_id: String,
    planning_approval_1_500: shapeObj,
    investment_approval: shapeObj,
    me_drawing_for_per_apartment: shapeObj,
    acceptance_of_fire_protection_system: shapeObj,
    acceptance_of_works: shapeObj,
    agreement_on_connection_of_electricity_and_water_works: shapeObj,
    agreement_on_traffic_connection: shapeObj,
    agreement_redbook_investor_and_buyer: shapeObj,
    approval_of_backfill_design: shapeObj,
    approval_of_basic_design: shapeObj,
    approval_of_construction_design: shapeObj,
    approval_of_engineering_design: shapeObj,
    approval_of_fire_prevention_and_fighting: shapeObj,
    approval_of_infrastructure_design: shapeObj,
    bank_guarantee: shapeObj,
    basement_foundation_acceptance: shapeObj,
    certificate_of_business_registration: shapeObj,
    certificate_of_investment_registration: shapeObj,
    certification_of_investor: shapeObj,
    confirmation_of_infrastructure_completion: shapeObj,
    construction_permit: shapeObj,
    construction_survey_report: shapeObj,
    deal_of_construction_height: shapeObj,
    deal_of_embankment_edges: shapeObj,
    decisions_on_land_allocation_land_lease: shapeObj,
    economic_technical_report: shapeObj,
    environmental_impact_assessment_report: shapeObj,
    environmental_protection_plan: shapeObj,
    escrow_agreement: shapeObj,
    feasibility_study_repor: shapeObj,
    finish_dossiers: shapeObj,
    home_recieving_notice_to_buyers: shapeObj,
    hotel_standard_certificate: shapeObj,
    house_numbering_decision: shapeObj,
    location_drawing: shapeObj,
    minutes_of_handover_of_houses_or_construction_to_buyers: shapeObj,
    notice_of_approval_of_sample_contract: shapeObj,
    notice_of_eligibility_for_capital_raising: shapeObj,
    notification_of_payment_of_land_use: shapeObj,
    notification_of_the_residences_eligibility_for_sale: shapeObj,
    notifications_of_request_for_registration_fee: shapeObj,
    other_permit: shapeObj,
    redbook_buyer: shapeObj,
    redbook_land: shapeObj,
    social_permit: shapeObj,
    strategic_environmental_assessment_report: shapeObj,
  },
  {
    collection: 'project_legal_info',
  }
);

module.exports = mongoose.model('project_legal_info', projectLegalInfoSchema);
