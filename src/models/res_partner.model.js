const mongoose = require("mongoose");
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const Schema = mongoose.Schema;
var PartnerCategorySchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: newId
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name:{type: String,
            required: [true, "partner name is required"]},
        display_name: {
            type: String,
        },
        commercial_name: {
            type: String,
        },
        description: { type: String, default: "" },
        code: {type:String, required:[true,'partner code is required'], index: { unique: true }},
        sequence: { type: Number, default:0},
        website:{type:String,default:''},
        address:{type:String,default:''},
        ward_id:{type:Schema.Types.ObjectId,default:null,ref:'res_ward'},
        zip:{type:String,default:''},
        email:{type:String,default:''},
        barcode:{type:String,default:''},
        vat:{type:String,default:''},
        phone:{type:String,default:''},
        mobile:{type:String,default:''},
        fax:{type:String,default:''},
        website:{type:String,default:''},
        category_id:{type:Schema.Types.ObjectId,default:null,ref:'res_partner_category'},
        gender:{type:String,default:''},
        birthday:{type:Date,default:null},
        contact_title:{type:String,default:''},
        contact_position:{type:String,default:''},
        contact_phone:{type:String,default:''},
        contact_mobile:{type:String,default:''},
        contact_email:{type:String,default:''},

        keyword: { type: String, default: "" },
    },
    { collection: "res_partner" }
);
PartnerCategorySchema.pre("find", function() {
    this.where({ status: true });
});
PartnerCategorySchema.pre("countDocuments", function() {
    this.where({ status: true });
});
PartnerCategorySchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
module.exports = mongoose.model("res_partner", PartnerCategorySchema);
