const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var AskSchema = mongoose.Schema(
  {
    create_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    create_date: {
      type: Date,
      default: Date.now()
    },
    write_uid: {
      type: Schema.Types.ObjectId,
      default: null,
      ref: "res_user"
    },
    write_date: {
      type: Date,
      default: Date.now()
    },
    company_id: {
      type: String,
      default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },
    category_id: [{ type: Schema.Types.ObjectId,ref:'ask_category' }],
    reaction:[{ type: Schema.Types.ObjectId,ref:'res_reaction', default:[]}],
    comment:{ type: Schema.Types.ObjectId,ref:'comment' },
    name: { type: String, required: true },
    asset_name: { type: String,default:''},
    sequence:  { type: Number,default:0},
    url_name:  { type: String,default:''},
    content:{type:String,default:''},
    approve:{
      type:String,
      enum: ['PENDING', 'APPROVED','DENY'],
      default:'PENDING'
    }
  },
  { collection: "ask" }
);
AskSchema.pre("find", function() {
  this.where({ status: { $ne: false } });
});
AskSchema.pre("countDocuments", function() {
  this.where({ status: { $ne: false } });
});
AskSchema.pre("aggregate", function() {
  this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
AskSchema.virtual("category", {
  type: "ObjectId",
  ref: "ask_category",
  localField: "category_id",
  foreignField: "_id",
  justOne: false
});
AskSchema.set("toObject", { virtuals: true, minimize: true });
AskSchema.set("toJSON", { virtuals: true, minimize: true });


module.exports = mongoose.model("ask", AskSchema);