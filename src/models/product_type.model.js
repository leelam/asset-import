var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var ProductTypeSchema = new Schema({
    create_uid: { type: String },

    create_date: {
        type: Date
    },

    write_uid: {
        type: String
    },
    write_date: {
        type: Date
    },
    company_id: {
        type: String,
        default: null
    },
    status: Boolean,
    delete_id: String,

    type_name: {
        type: String,
        required: true
    },
    type: String,
    parent_id: {
        type: mongoose.Types.ObjectId,
        default: null
    },
    type_code: String,
    marker_url:{
        type: String,
        required: true
    },
    marker_hover_url: {
        type: String,
        required: true
    },
    color_code: {
        type: String,
        required: true
    }

}, {collection: 'product_type'})
ProductTypeSchema.pre("find", function () {
    this.where({ status: { $ne: false } });
});
ProductTypeSchema.pre("countDocuments", function () {
    this.where({ status: { $ne: false } });
});
ProductTypeSchema.pre("aggregate", function () {
    this.pipeline().unshift({ $match: { status: { $ne: false } } });
});

module.exports = mongoose.model('product_type', ProductTypeSchema)