const mongoose = require('mongoose');
var newId = new mongoose.mongo.ObjectId("5dbb9d3b65bb02ebb0ec7fb3");
const Schema = mongoose.Schema;
var ResRoadCategoryModel = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  write_uid: {
    type: Schema.Types.ObjectId,
  },
  write_date: {
    type: Date,
    default: Date.now
  },
  company_id: {
    type: String,
    default: "ROOT"
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  code: {
    unique: true,
    type: String,
  },
  name: {
    type: String,
    required: true
  },
  district_id: [{
    type: Schema.Types.ObjectId,
    ref: 'res_district'
  }]

},
  { collection: 'res_road_category' }
);
ResRoadCategoryModel.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_road_category', ResRoadCategoryModel);