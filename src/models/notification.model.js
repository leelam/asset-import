const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var NotificationSchema = mongoose.Schema(
    {
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: null
        },
        receiver: {
            type: Schema.Types.ObjectId,
            default: null,
            ref:'res_user'
        },
        content:{type:String,required:[true,'Missing content']},
        url:{type:String,default:''},
        is_read:{type:Boolean,default:false},
        read_date:{
            type: Date,
            default: null
        },
        is_view:{type:Boolean,default:false},
        view_date:{
            type: Date,
            default: null
        },
        category_id:{
            type: Schema.Types.ObjectId,
            default: null,
            ref:'notification_category'
        }
    },
    { collection: "notification" }
);

NotificationSchema.pre("find", function() {
    this.where({ status: {$ne:false} });
});
NotificationSchema.pre("countDocuments", function() {
    this.where({ status: {$ne:false} });
});
NotificationSchema.pre("aggregate", function() {
    this.pipeline().unshift({$match:{ status: { $ne: false } }});
});
NotificationSchema.virtual("category", {
    type: "ObjectId",
    ref: "res_module_category",
    localField: "category_id",
    foreignField: "_id",
    justOne: true
  });
  NotificationSchema.virtual("create_user", {
    type: "ObjectId",
    ref: "res_user",
    localField: "create_uid",
    foreignField: "_id",
    justOne: true
  });

  NotificationSchema.set("toObject", { virtuals: true, minimize: true });
  NotificationSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("notification", NotificationSchema);
