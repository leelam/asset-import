const mongoose = require('mongoose');

const Schema = mongoose.Schema;
var ProjectConsDesignInfoSchema = mongoose.Schema({
    create_uid: {
        type: Schema.Types.ObjectId,
        default: null
    },
    create_date: {
        type: Date,
        default: Date.now()
    },
    write_uid: {
        type: Schema.Types.ObjectId,
        default: null
    },
    write_date: {
        type: Date,
        default: Date.now()
    },
    company_id: {
        type: String,
        default: "ROOT"
    },
    status: { type: Boolean, default: true },
    delete_id: { type: Schema.Types.ObjectId, default: null },

    general_info_of_cons: {
        design_info: {
            design_style: String,
            design_type: String,
            design_standard: String,
            handover_condition: String
        }
    },
    material_structure: {
        basement_foundation: {
            basement: Number,
            basement_unit_id: String,
            basement_foundation_type: String,
            basement_structure: String
        },
        rough_cons: {
            floor_min: Number,
            floor_min_unit_id: String,
            number_of_floor: Number,
            floor_unit_id: String,
            structure: String
        },
        pccc: {
            pccc_standards: String,
            fire_alarm: String,
            fire_materials: String,
            fire_pump: Number,
            fire_pump_unit_id: String
        },
        me_system: {
            elevantor_no: Number,
            elevantor_no_unit_id: String,
            elevantor_type: String,
            backup_generator: String,
            pipe_system: String
        },
        ext_finishing: {
            wall: String,
            glass: String,
            door: String
        },
        hall: {
            scaffold: String,
            wall: String,
            ceiling: String,
            main_door: String,
            front_desk: String
        }
    },
    int_finishing: {
        living_room: {
            scaffold: String,
            wall: String,
            ceiling: String,
            main_door: String,
            main_door_lock: String,
            window: String,
            air_conditioner: String,
            shoes_cabinet: String,
            electrical_equipment: String,
            telecom_equipment: String
        },
        bedroom: {
            scaffold: String,
            wall: String,
            ceiling: String,
            main_door: String,
            main_door_lock: String,
            window: String,
            air_conditioner: String,
            wardrobe: String,
            electrical_equipment: String,
            telecom_equipment: String
        },
        bathroom: {
            scaffold: String,
            wall: String,
            ceiling: String,
            main_door: String,
            main_door_lock: String,
            window: String,
            lavabo: String,
            lavabo_surface: String,
            lavabo_cabinet_down: String,
            lavabo_cabinet_up: String,
            bathroom: String,
            bathtub: String,
            shower: String,
            toilet: String,
            accessories: String,
            electrical_equipment: String
        },
        kitchen: {
            scaffold: String,
            wall: String,
            ceiling: String,
            cabinet_kitchen_up: String,
            cabinet_kitchen_down: String,
            kitchen_surface: String,
            sink: String,
            electrical_equipment: String,
            smoke_consumer: String,
            oven: String,
            induction_bob: String
        },
        balcony: {
            scaffold: String,
            wall: String
        }
    }
},
    { collection: 'project_cons_design_info' }
);

module.exports = mongoose.model('project_cons_design_info', ProjectConsDesignInfoSchema);