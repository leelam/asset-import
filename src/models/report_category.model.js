const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var ReportCategorySchema = mongoose.Schema(
    {
        id:String,
        create_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        create_date: {
            type: Date,
            default: Date.now()
        },
        write_uid: {
            type: Schema.Types.ObjectId,
            default: null
        },
        write_date: {
            type: Date,
            default: Date.now()
        },
        company_id: {
            type: String,
            default: "ROOT"
        },
        status: { type: Boolean, default: true },
        delete_id: { type: Schema.Types.ObjectId, default: null },
        name:{type:String,required:[true,'Missing name']},
        parent_id: { type: Schema.Types.ObjectId, ref:'report_category',default: null },
        code:String,
        sequence:{type:Number,default:0},
        keyword:String,
        description:String

    },
    { collection: "report_category" }
);

ReportCategorySchema.pre("countDocuments", function() {
    this.where({ status: {$ne:false} });
});
ReportCategorySchema.pre("find", function() {
    this.where({ status: {$ne:false} });
});
module.exports = mongoose.model("report_category", ReportCategorySchema);
