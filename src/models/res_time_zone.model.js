const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResTimeZoneSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: null
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  name: { type: String, required: true },
  code: { type: String, required: true },
  gmt_offset: { type: Number },
},
  { collection: 'res_time_zone' }
);
ResTimeZoneSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_time_zone', ResTimeZoneSchema);