const mongoose = require("mongoose");
var Schema = mongoose.Schema;
var newId = new mongoose.mongo.ObjectId("5d679819325ab70ab0157ce5");
const ObjectGuideSchema = {
  create_uid: {
    type: Schema.Types.ObjectId,
    default: newId,
    ref: "res_user"
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: newId
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: "ROOT"
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },

  category_id: [
    {
      type: Schema.Types.ObjectId,
      ref: "guide_category",
    }
  ],
  name: {
    type: String,
    required: true
  },
  url_name:String,
  description: {type:String,default:''},
  answer: {
    type: String,
    required: true
  },
  sequence:{type:Number,default:0},
  keyword:{type:String,default:''},
  reaction: [
    {
      type: Schema.Types.ObjectId,
      ref: "res_react",
      default:[]
    }
  ]
}
var GuideSchema = new Schema(
  ObjectGuideSchema,
  { collection: "guide" }
);
GuideSchema.virtual("category", {
  type: "ObjectId",
  ref: "guide_category",
  localField: "category_id",
  foreignField: "_id",
  justOne: false
});
GuideSchema.pre("find", function() {
  this.where({ status: { $ne: false } });
});
GuideSchema.pre("countDocuments", function() {
  this.where({ status: { $ne: false } });
});
GuideSchema.pre("aggregate", function() {
  this.pipeline().unshift({ $match: { status: { $ne: false } } });
});
GuideSchema.set("toObject", { virtuals: true, minimize: true });
GuideSchema.set("toJSON", { virtuals: true, minimize: true });
module.exports = mongoose.model("guide", GuideSchema);
