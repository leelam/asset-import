const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ResTimeZoneSchema = mongoose.Schema({
  create_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  create_date: {
    type: Date,
    default: Date.now()
  },
  write_uid: {
    type: Schema.Types.ObjectId,
    default: null
  },
  write_date: {
    type: Date,
    default: Date.now()
  },
  company_id: {
    type: String,
    default: null
  },
  status: { type: Boolean, default: true },
  delete_id: { type: Schema.Types.ObjectId, default: null },
  name: { type: String, required: true },
  code: { type: String, required: true },
  iso_code: { type: String, required: true },
  decimal_point: { type: Number, required: true },
  date_format: String,
  time_format: String,
  thousands_sep: { type: String,default:'' },
},
  { collection: 'res_lang' }
);
ResTimeZoneSchema.pre("find", function () {
  this.where({ status: { $ne: false } });
});
module.exports = mongoose.model('res_lang', ResTimeZoneSchema);