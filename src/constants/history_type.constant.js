exports.ALL = ["INSERT","UPDATE","DELETE"];
exports.INSERT = "INSERT";
exports.UPDATE = "UPDATE";
exports.DELETE = "DELETE";
exports.REVERT = 'REVERT'