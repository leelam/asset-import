const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')
const authConfig = require('../configs/auth.config')
const publicTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/publicToken.key`), 'utf8')
const publicRoleTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/publicRoleToken.key`), 'utf8')
const {CheckTokenServer} = require('../helpers/token.helper')
const {GetGroup} = require('../controllers/auth.controller')
const checkToken = async token => {
    try{
    const result = await jwt.verify(token, publicTokenKey,{algorithms: ["RS256"]})
    return {
        status: true,
        data: result
    }
    } catch (e){
        return false
    }
}

module.exports = async(req, res, next) =>{
    const token = req.cookies.token || req.headers.authorization || req.headers.token;
    const roleToken = req.cookies.roleToken || req.headers.roletoken;
    if(token){
        const user = await checkToken(token);
        if(user && user.status){
            let role = [];
            if(roleToken){
                const dataToken = await CheckTokenServer(roleToken, publicRoleTokenKey);
                role = await GetGroup(user.data.id,dataToken.group_id,dataToken.company_id);
            }
            req.user = {
                _id: user.data.id,
                role: role[0]
            };
            return next();
        }
        return res.json({
            status: false,
            code:'TOKEN_FAIL',
            message: 'Token không hợp lệ'
        })
    }
    return res.json({
        status: false,
        message: 'Không thể tìm thấy token'
    })
}