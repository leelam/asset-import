const express = require('express')
const multer = require('multer');
const mime = require('mime')
const fs= require('fs-extra');

const dirAvatar = './resources/avatar';
const dirPhotoProduct = './resources/product';
const dirFileProject = './resources/project'

const storeageImage = multer.diskStorage({
    destination: (req, file, cb) =>{
        if(!fs.existsSync(dirAvatar))
            fs.mkdirSync(dirAvatar)
        cb(null, dirAvatar)
    },
    filename: (req, file, cb) =>{
        const { _id } = req.user || {};
        cb(null, `${_id}-${Date.now()}.${mime.getExtension(file.mimetype)}`)
    }
})
const storeagePhotoProduct = multer.diskStorage({
    destination: (req, file, cb) =>{
        const _id = req.idProduct || 'product';
        const fullDirPhoto = `${dirPhotoProduct}/${_id}`
        if(!fs.existsSync(fullDirPhoto))
            fs.mkdirSync(fullDirPhoto)
        cb(null, fullDirPhoto)
    },
    filename: (req, file, cb) =>{
        const _id = req.idProduct || 'product';
        const { fieldname, originalname } = file;
        cb(null, `${_id}-${fieldname}-${originalname}-${Date.now()}.${mime.getExtension(file.mimetype)}`)
    }
})

const storeageFileProject = multer.diskStorage({
    destination: (req, file, cb) =>{
        const _id = req.idProject || 'project';
        const fullDirFile = `${dirFileProject}/${_id}`
        if(!fs.existsSync(fullDirFile))
            fs.mkdirSync(fullDirFile)
        cb(null, fullDirFile)
    },
    filename: (req, file, cb) =>{
        const _id = req.idProject || 'project';
        const { fieldname, originalname } = file;
        cb(null, `${_id}-${fieldname}-${originalname}-${Date.now()}.${mime.getExtension(file.mimetype)}`)
    }
})

exports.uploadAvatar = multer({
    storage: storeageImage,
    limits: {
        fileSize: 1024*10*1024
    },
    fileFilter: (req, file, cb) =>{
        if(file.mimetype ==='image/jpeg' || file.mimetype === 'image/png')
            cb(null, true)
        else cb('Vui lòng không upload gì ngoài hình ảnh')
    }
}).single('avatar')

exports.uploadPhotoProduct = multer({
    storage: storeagePhotoProduct,
    limits: {
        fileSize: 1024*10*1024
    },
    fileFilter: (req, file, cb) =>{
        if(file.fieldname ==='image' || file.fieldname=== 'sub_image')
            if(file.mimetype ==='image/jpeg' || file.mimetype === 'image/png')
            cb(null, true)
            else cb('Vui lòng không upload gì ngoài hình ảnh')
        else cb(null, true)
    }
}).fields(
    [
        { name: 'image', maxCount: 1},
        { name: 'sub_image', maxCount: 8},
        { name: 'document', maxCount: 8},
        {name: "detailed_planning_1_500_link", maxCount: 1},
        {name: "investment_approval_link", maxCount: 1},
        {name: "mae_drawing_for_per_apartment_link", maxCount: 1},
        {name: "acceptance_of_fire_protection_system_link", maxCount: 1},
        {name: "acceptance_of_works_link", maxCount: 1},
        {name: "agreement_on_connection_of_electricity_and_water_works_link", maxCount: 1},
        {name: "agreement_on_traffic_connection_link", maxCount: 1},
        {name: "agreement_redbook_investor_and_buyer_link", maxCount: 1},
        {name: "approval_of_backfill_design_link", maxCount: 1},
        {name: "approval_of_basic_design_link", maxCount: 1},
         {name: "approval_of_construction_design_link", maxCount: 1},
         {name: "approval_of_engineering_design_link", maxCount: 1},
         {name: "approval_of_fire_prevention_and_fighting_link", maxCount: 1},
         {name: "approval_of_infrastructure_design_link", maxCount: 1},
         {name: "bank_guarantee_link", maxCount: 1},
         {name: "basement_foundation_acceptance_link", maxCount: 1},
         {name: "certificate_of_business_registration_link", maxCount: 1},
         {name: "certificate_of_investment_registration_link", maxCount: 1},
         {name: "certification_of_investor_link", maxCount: 1},
         {name: "confirmation_of_infrastructure_completion_link", maxCount: 1},
         {name: "construction_permit_link", maxCount: 1},
         {name: "construction_survey_report_link", maxCount: 1},
         {name: "deal_of_construction_height_link", maxCount: 1},
         {name: "deal_of_embankment_edges_link", maxCount: 1},
         {name: "decisions_on_land_allocation_land_lease_link", maxCount: 1},
         {name: "economic_technical_report_link", maxCount: 1},
         {name: "environmental_impact_assessment_report_link", maxCount: 1},
         {name: "escrow_agreement_link", maxCount: 1},
         {name: "feasibility_study_repor_link", maxCount: 1},
         {name: "finish_dossiers_link", maxCount: 1},
         {name: "home_recieving_notice_to_buyers_link", maxCount: 1},
         {name: "house_numbering_decision_link", maxCount: 1},
         {name: "location_drawing_link", maxCount: 1},
         {name: "minutes_of_handover_of_houses_or_construction_to_buyers_link", maxCount: 1},
         {name: "notice_of_approval_of_sample_contract_link", maxCount: 1},
         {name: "notice_of_eligibility_for_capital_raising_link", maxCount: 1},
         {name: "notification_of_payment_of_land_use_link", maxCount: 1},
         {name: "notification_of_the_residences_eligibility_for_sale_link", maxCount: 1},
         {name: "notifications_of_request_for_registration_fee_link", maxCount: 1},
         {name: "redbook_buyer_link", maxCount: 1},
         {name: "redbook_land_link", maxCount: 1},
         {name: "strategic_environmental_assessment_report_link", maxCount: 1},
         {name: "other_permit_link", maxCount: 1},
         {name: "social_permit_link", maxCount: 1},
         {name: "environmental_protection_plan_link", maxCount: 1},
         {name: "hotel_standard_certificate_link", maxCount: 1},
    ])

exports.uploadFileProject = multer({
        storage: storeageFileProject,
        limits: {
            fileSize: 1024*10*1024
        },
        fileFilter: (req, file, cb) =>{
            if(file.fieldname ==='image' || file.fieldname=== 'sub_image')
                if(file.mimetype ==='image/jpeg' || file.mimetype === 'image/png')
                cb(null, true)
                else cb('Vui lòng không upload gì ngoài hình ảnh')
            else cb(null, true)
        }
    }).fields(
        [
            { name: 'image', maxCount: 1},
            { name: 'sub_image', maxCount: 8},
            { name: 'documents', maxCount: 8},
            { name: 'document', maxCount: 8},
            // {name: "detailed_planning_1_500_link", maxCount: 1},
            // {name: "investment_approval_link", maxCount: 1},
            // {name: "mae_drawing_for_per_apartment_link", maxCount: 1},
            // {name: "acceptance_of_fire_protection_system_link", maxCount: 1},
            // {name: "acceptance_of_works_link", maxCount: 1},
            // {name: "agreement_on_connection_of_electricity_and_water_works_link", maxCount: 1},
            // {name: "agreement_on_traffic_connection_link", maxCount: 1},
            // {name: "agreement_redbook_investor_and_buyer_link", maxCount: 1},
            // {name: "approval_of_backfill_design_link", maxCount: 1},
            // {name: "approval_of_basic_design_link", maxCount: 1},
            //  {name: "approval_of_construction_design_link", maxCount: 1},
            //  {name: "approval_of_engineering_design_link", maxCount: 1},
            //  {name: "approval_of_fire_prevention_and_fighting_link", maxCount: 1},
            //  {name: "approval_of_infrastructure_design_link", maxCount: 1},
            //  {name: "bank_guarantee_link", maxCount: 1},
            //  {name: "basement_foundation_acceptance_link", maxCount: 1},
            //  {name: "certificate_of_business_registration_link", maxCount: 1},
            //  {name: "certificate_of_investment_registration_link", maxCount: 1},
            //  {name: "certification_of_investor_link", maxCount: 1},
            //  {name: "confirmation_of_infrastructure_completion_link", maxCount: 1},
            //  {name: "construction_permit_link", maxCount: 1},
            //  {name: "construction_survey_report_link", maxCount: 1},
            //  {name: "deal_of_construction_height_link", maxCount: 1},
            //  {name: "deal_of_embankment_edges_link", maxCount: 1},
            //  {name: "decisions_on_land_allocation_land_lease_link", maxCount: 1},
            //  {name: "economic_technical_report_link", maxCount: 1},
            //  {name: "environmental_impact_assessment_report_link", maxCount: 1},
            //  {name: "environmental_protection_plan_link", maxCount: 1},
            //  {name: "escrow_agreement_link", maxCount: 1},
            //  {name: "feasibility_study_repor_link", maxCount: 1},
            //  {name: "finish_dossiers_link", maxCount: 1},
            //  {name: "home_recieving_notice_to_buyers_link", maxCount: 1},
            //  {name: "hotel_standard_certificate_link", maxCount: 1},
            //  {name: "house_numbering_decision_link", maxCount: 1},
            //  {name: "location_drawing_link", maxCount: 1},
            //  {name: "minutes_of_handover_of_houses_or_construction_to_buyers_link", maxCount: 1},
            //  {name: "notice_of_approval_of_sample_contract_link", maxCount: 1},
            //  {name: "notice_of_eligibility_for_capital_raising_link", maxCount: 1},
            //  {name: "notification_of_payment_of_land_use_link", maxCount: 1},
            //  {name: "notification_of_the_residences_eligibility_for_sale_link", maxCount: 1},
            //  {name: "notifications_of_request_for_registration_fee_link", maxCount: 1},
            //  {name: "other_permit_link", maxCount: 1},
            //  {name: "redbook_buyer_link", maxCount: 1},
            //  {name: "redbook_land_link", maxCount: 1},
            //  {name: "social_permit_link", maxCount: 1},
            //  {name: "strategic_environmental_assessment_report_link", maxCount: 1}                                 
            {name: "detailed_planning_1_500_link", maxCount: 1},
            {name: "investment_approval_link", maxCount: 1},
            {name: "mae_drawing_for_per_apartment_link", maxCount: 1},
            {name: "acceptance_of_fire_protection_system_link", maxCount: 1},
            {name: "acceptance_of_works_link", maxCount: 1},
            {name: "agreement_on_connection_of_electricity_and_water_works_link", maxCount: 1},
            {name: "agreement_on_traffic_connection_link", maxCount: 1},
            {name: "agreement_redbook_investor_and_buyer_link", maxCount: 1},
            {name: "approval_of_backfill_design_link", maxCount: 1},
            {name: "approval_of_basic_design_link", maxCount: 1},
             {name: "approval_of_construction_design_link", maxCount: 1},
             {name: "approval_of_engineering_design_link", maxCount: 1},
             {name: "approval_of_fire_prevention_and_fighting_link", maxCount: 1},
             {name: "approval_of_infrastructure_design_link", maxCount: 1},
             {name: "bank_guarantee_link", maxCount: 1},
             {name: "basement_foundation_acceptance_link", maxCount: 1},
             {name: "certificate_of_business_registration_link", maxCount: 1},
             {name: "certificate_of_investment_registration_link", maxCount: 1},
             {name: "certification_of_investor_link", maxCount: 1},
             {name: "confirmation_of_infrastructure_completion_link", maxCount: 1},
             {name: "construction_permit_link", maxCount: 1},
             {name: "construction_survey_report_link", maxCount: 1},
             {name: "deal_of_construction_height_link", maxCount: 1},
             {name: "deal_of_embankment_edges_link", maxCount: 1},
             {name: "decisions_on_land_allocation_land_lease_link", maxCount: 1},
             {name: "economic_technical_report_link", maxCount: 1},
             {name: "environmental_impact_assessment_report_link", maxCount: 1},
             {name: "escrow_agreement_link", maxCount: 1},
             {name: "feasibility_study_repor_link", maxCount: 1},
             {name: "finish_dossiers_link", maxCount: 1},
             {name: "home_recieving_notice_to_buyers_link", maxCount: 1},
             {name: "house_numbering_decision_link", maxCount: 1},
             {name: "location_drawing_link", maxCount: 1},
             {name: "minutes_of_handover_of_houses_or_construction_to_buyers_link", maxCount: 1},
             {name: "notice_of_approval_of_sample_contract_link", maxCount: 1},
             {name: "notice_of_eligibility_for_capital_raising_link", maxCount: 1},
             {name: "notification_of_payment_of_land_use_link", maxCount: 1},
             {name: "notification_of_the_residences_eligibility_for_sale_link", maxCount: 1},
             {name: "notifications_of_request_for_registration_fee_link", maxCount: 1},
             {name: "redbook_buyer_link", maxCount: 1},
             {name: "redbook_land_link", maxCount: 1},
             {name: "strategic_environmental_assessment_report_link", maxCount: 1},
             {name: "other_permit_link", maxCount: 1},
             {name: "social_permit_link", maxCount: 1},
             {name: "environmental_protection_plan_link", maxCount: 1},
             {name: "hotel_standard_certificate_link", maxCount: 1},
        ])



