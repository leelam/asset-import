var Joi = require('@hapi/joi');

exports.SigninValidation = (data) => {
    const schema = {
        first_name: Joi.string().min(1).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng họ!";
                        break;
                    case "string.min":
                        err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        last_name: Joi.string().min(1).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng tên!";
                        break;
                    case "string.min":
                        err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        email: Joi.string().min(6).email().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "string.min":
                        err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    case "string.email":
                        err.message = "Email không phù hợp!";
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        password: Joi.string().min(6).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không được để rỗng mật khẩu!";
                        break;
                    case "string.min":
                        err.message = `Mật khẩu phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        referral_code: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Bạn phải nhập mã giới thiệu để đang ký!";
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        phone: Joi.string().min(8).max(10).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng số điện thoại!";
                        break;
                    case "string.min":
                        err.message = `Số điện thoại phải nhập tối thiểu ${err.context.limit} số!`;
                        break;
                    case "string.max":
                        err.message = `Số điện thoại phải nhập tối đa ${err.context.limit} số!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        country_code: Joi.number().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Bạn phải nhập mã vùng để đăng ký";
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
    };
    return Joi.validate(data, schema);
};

exports.LoginValidation = (data) => {
    const schema = {
        phone: Joi.string().min(10).max(10).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng số điện thoại!";
                        break;
                    case "string.min":
                        err.message = `Số điện thoại phải nhập tối thiểu ${err.context.limit} số!`;
                        break;
                    case "string.max":
                        err.message = `Số điện thoại phải nhập tối đa ${err.context.limit} số!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        password: Joi.string().min(6).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng mật khẩu!";
                        break;
                    case "string.min":
                        err.message = `Mật khẩu phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        })
    };
    return Joi.validate(data, schema);
};

exports.ChangePasswordValidate = (data) => {
    const schema = {
        newPassword: Joi.string().min(6).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng mật khẩu!";
                        break;
                    case "string.min":
                        err.message = `Mật khẩu phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        })
    }

    return Joi.validate(data, schema);
}

exports.ChangeUsernameValidate = data => {
    const schema = {
        first_name: Joi.string().min(1).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng họ!";
                        break;
                    case "string.min":
                        err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        last_name: Joi.string().min(1).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = "Không đươc để rỗng tên!";
                        break;
                    case "string.min":
                        err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        })
    }
    return Joi.validate(data, schema);
}

exports.CheckValidationEmail = email =>{
   const schema = {
    email: Joi.string().min(6).email().error(errors => {
        errors.forEach(err => {
            switch (err.type) {
                case "string.min":
                    err.message = `Phải nhập tối thiểu ${err.context.limit} ký tự!`;
                    break;
                case "string.email":
                    err.message = "Email không phù hợp!";
                    break;
                default:
                    break;
            }
        });
        return errors;
    })
   }
   return Joi.validate(email, schema);
}