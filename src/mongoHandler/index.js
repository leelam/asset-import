const util = require('util');
const exec = util.promisify(require('child_process').exec);
const path = require('path');

exports.dumpDatabase = async (collection, database) => {
  const uri = `mongodb://dev:supermasterdev@192.168.1.254:27017/${database}?authSource=admin`;
  const { stdout, stderr } = await exec(
    `mongodump --uri=${uri} -c=${collection} -o=${path.resolve('dataDump')}`
  );
  console.log('vo day');
};

exports.restoreDatabase = async (collection, database, output) => {
  const uri = `mongodb://admin:9V94cJ7BMKbeVHfZ@192.168.1.254:27017/${database}?authSource=admin`;
  const { stdout, stderr } = await exec(
    `mongorestore --uri=${uri} --db=${database} -c=${collection} "${path.resolve(
      `dataDump/asset_dev/${collection}.bson"`
    )}`
  );
  console.log(stdout);
};

exports.dropDatabase = async (database) => {};
