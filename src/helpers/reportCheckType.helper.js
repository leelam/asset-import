module.exports = (type) =>{
    return ["APPROVED","DENY","PENDING"].includes(type)
}