const axios = require('axios')
const { urlFb, accessTokenApp, googleClientId } = require('../configs/auth.config');
const jwt = require('jsonwebtoken')
exports.CheckTokenFb = async (token) =>{
    const result = await axios({
        url: `${urlFb}?input_token=${token}&access_token=${accessTokenApp}`
    })
    if(result.data){
        return {
            is_valid: result.data.data.is_valid,
            user_id: result.data.data.user_id
        }
    }
    return false;
}

exports.CheckTokenIdGoogle = async (tokenId) => {
    const result = await axios({
        url: `https://oauth2.googleapis.com/tokeninfo?id_token=${tokenId}`
    })
    if(result.data && result.data.aud === googleClientId)
    return result.data
    else return false
}
exports.CheckTokenServer = async (token, publicKey) => {
    try{
    const result = await jwt.verify(token, publicKey,{algorithms: ["RS256"]})
    return result
    } catch (e){
        return false
    }
}