const resWardModel = require('../models/res_ward.model');
const projectModel = require('../models/project.model');

const mongoose = require('mongoose');

exports.lookupByCode = async (
  model,
  value,
  fid,
  field,
  code,
  require,
  isNewObj,
  obj
) => {
  if (!value) return null;
  if (typeof value === 'string') value = value.trim();
  let result = await mongoose.model(model).findOne({ code: value });
  if (!result) result = await mongoose.model(model).findOne({ name: value });
  if (!result)
    result = await mongoose.model(model).findOne({ category_code: value });
  if (!result) {
    if (require === false) return null;
    if (isNewObj) {
      const project = await mongoose.model('project').findOne({ code });
      if (!project) {
        const newDocModel = new mongoose.model(model)(obj);
        result = await newDocModel.save();
        return result._id;
      } else {
        const checkDoc = await mongoose
          .model(model)
          .findOne({ _id: project.legal_info_id });
        if (!checkDoc || !project.legal_info_id) {
          const newDocModel = new mongoose.model(model)(obj);
          result = await newDocModel.save();
          return result._id;
        }
        Object.assign(checkDoc, obj);
        await checkDoc.save();
        return checkDoc._id;
      }
    }
    throw new Error(
      `Khong tim thay thong tin field ${field}(${value}) trong Model: ${model}. code: ${code} fid: ${fid}`
    );
  }
  return result._id;
};

exports.lookupById = async (model, value) => {
  if (typeof value === 'string') value = value.trim();
  let result = await mongoose.model(model).findOne({ _id: value });
  if (!result) result = await mongoose.model(model).findOne({ _id: value });
  if (!result) {
    throw new Error(
      `Khong tim thay thong tin field ${field}(${value}) trong Model: ${model}. code: ${code} fid: ${fid}`
    );
  }
  return result.code;
};
