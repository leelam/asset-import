const _ = require('lodash');
exports.isPoint = point => {
  if (_.isArray(point) && _.isNumber(point[0]) && _.isNumber(point[1]))
    return true;
  return false;
};
exports.isPolygon = polygon => {
  if (_.isArray(polygon) && polygon.length) {
    for (var i = 0; i < polygon.length; i++) {
      if (
        !polygon[i] ||
        polygon[i].length !== 2 ||
        !_.isNumber(polygon[i][0]) ||
        !_.isNumber(polygon[i][1])
      )
        return false;
    }
    return true;
  }
  return false;
};
