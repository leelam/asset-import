const mongoose = require("mongoose");
module.exports = filter => {
  if (!filter) {
    return [];
  }
  var filterQuery = []
  if(filter.code ) {
    filterQuery.push({
      $match: {
        code:  filter.code 
      }
    });
  }
  var filterQuery = []
  if (filter.category_id && filter.category_id.length) {
    var listCategory_id = filter.category_id.map(x =>
      mongoose.Types.ObjectId(x)
    );
    filterQuery.push({
      $match: {
        category_id: { $in: listCategory_id }
      }
    });
  }
  return filterQuery
};
