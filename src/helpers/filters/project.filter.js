const mongoose = require("mongoose");
module.exports = filter => {
  if (!filter) {
    return [];
  }
  var filterQuery = []
  if(filter.code ) {
    filterQuery.push({
      $match: {
        code:  filter.code 
      }
    });
  }
  if (filter.category_id && filter.category_id.length) {
    var listCategory_id = filter.category_id.map(x =>
      mongoose.Types.ObjectId(x)
    );
    filterQuery.push({
      $match: {
        category_id: { $in: listCategory_id }
      }
    });
  }
  if (filter.status_id && filter.status_id.length) {
    var listStatusID = filter.status_id.map(x => mongoose.Types.ObjectId(x));
    filterQuery.push({
      $match: {
        $or: [
          { "status_detail.parent_id": { $in: listStatusID } },
          { "status_detail._id": { $in: listStatusID } }
        ]
      }
    });
  }
  // filter price
  try {
    if (filter.price) {
      var { price } = filter;
      if (price.price_min && price.price_max == null) {
        filterQuery.push({
          $match: {
            a_price_value: { $gte: parseInt(price.price_min) }
          }
        });
      } else if (price.price_max && price.price_min == null) {
        filterQuery.push({
          $match: {
            a_price_value: { $lte: parseInt(price.price_max) }
          }
        });
      } else if (price.price_min && price.price_max) {
        filterQuery.push({
          $match: {
            $and: [
              { a_price_value: { $gte: parseInt(price.price_min) } },
              { a_price_value: { $lte: parseInt(price.price_max) } }
            ]
          }
        });
      } else {
      }
    }
  } catch (err) {
    console.log(err);
  }
  return filterQuery
};
