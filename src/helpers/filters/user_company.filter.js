module.exports = (filter)=>{
    if(!filter) return [];
    var filterQuery = [];
    if(filter.company_code)  filterQuery.push({$match:{  'company.code': filter.company_code }})
    if(filter.user_phone)  filterQuery.push({$match:{ 'user.phone': filter.user_phone  }})
    return filterQuery;
}