module.exports = filter => {
  var query = [];
  if (filter && filter.category_id && filter.category_id.length) {
    query.push(
      {
        $match: {
          "category_id": { $in: filter.category_id }
        }
      }
    );
  }
  return query;
};
