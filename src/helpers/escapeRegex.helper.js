module.exports = function escapeRegex(text) {
    return text.toString().replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }