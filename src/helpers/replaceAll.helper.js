module.exports = (srcString,oldString,newString) => {
    // This function cannot be optimised, it's best to
    // keep it small!
   return srcString ? srcString.split(oldString).join(newString) : '';
  };
  