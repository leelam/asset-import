module.exports = json => {
  // This function cannot be optimised, it's best to
  // keep it small!
  var parsed;

  try {
    parsed =  JSON.parse(json);
  } catch (e) {
    return undefined;
  }

  return parsed; // Could be undefined!
};
