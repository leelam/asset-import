const admin = require('firebase-admin');

const serviceAccount = require('../configs/sercetAccountGoogleKey.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://assetvn-254207.firebaseio.com"
})
const verifyIdtoken = async token => {
    try{
        
    const checkToken = await admin.auth().verifyIdToken(token);
    return true
    } catch (e){
        return false
    }
}

module.exports =  verifyIdtoken
