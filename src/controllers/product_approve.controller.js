var mongoose = require('mongoose')
var asset_approve_processModel = require('../models/asset_approve_process.model');
const productDemoModel = require('../models/product.model')
const product_sentModel = require('../models/product_sent.model')
const res_companyModel = require('../models/res_company.model')
const res_user_groupModel = require('../models/res_user_group.model')
const legal_infoModel = require('../models/legal_info.model')
const cons_design_infoModel = require('../models/product_cons_design.model')
const landlord_infoModel = require('../models/product_landlord_info.model')

var _ = require("lodash");

exports.List = async (limit, page, filter, project, sort) => {
  return await product_sentModel.find({
    status_approve: 'PENDING'
  })
}

exports.Count = async (role, filter) => {
  return await asset_approve_processModel.countDocuments({
    group_approve: role,
    status_approve: filter
  })
}

exports.Detail = async (id) => {
  return await product_sentModel.findOne({
    _id: mongoose.Types.ObjectId(id)
  })
}

exports.ListApprovedDT = async () => {
  return await product_sentModel.find({
    process_approve: "DT",
    status_approve: 'PENDING'
  }).populate('category_id')
}

exports.ListApprovedVM = async () => {
  return await product_sentModel.find({
    process_approve: "VM",
    status_approve: 'PENDING'
  }).populate('category_id')
}

exports.ListApprovedRM = async () => {
  return await product_sentModel.find({
    process_approve: "RM",
    status_approve: 'PENDING'
  }).populate('category_id')
}

exports.GetDetailProduct = async (_id, notSent) => {
  try {
    let product
    if(notSent)
      product = await FindOneProductExistedByID(_id)
    else product = await FindOneByID(_id)
    const legal_info = await getLegalInfoLink(product.legal_info_id);
    const cons_design_info = await getConsDesignInfo(product.cons_design_info_id)
    const landlordInfo = await getLandlordInfo(product.landlord_info_id)
    if (!product)
      return res.json({
        status: false,
        message: 'Can not find product'
      })
    let result = {
      general_info: {
        ...product
      },
      legal_info: {
        ...legal_info
      },
      description: {
        description: product.description
      },
      cons_design_info: {
        ...cons_design_info
      },
      landlord_info: {
        ...landlordInfo
      }
    }
    return {
      status: true,
      data: result
    }
  } catch (e) {
    console.log(e)
    return {
      status: false,
      message: 'Server error'
    }
  }
}

getConsDesignInfo = async _id => {
  
  let cons_design_info = await cons_design_infoModel.findOne({
    _id
  })
  if(!cons_design_info)
    return {}
  cons_design_info = cons_design_info.toObject();
  cons_design_info.detail = {}
  if (!cons_design_info)
    return false
  if (cons_design_info.general_info_of_cons) {
    if (cons_design_info.general_info_of_cons.company && cons_design_info.general_info_of_cons.company.contractor_id)
      cons_design_info.detail.contractor = await res_companyModel.findOne({
        _id: cons_design_info.general_info_of_cons.company.contractor_id
      }, 'display_name')
    if (cons_design_info.general_info_of_cons.company && cons_design_info.general_info_of_cons.company.designer_id)
      cons_design_info.detail.designer = await res_companyModel.findOne({
        _id: cons_design_info.general_info_of_cons.company.designer_id
      }, 'display_name')
    if (cons_design_info.general_info_of_cons.company && cons_design_info.general_info_of_cons.company.manager_id)
      cons_design_info.detail.manager = await res_companyModel.findOne({
        _id: cons_design_info.general_info_of_cons.company.manager_id
      }, 'display_name')
  }
  return cons_design_info;
}

exports.getConsDesignInfo = getConsDesignInfo
getLandlordInfo = async _id => {
  const landlordInfo = await landlord_infoModel.findOne({
    _id
  });
  if (!landlordInfo)
    return false
  return landlordInfo.toObject()
}
exports.getLandlordInfo = getLandlordInfo

const FindOneByID = async _id => {
  try {
    const result = await product_sentModel.aggregate([{
        $match: {
          _id: mongoose.Types.ObjectId(_id)
        }
      },
      {
        $lookup: {
          from: 'product_category',
          localField: 'category_id',
          foreignField: '_id',
          as: 'category'
        }
      },
      {
        $unwind:{
          path: "$category",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'project',
          localField: 'project_id',
          foreignField: '_id',
          as: 'projectDetail'
        }
      },
      {
        $lookup: {
          from: 'res_company',
          localField: 'investor_id',
          foreignField: '_id',
          as: 'investorDetail'
        }
      },
      {
        $unwind: {
          path: "$investorDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'assets_planning_category',
          localField: 'land_type',
          foreignField: '_id',
          as: 'landTypeDetail'
        }
      },
      {
        $unwind: {
          path: "$landTypeDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'product_type',
          localField: 'type_id',
          foreignField: '_id',
          as: 'typeDetail'
        }
      },
      {
        $unwind: {
          path: "$typeDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'project_status',
          localField: 'cons_status',
          foreignField: '_id',
          as: 'cons_statusDetail'
        }
      },
      {
        $unwind: {
          path: "$cons_statusDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $addFields: {
          category_name: '$category.category_name'
        }
      }
    ])
    return result[0];
  } catch (e) {
    return false;
  }
}
exports.FindOneByID = FindOneByID
const getLegalInfoLink = async _id => {
  const legal_info = await legal_infoModel.findOne({
    _id
  })
  if (!legal_info)
    return false
  let legal_infoObj = legal_info.toObject()
  const keys = Object.keys(legal_infoObj);
  let result
  keys.forEach(item => {
    if (legal_info[item]) {
      result = {
        ...result,
        [item]: '',
        [`item_link`]: ''
      }
      if (legal_info[item].status) {
        result[item] = legal_info[item].status;
        result[`${item}_link`] = legal_info[item].file;
      }
    }
  })
  return result
}
exports.getLegalInfoLink = getLegalInfoLink
const updateProduct = async (idUser, idProduct, general_info, legal_info, description, cons_design_info, landlord_info, group_id, type) => {
  try {
    const product = await product_sentModel.findOne({
      _id: idProduct
    });
    
    if (general_info) {
      const updateGeneral = await product_sentModel.findOneAndUpdate({
        _id: idProduct
      }, {
        $set: {
          ...general_info,
        }
      })
      if (!updateGeneral)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (description) {
      const updateDescription = await product_sentModel.findOneAndUpdate({
        _id: idProduct
      }, {
        description: description.description
      })
      if (!updateDescription)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (legal_info) {
      const updateLegal = await legal_infoModel.findOneAndUpdate({
        _id: product.legal_info_id
      }, {
        ...legal_info
      })
      if (!updateLegal)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }

    if (cons_design_info) {
      const updateConsDesign = await cons_design_infoModel.findOneAndUpdate({
        _id: product.cons_design_info_id
      }, {
        ...cons_design_info
      })
      if (!updateConsDesign)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (landlord_info) {
      const updateLandlord = await landlord_infoModel.findOneAndUpdate({
        _id: product.landlord_info_id
      }, {
        ...landlord_info
      })
      if (!updateLandlord)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    const approveProduct = await changeApproveStatus(group_id, idUser, idProduct, 'APPROVED', type)

    if (!approveProduct.status)
      return {
        status: false,
        message: approveProduct.message
      }
    return {
      status: true,
      data: {
        product: idProduct,
        approve: approveProduct.data
      }
    }
  } catch (e) {
    console.log(e)
    return {
      status: false,
      message: e
    }
  }
}
exports.updateProduct = updateProduct
const checkApprove = async (roleId, idProduct) => {
  const check = await asset_approve_processModel.findOne({
    rel_id: idProduct,
    group_approve: roleId,
    status_approve: 'PENDING'
  })
  if (!check)
    return false;
  return true
}
exports.checkApprove = checkApprove

const changeApproveStatus = async (group_id, idUser, idProduct, status, feedback, type) => {
  try {
    let product
    const product_process = await asset_approve_processModel.findOneAndUpdate({
      rel_id: idProduct,
      group_approve: group_id
    }, {
      status_approve: status,
      user_approved: idUser,
      status: false,
      feedback
    })
    if (!product_process)
      return {
        status: false,
        message: 'Server lỗi'
      }
    product = await product_sentModel.findOne({
      _id: idProduct
    })
    if (status === 'APPROVED') {
      if (product.process_approve === 'DT') {
        product.process_approve = 'RM';
        await product.save();
        const rmTeam = await res_user_groupModel.findOne({code: 'QLBDS'})
        if(!rmTeam){
          console.log('no RM available')
          return {
            status: false,
            message: 'Server error'
          }
        }
        const newProcess = new asset_approve_processModel({
          create_uid: idUser,
          rel_id: idProduct,
          group_approve: rmTeam._id,
          status: true,
          status_approve: 'PENDING'
        })
        let result = await newProcess.save();
        return {
          status: true,
          data: result
        }
      }
      if (product.process_approve === 'RM') {
        const productSent = await product_sentModel.findOne({
          _id: idProduct
        }, '-status_approve -process_approve');
        productSent.status_approve = 'APPROVED'
        await productSent.save();
        delete productSent._id
        if(productSent.type ==='edit'){
          const productSentObj = productSent.toObject()
          const aValueRate = productSentObj.a_value_unit_id ==='tỷ' ? productSentObj.a_value*1000 : productSentObj.a_value
          delete productSentObj._id
          const product = await productDemoModel.findOneAndUpdate({_id: productSent.oldId}, {
            $set: {
              ...productSentObj,
              a_value_rate: aValueRate,
              code: productSent.location,
              status: true,
              _id: new mongoose.Types.ObjectId()
            }
          })
          if (product){
            return {
              status: false,
              message: 'Server error'
            }
          }
          return {
            status: true
          }
        } else{
        const product = new productDemoModel({
          ...productSent.toObject(),
          code: productSent.location,
          status: true,
        });
        result = await product.save();
        return {
          status: true,
          data: result
        }
      }
      }
    } else {
      product.status_approve = 'REJECTED'
      await product.save();
      return {
        status: true,
        message: 'Từ chối bất động sản thành công'
      }
    }
  } catch (e) {
    console.log(e)
    return {
      status: false,
      message: e
    }
  }
}
exports.changeApproveStatus = changeApproveStatus
const FindOneProductExistedByID = async _id => {
  try {
    const result = await productDemoModel.aggregate([{
        $match: {
          _id: mongoose.Types.ObjectId(_id)
        }
      },
      {
        $lookup: {
          from: 'product_category',
          localField: 'category_id',
          foreignField: '_id',
          as: 'category'
        }
      },
      {
        $unwind:{
          path: "$category",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'project',
          localField: 'project_id',
          foreignField: '_id',
          as: 'projectDetail'
        }
      },
      {
        $lookup: {
          from: 'res_company',
          localField: 'investor_id',
          foreignField: '_id',
          as: 'investorDetail'
        }
      },
      {
        $unwind: {
          path: "$investorDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'assets_planning_category',
          localField: 'land_type',
          foreignField: '_id',
          as: 'landTypeDetail'
        }
      },
      {
        $unwind: {
          path: "$landTypeDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'product_type',
          localField: 'type_id',
          foreignField: '_id',
          as: 'typeDetail'
        }
      },
      {
        $unwind: {
          path: "$typeDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'project_status',
          localField: 'cons_status',
          foreignField: '_id',
          as: 'cons_statusDetail'
        }
      },
      {
        $unwind: {
          path: "$cons_statusDetail",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $addFields: {
          category_name: '$category.category_name'
        }
      }
    ])
    return result[0];
  } catch (e) {
    return false;
  }
}
exports.FindOneProductExistedByID = FindOneProductExistedByID

const updateProductExisted = async (idUser, idProduct, general_info, legal_info, description, cons_design_info, landlord_info, group_id, type) => {
  try {
    const product = await productDemoModel.findOne({
      _id: idProduct
    });
    if (general_info) {
      const updateGeneral = await productDemoModel.findOneAndUpdate({
        _id: idProduct
      }, {
        $set: {
          ...general_info,
        }
      })
      if (!updateGeneral)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (description) {
      const updateDescription = await productDemoModel.findOneAndUpdate({
        _id: idProduct
      }, {
        description: description.description
      })
      if (!updateDescription)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (legal_info) {
      const updateLegal = await legal_infoModel.findOneAndUpdate({
        _id: product.legal_info_id
      }, {
        ...legal_info
      })
      if (!updateLegal)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }

    if (cons_design_info) {
      const updateConsDesign = await cons_design_infoModel.findOneAndUpdate({
        _id: product.cons_design_info_id
      }, {
        ...cons_design_info
      })
      if (!updateConsDesign)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    if (landlord_info) {
      const updateLandlord = await landlord_infoModel.findOneAndUpdate({
        _id: product.landlord_info_id
      }, {
        ...landlord_info
      })
      if (!updateLandlord)
        return {
          status: false,
          message: 'Lỗi trong quá trình update'
        }
    }
    return {
      status: true,
    }
  } catch (e) {
    console.log(e)
    return {
      status: false,
      message: e
    }
  }
}
exports.updateProductExisted = updateProductExisted