var postModel = require("../models/post.model");
var mongoose = require("mongoose");
var searchModel = require('../models/res_search_string.model')
var history = require('./history.controller')
var historyModel = require('../models/post_history.model')
var moment = require('moment')
const historyType = require('../constants/history_type.constant')
var _ = require('lodash');
var replaceAll = require('../helpers/replaceAll.helper')
exports.List = async (limit = 10, page = 1, filter) => {
  return await postModel.find({})
    .sort({ publish_date: -1 })
    .skip((page - 1) * limit)
    .limit(limit)
    .populate({ path: 'category', select: 'name', match: { status: { $ne: false } } })
    .populate({ path: 'create_uid', select: 'first_name last_name' })
    .lean();
}
exports.Count = async filter => {
  return await postModel.countDocuments();
};

exports.Detail = async (id) => {
  var post = await postModel.findOne({ _id: id }).populate({ path: 'category', select: 'name', match: { status: { $ne: false } } })
    .populate({ path: 'create_uid', select: 'first_name last_name' })
  return post
}

exports.Add = async data => {
  var post = new postModel(data);
  post.create_date = Date.now();
  post.publish_date = (!data.publish && data.publish_date) ? data.publish_date : Date.now();
  var validate = post.validateSync();
  var result = {};
  if (!validate) {
    await post
      .save().then(async document => {
        if (data.publish) {
          var search = new searchModel(
            {
              name: 'post',
              rel_id: post._id,
              title: post.name,
              search: `${post.name} ${func(post.name)} ${post.description} ${func(post.description)} ${post.keyword} ${func(post.keyword)}`,
              rating: 1
            }
          )
          await search.save()
          await history.AddPostHistory(post._id, post.create_uid, {}, post, historyType.INSERT)
        }
        result = { status: true, data: document };
      })
      .catch(err => {
        result = { status: false, message: err.toString() };
      });
  } else {
    result = { status: false, message: validate }
  }
  return result;
};

exports.Delete = async (id) => {
  var result;
  await postModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(id) },
      { $set: { status: false } }
    )
    .then(async (doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
        await searchModel.deleteMany({ rel_id: mongoose.Types.ObjectId(id) })
      } else

        result = {
          status: false
        };
    });
  return result;
}

exports.Edit = async (newPost = {}) => {
  if (!newPost._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  newPost.write_date = Date.now();
  var result;
  await postModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newPost._id) },
      { $set: _.omit(newPost, ["_id"]) }
    )
    .then(async doc => {
      if (!doc) {
        result = {
          status: false,
        };
      } else
        result = {
          status: true
        };
      await searchModel.findOneAndUpdate(
        { rel_id: mongoose.Types.ObjectId(newPost._id) },
        {
          title: newPost.name,
          search: `${newPost.name} ${func(newPost.name)} ${newPost.description} ${func(newPost.description)} ${newPost.keyword} ${func(newPost.keyword)}`
        })
      let response = await history.AddPostHistory(newPost._id, newPost.write_uid, doc, newPost, historyType.UPDATE, 'post');
    });
  return result;
};






const path = require("path");
const fs = require("fs");
var func = require('../helpers/toUnicode.helper')

exports.Test = async () => {
  await postModel.find({}, async function (err, post) {
    for (var i = 0; i < post.length; i++) {
      var search = new searchModel(
        {
          name: 'post',
          rel_id: post[i]._id,
          title: post[i].name,
          search: `${post[i].name} ${func(post[i].name)} ${post[i].description} ${func(post[i].description)} `,
          rating: 1
        }
      )
      await search.save()
    }
    // const directoryPath = path.join(
    //   __dirname,
    //   `../resources/images/post`
    // );
    //var ListItem = fs.readdirSync(directoryPath);
    // var list = ListItem.map(x=>path.parse(x).name)
    // console.log(list)
    // for(var i = 0;i<post.length;i++) {
    //   let content = post[i].content;
    //   // for (var j = 0;j<list.length;j++) {
    //   //   content = replaceAll(content,`document?id=${list[j]}`,`http://localhost:7777/images/post/${list[j]}.jpg`)
    //   // }
    //   content = replaceAll(content,`http://localhost:7777`,`http://27.74.250.96:8386`)
    //   var item = {content:content}

    //   await postModel.findOneAndUpdate({_id:mongoose.Types.ObjectId(post[i]._id) },{$set:item})
    // }

  })
}
exports.RevertHistory = async (ID, userID) => {
  let historyItem = await historyModel.findOne({ _id: mongoose.Types.ObjectId(ID) })
  let result = {};
  let nowPost = await postModel.findOne({ _id: mongoose.Types.ObjectId(historyItem.rel_id) })
  let maxVersion = await historyModel.findOne({rel_id: mongoose.Types.ObjectId(historyItem.rel_id)}).sort({version: -1})

  for (let i = maxVersion.version; i >= historyItem.version; i--) {
    let historyStep = await historyModel.findOne({ rel_id: mongoose.Types.ObjectId(historyItem.rel_id), version: i })
    await postModel.findOneAndUpdate({ _id: historyStep.rel_id }, { $set: historyStep.old_version })
      .then(async doc => {
        if (doc) {
          result = {
            status: true
          }
        }
      }).catch(err => { result = { status: false, message: err.toString() } })
  }
  if (result.status) {
    let updateItem = await postModel.findOne({ _id: mongoose.Types.ObjectId(historyItem.rel_id) })
    await history.AddPostHistory(nowPost._id, userID, nowPost, updateItem, historyType.REVERT, 'post', true);
  }


  return result
}