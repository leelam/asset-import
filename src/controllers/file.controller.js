const path = require("path");
const fs = require("fs");
var directionConfig = require("../configs/direction.config");
//joining path of directory
exports.ListImages = async (direction = "") => {
  try {
    const directoryPath = path.join(
      __dirname,
      `../../${directionConfig.RESOURCE}/${direction}`
    );
    const stats = fs.statSync(directoryPath)
    var ListItem = fs.readdirSync(directoryPath);
    return ListItem.map(x => `${direction}/${x}`);
  } catch (err) {
    console.log(err)
    return [];
  }
};