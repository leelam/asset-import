var recruitModel = require("../models/recruit.model");
var mongoose = require("mongoose");
var _ = require('lodash');
var replaceAll = require('../helpers/replaceAll.helper')
exports.List = async(limit = 10,page = 1,filter)=>{
  return await recruitModel.find({}).populate({ path: 'category', select: 'name' })
  .sort({create_date:-1})
  .skip((page-1)*limit)
  .limit(limit)
  .lean();
}
exports.Count = async filter => {
  return await recruitModel.countDocuments();
};

exports.Detail = async (id)=>{
  var post= await recruitModel.findOne({_id:id}).populate({ path: 'category', select: 'name' })
  .populate({ path: 'create_uid', select: 'first_name last_name' })
  return post
}

exports.Add = async data => {
  var post = new recruitModel(data);
  post.create_date = Date.now();
  var validate = post.validateSync();
  var result = {};
  if (!validate) {
    await post
      .save().then(document => {
        result = { status: true, data: document };
      })
      .catch(err => {
        result = { status: false, message: err.toString() };
      });
  }else {
      result = {status:false,message:validate.toString()}
  }
  return result;
};

exports.Delete = async(id)=>{
    var result;
    await recruitModel
      .findOneAndUpdate(
        { _id: mongoose.Types.ObjectId(ID) },
        { $set: { status: false } }
      )
      .then((err, doc) => {
        if (err) {
          result = {
            status: false,
            error: err
          };
        }
        result = {
          status: true
        };
      });
    return result;
}

exports.Delete = async ID => {
  var result;
  await recruitModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then((err, doc) => {
      if (err) {
        result = {
          status: false,
          error: err
        };
      }
      result = {
        status: true
      };
    });
  return result;
};


exports.Edit = async (newPost = {}) => {
  if (!newPost._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  newPost.write_date =Date.now();
  var result;
  await recruitModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newPost._id) },
      { $set: _.omit(newPost, ["_id"]) }
    )
    .then((err, doc) => {
      if (err) {
        result = {
          status: false,
          error: err
        };
      }
      result = {
        status: true
      };
    });
  return result;
};






const path = require("path");
const fs = require("fs");
exports.Test =async ()=>{
  await postModel.find({},async function (err,post){
    const directoryPath = path.join(
      __dirname,
      `../resources/images/post`
    );
    var ListItem = fs.readdirSync(directoryPath);
    var list = ListItem.map(x=>path.parse(x).name)
    for(var i = 0;i<post.length;i++) {
      let content = post[i].content;
      for (var j = 0;j<list.length;j++) {
        content = replaceAll(content,`document?id=${list[j]}`,`http://localhost:7777/images/post/${list[j]}.jpg`)
      }

      var item = {content:content}
  
      await postModel.findOneAndUpdate({_id:mongoose.Types.ObjectId(post[i]._id) },{$set:item})
    }
     
    })
}