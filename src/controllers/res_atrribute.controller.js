var resAttributeModel = require('../models/res_attribute.model')
exports.List = async()=>{
    return await resAttributeModel.find()
}
exports.Add = async()=>{
    var attribute = new resAttributeModel({
        name:'Diện tích',
        code:'area'
    })
    await attribute.save();
    return attribute
}