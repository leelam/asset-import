var guideCategoryModel = require("../models/guide_category.model");
var _ = require("lodash");
var mongoose = require("mongoose");
exports.List = async (limit, page = 1, filter) => {
  return await guideCategoryModel
    .find({})
    .skip((page - 1) * limit)
    .limit(limit)
    .populate("parent_id");
};

exports.ListTree = async () => {
  return await guideCategoryModel.aggregate([
    {
      $match: {
        parent_id: null
      }
    },
    {
      $lookup: {
        from: "guide_category",
        "let": { "bId": "$_id" },
        "pipeline": [{
          "$match": {
              $expr: { $eq: [ "$parent_id", "$$bId" ] },
              status:{$ne:false}
          }
      }],
        as: "children"
      }
    }
  ]);
};

exports.Count = async filter => {
  return await guideCategoryModel.countDocuments();
};
exports.Detail = async ID => {
  return await guideCategoryModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(ID)
      }
    },
    {
      $lookup: {
        from: "guide_category",
        "let": { "bId": "$_id" },
        "pipeline": [{
          "$match": {
              $expr: { $eq: [ "$parent_id", "$$bId" ] },
              status:{$ne:false}
          }
      }],
        as: "children"
      }
    },
    {
      $lookup: {
        from: "guide_category",
        localField: "parent_id",
        foreignField: "_id",
        as: "parent"
      }
    },
    {
      $unwind: {
        path: "$parent",
        preserveNullAndEmptyArrays: true
      }
    }
  ]);
};

exports.Add = async data => {
  var category = new guideCategoryModel(data);
  var validate = category.validateSync();
  var result = {};
  if (!validate) {
    await category
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await guideCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      }else
      result = {
        status: false
      };
    }).catch(err=>{
        result= {
            status: false,
            message:err.toString()
        }
    });
  return result;
};
exports.Edit = async (newCategory = {}) => {
  if (!newCategory._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await guideCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newCategory._id) },
      { $set: _.omit(newCategory, ["id"]) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      }
      else result = {
        status: false
      };
    });
  return result;
};
