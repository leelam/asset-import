const _ = require("lodash");
var mongoose = require("mongoose");
const companyModel = require("../models/res_company.model");
const partnerModel = require("../models/res_partner.model");
const partnerCategoryModel = require('../models/res_partner_category.model')
var companyFilter = require("../helpers/filters/company.filter");
var geometry = require('../helpers/geometry.helper')
const PARTNER_CODE = require('../constants/partner_code.constant')
const pickPartnerAttr = ["code","company_name","phone","mobile","fax","email","website","vat","address","commercial_name"];
const pickCompanyAttr = ["code","company_name","category_id","icon","logo","sequence","points","banner","working_area","working_area_collection_name"];

exports.ListSugesstion =async (keyword, code) =>{
    if(!_.isString(keyword)) return {
        status: false,
        message: 'Từ khóa không đúng'
    };
    const queryCode = code ? [{$match:{'category.code': code}}] : []
    const result =  await companyModel.aggregate([
        {
            $match: {
                search: {
                    $regex: keyword,
                    $options: 'i'
                },
            }
        },
        {
            $lookup: {
                from: 'res_company_category',
                localField: 'category_id',
                foreignField: '_id',
                as: 'category'
            }
        },
        ...queryCode,
        {
            $limit: 10
        },{
            $project: {
                display_name: 1
            }
        }
    ])
    return {
        status: true,
        data: result
    }
}
exports.List = async (limit, page, filter, project, sort) => {
    var customQuery = [];
    var queryFilter = companyFilter(filter);
    var customLookup = [
      {
        $lookup: {
          from: "res_company_category",
          localField: "category_id",
          foreignField: "_id",
          as: "category"
        }
      },
      {
        $unwind: {
          path: "$category",
          preserveNullAndEmptyArrays: true
        }
      }
    ];
    var textQuery = [];
    if (filter.keyword) {
      textQuery.push({
        $match: {
            company_name: { $regex: filter.keyword, $options: 'i'}
        }
      });
    
    }
    queryFilter.push({
      $match: {
        status: { $ne: false }
      }
    })
    if (project) customQuery.push({ $project: project });
    if (sort) customQuery.push({ "$sort": sort })
    customQuery.push({ $skip: (page - 1) * limit });
    customQuery.push({ $limit: limit });
    return await companyModel.aggregate([
      ...textQuery,
      ...queryFilter,
      ...customLookup,
      ...customQuery
    ]);
  };
exports.Add = async data => {
    var partnerCategory =  await partnerCategoryModel.findOne({code:PARTNER_CODE.COMPANY})
    if(partnerCategory) {
         
     
        var partner = new partnerModel({..._.pick(data,pickPartnerAttr),name:data.company_name});
        partner.category_id = partnerCategory._id;
        var company = new companyModel(_.pick(data,pickCompanyAttr));
        company.points = geometry.isPoint(company.points) ?{type:'Point',coordinates:company.points}  : null;
        company.partner_id = partner._id;
        var result = {};
        var validatePartner = partner.validateSync();
        var validateCompany = company.validateSync();

        if(!validatePartner &&  !validateCompany) {
            await partner.save().then(async (docPartner)=>{
                if(!docPartner) {
                    result =  {
                        status:false,
                        errorCode:003,
                        message:"Error when saving partner"
                    }
                }else {
                    await company.save().then(async(docCompany)=>{
                        if(!docCompany) {
                             result = await {
                                status:false,
                                errorCode:003,
                                message:"Error when saving company"
                            }
                        }else {
                            result = await {
                                status:true,
                                company:docCompany,
                                partner:docPartner
                            }
                        }
                    }).catch(async err=>{
                        result = await {
                            status:false,
                            message:err.toString()
                        }
                    });
                }
            }).catch(async err =>{
                        
                result =await  {
                    status:false,
                    message:err.toString()
                }
            });
           return await result;
            
        } else {
            return {
                status:false,
                errorCode:002,
                message:validatePartner ? validatePartner.toString(): "" +" - "+ validateCompany ?validateCompany.toString(): ""
            }
        }
    }
    else {
        return {
            status:false,
            errorCode:001,
            message:'Missing partner category code [COMPANY]'
        }
    }
};

exports.Edit = async  (data)=>{
    data = data || {};
    companyID = data._id;
    var result = {};
    if(!mongoose.Types.ObjectId.isValid(data._id)) return {status:false,message:'_id is not valid'}
    await companyModel.findOneAndUpdate({_id:mongoose.Types.ObjectId(data._id)}, _.pick(data,pickCompanyAttr)).then( async company=>{
        if(company){
            await partnerModel.updateOne({_id:company.partner_id},_.pick(data,pickPartnerAttr),{upsert:true}).then(async partner=>{
                result = {
                    status:true
                }
            })
        }else{
            result = {status:false,'message':"Not found company"}
        }
    }).catch(errr=>result={status:false,message:err.toString()})
    return result;
}

exports.Count = async (filter) => {
    var queryFilter = companyFilter(filter);
    var textQuery = []
    if (filter && filter.keyword) {
      textQuery.push({
        $match: {
            company_name: { $regex: filter.keyword, $options: 'i'}
        }
      });
    }
    return await companyModel.aggregate([
      ...textQuery,
      ...queryFilter,
      { $count: "total" }
    ])
  }
exports.Detail = async id =>{
    if(!mongoose.Types.ObjectId.isValid(id)) return {}
    return await companyModel.findOne({_id:mongoose.Types.ObjectId(id)})
}