var productTypeModel = require('../models/product_type.model');
var mongoose = require('mongoose');
var _ = require('lodash');

exports.List = async (limit, page = 1, filter) => {
    return await productTypeModel.find({}).sort({sequence:1}).skip((page - 1) * limit).limit(limit)
}

exports.Count = async () => {
    return await productTypeModel.countDocuments({})
}
exports.Add = async (data) => {
    var type = {
        _id: mongoose.Types.ObjectId(),
        type_name: data.type_name,
        type_code: data.type_code,
        marker_url: data.marker_url,
        marker_hover_url: data.marker_hover_url,
        color_code: data.color_code
    }
    var productType = new productTypeModel(type)
    var validate = productType.validateSync()
    var result = {status: false}
    if(!validate){
        await productType.save().then(document =>{
            result = {
                status: true,
                data: document
            }
        }).catch(err =>{
            result = {
                status: false,
                data: err.toString()
            }
        })
    }else 
    result = {status: false, data: validate.toString()}

    return result
}

exports.Detail = async (ID) =>{
    return await productTypeModel.findOne({_id:ID})
}

exports.Edit = async ( newType = {}) =>{
    if (!newType._id) {
        return {
          status: false,
          error: 'ID is null'
        }
      }
      var result;
      await productTypeModel.findOneAndUpdate(
        { _id: newType._id },
        { $set: _.omit(newType, ["_id"]) }
      ).then((doc) => {
        if (doc) {
          result = {
            status: true,
            data: doc
          }
        } else
          result = { status: false }
      })
      return result
}

exports.Delete = async (id) =>{
    if (id) {
        var result;
        await productTypeModel.findOneAndUpdate(
          { _id: id },
          { $set: { status: false } }
        ).then(async doc => {
          if (doc) {
            result = {
              status: true,
              data: doc
            }
          } else
            result = { status: false }
        })
      } else
        result = { status: false };
    
      return result
}