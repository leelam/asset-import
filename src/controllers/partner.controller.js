var partnerModel = require('../models/res_partner.model');
exports.List = async (limit,page,filter,sort,project)=>{
    var sort = sort || {sequence : -1};
    return partnerModel.find({}).skip((page-1)*limit).limit(limit).sort(sort);
}