var projectStatusModel = require('../models/project_status.model')
var mongoose = require("mongoose");
var _ = require("lodash");
exports.List = async (limit, page = 1, filter) => {
  return await projectStatusModel
    .find({})
    .sort({sequence:1})
    .skip((page - 1) * limit)
    .limit(limit)
   
    .populate("parent_id");
};

exports.ListTree = async () => {
  return await projectStatusModel.aggregate([
    {
      $match: {
        parent_id: null
      }
    },
    {
      $lookup: {
        from: "project_status",
        localField: "_id",
        foreignField: "parent_id",
        as: "children"
      }
    }
  ]);
};

exports.Count = async filter => {
  return await projectStatusModel.countDocuments();
};
exports.Detail = async ID => {
  return await projectStatusModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(ID)
      }
    },
    {
      $lookup: {
        from: "project_status",
        localField: "_id",
        foreignField: "parent_id",
        as: "children"
      }
    },
    {
      $lookup: {
        from: "project_status",
        localField: "parent_id",
        foreignField: "_id",
        as: "parent"
      }
    },
    {
      $unwind: {
        path: "$parent",
        preserveNullAndEmptyArrays: true
      }
    }
  ]);
};

exports.Add = async data => {
  var category = new projectStatusModel(data);
  var validate = category.validateSync();
  var result = {};
  if (!validate) {
    await category
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate.toString()
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await projectStatusModel
    .deleteOne(
      { _id: mongoose.Types.ObjectId(ID) }
    )
    .then(doc=>{
      result = {
        status:doc.deletedCount !== 0
      }
    })
  return result;
};
exports.Edit = async (data = {}) => {
  if (!mongoose.Types.ObjectId.isValid(data._id)) {
    return {
      status: false,
      error: "ID is not valid"
    };
  }
  var result;
  await projectStatusModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(data._id) },
      { $set: _.omit(data, ["_id"]) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          error: doc
        };
      }else
      result = {
        status: false
      };
    });
  return result;
};
