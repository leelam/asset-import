var notificationCategoryModel = require("../models/notification_category.model");
var notificationModel = require("../models/notification.model");
var mongoose = require("mongoose");
var _ = require("lodash");
var mongoose = require("mongoose");
exports.List = async (limit, page, filter, receiver) => {
  if (!mongoose.Types.ObjectId.isValid(receiver)) return [];
  return await notificationModel
    .find({  receiver: mongoose.Types.ObjectId(receiver) })
    .sort({is_read:1, create_date: -1 })
    .limit(limit)
    .skip(limit * (page - 1))
    .populate("create_user");
};
exports.ReadAll = async receiver => {
  if (!mongoose.Types.ObjectId.isValid(receiver))
    return { status: false, message: "receiver id is not valid" };
  var response = await notificationModel.updateMany(
    { receiver: mongoose.Types.ObjectId(receiver) },
    { $set: { is_read: true } }
  );
  return { status: true };
};
exports.CountUnread = async receiver => {
  if (!mongoose.Types.ObjectId.isValid(receiver)) return 0;
  var response = await notificationModel.countDocuments({
    receiver: mongoose.Types.ObjectId(receiver),
    is_read: false
  });
  if(response) return {status:true,data:response}
  return {status:false};
};
exports.Add = async data => {
  var noti = new notificationModel(data);
  var validate = noti.validateSync();
  var result = {};
  if (!validate) {
    await noti
      .save()
      .then(async document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          message: err.toString()
        };
      });
  } else {
    result = {
      status: false,
      message: validate.toString()
    };
  }
  return result;
};
exports.ListCategory = async (limit, page = 1, filter) => {
  return await notificationCategoryModel
    .find({})
    .skip((page - 1) * limit)
    .limit(limit)
    .populate("parent_id");
};

exports.CountCategory = async filter => {
  return await notificationCategoryModel.countDocuments();
};
exports.DetailCategory = async ID => {
  return await notificationCategoryModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(ID)
      }
    },
    {
      $lookup: {
        from: "notification_category",
        let: { bId: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: { $eq: ["$parent_id", "$$bId"] },
              status: { $ne: false }
            }
          }
        ],
        as: "children"
      }
    },
    {
      $lookup: {
        from: "notification_category",
        localField: "parent_id",
        foreignField: "_id",
        as: "parent"
      }
    },
    {
      $unwind: {
        path: "$parent",
        preserveNullAndEmptyArrays: true
      }
    }
  ]);
};

exports.AddCategory = async data => {
  var category = new notificationCategoryModel(data);
  var validate = category.validateSync();
  var result = {};
  if (!validate) {
    await category
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate
    };
  }
  return result;
};
exports.DeleteCategory = async ID => {
  var result;
  await notificationCategoryModel
    .deleteOne({ _id: mongoose.Types.ObjectId(ID) })
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    });
  return result;
};
exports.EditCategory = async (newCategory = {}) => {
  if (!newCategory._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await notificationCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newCategory._id) },
      { $set: _.omit(newCategory, ["id"]) }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    });
  return result;
};
