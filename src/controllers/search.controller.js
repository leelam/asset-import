const searchModel = require('../models/res_search_string.model')
const keywordModel = require('../models/res_keyword.model')
const _ = require('lodash')
const mongoose = require('mongoose')
const escapeRegex = require('../helpers/escapeRegex.helper')
const {uniqBy} = require('lodash')
exports.Add = async (data)=>{
    var search = new searchModel(data);
    if(search.validateSync()) return {status:false, message:search.validateSync().toString()}
    var result = {};
    search.save().then(document=>{
        if(document) result = {status:true};
        else return {status:false}
    }).catch(err=>result = {status:false,message:err.toString()})
    return result;
}

exports.Delete = async(rel_id)=>{
    
    if(mongoose.Types.ObjectId.isValid(rel_id)) {
        return await searchModel.deleteOne({rel_id});
    }else {
        return {status:false, message:'rel_id is not valid'}
    }
}
exports.CloudTag = async ()=>{
    return await keywordModel.aggregate([
        {"$group" : {_id:"$name", count:{$sum:1}}},
    ])
}
exports.Edit = async (data)=>{
    if(!_.isObject(data)) return {status:false,message:'input is not a object'}
    if(!mongoose.Types.ObjectId.isValid(data.rel_id)) return {status:false, message:'rel_id is not valid'}
    return await searchModel.findOneAndUpdate({rel_id:mongoose.Types.ObjectId(data.rel_id)},data,{upsert:true});
}

exports.find = async (keyword, limit, type) => {
    if (keyword[0] === '@')
      type = 'product'
    const regex = new RegExp(escapeRegex(keyword), 'gi')
    if(!type){
      let check = await searchModel.find({ $text: {$search:keyword}},{score: {$meta: "textScore"}}).sort({score:{$meta:"textScore"}}).limit(limit)
      if(check.length === limit)
        return check
      else {
        const result = await searchModel.find({search: regex, name: {$nin: ['res_address','product']}}).limit(limit-check.length)
        check.push(...result)
        check = uniqBy(check,'rating')
        return check;
      }
    } else {
      let check = await searchModel.find({ $text: {$search:keyword}, name: type},{score: {$meta: "textScore"}}).sort({score:{$meta:"textScore"}}).limit(limit)
      if(check.length === limit)
        return check
      else {
        const result = await searchModel.find({search: regex, name: type}).limit(limit-check.length)
        check.push(...result)
        check = uniqBy(check,'rating')
        console.log(check)
        return check;
      } 
    }
  };
  