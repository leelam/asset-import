var planningModel = require("../models/asset_planning.model");
var filterHelper = require("../helpers/filters/planning.filter");
exports.Count = async (filter = []) => {
  filter = filterHelper(filter);
  return await planningModel.aggregate([...filter, { $count: "total" }]);
};

exports.List = async ( limit,page, filter) => {
  filter = filterHelper(filter);
  return await planningModel.aggregate([
    ...filter,
    { $skip: (page - 1) * limit },
    { $limit: limit }
  ]);
};

exports.Detail = async (ID)=>{
  return await planningModel.aggregate([
    {
      $match:{
        '_id':ID
      }
    },
    {
      $lookup:{
        from:'assets_planning_category',
        localField:'category_id',
        foreignField:'_id',
        as:'category'
      }
    },
    {$unwind:'$category'}
  ])
}