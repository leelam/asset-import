var askModel = require("../models/ask.model");
var checkTypeAsk = require("../helpers/askCheckType.helper");
var searchController = require("./search.controller");
var toUnicode = require("../helpers/toUnicode.helper");
var mongoose = require("mongoose");
var _ = require("lodash");
var history = require('./history.controller')
const historyType = require('../constants/history_type.constant')
exports.List = async (limit, page, filter, sort, type) => {
  if (!checkTypeAsk(type)) return [];
  return await askModel
    .find({ approve: type })
    .skip((page - 1) * limit)
    .limit(limit)
    .populate({
      path: "category",
      select: "name",
      match: { status: { $ne: false } }
    });
};
exports.Count = async (filter, type) => {
  if (!checkTypeAsk(type)) return 0;
  return await askModel.countDocuments({ approve: type });
};
exports.Detail = async ID => {
  var ask = await askModel
    .findOne({ _id: ID })
    .populate({ path: "category", select: "name" })
    .populate({ path: "create_uid", select: "first_name last_name" });
  return ask;
};

exports.Add = async data => {
  var ask = new askModel(data);
  var validate = ask.validateSync();
  var result = {};
  if (!validate) {
    await ask
      .save()
      .then(async document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          message: err.toString()
        };
      });
  } else {
    result = {
      status: false,
      message: validate.toString()
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await askModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then(async doc => {
      if (doc) {
        var searchResult = await searchController.Delete(
          mongoose.Types.ObjectId(ID)
        );
        result = {
          status: true,
          data: searchResult
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};
exports.Edit = async (newask = {}, userEditID) => {
  if (!newask._id || !mongoose.Types.ObjectId.isValid(newask._id)) {
    return {
      status: false,
      error: "_id is not valid"
    };
  }
  var result;
  await askModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newask._id) },
      { $set: _.omit(newask, ["id"]) },
      { runValidators: true }
    )
    .then(async doc => {
      if (doc) {
        await history.AddPostHistory(newask._id,userEditID,doc,newask,historyType.UPDATE, 'ask');
        var search = {
          rel_id: mongoose.Types.ObjectId(doc._id),
          name: "ask",
          title: newask.name || doc.name,
          search: `${newask.name || doc.name} ${
            newask.name ? toUnicode(newask.name) : toUnicode(doc.name)
          }`
        };
        if (newask.approve === "APPROVED") await searchController.Edit(search);
        else await searchController.Delete(mongoose.Types.ObjectId(newask._id));
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};
