var planningCategoryModel = require("../models/asset_planning_category.model");
exports.List = async (limit, page = 1, filter) => {
  return await planningCategoryModel.aggregate([
    
    { $skip: (page - 1) * limit },
    { $limit: limit },
   
  ]);
};
exports.Count = async (filter)=>{
    return await planningCategoryModel.countDocuments({});
}

exports.Detail = async (ID)=>{
  return await planningCategoryModel.find({_id:ID}).limit(1);
}
