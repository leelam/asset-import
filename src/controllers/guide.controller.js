var guideModel = require("../models/guide.model");
var notificationController = require('./notification.controller')
var _ = require("lodash");
var mongoose = require("mongoose");
exports.List = async (limit, page = 1,filter,sort) => {
  return await guideModel
    .find({})
    .skip((page - 1) * limit)
    .limit(limit)
    .populate("category");
};

exports.Count = async filter => {
  return await guideModel.countDocuments();
};
exports.Detail = async ID => {
    var guide= await guideModel.findOne({_id:ID}).populate({ path: 'category', select: 'name' })
    .populate({ path: 'create_uid', select: 'first_name last_name' })
    return guide
};

exports.Add = async data => {
  var guide = new guideModel(data);
  var validate = guide.validateSync();
  var result = {};
  if (!validate) {
    await guide
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await guideModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      }else
      result = {
        status: false
      };
    }).catch(err=>{
        result= {
            status: false,
            message:err.toString()
        }
    });
  return result;
};
exports.Edit = async (newGuide = {}) => {
  if (!newGuide._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await guideModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newGuide._id) },
      { $set: _.omit(newGuide, ["id"]) }
    )
    .then(async doc => {
      if (doc) {
        if(doc.create_uid !== newGuide.write_uid) {
          var notiData = {
            url: `guide?view=${newGuide._id}`,
            content:`đã chỉnh sửa bài hướng dẫn <strong>${newGuide.name}</strong>`,
            receiver:mongoose.Types.ObjectId(doc.create_uid),
            create_date:Date.now(),
            create_uid:mongoose.Types.ObjectId(newGuide.write_uid)
          }
          var notiResponse = await notificationController.Add(notiData);
        }
        
        result = {
          status: true,
          data: doc
        };
        if(notiResponse && notiResponse.status) result.noti = notiData;
      }
      else result = {
        status: false
      };
    });
  return result;
};
