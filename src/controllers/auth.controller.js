var res_user_Model = require("../models/res_user.model");
var userCompanyModel = require("../models/res_user_company.model");
var userGroupModel = require("../models/res_user_group.model");
const authConfig = require("../configs/auth.config");
var jwt = require("jsonwebtoken");
var mongoose = require("mongoose");
const fs = require("fs");
const path = require("path");
var privateTokenKey = fs.readFileSync(
  path.resolve(`${authConfig.tokenPath}/privateToken.key`),
  "utf8"
);
var privateRfTokenKey = fs.readFileSync(
  path.resolve(`${authConfig.tokenPath}/privateRfToken.key`),
  "utf8"
);
const tokenHelper = require("../helpers/token.helper");

exports.GetGroup = async (user_id, group_id, company_id) => {
  var query = [
    {
      user_id: mongoose.Types.ObjectId(user_id)
    }
  ];
  if (mongoose.Types.ObjectId.isValid(group_id))
    query.push({
      group_id: mongoose.Types.ObjectId(group_id)
    });
  if (mongoose.Types.ObjectId.isValid(company_id))
    query.push({
      company_id: mongoose.Types.ObjectId(company_id)
    });
  if (!mongoose.Types.ObjectId.isValid(user_id))
    return { status: false, message: "Wrong format user_id" };
  return await userCompanyModel.aggregate([
    { $unwind: "$group_id" },
    {
      $match: { $and: query }
    },
    {
      $lookup: {
        from: "res_company",
        localField: "company_id",
        foreignField: "_id",
        as: "company"
      }
    },
    {
      $lookup: {
        from: "res_user_group",
        localField: "group_id",
        foreignField: "_id",
        as: "group"
      }
    },
    {
      $unwind: "$group"
    },
    {
      $unwind: "$company"
    },
    {
      $project: {
        "company.name": 1,
        "company._id": 1,
        "group._id": 1,
        "group.module": 1,
        "group.module_id": 1,
        "group.permission": 1,
        "group.group_name": 1,
         "group.code": 1,
      }
    },
    {
      $lookup: {
        from: "res_permission",
        localField: "group.permission_id",
        foreignField: "_id",
        as: "group.permission"
      }
    },
    {
      $lookup: {
        from: "res_module",
        let: { res_module_id: "$group.module_id" },
        pipeline: [
          { $match: { $expr: { $in: ["$_id", "$$res_module_id"] } } },
          {
            $lookup: {
              from: "res_module_category",
              localField: "category_id",
              foreignField: "_id",
              as: "category"
            }
          },
          { $unwind: "$category" },
          {
            $project: {
              name: 1,
              "category.name": 1,
              "category._id": 1,
              url: 1,
              method: 1,
              code: 1,
              type: 1,
              "category.icon": 1
            }
          }
        ],
        as: "group.module"
      }
    }
  ]);
};

exports.CheckEmail = data => {
  return res_user_Model.findOne({ email: data.email });
};
exports.CheckPhone = async data => {
  return await res_user_Model
    .findOne({ phone: data.phone, inactive: false })
    .lean();
};
exports.Detail = async (_id, phone) => {
  try {
    if (!_id && !phone) return {};
    let query;
    if (_id) query = { _id: mongoose.Types.ObjectId(_id) };
    else query = { phone: phone };
    return await res_user_Model
    .findOne(query)
    .populate({
      path: "ward",
      populate: {
        path: "district",
        populate: {
          path: "province"
        }
      }
    })
    .populate('time_zone')
    .populate('language')
    .populate('country')
    .populate('currency')
    .lean();
  } catch (err) {
    return {};
  }
};
exports.CheckPassword = async (passWriteByUser, password) => {
  return await bcrypt.compare(passWriteByUser, password);
};
exports.CheckReferralCode = async data => {
  return await res_user_Model.findOne({
    user_referral_code: data.referral_code
  });
};
exports.CheckTypeNumber = async data => {
  return !isNaN(parseFloat(data.phone)) && isFinite(data.phone);
};
exports.CreateUser = async data => {
  const { first_name, last_name, email, referral_code, phone } = data;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(data.password, salt);
  let user;
  user = await res_user_Model.findOneAndUpdate(
    { phone },
    {
      first_name,
      last_name,
      email,
      referral_code,
      phone,
      user_referral_code: makeReferralCode(),
      inactive: true,
      password: hashedPassword
    }
  );
  if (!user) {
    user = new res_user_Model({
      first_name,
      last_name,
      email,
      referral_code,
      phone,
      user_referral_code: makeReferralCode(),
      inactive: true,
      password: hashedPassword
    });
  }
  return await user.save();
};

exports.DecodedToken = async (token, secrect, opts) => {
  try {
    const tokenIsValid = await jwt.verify(token, secrect, opts);
    if (tokenIsValid) return tokenIsValid;
  } catch (error) {
    return error;
  }
};

exports.GenToken = (info, secrect, opts) => {
  return jwt.sign(info, secrect, opts);
};

exports.PushReferralUser = (phone, referral) => {
  return res_user_Model.updateOne(
    { user_referral_code: `${referral}` },
    { $push: { list_referral_user: phone } }
  );
};

var makeReferralCode = () => {
  var fist = "";
  var last = "";
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var numbers = "1234567890";
  var charactersLength = characters.length;
  var numbersLength = numbers.length;
  for (var i = 0; i < 3; i++) {
    fist += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  for (var i = 0; i < 3; i++) {
    last += numbers.charAt(Math.floor(Math.random() * numbersLength));
  }
  var result = `${fist}${last}`;
  return result;
};

exports.ChangePassword = async (newPassword, phone) => {
  // hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(newPassword, salt);
  return await res_user_Model.updateOne(
    { phone: phone },
    { $set: { password: hashedPassword } }
  );
};

exports.ChangeUsername = async (data, phone) => {
  return await res_user_Model.updateOne(
    { phone: phone },
    { $set: { last_name: data.last_name, first_name: data.first_name } }
  );
};

exports.LoginWithFaceBook = async data => {
  try {
    const { accessToken, userID, email, first_name, last_name, avatar } = data;
    const checkToken = await tokenHelper.CheckTokenFb(accessToken);
    if (checkToken && checkToken.is_valid && userID === checkToken.user_id) {
      let user;
      const checkUser = await res_user_Model.findOneAndUpdate(
        { fb_id: userID },
        {
          first_name,
          last_name,
          avatar
        }
      );
      if (checkUser) user = checkUser;
      else {
        const newUser = new res_user_Model({
          first_name,
          last_name,
          email,
          avatar,
          fb_id: userID
        });
        user = await newUser.save();
      }
      const payload = {
        id: user._id,
        role: "admin"
      };
      const token = jwt.sign(payload, privateTokenKey, {
        expiresIn: 1000 * 60 * 60 * 24 * 365,
        algorithm: "RS256"
      });
      const rfToken = jwt.sign(payload, privateRfTokenKey, {
        expiresIn: 1000 * 60 * 60 * 24 * 365 * 2,
        algorithm: "RS256"
      });
      const userPayload = {
        avatar,
        first_name,
        last_name,
        phone: user.phone,
        token,
        id: user._id,
        save_product: user.save_product,
        save_project: user.save_project
      };
      return {
        message: "Đăng nhập thành công!",
        isLogin: true,
        status: true,
        token: `${token}`,
        refreshToken: rfToken,
        user: userPayload
      };
    }
    return {
      status: false,
      message: "Đăng nhập thất bại"
    };
  } catch (e) {
    return {
      status: false,
      message: "Server lỗi"
    };
  }
};

exports.LoginWithGoogle = async data => {
  try {
    const { profileObj, tokenId, googleId } = data;
    const checkTokenId = await tokenHelper.CheckTokenIdGoogle(tokenId);
    if (checkTokenId) {
      let user;
      const { familyName, givenName, imageUrl, email } = profileObj;
      const checkUser = await res_user_Model.findOneAndUpdate(
        {
          google_id: googleId
        },
        {
          first_name: givenName,
          last_name: familyName,
          avatar: imageUrl
        }
      );
      if (checkUser) {
        user = checkUser;
      } else {
        const newUser = new res_user_Model({
          first_name: givenName,
          last_name: familyName,
          email,
          avatar: imageUrl,
          google_id: googleId
        });
        user = await newUser.save();
      }
      const payload = {
        id: user._id,
        role: "admin"
      };
      const token = jwt.sign(payload, privateTokenKey, {
        expiresIn: 1000 * 60 * 60 * 24 * 365,
        algorithm: "RS256"
      });
      const rfToken = jwt.sign(payload, privateRfTokenKey, {
        expiresIn: 1000 * 60 * 60 * 24 * 365 * 2,
        algorithm: "RS256"
      });
      const userPayload = {
        avatar: imageUrl,
        first_name: givenName,
        last_name: familyName,
        phone: user.phone,
        id: user._id,
        token,
        save_product: user.save_product,
        save_project: user.save_project
      };
      return {
        message: "Đăng nhập thành công!",
        isLogin: true,
        status: true,
        token: `${token}`,
        refreshToken: rfToken,
        user: userPayload
      };
    }
    return {
      status: false,
      message: "Đăng nhập thất bại"
    };
  } catch (e) {
    return {
      status: false,
      message: "Server lỗi"
    };
  }
};
