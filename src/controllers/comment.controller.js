var commentModel = require('../models/comment.model');
var _ = require('lodash')
var mongoose =require('mongoose')
exports.List = async (rel_id,create_date,parent_id,offset,limit)=>{
    if(!rel_id) return {
        status:false,
        message:"Missing param rel_id"
    }
    var matchQuery = [];
    matchQuery.push(rel_id.length===24 ? {
        rel_id:mongoose.Types.ObjectId(rel_id)
    }: {
        rel_id_string:rel_id
    })
    if(create_date){
      matchQuery.push({create_date:{$lt:new Date(create_date)}})
    }
    matchQuery.push(parent_id? {parent_id:mongoose.Types.ObjectId(parent_id)}:{parent_id:null})
    return commentModel.aggregate([
        {
            $match:{$and:[...matchQuery]}
        },
        {$sort:{create_date:-1}},
        {$skip:limit *offset},
        {$limit:limit},
        {
            $lookup:{
                from:'res_user',
                localField:'create_uid',
                foreignField:'_id',
                as:'user'
            }
        },
        {
            $unwind: {
              path: "$user",
              preserveNullAndEmptyArrays: true
            }
          },
          {$project:{
            rel_id:1,
            rel_id_string:1,
            title:1,
            parent_id:1,
            'user._id':1,
            'user.avatar':1,
            'user.first_name':1,
            'user.last_name':1,
            create_date:1,
            children:1,
            reaction: 1
          }}
    ])
}

exports.Add = async (user,rel_id,title,parent_id)  =>{
    try {
    if(!title) {return {status:false,message:'missage title'}}
    var idParse = _.get(rel_id,'length')===24 ?  {rel_id:mongoose.Types.ObjectId(rel_id) } :{rel_id_string:rel_id};
    var parentID = parent_id ? {parent_id:mongoose.Types.ObjectId(parent_id) } : {};
    var comment = new commentModel({
        ...idParse,
        ...parentID,
        title,
        create_date:Date.now(),
        create_uid:mongoose.Types.ObjectId(user._id)
    })
    var result = {};
    var error =  comment.validateSync();
    if(error) {return {status:false,message:error.toString()}};
    await comment
      .save()
      .then(async document => {
        result = {
          status: true,
          data: document
        };
        await commentModel.findOneAndUpdate({_id:mongoose.Types.ObjectId(parent_id)},{$addToSet:{children:document._id}})
      })
      .catch(err => {
        result = {
          status: false,
          message: err
        };
      });
      if(parent_id && parent_id.length === 24) {
        
      }
      return result;
    } catch(err){

        return {
            status:false,
            message:err.toString()
        }
    }
}
exports.Count = async(rel_id) => {
  try {
    const idParse = _.get(rel_id,'length')===24 ?  {rel_id:mongoose.Types.ObjectId(rel_id) } :{rel_id_string:rel_id};
     const result = await commentModel.find({
      ...idParse
    });
    if(result)
      return {
        status: true,
        data: result.length
      }
      return {
        status: true,
        data: 0
      }
  } catch (e){
      return {
        status: false,
        message: e
      }
  }
}