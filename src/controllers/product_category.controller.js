var productCategoryModel = require('../models/product_category.model')
var _ = require('lodash')
var mongoose = require('mongoose')
var resAttr = require('../models/res_attribute.model')
// exports.List = async (limit, page = 1, filter) => {
//   var listAttr = await resAttr.find();
//   return await productCategoryModel.findOne({attribute:listAttr[0]._id}).populate('attribute')
// };
exports.Count = async (filter) => {
  return await productCategoryModel.countDocuments({});
}

exports.Detail = async (ID) => {
  return await productCategoryModel.find({ _id: ID }).limit(1);
}

exports.Test = async () => {
  return await productCategoryModel.find()
}

//new controller

exports.List = async (limit, page = 1, filter) => {
  return await productCategoryModel.find({}).skip((page - 1) * limit).limit(limit)
};

exports.Edit = async (newCategory = {}) => {
  if (!newCategory._id) {
    return {
      status: false,
      error: 'ID is null'
    }
  }
  var result;
  await productCategoryModel.findOneAndUpdate(
    { _id: newCategory._id },
    { $set: _.omit(newCategory, ["_id"]) }
  ).then((doc) => {
    if (doc) {
      result = {
        status: true,
        data: doc
      }
    } else
      result = { status: false }
  })
  return result
}

exports.Delete = async (id) => {
  if (id) {
    var result;
    await productCategoryModel.findOneAndUpdate(
      { _id: id },
      { $set: { status: false } }
    ).then(async doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        }
      } else
        result = { status: false }
    })
  } else
    result = { status: false };

  return result
}

exports.Add = async data => {
  var category = {
    _id: mongoose.Types.ObjectId(),
    category_name: data.category_name,
    code: data.code
  }
  var productCategory = new productCategoryModel(category);
  var validate = productCategory.validateSync();
  var result = { status: false };
  if (!validate) {
    await productCategory.save().then(document => {
      result = {
        status: true,
        data: document
      };
    }).catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    })
  } else {
    result = { status: false, message: validate.toString() }
  }
  return result;
}