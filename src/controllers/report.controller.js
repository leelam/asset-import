var reportModel = require("../models/report.model");
var checkTypeReport = require("../helpers/reportCheckType.helper");
var searchController = require("./search.controller");
var toUnicode = require("../helpers/toUnicode.helper");
var mongoose = require("mongoose");
var _ = require("lodash");
exports.List = async (limit, page, filter, sort, type) => {
  if (!checkTypeReport(type)) return [];
  return await reportModel
    .find({ approve: type })
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({sequence: 1})
    .populate({
      path: "category_id",
      select: "name",
      match: { status: { $ne: false } }
    });
};
exports.Count = async (filter, type) => {
  if (!checkTypeReport(type)) return 0;
  return await reportModel.countDocuments({ approve: type });
};
exports.Detail = async ID => {
  var report = await reportModel
    .findOne({ _id: ID })
    .populate({ path: "category_id", select: "name" })
    .populate({ path: "create_uid", select: "first_name last_name" })
    .populate({ path: "approved_uid", select: "first_name last_name" });
  return report;
};

exports.Add = async data => {
  var ask = new reportModel(data);
  var validate = ask.validateSync();
  var result = {};
  if (!validate) {
    await ask
      .save()
      .then(async document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err.toString()
        };
      });
  } else {
    result = {
      status: false,
      error: validate.toString()
    };
  }
  return result;
};
exports.Delete = async (ID, deleteID) => {
  var result;
  await reportModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false, delete_id: mongoose.Types.ObjectId(deleteID) } }
    )
    .then(async doc => {
      if (doc) {
        result = {
          status: true
        }
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};
exports.Edit = async (newReport = {}) => {
  if (!newReport._id || !mongoose.Types.ObjectId.isValid(newReport._id)) {
    return {
      status: false,
      error: "_id is not valid"
    };
  }
  var result;
  await reportModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newReport._id) },
      { $set: {
        approve: newReport.approve,
        answer: newReport.answer,
        approved_uid: mongoose.Types.ObjectId(newReport.approved_uid)
      } },
      { runValidators: true }
    )
    .then(async doc => {
      if (doc) {
        result={
          status: true
        }
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};

exports.EditSequence = async (newReport = {}) => {
  if (!newReport.id || !mongoose.Types.ObjectId.isValid(newReport.id)) {
    return {
      status: false,
      error: "_id is not valid"
    };
  }
  var result;
  await reportModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newReport.id) },
      { $set: {sequence: parseInt(newReport.sequence)}},
      { runValidators: true }
    )
    .then(async doc => {
      if (doc) {
        result={
          status: true
        }
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
    return result
  }