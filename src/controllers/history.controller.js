const postHistory = require('../models/post_history.model')
const mongoose = require('mongoose')
const historyType = require('../constants/history_type.constant')
const _ = require('lodash')
const objectIDField = require('../constants/objectIDField.constant')
var moment = require('moment')
exports.AddPostHistory = async (post_id, create_uid, old_version, new_version, type, collectionName, toJson = false) => {
  if (!mongoose.Types.ObjectId.isValid(post_id) ||
    !mongoose.Types.ObjectId.isValid(create_uid) ||
    !_.isObject(old_version) ||
    !_.isObject(new_version) ||
    !collectionName
  ) return { status: false, message: 'Input is not valid' };
  let lastestHistory = await postHistory.find({ rel_id: post_id }).sort({ version: -1 }).limit(1);
  let inc_ver = (lastestHistory && lastestHistory.length && _.isNumber(lastestHistory[0].version)) ? (lastestHistory[0].version + 1) : 1;
  let newKeys;
  if (toJson) {
    newKeys = Object.keys(new_version.toJSON())
    // old_version = old_version.toJSON()
    // console.log(old_version)
  } else{
    newKeys = Object.keys(new_version)
  }
  let pickKey = []
  newKeys.map(item => {
    if (new_version[item] !== old_version[item] && item !== '_id') {
      let i = 0, j = 0;
      objectIDField.listObjectID.map(object => {
        i++;
        if (item === object) {
          i--;
          if (_.isArray(new_version[item])) {
            let newCategory = []
            new_version[item].map(t => {
              newCategory.push(t.toString())
            })
            let oldCategory = []
            old_version[item].map(t => {
              oldCategory.push(t.toString())
            })
            if (_.difference(newCategory, oldCategory).length !== 0) {
              pickKey.push(item)
              let objectIdNewCategory = newCategory.map(t => mongoose.Types.ObjectId(t))
              new_version.category_id = objectIdNewCategory
              let objectIdOldCategory = oldCategory.map(t => mongoose.Types.ObjectId(t))
              old_version.category_id = objectIdOldCategory
            }
          } else {
            let newCategory = `${new_version[item]}`
            let oldCategory = `${old_version[item]}`
            if (!(newCategory === oldCategory)) {
              pickKey.push(item)
              new_version.category_id = mongoose.Types.ObjectId(newCategory)
              old_version.category_id = mongoose.Types.ObjectId(oldCategory)
            }
          }
        }
      })
      objectIDField.listDate.map(dateItem => {
        j++;
        if (item === dateItem) {
          j--;
          old_version[dateItem] = new Date(moment(old_version[dateItem]).format("YYYY-MM-DD HH:mm:ss"))
          new_version[dateItem] = new Date(moment(new_version[dateItem]).format("YYYY-MM-DD HH:mm:ss"))
          if (+old_version[dateItem] !== +new_version[dateItem]) {
            pickKey.push(dateItem)
          }
        }
      })
      if (i === objectIDField.listObjectID.length && j === objectIDField.listDate.length) {
        pickKey.push(item)
      }
    }
  })
  if (pickKey.length !== 0) {
    let newHistory = new postHistory({
      type,
      create_date: Date.now(),
      version: inc_ver,
      collection_name: collectionName,
      rel_id: post_id,
      create_uid,
      new_version: _.pick(new_version, pickKey),
      old_version: _.pick(old_version, pickKey)
    })
    var validate = newHistory.validateSync();
    var result = {};
    if (!validate) {
      await newHistory
        .save()
        .then(async document => {
          result = {
            status: true,
            data: document
          };
        })
        .catch(err => {
          result = {
            status: false,
            error: err.toString()
          };
        });
    } else {
      result = {
        status: false,
        error: validate.toString()
      };
    }
    return result;
  }
}
exports.ListPostHistory = async (post_id, type, limit) => {
  let query = { rel_id: mongoose.Types.ObjectId(post_id) };
  if (historyType.ALL.includes(type)) query.type = type;
  limit = limit < 10 ? limit : 10;
  return await postHistory.find(query).sort({ create_date: -1 }).limit(limit).populate({ path: "create_uid", select: "first_name last_name avatar" })
}

exports.Detail = async(ID) =>{
  if(mongoose.Types.ObjectId.isValid(ID)){
    return {
      status: true,
      detail: await postHistory.findOne({_id: mongoose.Types.ObjectId(ID)})
    }
  } else {
    return {status: false, message: 'ID is not define!'}
  }
}