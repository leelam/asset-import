var userModel = require("../models/res_user.model");
var timeZoneModel = require("../models/res_time_zone.model")
var currencyModel = require('../models/res_currency.model')
var langModel = require('../models/res_lang.model')
var mongoose = require("mongoose");
var _ = require("lodash");
// var bcrypt = require("bcrypt");
exports.ListSuggestion = async keyword => {
  if(!_.isString(keyword)) return [];
  return await userModel
    .find({ 
      $or:[{first_name: { $regex: keyword, $options: "i" } },{phone: { $regex: keyword, $options: "i" } },{last_name: { $regex: keyword, $options: "i" } }]
      
    })
    .limit(10);
};
exports.CheckTypeNumber = async data => {
  return !isNaN(parseFloat(data.phone)) && isFinite(data.phone);
};
exports.List = async (limit, page = 1, filter) => {
  return await userModel
    .find({})
    .skip((page - 1) * limit)
    .limit(limit);
};
exports.Count = async filter => {
  return await userModel.countDocuments();
};
exports.CheckPhone = data => {
  return userModel.findOne({ phone: data.phone });
};
exports.Detail = async ID => {
  return userModel.findOne({ _id: mongoose.Types.ObjectId(ID) });
};

exports.Add = async data => {
  if (!data.password) {
    return { status: false, error: "missing password" };
  }
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(data.password, salt);
  var user = new userModel({
    ...data,
    password: hashedPassword
  });
  var validate = user.validateSync();
  var result = {};
  if (!validate) {
    await user
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate.toString()
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await userModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};
exports.Edit = async (newCategory = {}) => {
  if (!newCategory._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await userModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newCategory._id) },
      { $set: _.omit(newCategory, ["id"]) }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = { status: false, message: err.toString() };
    });
  return result;
};

exports.UploadAvatar = async (user_id,avatar)=>{
  if (!user_id) return {status:false,message:"Missing user_id"}
  var result;
  await userModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(user_id) },
      { $set: {avatar} }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: {...doc,avatar}
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = { status: false, message: err.toString() };
    });
  return result;
}

exports.ListTimeZone = async ()=>{
  return await timeZoneModel.find({},{code:1,name:1,gmt_offset:1})
}
exports.ListCurrency = async ()=>{
  return await currencyModel.find({})
}
exports.ListLang = async ()=>{
  return await langModel.find()
}