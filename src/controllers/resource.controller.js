var mongoose = require('mongoose');
var resourceModel = require('../models/res_resource.model')
var _ = require("lodash");

exports.List = async (limit, page, filter, project, sort) => {
    var customQuery = [];
    // var queryFilter = projectFilter(filter);
    var customLookup = [
        {
            $lookup: {
                from: "res_resource_category",
                localField: "category_id",
                foreignField: "_id",
                as: "category"
            }
        },
        {
            $unwind: {
                path: "$category",
                preserveNullAndEmptyArrays: true
            }
        },
        {
          $lookup: {
              from: "res_user",
              localField: "create_uid",
              foreignField: "_id",
              as: "user"
          }
      },
      {
          $unwind: {
              path: "$user",
              preserveNullAndEmptyArrays: true
          }
      }
    ];
    var textQuery = [];
    if (filter.keyword) {
        textQuery.push({
            $match: {
                $text: { $search: filter.keyword }
            }
        });
        if (!sort) { sort = { score: { $meta: "textScore" } } }
    }
    if (sort) customQuery.push({ "$sort": sort })
    customQuery.push({ $skip: (page - 1) * limit });
    customQuery.push({ $limit: limit });
    return await resourceModel.aggregate([
        ...textQuery,
        //   ...queryFilter,
        ...customLookup,
        ...customQuery
    ]);
};

exports.Count = Count = async (filter) => {
    var customQuery = [];
    // var queryFilter = projectFilter(filter);
    var customLookup = [
        {
            $lookup: {
                from: "res_resource_category",
                localField: "category_id",
                foreignField: "_id",
                as: "category"
            }
        },
        {
            $unwind: {
                path: "$category",
                preserveNullAndEmptyArrays: true
            }
        }
    ];
    var textQuery = [];
    // if (filter.keyword) {
    //     textQuery.push({
    //         $match: {
    //             $text: { $search: filter.keyword }
    //         }
    //     });
    //     if (!sort) { sort = { score: { $meta: "textScore" } } }
    // }
    // queryFilter.push({
    //   $match: {
    //     status: { $ne:false}
    //   }
    // })
    // if(project) customQuery.push({ $project: project });
    // if (sort) customQuery.push({ "$sort": sort })
    return await resourceModel.aggregate([
        // ...textQuery,
        // {
        //     $lookup: {
        //         from: "product_type",
        //         localField: "type_id",
        //         foreignField: "_id",
        //         as: "status_detail"
        //     }
        // },
        // {
        //     $unwind: {
        //         path: "$status_detail",
        //         preserveNullAndEmptyArrays: true
        //     }
        // },
        //   ...queryFilter,
        // ...customLookup,
        // ...customQuery,
        { $count: "total" }
    ]);
}
exports.Detail = async ID => {
    var guide= await resourceModel.findOne({_id:ID}).populate({ path: 'category', select: 'name' })
    .populate('user')
    return guide
};

exports.Add = async data => {
  var resource = new resourceModel(data);
  resource.create_date = Date.now();
  var validate = resource.validateSync();
  var result = {};
  if (!validate) {
    await resource
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err.toString()
        };
      });
  } else {
    result = {
      status: false,
      error: validate.toString()
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await resourceModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      }else
      result = {
        status: false
      };
    }).catch(err=>{
        result= {
            status: false,
            message:err.toString()
        }
    });
  return result;
};
exports.Edit = async (newGuide = {}) => {
  if (!newGuide._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await resourceModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newGuide._id) },
      { $set: _.omit(newGuide, ["id"]) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      }
      else result = {
        status: false
      };
    });
  return result;
};
