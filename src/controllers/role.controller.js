var moduleCategoryModel = require("../models/res_ module_category.model");
var permissionModel = require("../models/res_user_permission.model");
var userCompanyModel = require('../models/res_user_company.model')
var userGroupModel = require("../models/res_user_group.model");
var companyController = require('../controllers/company.controller')
const agencyCode = require('../constants/agency_code.constant')
const companyCategoryModel = require('../models/res_company_category.model')
var userCompanyFilter = require('../helpers/filters/user_company.filter')
const groupModel = require('../models/res_user_group.model')
const userModel = require('../models/res_user.model')
var _ = require('lodash')
var mongoose = require("mongoose");
var moduleModel = require("../models/module.model");
const handleGroupCode = async filter => {
  var groupCodeQuery = []
  if (filter && filter.group_code && filter.group_code.length) {
    var groupCodeResponse = await userGroupModel.find({ code: { $in: filter.group_code } });
    if (groupCodeResponse && groupCodeResponse.length) {
      groupCodeQuery = [{ $match: { group_id: { $in: groupCodeResponse.map(x => x._id) } } }]
    }
  }
  return groupCodeQuery
}
exports.ListModule = async category_id => {
  if (!mongoose.Types.ObjectId.isValid(category_id))
    return { status: false, message: "category_id is not valid" };
  var listModule = await moduleModel
    .find({ category_id: mongoose.Types.ObjectId(category_id) })
    .populate("category");
  return listModule;
};
exports.ListModuleCategory = async () => {
  var ListModuleCategory = await moduleCategoryModel.find();
  return ListModuleCategory;
};
exports.ListGroup = async () => {
  return await userGroupModel
    .find()
    .populate("permission")
    .populate("module");
};
exports.CountGroup = async () => {
  return await userGroupModel.countDocuments();
};
exports.GroupDetail = async ID => {
  var guide = await userGroupModel
    .findOne({ _id: ID })
    .populate("permission")
    .populate("module");
  return guide;
};

exports.AddGroup = async data => {
  var group = new userGroupModel(data);
  var validate = group.validateSync();
  var result = {};
  if (!validate) {
    await group
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate
    };
  }
  return result;
};
exports.DeleteGroup = async ID => {
  var result;
  await userGroupModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    })
    .catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    });
  return result;
};
exports.EditGroup = async (newGuide = {}) => {
  if (!newGuide._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await userGroupModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newGuide._id) },
      { $set: _.pick(newGuide, ["module_id", "permission_id", "group_name", "code"]) }
    )
    .then(doc => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    }).catch(err => {
      return { status: false, message: err.toString() }
    });
  return result;
};

exports.GetListUserCompany = async (page, limit, project, filter, sort) => {
  var filterQuery = userCompanyFilter(filter);
  return await userCompanyModel.aggregate([
    ...await handleGroupCode(filter),
    {
      $lookup: {
        from: "res_company",
        localField: "company_id",
        foreignField: "_id",
        as: "company"
      }
    },
    {
      $lookup: {
        from: "res_user_group",
        localField: "group_id",
        foreignField: "_id",
        as: "group"
      }
    },
    {
      $lookup: {
        from: "res_user",
        localField: "user_id",
        foreignField: "_id",
        as: "user"
      }
    },
    {
      $unwind: "$user"
    },
    {
      $unwind: "$company"
    },
    ...filterQuery,
    {
      $project: {
        "company.name": 1,
        "company.code": 1,
        "company._id": 1,
        "group": 1,
        "user": 1
      }
    },
    { $skip: (page - 1) * limit },
    { $limit: limit }
  ]);

};

exports.CountUserCompany = async (filter) => {
  var filterQuery = userCompanyFilter(filter);
  return await userCompanyModel.aggregate([
    ...await handleGroupCode(filter),
    {
      $lookup: {
        from: "res_company",
        localField: "company_id",
        foreignField: "_id",
        as: "company"
      }
    },
    {
      $lookup: {
        from: "res_user_group",
        localField: "group_id",
        foreignField: "_id",
        as: "group"
      }
    },
    {
      $lookup: {
        from: "res_user",
        localField: "user_id",
        foreignField: "_id",
        as: "user"
      }
    },
    {
      $unwind: "$user"
    },
    {
      $unwind: "$company"
    },
    ...filterQuery,
    { $count: "total" }
  ]);
}

const AddUserCompany = async data => {
  var userCompany = new userCompanyModel(data)
  var validate = userCompany.validateSync();
  var result = {};
  if (!validate) {
    await userCompany
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          message: err.toString()
        };
      });
  } else {
    result = {
      status: false,
      message: validate.toString()
    };
  }
  return result;
}
exports.AddUserCompany = AddUserCompany;
exports.AddUserAgency = async (data, create_uid) => {
  if (!mongoose.Types.ObjectId.isValid(data.user_id) || !mongoose.Types.ObjectId.isValid(data.group_id)) return { status: false, message: 'Input is not valid' };
  const listUserCompany = await userCompanyModel.find({ user_id: data._id, group_id: data.group_id })
  if (listUserCompany.length) return { status: false, message: 'Đã tồn tại cộng tác viện/đại lý này' }
  const group = await groupModel.findOne({ _id: mongoose.Types.ObjectId(data.group_id) });
  if (!group) return { status: false, message: "Không tìm thấy group" };
  const user = await userModel.findOne({ _id: mongoose.Types.ObjectId(data.user_id) });
  if (!user) return { status: false, message: "Không tìm thấy user" };
  const companyCategory = await companyCategoryModel.findOne({ code: group.code });
  if (!companyCategory) return { status: false, message: "Không tìm thấy danh mục công ty" };
  const newCompany = await companyController.Add({ company_name: `${user.first_name} ${user.last_name}`, code: "DEMO", category_id: companyCategory._id, working_area:data.working_area,working_area_collection_name:data.working_area_collection_name });
  if (!_.get(newCompany, 'status')) return { status: false, message: _.get(newCompany, 'message') || "Không thể thêm mới công ty" }
  return await AddUserCompany({ user_id: data.user_id, group_id: [data.group_id], company_id: newCompany.company._id,create_uid })
}
exports.DeleteUserCompany = async id => {
  var result;
  await userCompanyModel.deleteOne({ _id: mongoose.Types.ObjectId(id) }).then(data => {
    if (data.deletedCount) {
      result = { status: true }
    }
  }).catch(err => {
    result = { status: false, message: err.toString() }
  })
  return result;
}

exports.DetailUserCompany = async id => {
  if (!mongoose.Types.ObjectId.isValid(id)) return { status: false, message: 'id is not valid' }
  return await userCompanyModel
    .findOne({ _id: mongoose.Types.ObjectId(id) })
    .populate('user')
    .populate('group')
    .populate({
      path:'company',
      populate:{
        path:'working_area'
      }
    })
}