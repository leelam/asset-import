var cityModel = require('../models/res_province.model');
var districtModel = require('../models/res_district.model');
var wardModel = require('../models/res_ward.model');
var countryModel = require('../models/res_country.model')
var mongoose =require('mongoose')
exports.ListCountry = async ()=>{
    return await countryModel.find()
}
exports.Province = async ()=>{
    return await cityModel.find({},{code:1,name:1,type:1,points:1,country_id:1});
}
exports.District = async(province_id)=>{
    return await mongoose.Types.ObjectId.isValid(province_id) ? districtModel.find({province_id:mongoose.Types.ObjectId(province_id)}) : [];
}

exports.Ward = async(district_id)=>{
    return await mongoose.Types.ObjectId.isValid(district_id) ? wardModel.find({district_id:mongoose.Types.ObjectId(district_id)}) : [];
}
exports.getAddress = async _id => {
    try{
        const address = await wardModel.aggregate([
            {
                $match:
                {
                    _id: mongoose.Types.ObjectId(_id)
                }
            },
            {
                $lookup:
                {
                    from: 'res_district',
                    localField: 'district_id',
                    foreignField: '_id',
                    as: 'district'
                }
            },
            {
                $unwind: "$district"
            },
            {
                $lookup:
                {
                    from: 'res_province',
                    localField: 'district.province_id',
                    foreignField: '_id',
                    as: 'province'
                }
            },
            {
                $unwind: "$province"
            },
            {
                $addFields: {
                    ward:{
                        '_id': '$_id',
                        'name': '$name',
                        code: '$code'
                    }
                }
            },
            {
                $project:{
                    "province._id": 1,
                    "province.code": 1,
                    "province.name": 1,
                    "district.name": 1,
                    "district.code": 1,
                    "district._id":1,
                    'ward': 1
                }
            }
        ])
        if(!address.length)
            return {
                status: false,
                message: 'Không tìm thấy địa chỉ'
            }
        return {
            status: true,
            data: address[0]
        }
    } catch (e){
        console.log(e)
        return {
            status: false,
            message: 'Server lỗi'
        }
    }
}