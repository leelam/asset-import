var mongoose = require("mongoose");
var productModel = require("../models/product.model");
var searchModel = require("../models/res_search_string.model");
var geometry = require("../helpers/geometry.helper");
var parseKeyword = require("../helpers/toUnicode.helper");
var consDesignInfoModel = require("../models/product_cons_design.model");
var productLegalModel = require("../models/legal_info.model");
var productLandlordInfo = require("../models/product_landlord_info.model");
var _ = require("lodash");
const polygon ={
    "type" : "Polygon",
    "coordinates" : [ 
        [ 
            [ 
                106.727548404, 
                10.890134157
            ], 
            [ 
                106.72846445, 
                10.8909252180001
            ], 
            [ 
                106.729246377, 
                10.8894342130001
            ], 
            [ 
                106.732443374, 
                10.890640187
            ], 
            [ 
                106.731313361, 
                10.885954145
            ], 
            [ 
                106.736686881, 
                10.884585037
            ], 
            [ 
                106.734204341, 
                10.880520109
            ], 
            [ 
                106.738148341, 
                10.879160154
            ], 
            [ 
                106.739664301, 
                10.8810320900001
            ], 
            [ 
                106.741187306, 
                10.881355093
            ], 
            [ 
                106.742999278, 
                10.880319109
            ], 
            [ 
                106.745051293, 
                10.8815820900001
            ], 
            [ 
                106.74568134, 
                10.8807771190001
            ], 
            [ 
                106.744691265, 
                10.8800730510001
            ], 
            [ 
                106.747184306, 
                10.8773850430001
            ], 
            [ 
                106.748500271, 
                10.8782690410001
            ], 
            [ 
                106.749421273, 
                10.8767850460001
            ], 
            [ 
                106.750436987, 
                10.8772253380001
            ], 
            [ 
                106.751368244, 
                10.8754840400001
            ], 
            [ 
                106.746441272, 
                10.8738771160001
            ], 
            [ 
                106.733814271, 
                10.8702891140001
            ], 
            [ 
                106.731863283, 
                10.875014128
            ], 
            [ 
                106.729966273, 
                10.8734611520001
            ], 
            [ 
                106.729203346, 
                10.873781196
            ], 
            [ 
                106.728375337, 
                10.872765182
            ], 
            [ 
                106.726765314, 
                10.873064229
            ], 
            [ 
                106.724629288, 
                10.8714682190001
            ], 
            [ 
                106.723536283, 
                10.8718472470001
            ], 
            [ 
                106.721242322, 
                10.8763631800001
            ], 
            [ 
                106.715286374, 
                10.8774733010001
            ], 
            [ 
                106.714055372, 
                10.876966257
            ], 
            [ 
                106.713808399, 
                10.8778642660001
            ], 
            [ 
                106.712595365, 
                10.877949259
            ], 
            [ 
                106.71380038, 
                10.879149295
            ], 
            [ 
                106.717957377, 
                10.8808541990001
            ], 
            [ 
                106.716580412, 
                10.884711286
            ], 
            [ 
                106.717535397, 
                10.8878332210001
            ], 
            [ 
                106.715609432, 
                10.8877432730001
            ], 
            [ 
                106.714478418, 
                10.8867892920001
            ], 
            [ 
                106.713822403, 
                10.887527295
            ], 
            [ 
                106.714502469, 
                10.8881682480001
            ], 
            [ 
                106.713104109, 
                10.8898899820001
            ], 
            [ 
                106.71761045, 
                10.8963802870001
            ], 
            [ 
                106.726410394, 
                10.8907051950001
            ], 
            [ 
                106.727548404, 
                10.890134157
            ]
        ]
    ]
}
exports.Demo = async () => {
  var search = "79",
    rgx = new RegExp("^" + search);
    var bound = [[106.7175306999502,10.829025965913923], [106.7195463800249,10.830302351683725]]
  var query = {
    //type_id :{$ne:mongoose.Types.ObjectId('5d6e57310b4d9fd73db02be9')},
    points:{
        $geoWithin: {
            $box:bound
        } 
    },
    //code:rgx
  };
  return await productModel.find(query).limit(500);
};
exports.List = async (limit, page, filter, project, sort) => {
  var customQuery = [];
  // var queryFilter = projectFilter(filter);
  var customLookup = [
    {
      $lookup: {
        from: "product_category",
        localField: "category_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    }
  ];
  var textQuery = [];
  if (filter.keyword) {
    textQuery.push({
      $match: {
        $text: { $search: filter.keyword }
      }
    });
    if (!sort) {
      sort = { score: { $meta: "textScore" } };
    }
  }
  // queryFilter.push({
  //   $match: {
  //     status: { $ne:false}
  //   }
  // })
  // if(project) customQuery.push({ $project: project });
  if (sort) customQuery.push({ $sort: sort });
  customQuery.push({ $skip: (page - 1) * limit });
  customQuery.push({ $limit: limit });
  return await productModel.aggregate([
    ...textQuery,
    {
      $lookup: {
        from: "product_type",
        localField: "type_id",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
    //   ...queryFilter,
    ...customLookup,
    ...customQuery
  ]);
};

exports.Count = Count = async filter => {
  var customQuery = [];
  // var queryFilter = projectFilter(filter);
  var customLookup = [
    {
      $lookup: {
        from: "product_category",
        localField: "category_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    }
  ];
  var textQuery = [];
  // if (filter.keyword) {
  //     textQuery.push({
  //         $match: {
  //             $text: { $search: filter.keyword }
  //         }
  //     });
  //     if (!sort) { sort = { score: { $meta: "textScore" } } }
  // }
  // queryFilter.push({
  //   $match: {
  //     status: { $ne:false}
  //   }
  // })
  // if(project) customQuery.push({ $project: project });
  // if (sort) customQuery.push({ "$sort": sort })
  return await productModel.aggregate([
    // ...textQuery,
    // {
    //     $lookup: {
    //         from: "product_type",
    //         localField: "type_id",
    //         foreignField: "_id",
    //         as: "status_detail"
    //     }
    // },
    // {
    //     $unwind: {
    //         path: "$status_detail",
    //         preserveNullAndEmptyArrays: true
    //     }
    // },
    //   ...queryFilter,
    // ...customLookup,
    // ...customQuery,
    { $count: "total" }
  ]);
};

exports.Detail = async ID => {
  return await productModel.aggregate([
    { $match: { _id: mongoose.Types.ObjectId(ID) } },
    {
      $lookup: {
        from: "product_type",
        localField: "type_id",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "product_category",
        localField: "category_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "product_legal",
        localField: "legal_id",
        foreignField: "_id",
        as: "legal"
      }
    },
    {
      $unwind: {
        path: "$legal",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "product_legal",
        localField: "cons_design_info_id",
        foreignField: "_id",
        as: "cons_design_info"
      }
    },
    {
      $unwind: {
        path: "$cons_design_info",
        preserveNullAndEmptyArrays: true
      }
    }
  ]);
};

exports.Add = async data => {
  if (!data.product) data.product = {};
  if (!data.legal_info) data.legal_info = {};
  if (!data.cons_design_info) data.cons_design_info = {};
  if (!data.contact) data.contact = {};
  var product = new productModel(data.product),
    consDesignInfoProduct = new consDesignInfoModel(data.cons_design_info),
    legalProduct = new productLegalModel(data.legal),
    landlordInfoProduct = new productLandlordInfo(data.contact);
  consDesignInfoProduct.create_date = Date.now();
  cons_design_info.product_id = newProduct.product._id;
  legalProduct.product_id = newProduct.product._id;
  landlordInfoProduct.product_id = newProduct.product._id;
  product.create_date = Date.now();
  product.points = geometry.isPoint(product.points)
    ? { type: "Point", coordinates: product.points }
    : null;
  product.polygons = geometry.isPolygon(product.polygons)
    ? { type: "Polygon", coordinates: [product.polygons] }
    : null;
  product.type_id = mongoose.Types.ObjectId(product.type_id);
  product.legal_id = legalProduct._id;
  product.cons_design_info_id = consDesignInfoProduct._id;
  product.landlord_info = landlordInfoProduct._id;
  var validateProduct = product.validateSync(),
    conDesignInfoValidate = consDesignInfoProduct.validateSync(),
    legalValidate = legalProduct.validateSync(),
    landlordInfoValidate = landlordInfoProduct.validateSync();
  var result = {};

  product._id = mongoose.Types.ObjectId();
  if (
    !validateProduct &&
    !conDesignInfoValidate &&
    !legalValidate &&
    !landlordInfoValidate
  ) {
    await product
      .save()
      .then(async document => {
        var search = new searchModel({
          name: "product",
          rel_id: product._id,
          title: product.name,
          search: `${product.name} ${parseKeyword(product.name)} ${
            product.keyword
          } ${parseKeyword(product.keyword)}`,
          rating: 1
        });
        await search.save();
        await legalProduct.save();
        await consDesignInfoProduct.save();
        await landlordInfoProduct.save();

        result = { status: true, data: document };
      })
      .catch(err => {
        result = { status: false, message: err.toString() };
      });
  } else {
    result = {
      status: false,
      message: validateProduct,
      legal: legalValidate,
      consdesign: conDesignInfoValidate,
      landlordInfo: landlordInfoValidate
    };
  }
  return result;
};

exports.Edit = async newProduct => {
  if (!newProduct || !newProduct.product._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  if (geometry.isPoint(newProduct.product.points)) {
    newProduct.product.points = {
      type: "Point",
      coordinates: newProduct.product.points
    };
  } else {
    return { status: false, message: "sai tọa độ point" };
  }
  if (geometry.isPolygon(newProduct.product.polygons)) {
    newProduct.product.polygons = {
      type: "Polygon",
      coordinates: [newProduct.product.polygons]
    };
  } else {
    return { status: false, message: "sai tọa độ polygon" };
  }
  newProduct.cons_design_info.product_id = mongoose.Types.ObjectId(
    newProduct.product.product_id
  );
  newProduct.product.type_id = mongoose.Types.ObjectId(
    "5d6e56c88e18e6eaf5076069"
  );
  newProduct.product.status_approve = "APPROVED";
  var result;
  result = await productModel
    .findOneAndUpdate(
      { _id: newProduct.product._id },
      { $set: _.omit(newProduct.product, ["_id"]) }
    )
    .then(async document => {
      if (newProduct.cons_design_info && newProduct.cons_design_info !== {}) {
        newProduct.cons_design_info.genaral_info_of_cons = _.get(
          newProduct,
          "cons_design_info.genaral_info_of_cons",
          {}
        );
        newProduct.cons_design_info.material_structure = _.get(
          newProduct,
          "cons_design_info.material_structure",
          {}
        );
        newProduct.cons_design_info.int_finishing = _.get(
          newProduct,
          "cons_design_info.int_finishing",
          {}
        );
        await consDesignInfoModel.findOneAndUpdate(
          { product_id: newProduct.product._id },
          {
            genaral_info_of_cons:
              newProduct.cons_design_info.genaral_info_of_cons,
            material_structure: newProduct.cons_design_info.material_structure,
            int_finishing: newProduct.cons_design_info.int_finishing
          },
          { upsert: true }
        );
      }
      if (newProduct.legal_info && newProduct.legal_info !== {}) {
      }
      return { message: document, status: true };
    })
    .catch(e => {
      return { status: false, message: e };
    });
  return result;
};


exports.addProduct = async (idUser, idProduct, general_info, legal_info, description, cons_design_info, landlord, type, oldId) => {
  try {
    console.log(legal_info)
    const newLegal = new productLegalModel({
      ...legal_info
    })
    const saveLegal = await newLegal.save();
    const newConsDesign = new consDesignInfoModel({
      ...cons_design_info
    })
    const saveConsDesignInfo = await newConsDesign.save();
    delete landlord._id
    const newLandLordInfo = new productLandlordInfo(landlord)
    const saveLandLordInfo = await newLandLordInfo.save();
    if (general_info.status_approve) {
      return {
        status: false,
        message: 'Server lỗi'
      }
    }
    let updateUser = oldId ? {write_uid: idUser}: {create_uid: idUser}
    const newProduct = new productModel({
      ...updateUser,
      legal_info_id: saveLegal._id,
      ...general_info,
      code: general_info.location,
      _id: new mongoose.Types.ObjectId(idProduct),
      process_approve: 'AG2',
      status_approve: 'PENDING',
      name: general_info.name,
      description: description.description,
      cons_design_info_id: saveConsDesignInfo._id,
      landlord_info_id: saveLandLordInfo._id,
      type,
      oldId
    })
    const result = await newProduct.save();
    return {
      status: true,
      data: {
        product: result._id,
      }
    }
  } catch (e) {
    console.log(e)
    return {
      status: false,
      message: 'Server lỗi'
    }
  }
}
