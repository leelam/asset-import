var postCategoryModel = require("../models/post_category.model");
var _ = require("lodash");

var mongoose = require("mongoose");
exports.List = async (limit, page = 1, filter) => {
  return await postCategoryModel
    .find({})
    .skip((page - 1) * limit)
    .limit(limit)
    .populate("parent_id");
};

exports.ListTree = async () => {
  return await postCategoryModel.aggregate([
    {
      $match: {
        parent_id: null
      }
    },
    {
      $lookup: {
        from: "post_category",
        "let": { "bId": "$_id" },
        "pipeline": [{
          "$match": {
              $expr: { $eq: [ "$parent_id", "$$bId" ] },
              status:{$ne:false}
          }
      }],
        as: "children"
      },
     
  }
  ]);
};

exports.Count = async filter => {
  return await postCategoryModel.countDocuments();
};
exports.Detail = async ID => {
  return await postCategoryModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(ID)
      }
    },
    {
      $lookup: {
        from: "post_category",
        "let": { "bId": "$_id" },
        "pipeline": [{
          "$match": {
              $expr: { $eq: [ "$parent_id", "$$bId" ] },
              status:{$ne:false}
          }
      }],
       
        as: "children"
      }
    },
    {
      $lookup: {
        from: "post_category",
        localField: "parent_id",
        foreignField: "_id",
        as: "parent"
      }
    },
    {
      $unwind: {
        path: "$parent",
        preserveNullAndEmptyArrays: true
      }
    }
  ]);
};

exports.Add = async data => {
  var category = new postCategoryModel(data);
  var validate = category.validateSync();
  var result = {};
  if (!validate) {
    await category
      .save()
      .then(document => {
        result = {
          status: true,
          data: document
        };
      })
      .catch(err => {
        result = {
          status: false,
          error: err
        };
      });
  } else {
    result = {
      status: false,
      error: validate
    };
  }
  return result;
};
exports.Delete = async ID => {
  var result;
  await postCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ID) },
      { $set: { status: false } }
    )
    .then(async (doc) => {
      if ( doc) {
        result = {
          status: true,
          data: doc
        };
      }else
      result = {
        status: false
      };
    });
  return result;
};
exports.Edit = async (newCategory = {}) => {
  if (!newCategory._id) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  var result;
  await postCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newCategory._id) },
      { $set: _.omit(newCategory, ["id"]) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          error: doc
        };
      }else
      result = {
        status: false
      };
    });
  return result;
};
