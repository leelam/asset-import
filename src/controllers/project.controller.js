var mongoose = require("mongoose");
var projectModel = require("../models/project.model");
var parseKeyword = require("../helpers/toUnicode.helper");
var searchModel = require("../models/res_search_string.model");
const projectLegalInfoModel = require('../models/project_legal_info.model')
const consDesignInfoModel = require('../models/project_cons_design.model')
const project_status_Model = require('../models/project_status.model')
const res_companyModel = require('../models/res_company.model')
const saleStatusModel = require('../models/sale_status.model')
var projectFilter = require("../helpers/filters/project.filter");
var geometry = require('../helpers/geometry.helper')
var _ = require("lodash");
exports.All = async () => {
  return await projectModel.find({ status: { $ne: false } }).populate('category_id').populate('status_id')
}
exports.List = async (limit, page, filter, project, sort) => {
  var customQuery = [];
  var queryFilter = projectFilter(filter);
  var customLookup = [
    {
      $lookup: {
        from: "project_category",
        localField: "category_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    }
  ];
  var textQuery = [];
  if (filter.keyword) {
    textQuery.push({
      $match: {
        $text: { $search: filter.keyword }
      }
    });
    if (!sort) { sort = { score: { $meta: "textScore" } } }
  }
  queryFilter.push({
    $match: {
      status: { $ne: false }
    }
  })
  if (project) customQuery.push({ $project: project });
  if (sort) customQuery.push({ "$sort": sort })
  customQuery.push({ $skip: (page - 1) * limit });
  customQuery.push({ $limit: limit });
  return await projectModel.aggregate([
    ...textQuery,
    {
      $lookup: {
        from: "project_status",
        localField: "cons_status",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
    ...queryFilter,
    ...customLookup,
    ...customQuery
  ]);
};

exports.Delete = async (id) => {
  var result;
  await projectModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(id) },
      { $set: { status: false } }
    )
    .then(async (err, doc) => {
      if (err) {
        result = {
          status: false,
          error: err
        };
      }
      await searchModel.deleteMany({ rel_id: mongoose.Types.ObjectId(id) })
      result = {
        status: true
      };
    });
  return result;
}
exports.CountPieCategory = async() =>{
  return await projectModel.aggregate([
    {"$group" : {_id:"$category_id", count:{$sum:1}}},
    {
      $lookup: {
        from: "project_category",
        localField: "_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    },
  ])
}
exports.CountPieStatus =  async()=>{
  return await projectModel.aggregate([
    {"$group" : {_id:"$cons_status", count:{$sum:1}}},
    {
      $lookup: {
        from: "project_status",
        localField: "_id",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
  ])
}
exports.Add = async (idUser, idProject, general_info, legal_info, description, cons_design_info) => {
  var project = new projectModel({
    create_uid: idUser,
    _id: idProject,
    ...general_info,
    description: description.description,
  });
  project.create_date = Date.now();
  project.points = geometry.isPoint(project.points) ? { type: 'Point', coordinates: project.points } : null;
  project.polygons = geometry.isPolygon(project.polygons) ? { type: 'Polygon', coordinates: [project.polygons] } : null;
  var validate = project.validateSync();
  var result = {};
  if (!validate) {
    const addLegal = addProjectLegalInfo(legal_info)
    const addConsDesign = addConsDesignInfo(cons_design_info)
    const addLegalResult = await addLegal
    const addConsDesignResult = await addConsDesign
    if(!addLegalResult.status) return addLegalResult
    if(!addConsDesignResult.status) return addConsDesign
    console.log('asdfsdf')
      project.legal_info_id = addLegalResult.data
      project.cons_design_info_id = addConsDesignResult.data
    const save = await project.save()
    console.log(save)
    if(save)
      return {
        status: true,
        data: save._id
      }
    return {
      status: false,
      message: 'Server error'
    }
  } else {
    return {
      status: false,
      message: 'Field not validate'
    }
   }
};

exports.Count = async (filter) => {
  var queryFilter = projectFilter(filter);
  var textQuery = []
  if (filter && filter.keyword) {
    textQuery.push({
      $match: {
        $text: { $search: filter.keyword }
      }
    });
  }
  textQuery.push({
    $match: {
      status: { $ne: false }
    }
  })
  return await projectModel.aggregate([
    ...textQuery,
    {
      $lookup: {
        from: "project_status",
        localField: "cons_status",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
    ...queryFilter,
    { $count: "total" }
  ])
}

exports.Edit = async (_id,idProject,general_info, legal_info, cons_design_info) => {
  try{
  if (idProject) {
    return {
      status: false,
      error: "ID is null"
    };
  }
  if (geometry.isPoint(general_info.points)) {
    general_info.points = { type: "Point", coordinates: general_info.points }
  } else {
    return { status: false, message: 'sai tọa đổ point' }
  }
  if (geometry.isPolygon(general_info.polygons)) {
    general_info.polygons = { type: "Polygon", coordinates: [general_info.polygons] }
  } else {
    return { status: false, message: 'sai tọa độ polygon' }
  }
  var result;
  await projectModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(idProject) },
      { $set: {
        ..._.omit(general_info, ["_id",'status', 'legal_info_id', 'cons_design_info_id']),
        write_uid: _id
      } }
    )
    .then(async (doc) => {
      if (doc) {
        const updateLegal = updateProjectLegalInfo(doc.legal_info_id, legal_info)
        const updateConsDesignInfo = updateProjectConsDesign(doc.cons_design_info_id, cons_design_info)
        await updateLegal
        await updateConsDesignInfo
        if(!updateLegal.status || !updateConsDesignInfo.status){
          result = {
            status: false,
            data: 'Update error'
          };
          return
        }
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false,
          message: 'Không tìm thấy'
        };
    }).catch(err => {
      result = {
        status: false, message: err.toString()
      }
    });
  return result;
  } catch(e){
    console.log(e)
    return {
      status: false,
      message: ''
    }
  }
};

exports.Detail = async (ID) => {
  return projectModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(ID)
      }
    },
    {
      $lookup: {
        from: "project_status",
        localField: "cons_status",
        foreignField: "_id",
        as: "status_detail"
      }
    },
    {
      $unwind: {
        path: "$status_detail",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "res_ward",
        localField: "ward_id",
        foreignField: "_id",
        as: "ward"
      }
    },
    { $unwind: "$wardDetail" },
    {
      $lookup: {
        from: "res_district",
        localField: "ward.district_id",
        foreignField: "_id",
        as: "districtDetail"
      }
    },
    { $unwind: "$district" },
    {
      $lookup: {
        from: "res_province",
        localField: "district.province_id",
        foreignField: "_id",
        as: "provinceDetail"
      }
    },
    { $unwind: "$province" },
   
    {
      $lookup: {
        from: "project_category",
        localField: "category_id",
        foreignField: "_id",
        as: "category"
      }
    },
    {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "assets_planning_category",
        localField: "land_type",
        foreignField: "_id",
        as: "land_type"
      }
    },
    {
      $unwind: {
        path: "$land_type",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "project_legal_info",
        localField: "legal_info_id",
        foreignField: "_id",
        as: "legal_info_id"
      }
    },
    {
      $unwind: {
        path: "$legal_info_id",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "project_cons_design_info",
        localField: "cons_design_info_id",
        foreignField: "_id",
        as: "cons_design_info_id"
      }
    },
    {
      $unwind: {
        path: "$cons_design_info_id",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: "res_company",
        localField: "contractor_id",
        foreignField: "_id",
        as: "contractor_id"
      }
    },
    {
      $lookup: {
        from: "res_company",
        localField: "designer_id",
        foreignField: "_id",
        as: "designer_id"
      }
    },
    {
      $lookup: {
        from: "res_company",
        localField: "investor_id",
        foreignField: "_id",
        as: "investor_id"
      }
    },
    {
      $lookup: {
        from: "res_company",
        localField: "manager_id",
        foreignField: "_id",
        as: "manager_id"
      }
    },
  ])
}


exports.Test = async () => {
  await projectModel.find({}, async function (err, project) {
    for (var i = 0; i < project.length; i++) {
      var keyword = `${project[i].name} ${parseKeyword(project[i].name)}`;
        await projectModel.findOneAndUpdate({_id:project[i]._id },{$set:{keyword}}).then(doc=>{
        })
      // var search = new searchModel({
      //   name: "project",
      //   rel_id: project[i]._id,
      //   title: project[i].name,
      //   sub_title: project[i].location.substring(14),
      //   search: project[i].keyword,
      //   rating: 100
      // });
      // await search.save();
      //=====================
      //   var keyword = `${project[i].name} ${parseKeyword(project[i].name)}`;
      // console.log(project[i]._id)
      //   await projectModel.findOneAndUpdate({_id:project[i]._id },{$set:{keyword}}).then(doc=>{
      //       console.log(doc)
      //   })
      //=======================
    }
    // const directoryPath = path.join(
    //   __dirname,
    //   `../resources/images/post`
    // );
    //var ListItem = fs.readdirSync(directoryPath);
    // var list = ListItem.map(x=>path.parse(x).name)
    // console.log(list)
    // for(var i = 0;i<post.length;i++) {
    //   let content = post[i].content;
    //   // for (var j = 0;j<list.length;j++) {
    //   //   content = replaceAll(content,`document?id=${list[j]}`,`http://localhost:7777/images/post/${list[j]}.jpg`)
    //   // }
    //   content = replaceAll(content,`http://localhost:7777`,`http://27.74.250.96:8386`)
    //   var item = {content:content}

    //   await postModel.findOneAndUpdate({_id:mongoose.Types.ObjectId(post[i]._id) },{$set:item})
    // }
  });
};
const addProjectLegalInfo = async data => {
  try{
    const newLegal = new projectLegalInfoModel(data)
    const result = await newLegal.save()
    return {
      status: true,
      data: result._id
    }
  } catch(e){
    console.log(e)
    return {
      status: false,
      message: e.toString()
    }
  }
}

exports.addProjectLegalInfo = addProjectLegalInfo

const addConsDesignInfo = async data => {
  try{
    const newCOnsDesignInfo = new consDesignInfoModel(data)
    const result = await newCOnsDesignInfo.save()
    return {
      status: true,
      data: result._id
    }
  } catch (e){
    console.log(e)
    return {
      status: false,
      message: 'Add ConsDesign Error'
    }
  }
}
exports.addConsDesignInfo = addConsDesignInfo

exports.updateProjectLegalInfo = async (_id, data) => {
  try{
    if(!data) return { status: true }
    const newProjectLegalInfo = await projectLegalInfoModel.findByIdAndUpdate(_id, {
      $set:{
        ...data
      }
    }, {new: true})
    if(newProjectLegalInfo)
      return {
        status: true,
        data: newProjectLegalInfo._id
      }
    return {
      status: false,
      message: 'Add ConsDesign Error'
    }
  } catch (e){
    console.log(e)
    return {
      status: false,
      message: 'Add ConsDesign Error'
    }
  }
}

exports.updateProjectConsDesign = async (_id, data) => {
  try{
    if(!data) return { status: true }
    const newProjectConsDesign = await consDesignInfoModel.findByIdAndUpdate(_id, {
      $set: {
        ...data
      }
    }, {new: true})
    if(newProjectConsDesign)
      return true
  } catch(e){
    console.log(e)
    return false
  }
}

exports.getParentByCode = async code => {
  try{
    const reg = new RegExp(code,'gi')
    const project = await projectModel.find({$or: [{code: reg}, {name: reg}]}, 'code name')
    return {
      status: true,
      data: project
    }
  } catch (e){
    return {
      status: false,
      message: e.toString()
    }
  }
}

exports.getListSaleStatus = async () => ({
  status: true,
  data: await saleStatusModel.find()
})

exports.addSaleStatus = async (id, name) => {
  try{
       const newSaleStatus = new saleStatusModel({
         create_uid: id,
         name
       })
       await newSaleStatus.save()
       return {
         status: true,
         message: 'Add sale status successfully'
       }
  } catch(e){
    console.log(e)
    return{
      status: false,
      message: e.toString()
    }
  }
}
exports.ListStatus = async () => {
  return await project_status_Model.find({ parent_id: null })
}

exports.getLegalInfoLink = async _id => {
  const legal_info = await projectLegalInfoModel.findOne({ _id })
  if (!legal_info)
    return false
  let legal_infoObj = legal_info.toObject()
  const keys = Object.keys(legal_infoObj);
  let result
  keys.forEach(item => {
    if (legal_info[item]) {
      result = {
        ...result,
        [item]: '',
        [`item_link`]: ''
      }
      if (legal_info[item].status) {
        result[item] = legal_info[item].status;
        result[`${item}_link`] = legal_info[item].file;
      }
    }
  })
  return result
}

exports.getConsDesignInfo = async _id => {
  let cons_design_info = await consDesignInfoModel.findOne({ _id })
  if(!cons_design_info)
    return {}
  cons_design_info = cons_design_info.toObject();
  cons_design_info.detail = {}
  if (!cons_design_info)
    return false
  if(cons_design_info.general_info_of_cons){
  if(cons_design_info.general_info_of_cons.company &&cons_design_info.general_info_of_cons.company.contractor_id)
    cons_design_info.detail.contractor = await res_companyModel.findOne({_id: cons_design_info.general_info_of_cons.company.contractor_id }, 'display_name')
  if(cons_design_info.general_info_of_cons.company &&cons_design_info.general_info_of_cons.company.designer_id)
    cons_design_info.detail.designer = await res_companyModel.findOne({_id: cons_design_info.general_info_of_cons.company.designer_id }, 'display_name')
  if(cons_design_info.general_info_of_cons.company &&cons_design_info.general_info_of_cons.company.manager_id)
    cons_design_info.detail.manager = await res_companyModel.findOne({_id : cons_design_info.general_info_of_cons.company.manager_id }, 'display_name')
  }
  return cons_design_info;
}

exports.getGeneralInfo = async _id =>{
  const result = await projectModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(_id)
      }
    },
    {
      $lookup: {
        from: 'project_category',
        localField: 'category_id',
        foreignField: '_id',
        as: 'category'
      }
    },
    {
      $unwind:{
        path: '$category',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'project',
        let: {parentID: '$parent_id'},
        pipeline:
          [
            {
            $match: {
              $expr: {
                $eq: ['$$parentID', '$_id']
              }
            }
          },{
            $project: {
              name: 1,
            }
          }
          ],
        as: 'parentDetail'
      }
    },
    {
      $unwind:{
        path: '$parentDetail',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'res_company',
        localField: 'investor_id',
        foreignField: '_id',
        as: 'investorDetail'
      }
    },
    {
      $lookup: {
        from: 'assets_planning_category',
        localField: 'land_type',
        foreignField: '_id',
        as: 'landTypeDetail'
      }
    },
    {
      $unwind:{
        path: '$landTypeDetail',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'project_status',
        localField: 'cons_status',
        foreignField: '_id',
        as: 'cons_statusDetail'
      }
    },
    {
      $unwind:{
        path: '$cons_statusDetail',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'sale_status',
        localField: 'sale_status',
        foreignField: '_id',
        as: 'sale_statusDetail'
      }
    },
    {
      $unwind:{
        path: '$sale_statusDetail',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $addFields: {
        category_name: '$category.category_name'
      }
    }
  ])
  return result[0]
}

exports.getLegalInfo = async _id => {
  try{
  const legal_info = await projectLegalInfoModel.findOne({ _id })
  if(legal_info)
    return legal_info.toObject()
  return false
  } catch(e){
    return false
  }
}