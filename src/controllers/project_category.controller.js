var projectCategoryModel = require('../models/project_category.model')
var mongoose = require("mongoose");
var _ = require('lodash')
exports.List = async ()=>{
    return await projectCategoryModel.find({}).sort({sequence:1});
}
exports.Count = async () => {
  return await projectCategoryModel.countDocuments();
}
exports.Add = async (item) => {
  var projectCategory = new projectCategoryModel(item);
  var validate = projectCategory.validateSync();
  var result = { status: false };
  if (!validate) {
    await projectCategory.save().then(document => {
      result = {
        status: true,
        data: document
      };
    }).catch(err => {
      result = {
        status: false,
        message: err.toString()
      };
    })
  } else {
    result = { status: false, message: validate.toString() }
  }
  return result;
}

exports.Detail = async (ID) => {
  return await projectCategoryModel.findOne({ _id: ID })
}

exports.Delete = async ID => {
  var result;
  await projectCategoryModel
    .deleteOne(
      { _id: mongoose.Types.ObjectId(ID) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          data: doc
        };
      } else
        result = {
          status: false
        };
    });
  return result;
};
exports.Edit = async (newCategory = {}) => {
  if (!mongoose.Types.ObjectId.isValid(newCategory._id)) {
    return {
      status: false,
      error: "ID is not valid"
    };
  }
  var result;
  await projectCategoryModel
    .findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(newCategory._id) },
      { $set: _.omit(newCategory, ["_id"]) }
    )
    .then((doc) => {
      if (doc) {
        result = {
          status: true,
          error: doc
        };
      }else
      result = {
        status: false
      };
    });
  return result;
};
