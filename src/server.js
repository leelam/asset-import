require('dotenv').config();
var express = require('express');
var path = require('path');
const fs = require('fs');
const https = require('https');
const http = require('http');
const mongodb = require('mongodb');

var bodyParser = require('body-parser');
var cors = require('cors');
var API_router = require('./router/verson1.router');
var cookieParser = require('cookie-parser');
var authConfig = require('./configs/auth.config');
var app = express();
var socketServer = require('./socket/socker.server');
const asdfdsf = require('./models/res_side.model');
// Configuring the database
const dbConfig = require('./configs/database.config.js');
const mongoose = require('mongoose');
const assert = require('assert');
const { dumpDatabase, restoreDatabase } = require('./mongoHandler');
const { isEmpty } = require('lodash');
mongoose.Promise = global.Promise;

// var privateKey  = fs.readFileSync('./tokenKey/SSLprivate.key', 'utf8');
// var certificate = fs.readFileSync('./tokenKey/csr.crt', 'utf8');

var server = http.createServer(app);
var io = require('socket.io')(server);
app.use(function (req, res, next) {
  req.io = io;
  next();
});
socketServer(io);
console.log(dbConfig);
mongoose
  .connect(dbConfig.url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    auth: { authSource: 'admin' },
    user: dbConfig.user,
    pass: dbConfig.pass
    
  })
  .then(async () => {
    console.log('Successfully connected to the database');
    const allModel = mongoose.modelNames();
    allModel.forEach((model) => {
      const modelInstance = mongoose.model(model);
      const save = modelInstance.prototype.save;
      modelInstance.prototype.save = async function (data = {}) {
        // await redis.del(model);
        // console.log(this, 'asdf');
        let condition = {};
        let filed = [];
        Object.keys(this.schema.tree).map((item) => {
          if (this.schema.tree[item].unique) {
            condition[item] = this[item];
            filed.push(item);
          }
        });
        const keyCondition = Object.keys(condition);
        if (!keyCondition.length) {
          return await save.apply(this, arguments);
        }
        const check = await modelInstance.findOne({ ...condition });
        if (!check && filed.length) {
          return await save.apply(this, arguments);
        }
        Object.assign(this, check);
        Object.assign(this, {
          ...data,
          import: true,
          create_uid: '5dbb9d3b65bb02ebb0ec7fb3',
          write_uid: '5dbb9d3b65bb02ebb0ec7fb3',
          write_date: Date.now(),
        });
        return await save.apply(this, arguments);
      };
    });
    const a = await mongoose.model('res_road').find();
    a.forEach((item) => item.save());

  })
  .catch((err) => {
    console.log(
      'Could not connect to the database. Exiting now...',
      err.toString()
    );
    process.exit();
  });

  //address_new_update 



mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

server.timeout = 60000000;
app.use(cookieParser());
var whitelist = [
  'http://localhost:4000',
  'http://192.168.1.153:4000',
  'http://192.168.1.43:4000',
  'http://192.168.1.156:4000',
];
var corsOptions = {
  origin: whitelist,
  optionsSuccessStatus: 200,
  credentials: true,
};

app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '50mb' })); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
  })
);
app.use(express.static(path.join(__dirname, '../resources')));
app.use('/api/v1', API_router);

server.listen(9009, () => {
  console.log('Server run on port: ' + 9009);
});
// dumpDatabase('project', 'asset_dev');
// restoreDatabase('project', 'asset');
