global.user_online = [];
const authConfig = require('../configs/auth.config');
const fs = require('fs');
const path = require('path');
const { CheckTokenServer } = require('../helpers/token.helper');
const publicTokenKey = fs.readFileSync(
  path.resolve(`${authConfig.tokenPath}/publicToken.key`),
  'utf8'
);
function addUserSocket(list, user_id, socket_id) {
  try {
    var isExist = false;
    for (var i = 0; i < list.length; i++) {
      if (list[i].user_id === user_id) {
        isExist = true;
        if (list[i].socket_id && list[i].socket_id.length) {
          //nếu đã có socket_id với cùng 1 user
          var isSocketIdExist = false;
          for (var j = 0; j < list[i].socket_id.length; j++) {
            if (list[i].socket_id[j] === socket_id) isSocketIdExist = true;
            else list[i].socket_id[j].push(socket_id);
          }
        } else {
          list[i].socket_id = [socket_id];
        }
      }
    }
    if (!isExist) {
      list.push({
        user_id,
        socket_id: [socket_id]
      });
    }
  } catch (err) {}
}
function removeUserSocket(list, socket_id) {
  for (var i = 0; i < list.length; i++) {
    if (list[i].socket_id && list[i].socket_id.length) {
      var isSocketExist = false;
      var socket = list[i].socket_id;
      for (var j = 0; j < socket.length; j++) {
        if (list[i].socket_id[j] === socket_id) {
          // socket_id in list
          if (list[i].socket_id.length > 1) list[i].socket_id.splice(j, 1);
          else list.splice(i, 1);
        }
      }
    }
  }
}

module.exports = io => {
  io.on('connection', socket => {
    global.socket = socket;
    socket.on('disconnect', () => {
      removeUserSocket(user_online, socket.id);
      console.log(global.user_online);
    });
    // socket.on('test', data => console.log(data));
  });
};
