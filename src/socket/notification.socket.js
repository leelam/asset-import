module.exports = {
    pushNotification: (response, io) => {
        try {
            if (response.noti) {
                const { user_online } = global || [];
                for (var i = 0; i < user_online.length; i++) {
                    if ((user_online[i].user_id == response.noti.receiver) && user_online[i].socket_id && user_online[i].socket_id.length) {
                        for (var j = 0; j < user_online[i].socket_id.length; j++) {
                            io.to(user_online[i].socket_id[j]).emit("NOTIFICATION", response.noti)
                        }
                    }
                }
            }
        }catch(err) {
            console.log(err)
        }
    }
}