var express = require("express");
var LoginAPI = require("../API/auth.api");
var ProductAPI = require("../API/product.api");
var PlanningAPI = require("../API/planning.api");
var PostAPI = require("../API/post.api");
var ResAttributeAPI = require("../API/res_attribute.api");
var FileAPI = require("../API/file.api");
var ProjectAPI = require("../API/project.api");
var AskAPI = require("../API/ask.api");
var RecruitAPI = require("../API/recruit.api");
var addressAPI = require("../API/address.api");
var guideAPI = require("../API/guide.api");
var userAPI = require("../API/user.api");
var companyAPI = require("../API/company.api");
var partnerAPI = require("../API/partner.api");
var resourceAPI = require("../API/resource.api");
var commentAPI = require("../API/comment.api");
var roleAPI = require('../API/role.api')
var searchAPI = require('../API/search.api')
// var resourceAPI = require('../API/resource.api')
var commentAPI = require('../API/comment.api')
var reportAPI = require('../API/report.api')
var historyAPI = require('../API/history.api')
var notificationAPI = require('../API/notification.api')
var importDataAPI = require('../API/import data/companycategory')
var importAddress = require('../API/import data/address')
var importJson = require('../API/import data/importJson')
var importKML = require('../API/import data/importKMLFile')
var importExel = require('../API/import data/importExel')
var exportExel = require('../API/import data/exportExel')
var kmlMap = require('../API/import data/kmlmap')
var router = express.Router();
require("../models/res_user.model");
router.use("/post", PostAPI);
router.use("/product", ProductAPI);
router.use("/auth", LoginAPI);
router.use("/planning", PlanningAPI);
router.use("/file", FileAPI);
router.use("/res_attribute", ResAttributeAPI);
router.use("/project", ProjectAPI);
router.use("/ask", AskAPI);
router.use("/recruit", RecruitAPI);
router.use("/address", addressAPI);
router.use("/guide", guideAPI);
router.use("/user", userAPI);
router.use("/company", companyAPI);
router.use("/partner", partnerAPI);
router.use("/resource", resourceAPI);
router.use("/role",roleAPI)
router.use('/notification',notificationAPI)
router.use("/comment", commentAPI);
router.use('/search',searchAPI)
router.use('/resource',resourceAPI)
router.use('/comment',commentAPI)
router.use('/report',reportAPI)
router.use('/import',importDataAPI)
router.use('/import_address', importAddress)
router.use('/import_json', importJson)
router.use('/import_KML', importKML)
router.use('/history', historyAPI)
router.use('/import_exel', importExel)
router.use('/export_exel', exportExel)
router.use('/kml_to_map', kmlMap);
module.exports = router;
