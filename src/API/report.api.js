var express = require('express');
var parseJSON =require('../helpers/safelyParseJSON')
var reportCategoryController = require('../controllers/report_category.controller')
var reportController = require('../controllers/report.controller')
var authenticateMiddleWare = require('../middlewares/authenticate.middleware')
var router = express.Router();

router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await reportCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})
router.get('/category/list_tree',async(req,res)=>{
    return res.json(await reportCategoryController.ListTree())
})
router.get('/category/all',async(req,res)=>{
    return res.json(await reportCategoryController.List());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await reportCategoryController.Count())
})
router.post('/category/add',async(req,res)=>{
    var result = await reportCategoryController.Add(req.body)
   return res.json(result)

    
})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing Post Category ID')
    }
    var postDetail = await reportCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body;
    return res.json(await reportCategoryController.Delete(id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await reportCategoryController.Edit(req.body))
})

// router.post('/add',async(req,res)=>{
//     return res.json(await reportController.Add(req.body))
// })

router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var type = ["APPROVED","PENDING"].includes(req.query.type) ? req.query.type : "PENDING"
    var listPost = await reportController.List(limit,page,filter, null, type);
    return res.json(listPost)
})

router.get('/count',async(req,res)=>{
    let type = req.query.type ? req.query.type : null
    return res.json(await reportController.Count(req.query.filter, type))
})
router.get('/detail',async(req,res)=>{
    return res.json(await reportController.Detail(req.query.id))
})
router.delete('/delete', authenticateMiddleWare,async(req,res)=>{
    return res.json(await reportController.Delete(req.body.id, req.user._id))
})
router.put('/edit', authenticateMiddleWare,async(req,res)=>{
    req.body={...req.body, approved_uid: req.user._id}
    return res.json(await reportController.Edit(req.body))
})
router.put('/edit_sequence',async(req,res)=>{
    return res.json(await reportController.EditSequence(req.body))
})

// router.get('/all',async(req,res)=>{
//     return res.json(await reportCategoryController.AllItem());
// })

module.exports = router;