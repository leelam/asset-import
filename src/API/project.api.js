var express = require('express');
var router = express.Router();
var parseJSON = require('../helpers/safelyParseJSON')
const mongoose = require('mongoose')
var projectStatusController = require('../controllers/project_status.controller')
var projectCategoryController = require('../controllers/project_category.controller')
var projectController = require('../controllers/project.controller')
const authMiddleware = require('../middlewares/authenticate.middleware')
const {uploadFileProject} = require('../middlewares/multer.middleware')
router.get('/count_pie_status',async(req,res)=>{
    return res.json(await projectController.CountPieStatus())
})
router.get('/count_pie_category',async(req,res)=>{
    return res.json(await projectController.CountPieCategory())
})

router.get('/parent/suggestion', async(req, res) => res.json(await projectController.getParentByCode(req.query.keyword)))

router.get('/all',async(req,res)=>{
    return res.json(await projectController.All())
})
router.get('/list', async (req, res) => {
    req.io.emit('test','abc');
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await projectController.List(
        limit,
        page,
        filter,
        parseJSON(req.query.project),
        parseJSON(req.query.sort)
    );
    return res.json(listCategory)
})
router.post('/add',authMiddleware,(req,res,next) =>{
    req.idProject = new mongoose.Types.ObjectId()
    return next();
},uploadFileProject,async(req,res)=>{
    if(!req.files.image || !req.files.sub_image)
    return res.json({
        status: false,
        message: 'Thiếu hình ảnh'
    })
    const {idProject} = req;
    const {_id} = req.user;
    const { general_info, legal_info, description } = req.body
    let cons_design_info = JSON.stringify({});
    if (req.body.cons_design_info) {
        cons_design_info = req.body.cons_design_info;
    }
    const legal_infoObj = JSON.parse(legal_info);
    const keys = Object.keys(legal_infoObj);
    const general_infoObj = JSON.parse(general_info)
    delete general_infoObj._id
    let shapeLegalInfo;
     keys.forEach((item, index) =>{
        shapeLegalInfo = {
           ...shapeLegalInfo,
           [item]: {
               status: '',
               file: null
           }
        }
            shapeLegalInfo[item].status = legal_infoObj[item]
            if(legal_infoObj[item] === 'Có' && req.files[`${item}_link`] ){
                let pathLegal = req.files[`${item}_link`][0].path;
                pathLegal = pathLegal.replace('resources','')
                shapeLegalInfo[item].file = pathLegal
            }
    })
    const addProject = await projectController.Add(
        _id,
        idProject,
        general_infoObj,
        shapeLegalInfo,
        JSON.parse( description),
        JSON.parse(cons_design_info))
    return res.json(addProject)
})
router.get('/count',async(req,res)=>{
    var filter = parseJSON(req.query.filter);
    var countProject = await projectController.Count(filter);
    return res.json(countProject.length ? countProject[0].total : 0)
})
router.get('/detail', async (req, res) => {
    var project = await projectController.Detail(req.query.id)
    return res.json(project.length ? project[0] : {})
})
router.put('/edit',authMiddleware, (req,res,next) =>{
    req.idProject = req.query.idProject
    return next();
},uploadFileProject,async(req,res)=>{
    let { general_info, legal_info, description, cons_design_info } = req.body;
    let general_infoObj = JSON.parse(general_info)
    const legal_infoObj = JSON.parse(legal_info);
    const keys = Object.keys(legal_infoObj);
    let shapeLegalInfo;
   keys.forEach(item =>{
    shapeLegalInfo = {
       ...shapeLegalInfo,
       [item]: {
           status: '',
           file: null
       }
    }
        shapeLegalInfo[item].status = legal_infoObj[item]
        shapeLegalInfo[item].file = legal_infoObj[`${item}_link`]
        if(legal_infoObj[item] === 'Có' && req.files[`${item}_link`] ){
            let pathLegal = req.files[`${item}_link`][0].path;
                    pathLegal = pathLegal.replace('resources','')
                    shapeLegalInfo[item].file = pathLegal
        }
})
    const project = await projectController.Edit(
        _id,
        idProject,
        general_infoObj,
        shapeLegalInfo,
        JSON.parse(description),
        cons_design_infoObj,
    )
    if(project)
        return res.json({
            status: true
        })
    return res.json({
        status: false
    })
})
router.delete('/delete',async(req,res)=>{
    return res.json(await projectController.Delete(req.body.id))
})
//==========Project Category API=================//
router.get('/category/list', async (req, res) => {
    return res.json(await projectCategoryController.List())
})
router.get('/category/count', async (req, res) => {
    return res.json(await projectCategoryController.Count())
})
router.post('/category/add', async (req, res) => {
    return res.json(await projectCategoryController.Add(req.body))
})
router.get('/category/detail', async (req, res) => {
    return res.json(await projectCategoryController.Detail(req.query.id))
})
router.put('/category/edit',async(req,res)=>{
   
    return res.json(await projectCategoryController.Edit(req.body))
})
router.delete('/category/delete', async (req, res) => {
    
    return res.json(await projectCategoryController.Delete(req.body.id))
})

//==========Project Status API=================//
router.get('/status/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await projectStatusController.List(limit, page, filter);
    return res.json(listCategory)
})

router.get('/status/list_tree', async (req, res) => {
    return res.json(await projectStatusController.ListTree());
})

router.get('/status/count', async (req, res) => {
    return res.json(await projectStatusController.Count())
})

router.post('/status/add', async (req, res) => {
    var result = await projectStatusController.Add(req.body)
    return res.json(result)

})
router.get('/status/detail', async (req, res) => {
    var { id } = req.query;
    if (!id) {
        return res.json('Missing Project status ID')
    }
    var postDetail = await projectStatusController.Detail(id);
    return res.json(postDetail.length ? postDetail[0] : {})
})

router.delete('/status/delete', async (req, res) => {
    var { id } = req.body;
    return res.json(await projectStatusController.Delete(id))
})

router.put('/status/edit', async (req, res) => {
    return res.json(await projectStatusController.Edit(req.body))
})

router.get('/test',async(req,res)=>{
    return res.json(await projectController.Test())
})

router.get('/sale_status/get', async (req, res) => res.json(await projectController.getListSaleStatus()))

router.post('/sale_status/add',authMiddleware, async (req, res) => res.json(await projectController.addSaleStatus(req.user._id, req.body.name)))

router.get('/list_status',async (req, res) =>{
    return res.json(await projectController.ListStatus())
})

router.get('/form/get',
authMiddleware,
  async(req, res)=>{
    const idProject = req.query.id;
    if(!idProject)
        return res.json({
            status: false,
            message: 'Thiếu thông tin'
        })
    const project = await projectController.getGeneralInfo(idProject)
    console.log(project)
    if(!project) return res.json({
        status: false,
        message: 'Không tìm thấy thông tin BĐS'
    })
    const legal_info = await projectController.getLegalInfoLink(project.legal_info_id);
    const cons_design_info = await projectController.getConsDesignInfo(project.cons_design_info_id)
    if(!project)
    return res.json({
            status: false,
            message: 'Không tìm thấy bất động sản'
        })
    let result = {
        general_info:{
            ...project
        },
        legal_info: {
            ...legal_info
        },
        description: {
            description: project.description
        },
        cons_design_info: {
            ...cons_design_info
        },
    }
    return res.json({
        status: true,
        data: result
    })
})
module.exports = router;