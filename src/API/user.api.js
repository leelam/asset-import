var express =require('express')
const router = express.Router();
const userController = require('../controllers/user.controller')
const _ = require('lodash')
var authMiddleware = require('../middlewares/authenticate.middleware')
const parseJSON =require('../helpers/safelyParseJSON')
router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listUser = await userController.List(
        limit,
        page,
        filter,
        parseJSON(req.query.project),
        parseJSON(req.query.sort)
    );
    return res.json(listUser)
})
router.post('/add',async(req,res)=>{
    var checkType = await userController.CheckTypeNumber(req.body);
    
    var phoneExist = await userController.CheckPhone(req.body);
    if (phoneExist) {
        return res.json({ message: 'Số điện thoại đã tồn tại', status: false })
    }
    return res.json(await userController.Add(req.body));
})
router.get('/count',async(req,res)=>{
    var filter = parseJSON(req.query.filter);
    var count = await userController.Count(filter);
    return res.json(count)
})
router.get('/detail', async (req, res) => {
    var user = await userController.Detail(req.query.id)
    return res.json(user)
})
router.put('/edit',async(req,res)=>{
    return  res.json(await userController.Edit(req.body))
})
router.delete('/delete',async(req,res)=>{
    return res.json(await userController.Delete(req.body.id))
})
router.get('/suggestion',async(req,res)=>{
    return res.json(await userController.ListSuggestion(req.query.keyword))
})

router.put('/upload_avatar',authMiddleware,async(req,res)=>{
    return res.json(await userController.UploadAvatar(req.user._id,req.body.avatar))
})
router.put('/update',authMiddleware,async(req,res)=>{
    var data = {..._.omit(req.body,["avatar"])};
    data._id = req.user._id;
    return res.json(await userController.Edit(data))
})
router.get('/list_time_zone',async(req,res)=>{
    return res.json(await userController.ListTimeZone())
})
router.get('/list_currency',async(req,res)=>{
    return res.json(await userController.ListCurrency())
})
router.get('/list_lang',async(req,res)=>{
    return res.json(await userController.ListLang())
})
module.exports = router;
