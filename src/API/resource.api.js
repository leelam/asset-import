var express = require('express');
var router = express.Router();
const authMiddleware = require('../middlewares/authenticate.middleware')
const parseJSON = require('../helpers/safelyParseJSON')
var resourceCategoryController = require('../controllers/resource_category.controller');
var resourceController = require('../controllers/resource.controller')

router.get('/category/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    return res.json(await resourceCategoryController.List(limit, page, filter))
})
router.get('/category/list_tree',async(req,res)=>{
    return res.json(await resourceCategoryController.ListTree());
})

router.post('/category/add',authMiddleware ,async (req, res) => {
    return res.json(await resourceCategoryController.Add({...req.body,create_uid:req.user._id}))
})

router.get('/category/count', async (req, res) => {
    return res.json(await resourceCategoryController.Count(null))
})

router.get('/category/detail', async (req, res) => {
    var productCategory = await resourceCategoryController.Detail(req.query.id);
    return res.json(productCategory.length ? productCategory[0] : {});
})

router.put('/category/edit', async (req, res) => {
    return res.json(await resourceCategoryController.Edit(req.body))
})

router.delete('/category/delete', async (req, res) => {
    var { id } = req.body;
    return res.json(await resourceCategoryController.Delete(id))
})

//resource
router.get('/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await resourceController.List(
        limit,
        page,
        filter,
        parseJSON(req.query.project),
        parseJSON(req.query.sort)
    );
    return res.json(listCategory)
})
router.post('/add', async (req, res) => {
    return res.json(await resourceController.Add(req.body))
})
router.get('/count', async (req, res) => {
    var filter = parseJSON(req.query.filter);
    var countProduct = await resourceController.Count(filter);
    return res.json(countProduct.length ? countProduct[0].total : 0)
})

router.put('/edit',async(req,res)=>{
    return res.json(await resourceController.Edit(req.body))
})

router.get('/detail',async(req,res)=>{
    return res.json(await resourceController.Detail(req.query.id))
})
router.delete('/delete',async(req,res)=>{
    return res.json(await resourceController.Delete(req.body._id))
})
module.exports = router;