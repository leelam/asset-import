var express = require('express')
var router = express.Router();
var historyController = require('../controllers/history.controller')

router.get('/detail', async(req, res) =>{
    return res.json(await historyController.Detail(req.query.id))
})

module.exports = router;