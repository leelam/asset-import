var express = require("express");
var searchController = require('../controllers/search.controller')
var router = express.Router();
router.get('/cloud_tag',async(req,res)=>{
    return res.json(await searchController.CloudTag())
})

router.get("/suggestion", async (req, res) => {
    try {
        var limit = parseInt(req.query.limit) || 8;
        const {type} = req.query
        var list_string_search;
        if (req.query.keyword) {
            list_string_search = await searchController.find(req.query.keyword, limit,type);
        } else {
            list_string_search = [];
        }
        return res.json(list_string_search)
    } catch (e) {
        console.log(e)
        return res.json([])
    }

});
module.exports = router;
