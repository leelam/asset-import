var express = require('express');
var router =express.Router();
var addressController = require('../controllers/address.controller');
router.get('/province',async(req,res)=>{
    return res.json(await addressController.Province())
})
router.get('/district',async(req,res)=>{
    return res.json(await addressController.District(req.query.province_id))
})

router.get('/ward',async(req,res)=>{
    return res.json(await addressController.Ward(req.query.district_id))
})
router.get('/country',async(req,res)=>{
    return res.json(await addressController.ListCountry())
})
router.get('/get-location', async(req, res) =>{
    const { ward } = req.query;
    return res.json(await addressController.getAddress(ward))
})

module.exports= router;
