var express = require('express');
var mongoose = require('mongoose');
var resRoadModel = require('../../models/res_road.model');
const {set} = require('lodash')
var excel = require('excel4node');
var workbook = new excel.Workbook();
const { lookupById } = require('../../helpers/lookupBycode.helper')
var router = express.Router();

router.get('/road', async (req, res) => {
  var worksheet = workbook.addWorksheet('Sheet 1');
  let list = [];
  var style = workbook.createStyle({
    font: {
      color: '#FF0800',
      size: 12
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -'
  });

  let result = await resRoadModel.find(
    {},
    { polygons: 0, points: 0, create_uid: 0, cross_ward: 0 }
  );
  const objTree = mongoose.model('res_road').schema.tree;
  const allKeys = Object.keys(objTree);
  allKeys.forEach((item, index) =>
    worksheet
      .cell(0, index + 1)
      .string(item)
      .style(style)
  );
  let item = {}
  for (let i = 1; i <= 1; i++) {
    item = result[i];
    await Promise.all(allKeys.map(async (key, index) => {
      if(objTree[key] && objTree[key].ref){
       const code =  await lookupById(objTree[key].ref, item[key]) 
       console.log(code);
       console.log(key);
       set(item,`${key}`, code)
        console.log(item[key]);
      }
      if (objTree[key].type === Number)
        worksheet
          .cell(i, index + 1)
          .number(item[key])
          .style(style);
      else
        worksheet
          .cell(i, index + 1)
          .string(item[key])
          .style(style);
    }));
  }
  workbook.write('Excel.xlsx');
  res.send(result);
});

module.exports = router;
