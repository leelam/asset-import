var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const path = require('path');
var productModel = require('../../models/product.model');
var resRoadModel = require('../../models/res_road.model');
var resRoadCategoryModel = require('../../models/res_road_category.model');
var resSearchStringModel = require('../../models/res_search_string.model');
var resDistrictModel = require('../../models/res_district.model');
var assetPlanningCategoryModel = require('../../models/asset_planning_category.model');
var productCategoryModel = require('../../models/product_category.model');
var resWardModel = require('../../models/res_ward.model');
var assetPlanningModel = require('../../models/asset_planning.model');
var projectModel = require('../../models/project.model');
const resAddressModel = require('../../models/res_address.model');
const convert = require('html-to-json-data');
var _ = require('lodash');
const { lookupByCode } = require('../../helpers/lookupBycode.helper');
const fs = require('fs');
const {
  group,
  text,
  number,
  href,
  src,
  uniq,
} = require('html-to-json-data/definitions');
var changeAlias = require('../../helpers/change_alias');

var point = (pointStr) => {
  if (_.isString(pointStr)) {
    var pointArr = pointStr.split(',');
    pointArr[0] = parseFloat(pointArr[0]);
    pointArr[1] = parseFloat(pointArr[1]);
    if (pointArr[0] && pointArr[1]) {
      return [pointArr[0], pointArr[1]];
    }
  }
  return null;
};

var polygon = (polygonStr) => {
  if (_.isString(polygonStr)) {
    var polygon = [];
    var polygonArr = polygonStr.split(' ');
    for (var i = 0; i < polygonArr.length; i++) {
      var point = polygonArr[i].split(',');
      point[0] = parseFloat(point[0]);
      point[1] = parseFloat(point[1]);
      if (point[0] && point[1]) polygon.push([point[0], point[1]]);
      else return null;
    }
    return polygon;
  }
  return null;
};

router.get('/asset_planning', async (req, res) => {
  let list = [];
  let listFile = [
    path.resolve('import-file/project/HN.kml'),
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\01\\HUYEN BINH CHANH.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\01\\QUAN 1.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\01\\QUAN 7.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\01\\QUAN 10.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\01\\QUAN TAN PHU.kmz",
    // //
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\02\\BINHTAN.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\02\\GOVAP.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\02\\NHABE.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\02\\PHUNHUAN.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\02\\THUDUC.kmz",
    // //
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\03\\QH_HUYENCUCHI.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\03\\QH_QUAN2.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\03\\QH_QUAN5.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\03\\QH_QUAN11.kmz",
    // //
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_BINHTHANH.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_CANGIO.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_HOCMON.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_QUAN3.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_QUAN6.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_QUAN8.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_QUAN9.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_QUAN12.kmz",
    // "D:\\import data official\\quy hoach\\tp ho chi minh\\04\\QH_TANBINH.kmz",
  ];

  for (let l = 0; l < listFile.length; l++) {
    await parseKMZ
      .toJson(listFile[l])
      .then(async (result) => {
        for (let i = 0; i < result.features.length; i++) {
          let item = result.features[i];
          let newPlanning = {};
          item.properties.description = item_.properties.description.replace(
            /(\r\n|\n|\r)/gm,
            ''
          );
          var json = convert(item.properties.description, text('td'));
          try {
            // newPlanning.points = {
            //     type: 'Point',
            //     coordinates: point(json[json.indexOf('points') + 1])
            // }
            // newPlanning.sequence = json[json.indexOf('sequence') + 1]
            // newPlanning.code = json[json.indexOf('code') + 1]
            // await resWardModel.findOne({ code: json[json.indexOf('ward_id') + 1] })
            //     .then(doc => { newPlanning.ward_id = mongoose.Types.ObjectId(doc._id) })

            // newPlanning.FID = json[json.indexOf('FID') + 1]
            console.log(json);
            // await assetPlanningCategoryModel
            //   .findOne({ code: json[json.indexOf('planing_category_id') + 1] })
            //   .then(doc => {
            //     newPlanning.planning_category_id = mongoose.Types.ObjectId(
            //       doc._id
            //     );
            //   });

            // switch (item.geometry.type) {
            //     case 'Polygon':
            //         item.geometry.coordinates.map(newItem => {
            //             newItem.map(t => {
            //                 t.length = 2
            //             })
            //         })
            //         newPlanning.polygons = item.geometry
            //         break;
            //     case 'GeometryCollection':
            //         newPlanning.polygons = {
            //             type: 'MultiPolygon',
            //             coordinates: []
            //         }
            //         item.geometry.geometries.map(newItem => {
            //             newItem.coordinates.map(newNewItem => {
            //                 newNewItem.map(t => {
            //                     t.length = 2
            //                 })
            //             })
            //             newPlanning.polygons.coordinates.push(newItem.coordinates)
            //         })
            //         break;
            //     default:
            //         break;
            // }
            // newPlanning.import = true;
            // let planningReal = new assetPlanningModel(newPlanning)
            // await planningReal.save()
            // await assetPlanningModel.findOneAndUpdate(
            //   { FID: json[json.indexOf('FID') + 1] },
            //   {
            //     $set: { planning_category_id: newPlanning.planning_category_id }
            //   }
            // );
            // list.push(item)
          } catch (err) {
            list.push(json[json.indexOf('code') + 1] + ' ' + err);
          }
        }
      })
      .catch(console.error);
  }

  return res.send(list);
});

router.get('/res_road', async (req, res) => {
  let list = [];
  await parseKMZ
    .toJson('D:AssetImportassetimportimport-file\res_roadBa Ria_VT.kmz')
    .then(async (result) => {
      for (let i = 0; i < result.features.length; i++) {
        try {
          let newResult = {};
          result.features[i].geometry.coordinates.map((newItem) => {
            newItem.length = 2;
          });
          console.log(result.features[i].geometry);
          newResult.polygons = result.features[i].geometry;
          result.features[i].properties.description = result.features[
            i
          ].properties.description.replace(/(\r\n|\n|\r)/gm, '');
          result.features[i].properties.description = convert(
            result.features[i].properties.description,
            text('td')
          );
          newResult.sequence =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('sequence') + 1
            ];
          newResult.FID =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('FID') + 1
            ];
          newResult.code =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('code') + 1
            ];
          newResult.name =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('name') + 1
            ];
          newResult.points = {
            type: 'Point',
            coordinates: point(
              result.features[i].properties.description[
                result.features[i].properties.description.indexOf('point') + 1
              ]
            ),
          };
          newResult.from_to =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('from_to') + 1
            ];
          newResult.location =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('location') + 1
            ];
          newResult.road_width =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('road_width') +
                1
            ];
          newResult.width_unit_id =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf(
                'width__unit_id'
              ) + 1
            ];
          newResult.original_price =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf(
                'original_price'
              ) + 1
            ];
          newResult.price_unit_id =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf(
                'price_unit_id'
              ) + 1
            ];
          newResult.adj_factor =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('adj_factor') +
                1
            ];
          newResult.factor_unit_id =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf(
                'factor_unit_id'
              ) + 1
            ];
          newResult.refernce =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('refernce') + 1
            ];
          newResult.a_value_unit_id =
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf(
                'avalue_unit_id'
              ) + 1
            ];
          newResult.create_uid = mongoose.Types.ObjectId(
            '5dbb9fbbebd384b5e4ecc368'
          );
          let t = await resDistrictModel.findOne({
            code:
              result.features[i].properties.description[
                result.features[i].properties.description.indexOf(
                  'district_id'
                ) + 1
              ],
          });
          newResult.district_id = mongoose.Types.ObjectId(t._id);
          await resRoadCategoryModel
            .findOne({
              code:
                result.features[i].properties.description[
                  result.features[i].properties.description.indexOf(
                    'category_id'
                  ) + 1
                ],
            })
            .then((res) => {
              newResult.category_id = mongoose.Types.ObjectId(res._id);
            });
          await assetPlanningCategoryModel
            .findOne({
              code:
                result.features[i].properties.description[
                  result.features[i].properties.description.indexOf(
                    'planning_category_id'
                  ) + 1
                ],
            })
            .then((res) => {
              newResult.planning_category_id = mongoose.Types.ObjectId(res._id);
            });
          newResult.import = true;
          if (newResult.road_width == '<Null>') {
            delete newResult.road_width;
          }
          // if(newResult.code === '7917S0067'){
          //     console.log(newResult)
          // }
          let newRoad = new resRoadModel(newResult);
          await newRoad
            .save()
            .then(async (document) => {
              const title = 'Đường ' + document.name;
              let newSearch = new resSearchStringModel({
                sub_title: `${document.from_to}-${document.location}`,
                title,
                search: `${changeAlias(
                  title.toLocaleLowerCase()
                )} ${title.toLocaleLowerCase()}`,
                name: 'res_road',
                rel_id: mongoose.Types.ObjectId(document._id),
                status: true,
                refPath: 'res_road',
                import: true,
              });
              await newSearch.save();
            })
            .catch((err) => list.push(newResult.code + ' ' + err));
        } catch (err) {
          list.push(
            result.features[i].properties.description[
              result.features[i].properties.description.indexOf('code') + 1
            ] +
              ' ' +
              err
          );
        }
        // result.features[i].properties.description = result.features[i].properties.description.replace(/(\r\n|\n|\r)/gm, "");
        // result.features[i].properties.description = convert(result.features[i].properties.description,
        //     text('td')
        // )
        // if (result.features[i].properties.description[result.features[i].properties.description.indexOf('code') + 1] == "<Null>") {
        //     list.push(result.features[i])
        // }
        // list.push(result.features[i])
      }
      return res.send(list);
    })
    .catch(console.error);
});

let iiiii = [
    'D:\\import data official\\ranh thua\\RANHTHUA_BINHTHANH (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_CANGIO (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_HOCMON (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_QUAN3 (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_QUAN6 (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_QUAN12.kmz',
    'D:\\import data official\\ranh thua\\01\\BINH CHANH.kmz',
    'D:\\import data official\\ranh thua\\01\\QUAN 1.kmz',
    'D:\\import data official\\ranh thua\\01\\QUAN 4.kmz',
    'D:\\import data official\\ranh thua\\01\\QUAN 7.kmz',
    'D:\\import data official\\ranh thua\\01\\QUAN 10.kmz',
    'D:\\import data official\\ranh thua\\01\\TAN PHU.kmz',]

router.get('/product', async (req, res) => {
  let list = [];
  let listProduct = [
    // 'D:\\import data official\\ranh thua\\RANHTHUA_BINHTHANH (1).kmz',
    // 'D:\\import data official\\ranh thua\\RANHTHUA_CANGIO (1).kmz',
    // 'D:\\import data official\\ranh thua\\RANHTHUA_HOCMON (1).kmz',
    'D:\\import data official\\ranh thua\\RANHTHUA_QUAN3 (1).kmz',
    // 'D:\\import data official\\ranh thua\\RANHTHUA_QUAN6 (1).kmz',
    // 'D:\\import data official\\ranh thua\\RANHTHUA_QUAN12.kmz',
    // 'D:\\import data official\\ranh thua\\01\\BINH CHANH.kmz',
    // 'D:\\import data official\\ranh thua\\01\\QUAN 1.kmz',
    // 'D:\\import data official\\ranh thua\\01\\QUAN 4.kmz',
    // 'D:\\import data official\\ranh thua\\01\\QUAN 7.kmz',
    // 'D:\\import data official\\ranh thua\\01\\QUAN 10.kmz',
    // 'D:\\import data official\\ranh thua\\01\\TAN PHU.kmz',
    // 'D:\\import data official\\ranh thua\\02\\binhtan.kmz',
    // 'D:\\import data official\\ranh thua\\02\\govap.kmz',
    // 'D:\\import data official\\ranh thua\\02\\phunhuan.kmz',
    // 'D:\\import data official\\ranh thua\\02\\thuduc.kmz',
    // 'D:\\import data official\\ranh thua\\03\\CU CHI.kmz',
    // 'D:\\import data official\\ranh thua\\03\\QUAN 2.kmz',
    // 'D:\\import data official\\ranh thua\\03\\QUAN 8.kmz',
    // 'D:\\import data official\\ranh thua\\03\\QUAN 5.kmz',
    // 'D:\\import data official\\ranh thua\\03\\QUAN 11.kmz',
    // 'D:\\import data official\\ranh thua\\03\\TAN BINH.kmz'
  ];
  parseKMZ
    .toJson(path.resolve('import-file/RANHTHUA_QUAN3 (1).kmz'))
    .then(async (result) => {
      for (let i = 0; i < 4; i++) {
        let item = result.features[i];
        let newProduct = {};
        try {
          item.properties.description = item.properties.description.replace(
            /(\r\n|\n|\r)/gm,
            ''
          );
          item.properties.description = convert(
            item.properties.description,
            text('td')
          );
          newProduct.points = {
            type: 'Point',
            coordinates: point(
              item.properties.description[
                item.properties.description.indexOf('points') + 1
              ]
            ),
          };
          newProduct.polygons = item.geometry;
          newProduct.type_id = mongoose.Types.ObjectId(
            '5d6e57310b4d9fd73db02be9'
          );
          newProduct.FID =
            item.properties.description[
              item.properties.description.indexOf('FID') + 1
            ];
          newProduct.code =
            item.properties.description[
              item.properties.description.indexOf('code') + 1
            ];
          newProduct.name =
            item.properties.description[
              item.properties.description.indexOf('name') + 1
            ];
          newProduct.land_area =
            item.properties.description[
              item.properties.description.indexOf('land_area') + 1
            ];
          newProduct.land_area_unit_id = 'm2';
          newProduct.planning_area =
            item.properties.description[
              item.properties.description.indexOf('planning_area') + 1
            ];
          newProduct.planning_area_unit_id = 'm2';
          newProduct.lost_area =
            item.properties.description[
              item.properties.description.indexOf('lost_area') + 1
            ];
          newProduct.lost_area_unit_id = 'm2';
          newProduct.number_of_sheet =
            item.properties.description[
              item.properties.description.indexOf('number_of_sheet') + 1
            ];
          newProduct.number_of_parcel =
            item.properties.description[
              item.properties.description.indexOf('number_of_parcel') + 1
            ];
          newProduct.investor_id = '';
          newProduct.create_uid = mongoose.Types.ObjectId(
            '5dbb9fbbebd384b5e4ecc368'
          );
          newProduct.import = true;

          newProduct.category_id =
            item.properties.description[
              item.properties.description.indexOf('category_id') + 1
            ];
          newProduct.planning_category_id =
            item.properties.description[
              item.properties.description.indexOf('planning_category_id') + 1
            ];
          newProduct.ward_id =
            item.properties.description[
              item.properties.description.indexOf('ward_id') + 1
            ];
          console.log(item.properties.description);
          let keys = Object.keys(newProduct);
          for (let j = 0; j < keys.length; j++) {
            if (
              newProduct[keys[j]] === '<Null>' ||
              newProduct[keys[j]] === ''
            ) {
              delete newProduct[keys[j]];
            }
          }
          if (newProduct.category_id)
            await productCategoryModel
              .findOne({
                code:
                  item.properties.description[
                    item.properties.description.indexOf('category_id') + 1
                  ],
              })
              .then((res) => {
                newProduct.category_id = mongoose.Types.ObjectId(res._id);
              });

          if (newProduct.planning_category_id)
            await assetPlanningCategoryModel
              .findOne({
                code:
                  item.properties.description[
                    item.properties.description.indexOf(
                      'planning_category_id'
                    ) + 1
                  ],
              })
              .then((res) => {
                newProduct.planning_category_id = mongoose.Types.ObjectId(
                  res._id
                );
              });

          if (newProduct.ward_id)
            await resWardModel
              .findOne({
                code:
                  item.properties.description[
                    item.properties.description.indexOf('ward_id') + 1
                  ],
              })
              .then((res) => {
                newProduct.ward_id = mongoose.Types.ObjectId(res._id);
              });

          switch (item.geometry.type) {
            case 'Polygon':
              item.geometry.coordinates.map((newItem) => {
                newItem.map((t) => {
                  t.length = 2;
                });
              });
              break;
            case 'GeometryCollection':
              newProduct.polygons = {
                type: 'MultiPolygon',
                coordinates: [],
              };
              item.geometry.geometries.map((newItem) => {
                newItem.coordinates.map((newNewItem) => {
                  newNewItem.map((t) => {
                    t.length = 2;
                  });
                });
                newProduct.polygons.coordinates.push(newItem.coordinates);
              });
              break;
            default:
              console.log(
                'loi dinh dang',
                item.properties.description[
                  item.properties.description.indexOf('FID') + 1
                ]
              );
              break;
          }
          let t = new productModel(newProduct);
          if (t.handover_time) list.push(t);
          // await t.save().then(doc => { }).catch(err => {
          //     console.log(err)
          // })
          // await productModel.findOneAndUpdate({FID: newProduct.FID}, {$set: {planning_category_id: newProduct.planning_category_id}})
        } catch (err) {
          list.push(
            item.properties.description[
              item.properties.description.indexOf('FID') + 1
            ] +
              ' ' +
              err
          );
        }
      }
      return res.send(list);
    })
    .catch(console.error);
});

router.get('/update_polygon_project', async (req, res) => {
  let list = [];
  await parseKMZ
    .toJson(
      'D:\\import data official\\du an\\import du an Fuji Residence\\fuji_ehome4.kmz'
    )
    .then(async (result) => {
      for (let i = 0; i < result.features.length; i++) {
        let item = result.features[i];
        try {
          let newItem = {};
          // newItem.points = {
          //     type: 'Point',
          //     coordinates: point(item.properties.description[item.properties.description.indexOf('points') + 1])
          // }
          switch (item.geometry.type) {
            case 'Polygon':
              item.geometry.coordinates.map((newItem) => {
                newItem.map((t) => {
                  t.length = 2;
                });
              });
              newItem.polygons = item.geometry;
              break;
            case 'GeometryCollection':
              newItem.polygons = {
                type: 'MultiPolygon',
                coordinates: [],
              };
              item.geometry.geometries.map((newItem1) => {
                newItem1.coordinates.map((newNewItem) => {
                  newNewItem.map((t) => {
                    t.length = 2;
                  });
                });
                newItem.polygons.coordinates.push(newItem1.coordinates);
              });
              break;
            default:
              console.log(
                'loi dinh dang',
                item.properties.description[
                  item.properties.description.indexOf('FID') + 1
                ]
              );
              break;
          }
          await projectModel.findOneAndUpdate(
            { code: item.properties.name },
            { $set: { polygons: newItem.polygons } }
          );
          list.push(newItem);
        } catch (error) {
          console.log(error);
        }
      }
      res.send(list);
    })
    .catch((err) => {
      console.log(err);
    });
});

router.get('/kmlFile', async (req, res) => {
  const tj = require('@tmcw/togeojson');
  const { fileName, model } = req.query;
  const dirPath = path.resolve(`import-file/${model}/${fileName}`);
  const DOMParser = require('xmldom').DOMParser;
  const kml = new DOMParser().parseFromString(fs.readFileSync(dirPath, 'utf8'));
  const converted = tj.kml(kml);
  const length = converted.features.length;
  global.socket.emit('send-length', length);
  const tree = mongoose.model(model).schema.tree;
  let countError = 0;
  for (let i = 0; i < converted.features.length; i++) {
    const data = {};
    try {
      const item = converted.features[i];
      let polygons = {
        type: '',
        coordinates: [],
      };
      item.properties.description = item.properties.description.replace(
        /(\r\n|\n|\r)/gm,
        ''
      );
      var json = convert(item.properties.description, text('td'));
      data.code = json[json.indexOf('code') + 1];
      data.points = {
        type: 'Point',
        coordinates: json[json.indexOf('points') + 1].split(',').map((item) => {
          if (isNaN(item) || !item){
            console.log(json)
            throw new Error(
              `Point không phải số. code:${data.code}. Point: ${
                json[json.indexOf('points') + 1]
              }`
            );
          }
          return parseFloat(item);
        }),
      };
      if (item.geometry) {
        if (
          item.geometry.type !== 'Polygon' &&
          item.geometry.type !== 'LineString'
        ) {
          polygons.type = 'MultiPolygon';
          const polygon = item.geometry.geometries.map((geo) => {
            if (
              geo.coordinates[0] &&
              geo.coordinates[0][0] &&
              geo.coordinates[0][0][0]
            )
              return geo.coordinates[0].map((coor) => {
                if (isNaN(coor[0]) || isNaN(coor[1])) {
                  throw new Error(`Polygon không phải số. code: ${data.code}`);
                }
                return [coor[0], coor[1]];
              });
            else {
              console.log('vp day')
              polygons.type = 'MultiLineString';
              return geo.coordinates.map((coor) => [coor[0], coor[1]]);
            }
          });
          polygons.coordinates = polygon;
        } else if (item.geometry.type === 'Polygon') {
          const polygon = item.geometry.coordinates.map((item) =>
            item.map((coor) => {
              if (isNaN(coor[0]) || isNaN(coor[1])) {
                throw new Error(`Polygon không phải số. code: ${data.code}`);
              }
              return [coor[0], coor[1]];
            })
          );
          polygons.type = item.geometry.type;
          polygons.coordinates = polygon;
        } else {
          const polygon = item.geometry.coordinates.map((coor) => {
            if (isNaN(coor[0]) || isNaN(coor[1]))
              throw new Error(`LineString không phải số. code: ${data.code}`);
            return [coor[0], coor[1]];
          });
          polygons.type = item.geometry.type;
          polygons.coordinates = polygon;
        }
      } else {
      }

      data.polygons = polygons;
      json.shift();
      json.shift();
      json.forEach((item, index) => {
        if (
          item === 'points' ||
          item === 'polygons' ||
          json[index - 1] === 'points' ||
          json[index - 1] === 'polygons'
        )
          return;
        if (index % 2 === 0) data[item] = '';
        else data[json[index - 1]] = item;
      });
      const listKeyData = Object.keys(data);
      const a = await Promise.all(
        listKeyData.map(async (item) => {
          if (
            tree[item] &&
            (tree[item].ref || (tree[item][0] && tree[item][0].ref))
          ) {
            if (tree[item][0]) {
              const value = data[item].split(';');
              data[item] = [];
              await Promise.all(
                value.map(async (child) => {
                  const result = await lookupByCode(
                    tree[item][0].ref,
                    child,
                    data.fid,
                    item,
                    data.code,
                    tree[item][0].require
                  );
                  data[item].push(result);
                })
              );
            } else {
              data[item] = await lookupByCode(
                tree[item].ref,
                data[item],
                data.fid,
                item,
                data.code,
                tree[item].require
              );
            }
          }
          return;
        })
      );
      if(data.length === '<Null>'){
        console.log('heeheheheheh');
        data.length = ''
      }
      if(data.width === '<Null>'){
        console.log('heeheheheheh');
        data.width = ''
      }
      let sup_id;
      if (data.code) {
        if (model === 'res_polygon') {
          if (data.code.length === 2) {
            data.collection_name = 'res_province';
            data.typeL = 'C';
          }
          if (data.code.length === 4) {
            data.collection_name = 'res_district';
            data.parent_collection = 'res_province'
            data.typeL = 'D';
          }
          if (data.code.length === 6) {
            data.collection_name = 'res_ward';
            data.parent_collection = 'res_district'
            data.typeL = 'W';
          }
          const rel_id = await mongoose.model(data.collection_name).findOne({code: data.code})
          data.rel_id = rel_id._id
        }
        if (model !== 'res_polygon') {
          const oldDoc = await mongoose.model(model).findOne({code: data.code})
          if (oldDoc) {
            sup_id = [oldDoc._id];
            if (model === 'res_district') {
              await mongoose.model(model).updateOne({code: data.code}, {$set: data})
            } else {
              Object.assign(oldDoc, data);
              await oldDoc.save()
            }
          } else {
            const newDoc = new mongoose.model(model)(data);
            sup_id = [newDoc._id];
            await newDoc.save(data);
          }
        }
      } else {
        const newDoc = new mongoose.model(model)(data);
        await newDoc.save(data);
      }
      if (model === 'res_polygon') {
        const oldAddress = await mongoose.model('res_address_new').findOneAndUpdate({code: data.code}, {
          $set: {
            polygon_id: sup_id,
            type: data.typeL,
            points: data.points,
            address_name: data.name,
          }
        })
        console.log(oldAddress);
        if (!oldAddress) {
          let parent_id = null;
          if (data.code > 2) {
            const parent = await mongoose.model(data.parent_collection).findOne({code: data.code.slice(0, data.code.length -2)})
            parent_id = parent._id ? parent._id : 0;
          }    
          const newDoc = new mongoose.model('res_address_new')({
            polygon_id: sup_id,
            type: data.typeL,
            points: data.points,
            code: data.code,
            address_name: data.name,
            parent_id: parent_id,
          });
          await newDoc.save()
        }
      }
      global.socket.emit('send-import-detail', i);
    } catch (e) {
      countError++;
      if (countError > 20) {
        global.socket.emit('send-error', e.toString() + `code: ${data.code}`);
        return res.json({
          status: false,
          message: 'Import false',
        });
      }
      console.log(e, data);
    }
  }
  res.json(true);
});

const imPortByFile = async (model, fileName, folderDir) => {
  const tj = require('@tmcw/togeojson');
  const dirPath = path.resolve(`${folderDir}/${fileName}`);
  const DOMParser = require('xmldom').DOMParser;
  const kml = new DOMParser().parseFromString(fs.readFileSync(dirPath, 'utf8'));
  const converted = tj.kml(kml);
  const length = converted.features.length;
  const tree = mongoose.model(model).schema.tree;
  let countError = 0;
  for (let i = 0; i < converted.features.length; i++) {
    const data = {};
    try {
      const item = converted.features[i];
      let polygons = {
        type: '',
        coordinates: [],
      };
      item.properties.description = item.properties.description.replace(
        /(\r\n|\n|\r)/gm,
        ''
      );
      var json = convert(item.properties.description, text('td'));
      data.code = json[json.indexOf('code') + 1];
      data.points = {
        type: 'Point',
        coordinates: json[json.indexOf('points') + 1].split(',').map((item) => {
          if (isNaN(item) || !item){
            console.log(json)
            throw new Error(
              `Point không phải số. code:${data.code}. Point: ${
                json[json.indexOf('points') + 1]
              }`
            );
          }
          return parseFloat(item);
        }),
      };
      if (item.geometry) {
        if (
          item.geometry.type !== 'Polygon' &&
          item.geometry.type !== 'LineString'
        ) {
          polygons.type = 'MultiPolygon';
          const polygon = item.geometry.geometries.map((geo) => {
            if (
              geo.coordinates[0] &&
              geo.coordinates[0][0] &&
              geo.coordinates[0][0][0]
            )
              return geo.coordinates[0].map((coor) => {
                if (isNaN(coor[0]) || isNaN(coor[1])) {
                  throw new Error(`Polygon không phải số. code: ${data.code}`);
                }
                return [coor[0], coor[1]];
              });
            else {
              console.log('vp day')
              polygons.type = 'MultiLineString';
              return geo.coordinates.map((coor) => [coor[0], coor[1]]);
            }
          });
          polygons.coordinates = polygon;
        } else if (item.geometry.type === 'Polygon') {
          const polygon = item.geometry.coordinates.map((item) =>
            item.map((coor) => {
              if (isNaN(coor[0]) || isNaN(coor[1])) {
                throw new Error(`Polygon không phải số. code: ${data.code}`);
              }
              return [coor[0], coor[1]];
            })
          );
          polygons.type = item.geometry.type;
          polygons.coordinates = polygon;
        } else {
          const polygon = item.geometry.coordinates.map((coor) => {
            if (isNaN(coor[0]) || isNaN(coor[1]))
              throw new Error(`LineString không phải số. code: ${data.code}`);
            return [coor[0], coor[1]];
          });
          polygons.type = item.geometry.type;
          polygons.coordinates = polygon;
        }
      } else {
      }

      data.polygons = polygons;
      json.shift();
      json.shift();
      json.forEach((item, index) => {
        if (
          item === 'points' ||
          item === 'polygons' ||
          json[index - 1] === 'points' ||
          json[index - 1] === 'polygons'
        )
          return;
        if (index % 2 === 0) data[item] = '';
        else data[json[index - 1]] = item;
      });
      const listKeyData = Object.keys(data);
      const a = await Promise.all(
        listKeyData.map(async (item) => {
          if (
            tree[item] &&
            (tree[item].ref || (tree[item][0] && tree[item][0].ref))
          ) {
            if (tree[item][0]) {
              const value = data[item].split(';');
              data[item] = [];
              await Promise.all(
                value.map(async (child) => {
                  const result = await lookupByCode(
                    tree[item][0].ref,
                    child,
                    data.fid,
                    item,
                    data.code,
                    tree[item][0].require
                  );
                  data[item].push(result);
                })
              );
            } else {
              data[item] = await lookupByCode(
                tree[item].ref,
                data[item],
                data.fid,
                item,
                data.code,
                tree[item].require
              );
            }
          }
          return;
        })
      );
      let sup_id = [];
      if (data.code) {
        console.log(`"${data.code}",`);
        if (model === 'res_polygon') {
          if (data.code.length === 2) {
            data.collection_name = 'res_province';
            data.typeL = 'C';
          }
          if (data.code.length === 4) {
            data.collection_name = 'res_district';
            data.parent_collection = 'res_province'
            data.typeL = 'D';
          }
          if (data.code.length === 6) {
            data.collection_name = 'res_ward';
            data.parent_collection = 'res_district'
            data.typeL = 'W';
          }
          const rel_id = await mongoose.model(data.collection_name).findOne({code: data.code})
          data.rel_id = rel_id._id
        }
        if (model !== 'res_polygon') {
          const oldDoc = await mongoose.model(model).findOne({code: data.code})
          if (oldDoc) {
            if (model === 'res_district') {
              await mongoose.model(model).updateOne({code: data.code}, {$set: data})
            } else {
              Object.assign(oldDoc, data);
              await oldDoc.save()
            }
          } else {
            const newDoc = new mongoose.model(model)(data);
            await newDoc.save(data);
          }
        } else {
          if (data.polygons.type === 'Polygon') {
            // console.log(data.code,data.polygons.type,data.polygons.coordinates);
            // const oldDoc = await mongoose.model(model).findOne({code: data.code})
            // const oldPolygon = oldDoc.toObject().polygons;
            // console.log(oldPolygon.coordinates);
            // if (JSON.stringify(oldPolygon.coordinates) === JSON.stringify(data.polygons.coordinates)) {
            //   console.log('hehe');
            // }
            const newPolygon = new mongoose.model(model)(data);
            await newPolygon.save(data);
            sup_id.push(newPolygon._id);
            // console.log(sup_id);
          }
          else {
            data.polygons.coordinates.forEach( async (item) => {
              // console.log(item);
              data.polygons = {
                type: "Polygon",
                coordinates: [item]
              }
              const newPolygon = new mongoose.model(model)(data);
              sup_id.push(newPolygon._id);
              // console.log(data.polygons.type);
              await newPolygon.save(data);
              // console.log(newPolygon._id);
              
            })
          }
        }
      } else {
        const newDoc = new mongoose.model(model)(data);
        await newDoc.save(data);
      }
      // console.log(data.code,data.polygons.coordinates);
      if (model === 'res_polygon') {
        // console.log(sup_id);
        const oldAddress = await mongoose.model('res_address_new').findOneAndUpdate({code: data.code}, {
          $set: {
            polygon_id: sup_id,
            type: data.typeL,
            points: data.points,
            address_name: data.name,
          }
        })
        // console.log(oldAddress);
        if (!oldAddress) {
          let parent_id = null;
          if (data.code > 2) {
            const parent = await mongoose.model(data.parent_collection).findOne({code: data.code.slice(0, data.code.length -2)})
            parent_id = parent._id ? parent._id : 0;
          }    
          const newDoc = new mongoose.model('res_address_new')({
            polygon_id: sup_id,
            type: data.typeL,
            points: data.points,
            code: data.code,
            address_name: data.name,
            parent_id: parent_id,
          });
          await newDoc.save()
        }
        // console.log(data.code,data.polygons.type,data.polygons.coordinates);
      }
    } catch (e) {
      countError++;
      if (countError > 20) {
        return res.json({
          status: false,
          message: 'Import false',
        });
      }
      console.log(e, data);
    }
  }
}
const folderDir =  path.resolve(`import-file/res_district`);

const getAllfile = async (folderDir, model) => {
  const x = fs.readdirSync(folderDir);
  console.log(x);
  x.forEach((item) => {
    imPortByFile(model, item, folderDir);
  })
}

getAllfile(folderDir, 'res_polygon')

const getTree = async (model) => {
  // if (model) {
  //   return
  // }
  const tree = await mongoose.model(model).schema.tree;
  // console.log(tree);
} 
getTree('res_ward')

module.exports = router;
