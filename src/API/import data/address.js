var express = require('express')
var router = express.Router()
var xlsx = require('xlsx')
var mongoose = require('mongoose')
var provinceModel = require('../../models/res_province.model')
var districtModel = require('../../models/res_district.model')
var wardModel = require('../../models/res_ward.model')
var resPolygonModel = require('../../models/res_polygon.model')
// var t = require('./VN_TINH.json')
var q = require('./VN_QUAN.json')
var p = require('./VN_PHUONG.json')
var fs = require('fs')
var _ = require('lodash')

var point = pointStr => {
    if (_.isString(pointStr)) {
        var pointArr = pointStr.split(",");

        pointArr[0] = parseFloat(pointArr[0]);
        pointArr[1] = parseFloat(pointArr[1]);
        if (pointArr[0] && pointArr[1]) {
            return [pointArr[0], pointArr[1]];
        }
    }
    return null;
};

var polygon = polygonStr => {
    if (_.isString(polygonStr)) {
        var polygon = []
        var polygonArr = polygonStr.split(' ');
        for (var i = 0; i < polygonArr.length; i++) {
            var point = polygonArr[i].split(',');
            point[0] = parseFloat(point[0])
            point[1] = parseFloat(point[1])
            if (point[0] && point[1]) polygon.push([point[0], point[1]])
            else return null;
        }
        return polygon
    }
    return null;
};

router.get('/test', async (req, res) => {
    let list = []
    p.features.map(item => {
        if (item.attributes.CODETONG == '010101') {
            list.push(item)
        }
    })
    res.send(list)
})

router.get('/ttt', async (req, res) => {
    // t.features.map(item => {
    //     if (item.attributes.code == '38') {
    //         let t = {t: item.geometry.rings.length}
    //         return res.send(t)
    //     }
    // })
    // res.send(t.features[0])

    let result = await provinceModel.find({}, { code: 1 })
    result.map(async item => {
        let n = new resPolygonModel({
            rel_id: mongoose.Types.ObjectId(item._id),
            code: item.code,
            collection_name: 'res_province'
        })
        await n.save().then(document => { }).catch(e => console.log(e))
    })
    res.send('ok')
})

router.get('/tinh_point', async (req, res) => {
    let province = await provinceModel.find({}, { code: 1 })
    let list = []
    province.map(item => {
        t.features.map(async newItem => {
            if (newItem.attributes.code.toString() === item.code) {
                list.push(newItem)
                fs.appendFile('./demo.txt', item, 'utf8', (err) => {
                    if (err) throw err;
                    console.log('saved')
                })
                let newPoint = {
                    type: 'Point',
                    coordinates: point(newItem.attributes.points)
                }
                await provinceModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(item._id) }, { $set: { points: newPoint } })
            }
        })
    })
    res.send(list)
})

router.get('/tinh_thieu_point', async (req, res) => {
    let province = await provinceModel.find({})
    let list = []
    province.map(item => {
        if (item.points === undefined) {
            list.push(item)
        }
    })

    res.send(list)
})

router.get('/tinh_polygon', async (req, res) => {
    let resPolygon = await resPolygonModel.find({ collection_name: 'res_province' }, { code: 1 })
    var list = []
    resPolygon.map(item => {
        try {
            t.features.map(async newItem => {
                if (newItem.attributes.code === item.code) {
                    if (newItem.geometry.rings.length > 1) {
                        let main_polygon = await provinceModel.findOne({ code: newItem.attributes.code, collection_name: 'res_province' }, { code: 1, _id: 1 })
                        let rel_id = mongoose.Types.ObjectId(main_polygon._id), code = main_polygon.code

                        for (let i = 0; i < newItem.geometry.rings.length; i++) {
                            let polygon = {
                                type: 'Polygon',
                                coordinates: [newItem.geometry.rings[i]]
                            }
                            if (i == 0) {
                                await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.code, collection_name: 'res_province' }, { $set: { polygons: polygon } })
                            } else {
                                let newResPolygon = new resPolygonModel({
                                    code: code,
                                    polygons: polygon,
                                    rel_id: rel_id,
                                    collection_name: 'res_province'
                                })
                                await newResPolygon.save().then(document => { })
                                    .catch(err => {
                                        newCode = `ma = ${code} + ${err}, `
                                        fs.appendFile('./demo.txt', newCode, 'utf8', (e) => {
                                            if (e) throw e;
                                        })
                                    })
                            }
                        }
                    } else {
                        let polygon = {
                            type: 'Polygon',
                            coordinates: [newItem.geometry.rings[0]]
                        }
                        await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.code, collection_name: 'res_province' }, { $set: { polygons: polygon } })
                    }
                }
            })
        } catch (e) {

        }
    })
    return res.send(list)
})

router.get('/quan_point', async (req, res) => {
    let district = await districtModel.find({}, { code: 1 })
    let list = []
    district.map(item => {
        q.features.map(async newItem => {
            if (newItem.attributes.CODE_QUAN.toString() === '7902') {
                list.push(newItem)
                let newPoint = {
                    type: 'Point',
                    coordinates: point(newItem.attributes.points)
                }
                let newDistrict = new districtModel({
                    create_date: Date.now(),
                    write_date: Date.now(),
                    company_id: 'ROOT',
                    status: true,
                    code: '7902',
                    name: 'Quận Bình Tân',
                    type: 'Quận',
                    province_id: mongoose.Types.ObjectId("5dc0cfc6bf871b3a3c6296bd"),
                    points: newPoint
                })
                await newDistrict.save()
                // await districtModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(item._id) }, { $set: { points: newPoint } })
                //     .then(document => { }).catch(err => {
                //         let loi = item.code + err + ','
                //         fs.appendFile('./quan.txt', loi, 'utf8', (err) => {
                //             if (err) throw err;
                //             console.log('saved')
                //         })
                //     })
            }
            // if(item.code === '0801' && newItem.attributes.CODE_QUAN.toString() === item.code){
            //     let newPoint = {
            //         type: 'Point',
            //         coordinates: point(newItem.attributes.points)
            //     }
            //     await districtModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(item._id) }, { $set: { points: newPoint } })
            //         .then(document => { }).catch(err => {
            //             let loi = item.code + err + ','
            //             fs.appendFile('./quan.txt', loi, 'utf8', (err) => {
            //                 if (err) throw err;
            //                 console.log('saved')
            //             })
            //         })
            //     console.log(newPoint)
            // }
        })
    })
    return res.send(list)
})


router.get('/quan_polygon', async (req, res) => {
    let resPolygon = await resPolygonModel.find({ collection_name: 'res_district', rel_id: mongoose.Types.ObjectId("5de0a277bdcc5e2ae0d9cac0") }, { code: 1 })
    var list = []
    resPolygon.map(item => {
        try {
            q.features.map(async newItem => {
                if (newItem.attributes.CODE_QUAN === item.code) {
                    if (newItem.geometry.rings.length > 1) {
                        let main_polygon = await resPolygonModel.findOne({ code: newItem.attributes.CODE_QUAN, collection_name: 'res_district' }, { code: 1, _id: 1 })
                        let rel_id = mongoose.Types.ObjectId(main_polygon._id), code = main_polygon.code

                        for (let i = 0; i < newItem.geometry.rings.length; i++) {
                            let polygon = {
                                type: 'Polygon',
                                coordinates: [newItem.geometry.rings[i]]
                            }
                            if (i == 0) {
                                await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.CODE_QUAN, collection_name: 'res_district' }, { $set: { polygons: polygon } })
                            } else {
                                let newResPolygon = new resPolygonModel({
                                    code: code,
                                    polygons: polygon,
                                    rel_id: rel_id,
                                    collection_name: 'res_district'
                                })
                                list.push(newResPolygon)
                                await newResPolygon.save().then(document => { })
                                    .catch(err => {
                                        newCode = `ma = ${code} + ${err}, `
                                        fs.appendFile('./quan.txt', newCode, 'utf8', (e) => {
                                            if (e) throw e;
                                        })
                                    })
                            }
                        }
                    } else {
                        let polygon = {
                            type: 'Polygon',
                            coordinates: [newItem.geometry.rings[0]]
                        }
                        list.push(polygon)
                        await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.CODE_QUAN, collection_name: 'res_district' }, { $set: { polygons: polygon } }).then(document => { })
                            .catch(err => {
                                newCode = `ma = ${code} + ${err}, `
                                fs.appendFile('./quan.txt', newCode, 'utf8', (e) => {
                                    if (e) throw e;
                                })
                            })
                    }
                }
            })
        } catch (e) {
            console.log(e)
        }
    })

    return res.send(resPolygon)
})

router.get('/import_district_polygo', async (req, res) => {
    let result = await districtModel.find({}, { _id: 1, code: 1 })
    result.map(async item => {
        let n = new resPolygonModel({
            rel_id: mongoose.Types.ObjectId(item._id),
            code: item.code,
            collection_name: 'res_district'
        })
        await n.save()
    })

    return res.send(result)
})

router.get('/phuong_point', async (req, res) => {
    let ward = await wardModel.find({district_id: mongoose.Types.ObjectId("5de0a277bdcc5e2ae0d9cac0")}, { code: 1 })
    // let list = []
    ward.map(item => {
        p.features.map(async newItem => {
            if (newItem.attributes.CODETONG.toString() === item.code) {
                let newPoint = {
                    type: 'Point',
                    coordinates: point(newItem.attributes.POINTS)
                }

                await wardModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(item._id) }, { $set: { points: newPoint } })
                    .then(async document => { await console.log(document) }).catch(err => {
                        console.log(err)
                        let loi = item.code + err + ','
                        fs.appendFile('./quan.txt', loi, 'utf8', (err) => {
                            if (err) throw err;
                            console.log('saved')
                        })
                    })
            }
        })
    })
    return res.send(ward)
})


router.get('/phuong_polygon', async (req, res) => {
    let resPolygon = await resPolygonModel.find({ rel_id: {$in: [mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5cb"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5cd"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5cf"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5d1"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5d3"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5d5"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5d7"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5d9"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5db"), mongoose.Types.ObjectId("5de0c9deefc45b2bc40ac5dd")]} }, { code: 1 })
    var list = []
    resPolygon.map(item => {
        try {
            p.features.map(async newItem => {
                if (newItem.attributes.CODETONG === item.code) {
                    if (newItem.geometry.rings.length > 1) {
                        let main_polygon = await resPolygonModel.findOne({ code: newItem.attributes.CODETONG, collection_name: 'res_ward' }, { code: 1, _id: 1 })
                        let rel_id = mongoose.Types.ObjectId(main_polygon._id), code = main_polygon.code

                        for (let i = 0; i < newItem.geometry.rings.length; i++) {
                            let polygon = {
                                type: 'Polygon',
                                coordinates: [newItem.geometry.rings[i]]
                            }
                            if (i == 0) {
                                await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.CODETONG, collection_name: 'res_ward' }, { $set: { polygons: polygon } })
                            } else {
                                let newResPolygon = new resPolygonModel({
                                    code: code,
                                    polygons: polygon,
                                    rel_id: rel_id,
                                    collection_name: 'res_ward'
                                })
                                list.push(newResPolygon)
                                await newResPolygon.save().then(async document => { await console.log(document) })
                                    .catch(err => {
                                        newCode = `ma = ${code} + ${err}, `
                                        fs.appendFile('./phuong.txt', newCode, 'utf8', (e) => {
                                            if (e) throw e;
                                        })
                                    })
                            }
                        }
                    } else {
                        let polygon = {
                            type: 'Polygon',
                            coordinates: [newItem.geometry.rings[0]]
                        }
                        list.push(polygon)
                        await resPolygonModel.findOneAndUpdate({ code: newItem.attributes.CODETONG, collection_name: 'res_ward' }, { $set: { polygons: polygon } }).then(document => { })
                            .catch(err => {
                                newCode = `ma = ${code} + ${err}, `
                                fs.appendFile('./phuong.txt', newCode, 'utf8', (e) => {
                                    if (e) throw e;
                                })
                            })
                    }
                }
            })
        } catch (e) {
            console.log(e)
        }
    })

    return res.send(resPolygon)
})

router.get('/update_polygon_double', async (req, res) => {
    let result = await provinceModel.find({}, { _id: 1, code: 1 })
    let polygon = await resPolygonModel.find({ collection_name: 'res_province' })

    polygon.map(item => {
        result.map(async newItem => {
            if (item.code === newItem.code) {
                await resPolygonModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(item._id) }, { $set: { rel_id: mongoose.Types.ObjectId(newItem._id) } })
            }
        })
    })

    return res.send(polygon)
})

router.get('/ward', async (req, res) => {
    let wb = xlsx.readFile('D:\\DULIEU28102019\\formmau_hanhchinh(3).xlsx')
    let ws = wb.Sheets['Ward']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]
    let list = []
    // let listProvince = await provinceModel.find({}, { code: 1, _id: 1 })
    // let listDistrict = await districtModel.aggregate([
    //     {
    //         $lookup: {
    //             from: "res_province",
    //             localField: "province_id",
    //             foreignField: "_id",
    //             as: "province"
    //         }
    //     },
    //     {
    //         $unwind: "$province"
    //     },
    // ])

    let resWardName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        if (data[i].district_id === '02' && data[i].province_id === '79') {
            for (let j = 0; j < resWardName.length; j++) {
                item[`${resWardName[j]}`] = data[i][resWardName[j]]
            }
            try {
                let resPolygonItem = item
                item.points = null
                resPolygonItem.polygons = null
                item.district_id = mongoose.Types.ObjectId("5de0a277bdcc5e2ae0d9cac0")

                let newPoint = new wardModel(item)
                await newPoint.save().then(async document => {
                    resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                    resPolygonItem.collection_name = 'res_ward'
                    resPolygonItem.code = item.code
                    let newPolygon = new resPolygonModel(resPolygonItem)
                    await newPolygon.save()
                })
                // list.push(item)
                
            } catch (e) {
                notifyError[1][`${i}`] = e
            }
        }
    }
    return res.send(notifyError)
})



module.exports = router