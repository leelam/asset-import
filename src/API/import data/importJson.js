var express = require("express");
var router = express.Router();
var mongoose = require('mongoose')
var resRoadCategoryModel = require('../../models/res_road_category.model')
var resDistrictModel = require('../../models/res_district.model')
var assetPlanningCategoryModel = require('../../models/asset_planning_category.model')
var resRoadModel = require('../../models/res_road.model')
var resSearchStringModel = require('../../models/res_search_string.model')
var _ = require('lodash')
var changeSearchString = require('../../helpers/change_alias')
// var mongoose = require('mongoose')
// var _ = require('lodash')
// var fs = require('fs')
// // var haNoiPlanning = require('./QUYHOACH_HN.json')
// var tho = require('./tay-ho.json')
// var assetPlanningModel = require('../../models/asset_planning.model')
// var assetPlanningCategoryModel = require('../../models/asset_planning_category.model')
// var wardModel = require('../../models/res_ward.model')

var point = pointStr => {
    if (_.isString(pointStr)) {
        var pointArr = pointStr.split(",");

        pointArr[0] = parseFloat(pointArr[0]);
        pointArr[1] = parseFloat(pointArr[1]);
        if (pointArr[0] && pointArr[1]) {
            return [pointArr[0], pointArr[1]];
        }
    }
    return null;
};

var polygon = polygonStr => {
    if (_.isString(polygonStr)) {
        var polygon = []
        var polygonArr = polygonStr.split(' ');
        for (var i = 0; i < polygonArr.length; i++) {
            var point = polygonArr[i].split(',');
            point[0] = parseFloat(point[0])
            point[1] = parseFloat(point[1])
            if (point[0] && point[1]) polygon.push([point[0], point[1]])
            else return null;
        }
        return polygon
    }
    return null;
};

// router.get('/asset_planning', async(req, res) =>{
//     let list = []
//     let wardName = await wardModel.find({}, {code: 1, _id: 1})
//     tho.features.map(item =>{
//         let newPlanning = {}
//         newPlanning.code = item.code;
//         wardName.map(wardItem =>{
//             if(wardItem.code === item.ward_id){
//                 newPlanning.ward_id = mongoose.Types.ObjectId(wardItem._id)
//             }
//         })
//         if(item.geometry.rings.length > 1){
//             list.push(item)
//         }
//     })
//     console.log(list.length)

//     res.send(list)
// })

router.get('/reset_import', async(req, res) =>{
    let result = await assetPlanningModel.find({import: true})

    res.send(result)
})

router.get('/planning', async(req, res) =>{
    let p = require('./HOCMON.json')
    let list = []
    try{
        p.features.map(item =>{
            if(item.geometry.rings.length > 1){
                list.push(item)
            }
        })
    } catch(e){

    }
    res.send(list)
})

router.get('/res_road', async(req, res) =>{
    let road = require('./quan thu duc 12 cu chi 1.json')
    let list = []
    try{
        road.features.map(async item =>{
            let newRoad = {};
            newRoad.code = item.attributes.code;
            newRoad.name = item.attributes.name;
            newRoad.points = {
                type: 'Point',
                coordinates: point(item.attributes.point)
            };
            newRoad.polygons = {
                type: 'LineString',
                coordinates: item.geometry.paths[0]
            };
            newRoad.district_id = [];
            resDistrictModel.findOne({code: item.attributes.district_i}).then(result =>{
                if(result){
                    newRoad.district_id.push(mongoose.Types.ObjectId(result._id))
                }
            })
            newRoad.from_to = item.attributes.from_to
            newRoad.category_id = await resRoadCategoryModel.findOne({code: item.attributes.category_i})
            newRoad.planning_category_id = await assetPlanningCategoryModel.findOne({code: item.attributes.planning_c})
            newRoad.location = item.attributes.location;
            newRoad.road_width = item.attributes.road_width;
            newRoad.width_unit_id = item.attributes.width__uni;
            newRoad.original_price = item.attributes.original_p;
            newRoad.price_unit_id = item.attributes.price_unit;
            newRoad.adj_factor = item.attributes.adj_factor;
            newRoad.factor_unit_id = item.attributes.factor_uni
            newRoad.refernce = item.attributes.refernce;
            newRoad.a_value_unit_id = 'triệu / m2';
            newRoad.exist_unit_id = 'm';
            newRoad.reaction = [];
            newRoad.import = true
            let result = new resRoadModel(newRoad)
            await result.save().then(async res =>{
                let newSearch = new resSearchStringModel({
                    name: 'res_road',
                    status: true,
                    rel_id: mongoose.Types.ObjectId(res._id),
                    title: res.name,
                    search: res.name + ' ' + changeSearchString(res.name),
                    refPath: 'res_road',
                    sub_title: res.from_to + res.location.slice(9),
                    import: true
                })
                await newSearch.save()
            })
        })
    } catch(e){
        console.log(e)
        list.push(e)
    }
    res.send(list)
})


module.exports = router;