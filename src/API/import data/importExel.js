var express = require('express');
var router = express.Router();
var xlsx = require('xlsx');
const fs = require('fs-extra');
const path = require('path');
var companyCategoryModel = require('../../models/res_company_category.model');
var resCompanyModel = require('../../models/res_company.model');
var assetPlanningCategoryModel = require('../../models/asset_planning_category.model');
var assetUtilityCategoryModel = require('../../models/asset_utility_category.model');
var assetUtilityModel = require('../../models/asset_utility.model');
var wardModel = require('../../models/res_ward.model');
var resRoadModel = require('../../models/res_road.model');
var bankModel = require('../../models/bank.model');
var provinceModel = require('../../models/res_province.model');
var districtModel = require('../../models/res_district.model');
var wardModel = require('../../models/res_ward.model');
var resSearchStringModel = require('../../models/res_search_string.model');
var projectCategoryModel = require('../../models/project_category.model');
var projectStatusModel = require('../../models/project_status.model');
var projectModel = require('../../models/project.model');
var projectConsDesignInfo = require('../../models/project_cons_design_info.model');
var mongoose = require('mongoose');
const { dumpDatabase, restoreDatabase } = require('../../mongoHandler');
const productModel = require('../../models/product.model');
var _ = require('lodash');
var formatString = require('../../helpers/change_alias');
const { lookupByCode } = require('../../helpers/lookupBycode.helper');
const { deleteByQuery } = require('../../../elasticsearch');
const landlordModel = require('../../models/product_landlord_info.model');

function ExcelDateToJSDate(serial) {
  var utc_days = Math.floor(serial - 25569);
  var utc_value = utc_days * 86400;
  var date_info = new Date(utc_value * 1000);

  var fractional_day = serial - Math.floor(serial) + 0.0000001;

  var total_seconds = Math.floor(86400 * fractional_day);

  var seconds = total_seconds % 60;

  total_seconds -= seconds;

  var hours = Math.floor(total_seconds / (60 * 60));
  var minutes = Math.floor(total_seconds / 60) % 60;

  return new Date(
    date_info.getFullYear(),
    date_info.getMonth(),
    date_info.getDate(),
    hours,
    minutes,
    seconds
  );
}

var point = (pointStr) => {
  if (_.isString(pointStr)) {
    var pointArr = pointStr.split(',');

    pointArr[0] = parseFloat(pointArr[0]);
    pointArr[1] = parseFloat(pointArr[1]);
    if (pointArr[0] && pointArr[1]) {
      return [pointArr[0], pointArr[1]];
    }
  }
  return null;
};

var polygon = (polygonStr) => {
  if (_.isString(polygonStr)) {
    var polygon = [];
    var polygonArr = polygonStr.split(' ');
    for (var i = 0; i < polygonArr.length; i++) {
      var point = polygonArr[i].split(',');
      point[0] = parseFloat(point[0]);
      point[1] = parseFloat(point[1]);
      if (point[0] && point[1]) polygon.push([point[0], point[1]]);
      else return null;
    }
    return polygon;
  }
  return null;
};

router.get('/update_asset_planning_category', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\du an\\update 16-01-2020\\ahduantphcm20200116it.xlsx'
  );
  let ws = wb.Sheets['Sheet2'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  let list = [];
  for (let i = 0; i < data.length; i++) {
    await assetPlanningCategoryModel
      .findOne({ code: data[i].code })
      .then(async (doc) => {
        if (!doc) {
          let newCategory = new assetPlanningCategoryModel({
            ...data[i],
            import: true,
          });
          await newCategory.save();
          list.push(newCategory);
        }
      });
  }
  return res.send(list);
});

router.get('/update-product', async (req, res) => {
  try {
    const { fileName, model } = req.query;
    const tree = mongoose.model(model).schema.tree;
    global.socket.emit('change-process', 'Đang đọc file');
    let wb = xlsx.readFile(path.resolve(`import-file/${model}/${fileName}`));
    const sheetNames = wb.SheetNames;
    const test = await new Promise((resolve, reject) => {
      global.socket.emit('send-list-sheet', sheetNames, function (data) {
        return resolve(data);
      });
    });
    let ws = wb.Sheets[test];
    let data = xlsx.utils.sheet_to_json(ws);
    let list = [];
    const length = data.length;
    global.socket.emit('send-length', length);
    for (let i = 0; i < length; i++) {
      try {
        const listKeyData = Object.keys(data[i]);
        const a = await Promise.all(
          listKeyData.map(async (item) => {
            if (
              tree[item] &&
              (tree[item].ref || (tree[item][0] && tree[item][0].ref))
            ) {
              if (tree[item][0]) {
                const value = data[i][item].toString().split(',');
                data[i][item] = [];
                await Promise.all(
                  value.map(async (child) => {
                    const result = await lookupByCode(
                      tree[item][0].ref,
                      child,
                      data[i].fid,
                      item,
                      data[i].code,
                      tree[item][0].require
                    );
                    data[i][item].push(result);
                  })
                );
              } else {
                const arrRef = listKeyData.filter((key) =>
                  key.includes(`${item}.`)
                );
                const objRef = {};
                if (arrRef.length) {
                  arrRef.forEach((attri) => {
                    objRef[attri.replace(`${item}.`, '')] =
                      data[i][attri] === 'Đang cập nhật' ||
                      data[i][attri] === 'Không có' ||
                      data[i][attri] === 'Chưa có'
                        ? {
                            status: data[i][attri],
                          }
                        : { status: 'Có', file: data[i][attri] };
                  });
                }
                data[i][item] = await lookupByCode(
                  tree[item].ref,
                  data[i][item],
                  data[i].fid,
                  item,
                  data[i].code,
                  tree[item].require,
                  arrRef.length,
                  objRef
                );
              }
            }
            return;
          })
        );
        if (model === 'product' && data[i].project_id) {
          const projectDetail = await mongoose
            .model('project')
            .findOne({ _id: data[i].project_id });
          data[i].points = projectDetail.points;
          data[i].polygons = projectDetail.polygons;
        } else {
          if (data[i].points) {
            const objPoint = {
              type: 'Point',
              coordinates: [],
            };
            objPoint.coordinates = data[i].points
              .split(',')
              .map((item) => parseFloat(item));
            data[i].points = objPoint;
          }
          if (data[i].polygons) {
            const objPolygon = {
              type: 'Polygon',
              coordinates: [],
            };
            objPolygon.coordinates = [
              data[i].polygons
                .split(' ')
                .map((item) => item.split(',').map((coor) => parseFloat(coor))),
            ];
            data[i].polygons = objPolygon;
          }
        }
        if (data[i].handover_time)
          data[i].handover_time = ExcelDateToJSDate(data[i].handover_time);
        if (data[i].status !== 'FALSE') {
          let test = new mongoose.model(model)();
          test = Object.assign(test, data[i]);
          test.FID = data[i].FID || data[i].fid;
          console.log('test',test);
          // const error = test.validateSync();
          // if (error) {

          //   global.socket.emit(
          //     'send-error',
          //     JSON.stringify(error.errors) + `code: ${data[i].code}`
          //   );
          //   return res.json({
          //     status: false,
          //     message: 'Import false'
          //   });
          // } else {
          if (model === 'product') {
            const newLandlord = new landlordModel({
              full_name: data[i].landlord,
              gender: data[i].gender,
              _id: new mongoose.Types.ObjectId(),
              address: data[i].landlord_address,
              owner: data[i].owner,
              note: data[i].note,
            });
            const resultNewLandlord = await newLandlord.save({
              full_name: data[i].landlord,
              gender: data[i].gender,
              address: data[i].landlord_address,
              owner: data[i].owner,
              note: data[i].note,
            });
            data[i].landlord_info_id = resultNewLandlord._id;
          }
          await test.save(data[i]);
          global.socket.emit('send-import-detail', i);
        }
      } catch (e) {
        console.log(e, i);
      }
      // }
    }
    // dumpDatabase(model, 'asset_dev_import');
    return res.send(list);
  } catch (e) {
    global.socket.emit('send-error', e.toString());
    return res.json({
      status: false,
      message: 'Import false',
    });
  }
});

router.get('/update_res_company', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\du an\\import du an Fuji Residence\\danh-ba-hcm-it-demo-120220.xlsx'
  );
  let ws = wb.Sheets['Danh ba'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  let list = [];
  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    try {
      if (item.parent_id) {
        await resCompanyModel.findOne({ code: item.parent_id }).then((doc) => {
          item.parent_id = mongoose.Types.ObjectId(doc._id);
        });
      } else {
        item.parent_id = null;
      }
      if (item.bank_id) {
        await bankModel.findOne({ code: item.bank_id }).then((doc) => {
          item.bank_id = mongoose.Types.ObjectId(doc._id);
        });
      } else {
        item.bank_id = null;
      }
      item.create_uid = mongoose.Types.ObjectId('5dbb9fbbebd384b5e4ecc368');
      item.create_date = Date.now();
      await companyCategoryModel
        .findOne({ code: item.category_id })
        .then((doc) => {
          if (doc) {
            item.category_id = mongoose.Types.ObjectId(doc._id);
          }
        });
      await wardModel.findOne({ code: item.ward_id }).then((doc) => {
        if (doc) {
          item.ward_id = mongoose.Types.ObjectId(doc._id);
        }
      });
      // list.push(item)
      await resCompanyModel
        .findOneAndUpdate({ code: item.code }, { $set: item }, { upsert: true })
        .then(async (result) => {
          let newSearch = {};
          (newSearch.status = true),
            (newSearch.name = 'company'),
            (newSearch.search = `${result.company_name} ${formatString(
              result.company_name
            )}`);
          newSearch.refPath = 'res_company';
          newSearch.rel_id = mongoose.Types.ObjectId(result._id);
          await resSearchStringModel.findOneAndUpdate(
            { rel_id: mongoose.Types.ObjectId(result._id) },
            { $set: newSearch },
            { upsert: true }
          );
        });
    } catch (err) {
      // list.push(item.code, err)
      console.log(err);
    }
  }
  return res.send(list);
});

// router.get('/update_ranh_hanh_chanh', async(req, res) =>{
//     let wb = xlsx.readFile("D:\\import\\Danh bạ HCM (IT) (161219).xlsx")
//     let ws = wb.Sheets['Danh ba']
//     let data = xlsx.utils.sheet_to_json(ws)
//     let notifyError = [{}, {}]
// })

router.get('/update_road_multi_ward', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\con duong\\update chieu dai va cac phuong di qua\\PN_CG_NB_5_8_11_GV.xls'
  );
  let ws = wb.Sheets['duong_phuong'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  for (let i = 0; i < data.length; i++) {
    let newRoad = {};
    newRoad.length = data[i].length;
    newRoad.refernce = data[i].refernce;
    // console.log(data[i].cross_ward)
    data[i].cross_ward = data[i].cross_ward.split(',');
    newRoad.cross_ward = [];
    for (let j = 0; j < data[i].cross_ward.length; j++) {
      await wardModel.findOne({ code: data[i].cross_ward[j] }).then((doc) => {
        if (doc) {
          newRoad.cross_ward.push(mongoose.Types.ObjectId(doc._id));
        }
      });
    }
    await resRoadModel.findOneAndUpdate(
      { code: data[i].code },
      { $set: newRoad }
    );
  }

  return res.send(data);
});

router.get('/get_file_name', async (req, res) => {
  // Require library
  var excel = require('excel4node');

  // Create a new instance of a Workbook class
  var workbook = new excel.Workbook();

  // Add Worksheets to the workbook
  var worksheet = workbook.addWorksheet('Sheet 1');
  var style = workbook.createStyle({
    font: {
      color: '#FF0800',
      size: 12,
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -',
  });

  const testFolder = 'D:\\TPHCM\\QUAN1\\hinhanh\\';
  const fs = require('fs');
  let list = [];
  let i = 0;
  await fs.readdir(testFolder, (err, files) => {
    files.forEach(async (file) => {
      i++;
      worksheet.cell(i, 1).string(file).style(style);
    });
    workbook.write('quan1_test.xlsx');
  });
  res.send(list);
});

router.get('/asset_utility', async (req, res) => {
  let list = [];
  const dirPath = path.resolve('import-file/tien ich');
  const files = fs.readdirSync(dirPath);
  list = files.map((file) => `${dirPath}/${file}`);
  let result = [];
  for (let l = 0; l < list.length; l++) {
    let wb = xlsx.readFile(list[l]);
    let ws = wb.Sheets['sheet'];
    let data = xlsx.utils.sheet_to_json(ws);
    let notifyError = [{}, {}];
    for (let i = 1; i < data.length; i++) {
      let item = data[i];
      try {
        item.points = {
          type: 'Point',
          coordinates: point(item.points),
        };
        await wardModel.findOne({ code: item.ward_id }).then((doc) => {
          item.ward_id = mongoose.Types.ObjectId(doc._id);
        });
        await assetUtilityCategoryModel
          .findOne({ code: item.category_id })
          .then((doc) => {
            item.category_id = mongoose.Types.ObjectId(doc._id);
          });
        if (item.link_img) {
          item.avatar = 'images\\Utility\\' + item.link_img;
        }
        item.create_uid = mongoose.Types.ObjectId('5dbb9d3b65bb02ebb0ec7fb3');
        item.import = true;
        let newUtility = new assetUtilityModel(item);
        await newUtility
          .save()
          .then((res) => {})
          .catch((err) => {
            result.push(item.code, err);
          });
        // result.push(newUtility)
      } catch (error) {
        result.push(`${item.code} ${list[l]} ${error.toString()}`);
        console.log(item.code, error);
      }
    }
  }
  return res.send(result);
});

router.get('/asset_utility_cateogory', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\tien ich 27122019\\utility_category\\c031404fdd1350f32de8d5b0945f278e_296107_JWNVBGUXVU5WZ.xlsx'
  );
  let ws = wb.Sheets['Code loại tiện ích'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  let result = [];

  for (let i = 0; i < data.length; i++) {
    try {
      let item = data[i];
      // let newCategory = new assetUtilityCategoryModel(item)
      let newutility = new assetUtilityCategoryModel(item);
      await newutility.save();
      result.push(newutility);
    } catch (error) {
      result.push(item.code, error);
    }
  }
  return res.send(result);
});

router.get('/cong_ty_import_update', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\danh ba\\Danh bạ HCM-IT (140119).xlsx'
  );
  let ws = wb.Sheets['Danh ba'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  let result = [];
  for (let i = 0; i < data.length; i++) {
    try {
      let item = data[i];
      await companyCategoryModel
        .findOne({ code: item.category_id })
        .then((doc) => {
          item.category_id = mongoose.Types.ObjectId(doc._id);
        });
      await wardModel.findOne({ code: item.ward_id }).then((doc) => {
        item.ward_id = mongoose.Types.ObjectId(doc._id);
      });
      // let newCategory = new assetUtilityCategoryModel(item)
      // let newutility = new assetUtilityCategoryModel(item)
      // await newutility.save()
      result.push(item);
    } catch (error) {
      result.push(item.code, error);
    }
  }
  return res.send(result);
});

router.get('/bank', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\danh ba\\ngan hang\\Nganhang-IT (140119).xlsx'
  );
  let ws = wb.Sheets['Danh ba'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];

  let result = [];

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    item.create_date = Date.now();
    item.create_uid = mongoose.Types.ObjectId('5dbb9fbbebd384b5e4ecc368');
    item.write_uid = mongoose.Types.ObjectId('5dbb9fbbebd384b5e4ecc368');
    await bankModel.findOneAndUpdate(
      { code: item.code },
      { $set: item },
      { upsert: true }
    );
    result.push(item);
  }

  return res.send(result);
});

router.get('/update_huyen_ranh_hanh_chinh', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\ranh hanh chanh\\ranh hanh chinh thieu\\140120(2)\\005-code-hanh-chinh.xlsx'
  );
  let ws = wb.Sheets['District'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];
  let list = [];

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    try {
      let newItem = _.pick(item, ['name', 'code', 'type', 'province_id']);
      await provinceModel.findOne({ code: newItem.province_id }).then((doc) => {
        newItem.province_id = mongoose.Types.ObjectId(doc._id);
      });
      newItem.create_date = Date.now();
      newItem.create_uid = mongoose.Types.ObjectId('5d679819325ab70ab0157ce5');
      newItem.reaction = [];
      await districtModel
        .findOneAndUpdate(
          { code: newItem.code },
          { $set: newItem },
          { upsert: true }
        )
        .then(async (doc) => {
          let subTitle = '';
          await provinceModel
            .findOne({ _id: mongoose.Types.ObjectId(doc.province_id) })
            .then((docu) => {
              subTitle = docu.name;
            });
          const newSearch = {
            name: 'res_address',
            rel_id: mongoose.Types.ObjectId(doc._id),
            sub_title: subTitle,
            title: doc.name,
            refPath: 'res_address_new',
            search: `${formatString(doc.name)} ${doc.name}`,
          };
          await resSearchStringModel.findOneAndUpdate(
            { rel_id: mongoose.Types.ObjectId(doc._id) },
            { $set: newSearch },
            { upsert: true }
          );
        });
      list.push(newItem);
    } catch (error) {}
  }

  return res.send(list);
});

router.get('/update_phuong_xa_ranh_hanh_chanh', async (req, res) => {
  let wb = xlsx.readFile(
    'D:\\import data official\\ranh hanh chanh\\ranh hanh chinh thieu\\140120(2)\\005-code-hanh-chinh.xlsx'
  );
  let ws = wb.Sheets['Ward'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];
  let list = [];

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    try {
      let newItem = _.pick(item, ['name', 'code', 'type']);
      newItem.district_id = `${item.code_tinh}${item.code_quan}`;
      newItem.create_date = Date.now();
      newItem.create_uid = mongoose.Types.ObjectId('5d679819325ab70ab0157ce5');
      newItem.reaction = [];
      await districtModel.findOne({ code: newItem.district_id }).then((doc) => {
        newItem.district_id = mongoose.Types.ObjectId(doc._id);
      });

      await wardModel
        .findOneAndUpdate(
          { code: newItem.code },
          { $set: newItem },
          { upsert: true }
        )
        .then(async (doc) => {
          let subTitle = `${item.name_quan} ${item.name_tinh}`;

          const newSearch = {
            name: 'res_address',
            rel_id: mongoose.Types.ObjectId(doc._id),
            sub_title: subTitle,
            title: doc.address_name,
            refPath: 'res_address_new',
            search: `${formatString(doc.name)} ${doc.name}`,
          };
          await resSearchStringModel.findOneAndUpdate(
            { rel_id: mongoose.Types.ObjectId(doc._id) },
            { $set: newSearch },
            { upsert: true }
          );
        });
      if (item.code === '270402') {
        list.push(newItem);
      }
    } catch (error) {
      console.log(item.code, error);
    }
  }

  return res.send(list);
});

router.get('/update_project', async (req, res) => {
  let wb = xlsx.readFile(
    path.resolve(
      'import-file/' + 'ah-thong-tin-tong-quan-du-an-ha-noi-it-20200305.xlsx'
    )
  );
  let ws = wb.Sheets['HN - DA IT'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];
  let list = [];

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    try {
      await wardModel.findOne({ code: item.ward_id }).then((doc) => {
        item.ward_id = mongoose.Types.ObjectId(doc._id);
      });
      await projectCategoryModel
        .findOne({ category_code: item.category_id })
        .then((doc) => {
          item.category_id = mongoose.Types.ObjectId(doc._id);
        });
      if (item.parent_id) {
        await projectModel.findOne({ code: item.parent_id }).then((doc) => {
          item.parent_id = mongoose.Types.ObjectId(doc._id);
        });
      }
      await projectStatusModel
        .findOne({ code: item.cons_status })
        .then((doc) => {
          item.cons_status = mongoose.Types.ObjectId(doc._id);
          // console.log(item.cons_status)
        });
      console.log(item.planing_category_id);
      await assetPlanningCategoryModel
        .findOne({ code: item.planing_category_id })
        .then((doc) => {
          item.planing_category_id = mongoose.Types.ObjectId(doc._id);
        });

      // investor of project
      if (item.investor_id) {
        let newInvestor = item.investor_id.split(',');
        let arrayInvestor = [];
        for (let inves = 0; inves < newInvestor.length; inves++) {
          console.log(newInvestor[inves]);
          await resCompanyModel
            .findOne({ code: newInvestor[inves] })
            .then((doc) => {
              arrayInvestor.push(mongoose.Types.ObjectId(doc._id));
            });
        }
        item.investor_id = arrayInvestor;
      } else {
        item.investor_id = null;
      }
      if (item.contractor_id) {
        let newContractor = item.contractor_id.split(',');
        let arrContractor = [];
        for (let contrac = 0; contrac < newContractor.length; contrac++) {
          await resCompanyModel
            .findOne({ code: newContractor[contrac] })
            .then((doc) => {
              arrContractor.push(mongoose.Types.ObjectId(doc._id));
            });
        }
        item.contractor_id = arrContractor;
      } else {
        item.contractor_id = null;
      }

      if (item.designer_id) {
        let newDesigner = item.designer_id.split(',');
        let arrDesign = [];
        for (let design = 0; design < newDesigner.length; design++) {
          await resCompanyModel
            .findOne({ code: newDesigner[design] })
            .then((doc) => {
              arrDesign.push(mongoose.Types.ObjectId(doc._id));
            });
        }
        item.designer_id = arrDesign;
      } else {
        item.designer_id = null;
      }

      if (item.manager_id) {
        let newManager = item.manager_id.split(',');
        let arrManager = [];
        for (let manager = 0; manager < newManager.length; manager++) {
          await resCompanyModel
            .findOne({ code: newManager[manager] })
            .then((doc) => {
              arrManager.push(mongoose.Types.ObjectId(doc._id));
            });
        }
        item.manager_id = arrManager;
      } else {
        item.manager_id = null;
      }
      let newItem = _.omit(item, ['points', 'polygons']);
      await projectModel.findOneAndUpdate(
        { code: item.code },
        { $set: newItem },
        { upsert: true }
      );
      await projectModel.findOne({ code: item.code }).then(async (result) => {
        console.log(result);
        let wardName = await wardModel.findOne({ _id: newItem.ward_id });
        let newSearch = {};
        newSearch.status = true;
        newSearch.name = 'project';
        newSearch.rel_id = mongoose.Types.ObjectId(result._id);
        newSearch.title = result.name;
        newSearch.sub_title = `${wardName.name} ${newItem.district} ${newItem.province}`;
        newSearch.refPath = 'project';
        newSearch.search = `${result.name} ${formatString(
          result.name
        )} ${`${wardName.name} ${newItem.district} ${newItem.province}`} ${formatString(
          `${wardName.name} ${newItem.district} ${newItem.province}`
        )}`;
        newSearch.weight = '0.5';
        await resSearchStringModel.findOneAndUpdate(
          { rel_id: mongoose.Types.ObjectId(result._id) },
          { $set: newSearch },
          { upsert: true }
        );
      });

      // await projectStatusModel
      // list.push(newItem)
    } catch (error) {
      // list.push(item.code, error)
      console.log(item.code, error);
    }
  }
  return res.send(list);
});

router.get('/update_project_design_build', async (req, res) => {
  let wb = xlsx.readFile(
    path.resolve(
      'import-file/' + 'ah-thiet-ke-xay-dung-du-an-ha-noi-it-20200305.xlsx'
    )
  );
  let ws = wb.Sheets['HN - THIET KE XAY DUNG IT'];
  let data = xlsx.utils.sheet_to_json(ws);
  let notifyError = [{}, {}];
  let list = [];

  // list = Object.keys(data[0])
  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    let newDesingInfo = {
      general_info_of_cons: {
        design_info: {},
      },
      material_structure: {
        basement_foundation: {},
        rough_cons: {},
        pccc: {},
        me_system: {},
        ext_finishing: {},
        hall: {},
      },
      int_finishing: {
        living_room: {},
        bedroom: {},
        bathroom: {},
        kitchen: {},
        balcony: {},
      },
    };
    console.log(item);
    if (item.design_stype) {
      newDesingInfo.general_info_of_cons.design_info.design_style =
        item.design_stype;
    }
    if (item.design_type) {
      newDesingInfo.general_info_of_cons.design_info.design_type =
        item.design_type;
    }
    if (item.design_standard) {
      newDesingInfo.general_info_of_cons.design_info.design_standard =
        item.design_standard;
    }
    if (item.handover_condition) {
      newDesingInfo.general_info_of_cons.design_info.handover_condition =
        item.handover_condition;
    }
    if (item.basement) {
      newDesingInfo.material_structure.basement_foundation.basement =
        item.basement;
      newDesingInfo.material_structure.basement_foundation.basement_unit_id =
        item.basement_unit_id;
    }
    if (item.basement_foundation_type) {
      newDesingInfo.material_structure.basement_foundation.basement_foundation_type =
        item.basement_foundation_type;
    }
    if (item.basement_structure) {
      newDesingInfo.material_structure.basement_foundation.basement_structure =
        item.basement_structure;
    }
    if (item.floor_min) {
      newDesingInfo.material_structure.rough_cons.floor_min = item.floor_min;
      newDesingInfo.material_structure.rough_cons.floor_min_unit_id =
        item.floor_min_unit_id;
    }
    if (item.number_of_floor) {
      newDesingInfo.material_structure.rough_cons.number_of_floor =
        item.number_of_floor;
      newDesingInfo.material_structure.rough_cons.floor_unit_id =
        item.floor_unit_id;
    }
    if (item.structure) {
      newDesingInfo.material_structure.rough_cons.structure = item.structure;
    }
    if (item.pccc_standards) {
      newDesingInfo.material_structure.pccc.pccc_standards =
        item.pccc_standards;
    }
    if (item.fire_alarm) {
      newDesingInfo.material_structure.pccc.fire_alarm = item.fire_alarm;
    }
    if (item.fire_materials) {
      newDesingInfo.material_structure.pccc.fire_materials =
        item.fire_materials;
    }
    if (item.fire_pump) {
      newDesingInfo.material_structure.pccc.fire_pump = item.fire_pump;
    }
    if (item.fire_pump) {
      newDesingInfo.material_structure.pccc.fire_pump = item.fire_pump;
      newDesingInfo.material_structure.pccc.fire_pump_unit_id =
        item.fire_pump_unit_id;
    }
    if (item.elevantor_no) {
      newDesingInfo.material_structure.me_system.elevator_no =
        item.elevantor_no;
      newDesingInfo.material_structure.me_system.elevator_no_unit_id =
        item.elevator_no_unit_id;
    }
    if (item.elevator_type) {
      newDesingInfo.material_structure.me_system.elevator_type =
        item.elevator_type;
    }
    if (item.backup_generator) {
      newDesingInfo.material_structure.me_system.backup_generator =
        item.backup_generator;
    }
    if (item.pipe_system) {
      newDesingInfo.material_structure.me_system.pipe_system = item.pipe_system;
    }
    if (item.wall) {
      newDesingInfo.material_structure.ext_finishing.wall = item.wall;
    }
    if (item.glass) {
      newDesingInfo.material_structure.ext_finishing.glass = item.glass;
    }
    if (item.door) {
      newDesingInfo.material_structure.ext_finishing.door = item.door;
    }
    if (item.hall_scaffold) {
      newDesingInfo.material_structure.hall.scaffold = item.hall_scaffold;
    }
    if (item.hall_wall) {
      newDesingInfo.material_structure.hall.wall = item.hall_wall;
    }
    if (item.hall_ceiling) {
      newDesingInfo.material_structure.hall.ceiling = item.hall_ceiling;
    }
    if (item.hall_main_door) {
      newDesingInfo.material_structure.hall.main_door = item.hall_main_door;
    }
    if (item.hall_front_desk) {
      newDesingInfo.material_structure.hall.front_desk = item.hall_front_desk;
    }
    if (item.living_room_scaffold) {
      newDesingInfo.int_finishing.living_room.scaffold =
        item.living_room_scaffold;
    }
    if (item.living_room_wall) {
      newDesingInfo.int_finishing.living_room.wall = item.living_room_wall;
    }
    if (item.living_room_ceiling) {
      newDesingInfo.int_finishing.living_room.ceiling =
        item.living_room_ceiling;
    }
    if (item.living_room_main_door) {
      newDesingInfo.int_finishing.living_room.main_door =
        item.living_room_main_door;
    }
    if (item.living_room_main_door_lock) {
      newDesingInfo.int_finishing.living_room.main_door_lock =
        item.living_room_main_door_lock;
    }
    if (item.living_room_window) {
      newDesingInfo.int_finishing.living_room.window = item.living_room_window;
    }
    if (item.living_room_air_conditioner) {
      newDesingInfo.int_finishing.living_room.air_conditioner =
        item.living_room_air_conditioner;
    }
    if (item.living_room_shoes_cabinet) {
      newDesingInfo.int_finishing.living_room.shoes_cabinet =
        item.living_room_shoes_cabinet;
    }
    if (item.living_room_electrical_equipment) {
      newDesingInfo.int_finishing.living_room.electrical_equipment =
        item.living_room_electrical_equipment;
    }
    if (item.living_room_telecom_equipment) {
      newDesingInfo.int_finishing.living_room.telecom_equipment =
        item.living_room_telecom_equipment;
    }
    if (item.bed_room_scaffold) {
      newDesingInfo.int_finishing.bedroom.scaffold = item.bed_room_scaffold;
    }
    if (item.bed_room_wall) {
      newDesingInfo.int_finishing.bedroom.wall = item.bed_room_wall;
    }
    if (item.bed_room_ceiling) {
      newDesingInfo.int_finishing.bedroom.ceiling = item.bed_room_ceiling;
    }
    if (item.bed_room_main_door) {
      newDesingInfo.int_finishing.bedroom.main_door = item.bed_room_main_door;
    }
    if (item.bed_room_main_door_lock) {
      newDesingInfo.int_finishing.bedroom.main_door_lock =
        item.bed_room_main_door_lock;
    }
    if (item.bed_room_window) {
      newDesingInfo.int_finishing.bedroom.window = item.bed_room_window;
    }
    if (item.bed_room_air_conditioner) {
      newDesingInfo.int_finishing.bedroom.air_conditioner =
        item.bed_room_air_conditioner;
    }
    if (item.bed_room_wardrobe) {
      newDesingInfo.int_finishing.bedroom.wardrobe = item.bed_room_wardrobe;
    }
    if (item.bed_room_electrical_equipment) {
      newDesingInfo.int_finishing.bedroom.electrical_equipment =
        item.bed_room_electrical_equipment;
    }
    if (item.bed_room_telecom_equipment) {
      newDesingInfo.int_finishing.bedroom.telecom_equipment =
        item.bed_room_telecom_equipment;
    }
    if (item.bath_room_scaffold) {
      newDesingInfo.int_finishing.bathroom.scaffold = item.bath_room_scaffold;
    }
    if (item.bath_room_wall) {
      newDesingInfo.int_finishing.bathroom.wall = item.bath_room_wall;
    }
    if (item.bath_room_ceiling) {
      newDesingInfo.int_finishing.bathroom.ceiling = item.bath_room_ceiling;
    }
    if (item.bath_room_main_door) {
      newDesingInfo.int_finishing.bathroom.main_door = item.bath_room_main_door;
    }
    if (item.bath_room_main_door_lock) {
      newDesingInfo.int_finishing.bathroom.main_door_lock =
        item.bath_room_main_door_lock;
    }
    if (item.bath_room_window) {
      newDesingInfo.int_finishing.bathroom.window = item.bath_room_window;
    }
    if (item.bath_room_lavabo) {
      newDesingInfo.int_finishing.bathroom.lavabo = item.bath_room_lavabo;
    }
    if (item.bath_room_lavabo_surface) {
      newDesingInfo.int_finishing.bathroom.lavabo_surface =
        item.bath_room_lavabo_surface;
    }
    if (item.bath_room_lavabo_cabinet_down) {
      newDesingInfo.int_finishing.bathroom.lavabo_cabinet_down =
        item.bath_room_lavabo_cabinet_down;
    }
    if (item.bath_room_lavabo_cabinet_up) {
      newDesingInfo.int_finishing.bathroom.lavabo_cabinet_up =
        item.bath_room_lavabo_cabinet_up;
    }
    if (item.bath_room_bathroom) {
      newDesingInfo.int_finishing.bathroom.bathroom = item.bath_room_bathroom;
    }
    if (item.bath_room_bathtub) {
      newDesingInfo.int_finishing.bathroom.bathtub = item.bath_room_bathtub;
    }
    if (item.bath_room_shower) {
      newDesingInfo.int_finishing.bathroom.shower = item.bath_room_shower;
    }
    if (item.bath_room_toilet) {
      newDesingInfo.int_finishing.bathroom.toilet = item.bath_room_toilet;
    }
    if (item.bath_room_accessories) {
      newDesingInfo.int_finishing.bathroom.accessories =
        item.bath_room_accessories;
    }
    if (item.bath_room_electrical_equipment) {
      newDesingInfo.int_finishing.bathroom.electrical_equipment =
        item.bath_room_electrical_equipment;
    }
    if (item.kitchen_scaffold) {
      newDesingInfo.int_finishing.kitchen.scaffold = item.kitchen_scaffold;
    }
    if (item.kitchen_wall) {
      newDesingInfo.int_finishing.kitchen.wall = item.kitchen_wall;
    }
    if (item.kitchen_ceiling) {
      newDesingInfo.int_finishing.kitchen.ceiling = item.kitchen_ceiling;
    }
    if (item.kitchen_cabinet_kitchen_up) {
      newDesingInfo.int_finishing.kitchen.cabinet_kitchen_up =
        item.kitchen_cabinet_kitchen_up;
    }
    if (item.kitchen_cabinet_kitchen_down) {
      newDesingInfo.int_finishing.kitchen.cabinet_kitchen_down =
        item.kitchen_cabinet_kitchen_down;
    }
    if (item.kitchen_kitchen_surface) {
      newDesingInfo.int_finishing.kitchen.kitchen_surface =
        item.kitchen_kitchen_surface;
    }
    if (item.kitchen_sink) {
      newDesingInfo.int_finishing.kitchen.sink = item.kitchen_sink;
    }
    if (item.kitchen_electrical_equipment) {
      newDesingInfo.int_finishing.kitchen.electrical_equipment =
        item.kitchen_electrical_equipment;
    }
    if (item.kitchen_smokeconsumer) {
      newDesingInfo.int_finishing.kitchen.smoke_consumer =
        item.kitchen_smokeconsumer;
    }
    if (item.kitchen_oven) {
      newDesingInfo.int_finishing.kitchen.oven = item.kitchen_oven;
    }
    if (item.kitchen_induction_hob) {
      newDesingInfo.int_finishing.kitchen.induction_bob =
        item.kitchen_induction_hob;
    }
    if (item.balcony_scaffold) {
      newDesingInfo.int_finishing.balcony.scaffold = item.balcony_scaffold;
    }
    if (item.balcony_wall) {
      newDesingInfo.int_finishing.balcony.wall = item.balcony_wall;
    }
    let designhhh = new projectConsDesignInfo(newDesingInfo);
    await designhhh.save().then(async (result) => {
      await projectModel.findOneAndUpdate(
        { code: item.code },
        { $set: { cons_design_info_id: mongoose.Types.ObjectId(result._id) } }
      );
    });
    list.push(designhhh);
  }

  return res.send(list);
});

router.get('/delete-resSearch-string', async (req, res) => {
  const res_search_stringDelelte = await mongoose
    .model('res_search_string')
    .find({ import: true });
  console.log(res_search_stringDelelte.length);
  await Promise.all(
    res_search_stringDelelte.map(async (item) => {
      await mongoose.model('res_search_string').deleteOne({ _id: item._id });
      await deleteByQuery('test', item.rel_id);
    })
  );
  res.json(true);
});

router.get('/road-testt', async (req, res, next) => {
  const ab = new mongoose.model('res_road')();
  const data = {
    code: '7901S0248',
    FID: '79S2675.2',
    a_value_unit_id: 'm',
  };
  ab.code = '7901S0248';
  ab.FID = '79S2675.1';
  ab.a_value_unit_id = 'm';
  await ab.save(data);
  return res.json(true);
});

router.get('/excel-json', async (req, res, next) => {
  try {
    const { fileName } = req.query;
    console.log(fileName);
    const obj = {
      type: 'FeatureCollection',
      features: [],
    };
    global.socket.emit('change-process', 'Đang đọc file');
    let wb = xlsx.readFile(path.resolve(`import-file/json/${fileName}`));
    const sheetNames = wb.SheetNames;
    const test = await new Promise((resolve, reject) => {
      global.socket.emit('send-list-sheet', sheetNames, function (data) {
        return resolve(data);
      });
    });
    let ws = wb.Sheets[test];
    let data = xlsx.utils.sheet_to_json(ws);
    let list = [];
    const length = data.length;
    global.socket.emit('send-length', length);
    for (let i = 0; i < length; i++) {
      try {
        const listKeyData = Object.keys(data[i]);
        const polygonCoor = [
          _.chunk(
            data[i].Polygon.split('0 ')
              .filter((item) => item)
              .map((item) => parseFloat(item)),
            2
          ),
        ];
        const feature = {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: polygonCoor,
          },
        };
        obj.features.push(feature);
        global.socket.emit('send-import-detail', i);
      } catch (e) {
        console.log(e, data[i]);
      }
      // }
    }
    const a = fs.writeJSONSync(
      path.resolve(`export-file/json/${fileName}.json`),
      obj
    );
    console.log(a);
    return res.send(list);
  } catch (e) {
    global.socket.emit('send-error', e.toString());
    return res.json({
      status: false,
      message: 'Import false',
    });
  }
});
module.exports = router;
