var express = require("express");
var router = express.Router();
var xlsx = require('xlsx')
var parseKeyword = require("../../helpers/toUnicode.helper");
var companyCategoryModel = require('../../models/res_company_category.model')
var productCategoryModel = require('../../models/product_category.model')
// var resCompanyDemo = require('../../models/res_company.model')
var resCompanyDemoModel = require('../../models/res_company.model')
var projectModel = require('../../models/project.model')
// var resWardModel = require('../../models/res_ward.model')
var assetPlanningCategoryModel = require('../../models/asset_planning_category.model')
var productTypeModel = require('../../models/product_type.model')
var searchModel = require('../../models/res_search_string.model')
var projectStatusModel = require('../../models/project_status.model')
var assetUtilityCategoryModel = require('../../models/asset_utility_category.model')
var assetUtilityModel = require('../../models/asset_utility.model')
var provinceModel = require('../../models/res_province.model')
var districtModel = require('../../models/res_district.model')
var wardModel = require('../../models/res_ward.model')
var resPolygonModel = require('../../models/res_polygon.model')
var projectCategoryModel = require('../../models/project_category.model')
let assetPlanningModel = require('../../models/asset_planning.model')
var resRoadCategoryModel = require('../../models/res_road_category.model')
var resRoadModel = require('../../models/res_road.model')
var mongoose = require('mongoose')
var _ = require('lodash')
var unicodeString = require('../../helpers/toUnicode.helper')
var productModel = require('../../models/product.model')
const { getXlsxStream } = require('xlstream')

var point = pointStr => {
    if (_.isString(pointStr)) {
        var pointArr = pointStr.split(",");

        pointArr[0] = parseFloat(pointArr[0]);
        pointArr[1] = parseFloat(pointArr[1]);
        if (pointArr[0] && pointArr[1]) {
            return [pointArr[0], pointArr[1]];
        }
    }
    return null;
};

var polygon = polygonStr => {
    if (_.isString(polygonStr)) {
        var polygon = []
        var polygonArr = polygonStr.split(' ');
        for (var i = 0; i < polygonArr.length; i++) {
            var point = polygonArr[i].split(',');
            point[0] = parseFloat(point[0])
            point[1] = parseFloat(point[1])
            if (point[0] && point[1]) polygon.push([point[0], point[1]])
            else return null;
        }
        return polygon
    }
    return null;
};


router.get('/company_category', async (req, res) => {
    let wb = xlsx.readFile(__dirname + '\\form-danh-ba-demo.xlsx')
    let ws = wb.Sheets['Nhom danh ba']
    let data = xlsx.utils.sheet_to_json(ws)
    let companyCategory = []
    for (let i = 2; i < data.length; i++) {
        let item = {}
        item.code = data[i][2]
        item.name = data[i][3]
        companyCategory.push(item)
    }

    let result = await companyCategoryModel.insertMany(companyCategory)

    return res.send(result);
})

router.get('/project', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\Duan_Hochiminh_28102019.xlsx')
    let ws = wb.Sheets['Du An']
    let data = xlsx.utils.sheet_to_json(ws)
    var projectName = Object.keys(data[1])
    projectName.splice(71)

    // get ward id
    let wardName = await wardModel.find({}, { code: 1, _id: 1 })
    let statusProject = await projectStatusModel.find({}, { code: 1 })

    let notifyError = [{}, {}]
    for (let i = 3; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < 72; j++) {
            item[data[1][projectName[j]]] = data[i][projectName[j]]
        }
        try {
            item.points = {
                type: 'Point',
                coordinates: point(item.points)
            }
            item.polygons = {
                type: 'Polygon',
                coordinates: [polygon(item.polygons)]
            }
            item.search = unicodeString(item.name)
            statusProject.map(newItem => {
                if (newItem.cons_status) {
                    if (item.cons_status === _.get(newItem, 'code', '')) {
                        item.cons_status = mongoose.Types.ObjectId(newItem._id)
                    } else {
                        item.cons_status = mongoose.Types.ObjectId("5db66cb206d6a109b1513792")
                    }
                } else {
                    item.cons_status = mongoose.Types.ObjectId("5db66cb206d6a109b1513792")
                }

            })

            wardName.map(newItem => {
                if (newItem.code === item.ward_id) {
                    item.ward_id = mongoose.Types.ObjectId(newItem._id)
                }
            })

            if (item.parent_id) {
                let parent = await projectModel.findOne({ code: item.parent_id }, { _id: 1 })
                item.parent_id = mongoose.Types.ObjectId(parent._id)
            }
            let t = new projectModel(item)
            t._id = mongoose.Types.ObjectId()
            await t.save().then(async document => {
                let search = new searchModel(
                    {
                        name: 'project',
                        rel_id: document._id,
                        title: document.name,
                        search: `${document.name} ${parseKeyword(document.name)} ${document.address} ${parseKeyword(document.address)}`,
                        rating: 1
                    }
                )
                await search.save()
            })
        } catch (e) {
            console.log(e)
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError);
})

router.get('/product', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\DU LIEU RANH THUA\\1.BINH TAN\\QUAN_BINHTAN_DOUBLE.xlsx")
    let ws = wb.Sheets['BDS rieng le']
    let data = xlsx.utils.sheet_to_json(ws)
    var productName = Object.keys(data[1])
    productName.splice(74)

    // get ward id
    let wardName = await wardModel.find({}, { code: 1, province_id: 1 })

    //get asset planning category
    let assetPlanningCategoryName = await assetPlanningCategoryModel.find({}, { _id: 1 })
    //get product category
    let productCategoryName = await productCategoryModel.find({}, { code: 1 })
    let productTypeName = await productTypeModel.find({}, { type_code: 1 })
    let projectName = await projectModel.find({}, { code: 1, _id: 1 })
    let notifyError = [{}, {}]
    let list = []
    for (let i = 3; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < productName.length; j++) {
            item[data[1][productName[j]]] = data[i][productName[j]]
        }
        try {
            if (item.code === data[i - 1]['2']) {
                let result = await productModel.findOne({ code: item.code })

            } else {
                item.points = {
                    type: 'Point',
                    coordinates: point(item.points)
                }
                item.polygons = {
                    type: 'Polygon',
                    coordinates: polygon(item.polygons)
                }
                item.search = unicodeString(item.name)
                //get value of ward_id
                if (item.province && item.district && item.ward_id) {
                    wardName.map(newItem => {
                        if (newItem.code === item.ward_id) {
                            item.ward_id = newItem._id
                        }
                    })
                }
                productCategoryName.map(newItem => {
                    if (item.category_id === newItem.code) {
                        item.category_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })
                productTypeName.map(newItem => {
                    if (item.type_id == newItem.type_code) {
                        item.type_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })

                if (item.project_id) {
                    projectName.map(newItem => {
                        if (newItem.code === item.project_id) {
                            item.project_id = newItem._id
                        }
                    })
                }
                list.push(item)
                let t = new productModel(item)
                t._id = mongoose.Types.ObjectId()
                // await t.save().then(async document => {
                //     let search = new searchModel(
                //         {
                //             name: 'product',
                //             rel_id: document._id,
                //             title: document.name,
                //             search: `${document.name} ${parseKeyword(document.name)} ${document.address} ${parseKeyword(document.address)}`,
                //             rating: 1
                //         }
                //     )
                //     await search.save()
                // })
            }

        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(list);
})

router.get('/test', async (req, res) => {
    let list = []
    const stream = await getXlsxStream({
        filePath: ("D:\\DULIEU28102019\\DU LIEU RANH THUA\\4.GO VAP\\GOVAP.xlsx"),
        sheet: 0,
        ignoreEmpty: true,
        withHeader: false
    })
    // get ward id
    let wardName = await wardModel.find({}, { code: 1, province_id: 1 })
    //get product category
    let productCategoryName = await productCategoryModel.find({}, { code: 1 })
    let projectName = await projectModel.find({}, { code: 1, _id: 1 })
    let notifyError = [{}, {}]
    let i = 0
    stream.on('data', async x => {
        try {

            if (x.raw.obj.A !== '#') {

                let t = x.raw.obj
                let item = {}
                item.code = t.B;
                if (t.C) {
                    item.project_id = t.C;
                }
                item.points = {
                    type: 'Point',
                    coordinates: point(t.E)
                }
                item.polygons = {
                    type: 'Polygon',
                    coordinates: [polygon(t.F)]
                }
                wardName.map(newItem => {
                    if (newItem.code === t.I.toString()) {
                        item.ward_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })

                productCategoryName.map(newItem => {
                    if (newItem.code === t.K) {
                        item.category_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })

                item.category_id = mongoose.Types.ObjectId("5db2c448f835f22804d62d3c")
                item.create_uid = mongoose.Types.ObjectId("5dbb9fbbebd384b5e4ecc368")
                item.name = t.L
                item.land_area = t.R;
                item.land_area_unit_id = t.S;
                item.planning_area = t.V;
                item.planning_area_unit_id = t.W;
                item.lost_area = t.X;
                item.lost_area_unit_id = t.Y;
                item.location = t.B + ' - ' + t.L
                item.number_of_sheet = t.AE;
                item.number_of_parcel = t.AF;
                item.investor_id = t.AG;
                item.handover_time = t.AH;
                item.cons_status = null;
                item.planning_category_id = t.AK;
                item.type_id = mongoose.Types.ObjectId("5d6e57310b4d9fd73db02be9")
                item.sale_price_unit_id = t.N;
                item.currency_unit_id = t.Q;
                item.floor_area_unit_id = t.U;
                item.width_unit_id = t.AA;
                item.length_unit_id = t.AC;
                item.deposit_unit_id = t.AO;
                item.a_value_unit_id = t.AS;
                item.address = t.J;
                item.a_rent_unit_id = t.AU;
                item.floor_unit_id = t.AZ;
                item.bedroom_unit_id = t.BB;
                item.bathroom_unit_id = t.BD;

                let newProduct = new productModel(item)

                await newProduct.save().then(async document => {

                    // let search = new searchModel(
                    //     {
                    //         name: 'product',
                    //         rel_id: document._id,
                    //         title: document.name,
                    //         search: `${document.name} ${parseKeyword(document.name)} ${document.address} ${parseKeyword(document.address)}`,
                    //         rating: 1
                    //     }
                    // )
                    // await search.save()
                    // console.log(document)
                }).catch(e => {
                    console.log(e)
                })
            }
        } catch (e) {
            console.log(e)
            notifyError[1][`${i}`] = e
        }
        i++;
    })

    return res.send(notifyError)
})

// router.get('/product_double', async (req, res) =>{
//     const stream = await getXlsxStream({
//         filePath: ("D:\\DULIEU28102019\\DU LIEU RANH THUA\\1.BINH TAN\\QUAN_BINHTAN.xlsx"),
//         sheet: 0,
//         ignoreEmpty: true,
//         withHeader: false
//     })
//     // get ward id
//     let wardName = await wardModel.find({}, { code: 1, province_id: 1 })
//     //get product category
//     let productCategoryName = await productCategoryModel.find({}, { code: 1 })
//     let projectName = await projectModel.find({}, { code: 1, _id: 1 })
//     let notifyError = [{}, {}]
//     let i = 0
//     stream.on('data', async x => {
//         try {
//             if (x.raw.obj.A >= 1) {

//                 let t = x.raw.obj
//                 if(t.B )
//                 let item = {}
//                 item.code = t.B;
//                 if (t.C) {
//                     item.project_id = t.C;
//                 }
//                 item.points = {
//                     type: 'Point',
//                     coordinates: point(t.E)
//                 }
//                 item.polygons = {
//                     type: 'Polygon',
//                     coordinates: polygon(t.F)
//                 }
//                 wardName.map(newItem => {
//                     if (newItem.code === t.I.toString()) {
//                         item.ward_id = mongoose.Types.ObjectId(newItem._id)
//                     }
//                 })
//                 productCategoryName.map(newItem => {
//                     if (newItem.code === t.K) {
//                         item.category_id = mongoose.Types.ObjectId(newItem._id)
//                     }
//                 })
//                 item.name = t.L;
//                 item.land_area = t.R;
//                 item.land_area_unit_id = t.S;
//                 item.planning_area = t.V;
//                 item.planning_area_unit_id = t.W;
//                 item.lost_area = t.X;
//                 item.lost_area_unit_id = t.Y;
//                 item.location = t.B + ' - ' + t.L
//                 item.number_of_sheet = t.AE;
//                 item.number_of_parcel = t.AF;
//                 item.investor_id = t.AG;
//                 item.handover_time = t.AH;
//                 item.cons_status = t.AI;
//                 item.planning_category_id = t.AK;
//                 item.type_id = mongoose.Types.ObjectId("5d6e57310b4d9fd73db02be9")
//                 let newProduct = new productModel(item)
//                 newProduct._id = mongoose.Types.ObjectId()
//                 await newProduct.save().then(async document => {
//                     console.log(i)
//                     let search = new searchModel(
//                         {
//                             name: 'product',
//                             rel_id: document._id,
//                             title: document.name,
//                             search: `${document.name} ${parseKeyword(document.name)} ${document.address} ${parseKeyword(document.address)}`,
//                             rating: 1
//                         }
//                     )
//                     await search.save()
//                     console.log(document)
//                 }).catch(e => {
//                     console.log(e)
//                 })
//             }
//         } catch (e) {
//             notifyError[1][`${i}`] = e
//         }
//         i++;
//     })

//     return res.send(notifyError)
// })

router.get('/product_category', async (req, res) => {
    var result = await productCategoryModel.find({})
    var newResult = []
    result.map(item => {
        item._id = mongoose.Schema.ObjectId()
        newResult.push(item)
    })
    productCategoryModel.insertMany(newResult)
    res.send(newResult[0])
})

router.get('/res_company', async (req, res) => {
    let wb = xlsx.readFile(__dirname + '\\danh bạ main.xlsx')
    let ws = wb.Sheets['Mã code danh bạ']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    // check duplicate code 
    let listCode = data.map(item => item['2'])
    listCode.some((item, index) => {
        if (listCode.indexOf(item) !== index) {
            notifyError[0][`${index}`] = item
        }
    })


    let companyName = Object.keys(data[0])
    companyName.splice(23)
    let listCategoryCompany = await companyCategoryModel.find({})
    for (let i = 2; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < 23; j++) {
            item[data[0][companyName[j]]] = data[i][companyName[j]]
        }
        try {
            if (item.parent_id) {
                let companyParent = await resCompanyDemoModel.find({ code: item.parent_id })
                item.parent_id = companyParent[0]['_id']
            }
            listCategoryCompany.map(newItem => {
                if (newItem.code === item.company_category_id) {
                    item.company_category_id = mongoose.Types.ObjectId(newItem._id)
                }
            })
            item.search = `${item.company_name} ${parseKeyword(item.company_name)} ${item.display_name} ${parseKeyword(item.display_name)}`
            let t = new resCompanyDemoModel(item)
            await t.save().then(async document => {
                let search = new searchModel(
                    {
                        name: 'company',
                        rel_id: document._id,
                        title: document.display_name,
                        search: `${document.company_name} ${parseKeyword(document.company_name)} ${document.display_name} ${parseKeyword(document.display_name)}`,
                        rating: 1
                    }
                )
                await search.save()
            })
        } catch (e) {
            notifyError[1][`${i}`] = item.code + e
        }

    }

    res.send(notifyError)
})

router.get('/res_company', async (req, res) => {
    let wb = xlsx.readFile(__dirname + '\\Thông tin Danh bạ giai đoạn 2(IT-demo-2).xlsx')
    let ws = wb.Sheets['Mã code danh bạ']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    // check duplicate code 
    let listCode = data.map(item => item['2'])
    listCode.some((item, index) => {
        if (listCode.indexOf(item) !== index) {
            notifyError[0][`${index}`] = item
        }
    })


    let companyName = Object.keys(data[0])
    companyName.splice(23)
    let listCategoryCompany = await companyCategoryModel.find({})
    for (let i = 2; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < 23; j++) {
            item[data[0][companyName[j]]] = data[i][companyName[j]]
        }
        // let t = new resCompanyDemoModel(item)
        // let result = await t.save();
        // let nameItem = Object.keys(item)
        if (!item['parent_id']) {
            try {
                if (item.parent_id) {
                    let companyParent = await resCompanyDemoModel.find({ code: item.parent_id })
                    item.parent_id = companyParent[0]['_id']
                }
                listCategoryCompany.map(newItem => {
                    if (newItem.code === item.company_category_id) {
                        item.company_category_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })
                let t = new resCompanyDemoModel(item)
                await t.save();
                // await resCompanyDemoModel.findOneAndUpdate(
                //     { code: item.code },
                //     { $set: item },
                //     { upsert: true }
                // )
            } catch (e) {
                notifyError[1][`${i}`] = item.code
            }
        }
    }
    for (let i = 2; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < 23; j++) {
            item[data[0][companyName[j]]] = data[i][companyName[j]]
        }
        // let t = new resCompanyDemoModel(item)
        // let result = await t.save();
        // let nameItem = Object.keys(item)
        if (item['parent_id']) {
            try {
                if (item.parent_id) {
                    let companyParent = await resCompanyDemoModel.find({ code: item.parent_id })
                    item.parent_id = companyParent[0]['_id']
                }
                listCategoryCompany.map(newItem => {
                    if (newItem.code === item.company_category_id) {
                        item.company_category_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })
                let t = new resCompanyDemoModel(item)
                await t.save();
                // await resCompanyDemoModel.findOneAndUpdate(
                //     { code: item.code },
                //     { $set: item },
                //     { upsert: true }
                // )
            } catch (e) {
                notifyError[1][`${i}`] = item.code
            }
        }
    }


    res.send(notifyError)
})

router.get('/assets_utility', async (req, res) => {
    let wb = xlsx.readFile(__dirname + '\\Tienich_Hochiminh_28102019.xlsx')
    let ws = wb.Sheets['Tien ich']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let assetUtilityCategory = await assetUtilityCategoryModel.find({}, { code: 1, _id: 1 })

    let assetUtilityName = Object.keys(data[0])
    for (let i = 2; i < data.length; i++) {
        let item = {}
        for (let j = 1; j < 23; j++) {
            item[data[0][assetUtilityName[j]]] = data[i][assetUtilityName[j]]
        }
        try {
            item.points = {
                type: 'Point',
                coordinates: point(item.points)
            }
            assetUtilityCategory.map(newItem => {
                if (newItem.code === item.category_id)
                    item.category_id = mongoose.Types.ObjectId(newItem._id)
            })

            let t = new assetUtilityModel(item)
            t._id = mongoose.Types.ObjectId()
            await t.save()
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }
    return res.send(notifyError)
})

router.get('/province', async (req, res) => {
    let wb = await xlsx.readFile('D:\\DULIEU28102019\\formmau_hanhchinh(3).xlsx')
    let ws = wb.Sheets['Province']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let resProvinceName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        for (let j = 0; j < resProvinceName.length; j++) {
            item[`${resProvinceName[j]}`] = data[i][resProvinceName[j]]
        }
        try {
            let resPolygonItem = { ...item }
            item.points = undefined
            resPolygonItem.polygons = undefined
            let newPoint = new provinceModel(item)
            await newPoint.save().then(async document => {
                resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                resPolygonItem.collection_name = 'res_province'
                resPolygonItem.code = item.code
                let newPolygon = new resPolygonModel(resPolygonItem)
                await newPolygon.save()
            })
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})


router.get('/province_double', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\formmau_hanhchinhFORIT_1.xlsx')
    let ws = wb.Sheets['Province_double']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let resProvinceName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        for (let j = 0; j < resProvinceName.length; j++) {
            item[`${resProvinceName[j]}`] = data[i][resProvinceName[j]]
        }
        try {
            if (data[i - 1] && data[i - 1].code === item.code) {

            } else {
                let resPolygonItem = item
                item.points = null
                resPolygonItem.polygons = null
                let newPoint = new provinceModel(item)
                await newPoint.save().then(async document => {
                    resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                    resPolygonItem.collection_name = 'res_province'
                    let newPolygon = new resPolygonModel(resPolygonItem)
                    await newPolygon.save()
                })
            }

        } catch (e) {
            console.log(e)
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})


router.get('/district', async (req, res) => {
    let wb = xlsx.readFile('D:\\formmau_hanhchinh(4).xlsx')
    let ws = wb.Sheets['District']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listProvince = await provinceModel.find({}, { code: 1, _id: 1 })

    let resDistrictName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        for (let j = 0; j < resDistrictName.length; j++) {
            item[`${resDistrictName[j]}`] = data[i][resDistrictName[j]]
        }
        try {
            let resPolygonItem = item
            item.points = null
            resPolygonItem.polygons = null
            listProvince.map(newItem => {
                if (item.province_id === newItem.code) {
                    item.province_id = mongoose.Types.ObjectId(newItem._id)
                }
            })
            let newPoint = new districtModel(item)
            await newPoint.save().then(async document => {
                resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                resPolygonItem.collection_name = 'res_district'
                resPolygonItem.code = item.code
                let newPolygon = new resPolygonModel(resPolygonItem)
                await newPolygon.save()
            })
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})


router.get('/district_double', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\formmau_hanhchinhFORIT_1.xlsx')
    let ws = wb.Sheets['District_double']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listProvince = await provinceModel.find({}, { code: 1, _id: 1 })

    let resDistrictName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        for (let j = 0; j < resDistrictName.length; j++) {
            item[`${resDistrictName[j]}`] = data[i][resDistrictName[j]]
        }
        try {
            if (data[i - 1] && data[i - 1].code === item.code) {

            } else {
                let resPolygonItem = item
                item.points = null
                resPolygonItem.polygons = null
                listProvince.map(newItem => {
                    if (item.province_id === newItem.code) {
                        item.province_id = mongoose.Types.ObjectId(newItem._id)
                    }
                })
                let newPoint = new districtModel(item)
                await newPoint.save().then(async document => {
                    resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                    resPolygonItem.collection_name = 'res_district'
                    let newPolygon = new resPolygonModel(resPolygonItem)
                    await newPolygon.save()
                })
            }

        } catch (e) {
            console.log(e)
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})

router.get('/ward', async (req, res) => {
    let wb = xlsx.readFile('D:\\DULIEU28102019\\formmau_hanhchinh(4).xlsx')
    let ws = wb.Sheets['Ward']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listProvince = await provinceModel.find({}, { code: 1, _id: 1 })
    let listDistrict = await districtModel.aggregate([
        {
            $lookup: {
                from: "res_province",
                localField: "province_id",
                foreignField: "_id",
                as: "province"
            }
        },
        {
            $unwind: "$province"
        },
    ])

    let resWardName = Object.keys(data[0])
    for (let i = 0; i < data.length; i++) {
        let item = {}
        for (let j = 0; j < resWardName.length; j++) {
            item[`${resWardName[j]}`] = data[i][resWardName[j]]
        }
        try {
            let resPolygonItem = item
            item.points = null
            resPolygonItem.polygons = null
            listDistrict.map(districtItem => {
                if (item.district_id.toString() === districtItem.main_code && item.province_id.toString() === districtItem.province.code) {

                    item.district_id = districtItem._id
                }
            })

            let newPoint = new wardModel(item)
            await newPoint.save().then(async document => {
                resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                resPolygonItem.collection_name = 'res_ward'
                resPolygonItem.code = item.code
                let newPolygon = new resPolygonModel(resPolygonItem)
                await newPolygon.save()
            })
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})


router.get('/ward_double', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\formmau_hanhchinhFORIT_1.xlsx')
    let ws = wb.Sheets['Ward_double']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listProvince = await provinceModel.find({}, { code: 1, _id: 1 })
    let listDistrict = await districtModel.aggregate([
        {
            $lookup: {
                from: "res_province",
                localField: "province_id",
                foreignField: "_id",
                as: "province"
            }
        },
        {
            $unwind: "$province"
        },
    ])

    let resWardName = Object.keys(data[0])
    //console.log(resWardName)
    for (let i = 0; i < data.length; i++) {
        // let item = {}
        // for (let j = 0; j < resWardName; j++) {
        //     item[`${resWardName[j]}`] = data[i][resWardName[j]]
        // }
        var item = _.pick(data[i], resWardName)
        try {
            if (data[i - 1] && data[i - 1].code === item.code) {
                // let relWard = await wardModel.findOne({ code: item.code })
                // item.polygons = {
                //     type: 'Polygon',
                //     coordinates: [polygon(item.polygons)]
                // }
                // item.rel_id = mongoose.Types.ObjectId(relWard._id)
                // item.collection_name = 'res_ward'
                // let newPolygon = new resPolygonModel(item)
                // await newPolygon.save()
            } else {
                let resPolygonItem = item
                item.points = null
                resPolygonItem.polygons = null
                listDistrict.map(districtItem => {
                    if (item.district_id.toString() === districtItem.main_code && item.province_id.toString() === districtItem.province.code) {

                        item.district_id = districtItem._id
                    }
                })
                let newPoint = new wardModel(item)
                await newPoint.save().then(async document => {
                    resPolygonItem.rel_id = mongoose.Types.ObjectId(document._id)
                    resPolygonItem.collection_name = 'res_ward'
                    let newPolygon = new resPolygonModel(resPolygonItem)
                    await newPolygon.save()
                })
            }

        } catch (e) {
            console.log(e)
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})

router.get('/update_project', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\Duan_Hochiminh_28102019.xlsx')
    let ws = wb.Sheets['Du An']
    let data = xlsx.utils.sheet_to_json(ws)
    let list = []
    // let notifyError = [{}, {}]
    // let project = await projectModel.find({}, { _id: 1, ward_id: 1 })
    // project.map(async item => {
    //     await projectModel.findOneAndUpdate({ _id: item._id }, { $set: { ward_id: mongoose.Types.ObjectId(item.ward_id) } })
    // })
    let wardName = await wardModel.find({}, { _id: 1, code: 1 })
    for (let i = 3; i < data.length; i++) {
        for (let j = 0; j < wardName.length; j++) {
            if (wardName[j].code === data[i]['8']) {
                await projectModel.findOneAndUpdate({ code: data[i]['2'] }, { $set: { ward_id: wardName[j]._id } })
            }
        }
    }

    return res.send(list)
})

router.get('/update_project_category', async (req, res) => {
    let project = await projectModel.find({}, { _id: 1, category_id: 1 })
    // let category = await projectCategoryModel.find({}, {_id: 1, category_code: 1})
    project.map(async item => {
        await projectModel.findOneAndUpdate({ _id: item._id }, { $set: { category_id: mongoose.Types.ObjectId(item.category_id) } })
    })
    return res.send('ok')
})

router.get('/asset_planning_category', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\DU LIEU QUY HOACH\\QUAN 1\\excel\\Quan1.xlsx")
    let ws = wb.Sheets['Ma loai dat']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let list = []
    for (let i = 1; i < data.length; i++) {
        let t = new assetPlanningCategoryModel(data[i])
        t._id = mongoose.Types.ObjectId()
        await t.save().then(document => { }).catch(e => { notifyError[1][`${i}`] = e })
    }
    return res.send(notifyError)
})


router.get('/asset_planning', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\new\\2.HOCMON\\HOCMON.xlsx")
    let ws = wb.Sheets['Planning']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let list = []
    let wardName = await wardModel.find({}, { _id: 1, code: 1 })
    let category = await assetPlanningCategoryModel.find({}, { _id: 1, code: 1 })
    for (let i = 1; i < data.length; i++) {
        try {
            // wardName.map(item => {
            //     if (item.code === data[i].ward_id) {
            //         data[i].ward_id = mongoose.Types.ObjectId(item._id)
            //     }
            // })
            // category.map(item => {
            //     if (item.code === data[i].planning_category_id) {
            //         data[i].planning_category_id = mongoose.Types.ObjectId(item._id)
            //     }
            // })
            // let t = new assetPlanningModel(data[i])
            // t.points = {
            //     type: 'Point',
            //     coordinates: point(data[i].points)
            // }
            // t.polygons = {
            //     type: 'Polygon',
            //     coordinates: [polygon(data[i].polygons)]
            // }
            // t._id = mongoose.Types.ObjectId()
            // await t.save().then(document => { }).catch(e => {
            //     notifyError[1][`${i}`] = e
            // })
            await assetPlanningModel.deleteMany({code: data[i].code})
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }
    res.send(notifyError)
})

router.get('/asset_planning_double', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\new\\2.HOCMON\\HOCMON_double.xlsx")
    let ws = wb.Sheets['Planning']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]
    console.log('tt')

    let list = []
    let wardName = await wardModel.find({}, { _id: 1, code: 1 })
    let category = await assetPlanningCategoryModel.find({}, { _id: 1, code: 1 })
    for (let i = 1; i < data.length; i++) {
        try {
            // if (data[i].code === data[i - 1].code) {
            //     let result = await assetPlanningModel.findOne({ code: data[i].code }, { _id: 1, polygons: 1 })
            //     result.polygons.coordinates[1].push(polygon(data[i].polygon2))
            //     await assetPlanningModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(result._id) }, { $set: { polygons: result.polygons } })
            // } else {
            //     data[i].polygons = {
            //         type: 'MultiPolygon',
            //         coordinates: [
            //             [polygon(data[i].polygons)],
            //             [polygon(data[i].polygon2)]
            //         ]
            //     }
            //     data[i].points = {
            //         type: 'Point',
            //         coordinates: point(data[i].points)
            //     }
            //     wardName.map(item => {
            //         if (item.code === data[i].ward_id) {
            //             data[i].ward_id = mongoose.Types.ObjectId(item._id)
            //         }
            //     })
            //     category.map(item => {
            //         if (item.code === data[i].planning_category_id) {
            //             data[i].planning_category_id = mongoose.Types.ObjectId(item._id)
            //         }
            //     })
            //     let t = new assetPlanningModel(data[i])
            //     t._id = mongoose.Types.ObjectId()
            //     await t.save().then(document => { }).catch(e => {
            //         notifyError[1][`${i}`] = e
            //         console.log(e)
            //     })
            // }
            await assetPlanningModel.deleteMany({code: data[i].code})
        } catch (e) {
            notifyError[1][`${i}`] = e
            console.log(e)
        }
    }
    res.send(notifyError)
})

router.get('/search_address', async (req, res) => {
    let provinceName = await provinceModel.find({}, { _id: 1, name: 1 })
    let districtName = await districtModel.find({}, { _id: 1, name: 1 })
    let wardName = await wardModel.find({}, { _id: 1, name: 1 })
    listError = []
    try {
        // provinceName.map(async item => {
        //     let search = new searchModel(
        //         {
        //             name: 'province',
        //             rel_id: mongoose.Types.ObjectId(item._id),
        //             title: item.name,
        //             search: `${item.name} ${parseKeyword(item.name)}}`,
        //             rating: 1
        //         }
        //     )
        //     await search.save()
        // })
        // districtName.map(async item =>{
        //     let search = new searchModel(
        //         {
        //             name: 'district',
        //             rel_id: mongoose.Types.ObjectId(item._id),
        //             title: item.name,
        //             search: `${item.name} ${parseKeyword(item.name)}}`,
        //             rating: 1
        //         }
        //     )
        //     await search.save()
        // })
        wardName.map(async item => {
            let search = new searchModel(
                {
                    name: 'ward',
                    rel_id: mongoose.Types.ObjectId(item._id),
                    title: item.name,
                    search: `${item.name} ${parseKeyword(item.name)}}`,
                    rating: 1
                }
            )
            await search.save()
        })
    } catch (e) {
        listError.push(e)
    }
    res.send(listError)
})

router.get('/res_road_category', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\DU LIEU CON DUONG\\HCM_Con đường(IT-giai đoạn 2).xlsx")
    let ws = wb.Sheets['Toàn đường']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listDistrict = await districtModel.find({}, { _id: 1, code: 1 })

    let list = []
    for (let i = 0; i < data.length; i++) {
        try {
            data[i].district_id = data[i].district_id.split(',')
            let newDistrict = []
            listDistrict.map(item => {
                if (data[i].district_id.indexOf(item.code) !== -1) {
                    newDistrict.push(mongoose.Types.ObjectId(item._id))
                }
            })
            data[i].district_id = newDistrict
            let t = new resRoadCategoryModel(data[i])
            await t.save().then(document => {

            }).catch(e => {
                notifyError[1][`${i}`] = data[i].code
            })
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(notifyError)
})

router.get('/res_road', async (req, res) => {
    let wb = xlsx.readFile("D:\\DULIEU28102019\\DU LIEU CON DUONG\\HCM_Con đường(IT-giai đoạn 2).xlsx")
    let ws = wb.Sheets['Phân đoạn']
    let data = xlsx.utils.sheet_to_json(ws)
    let notifyError = [{}, {}]

    let listDistrict = await districtModel.find({}, { _id: 1, code: 1 })
    let listPlanningCategory = await assetPlanningCategoryModel.find({}, { code: 1, _id: 1 })
    let listResRoadCategory = await resRoadCategoryModel.find({})

    let list = []
    for (let i = 0; i < data.length; i++) {
        // console.log(data[i])
        try {
            data[i].district_id = data[i].district_id.split(',')
            let newDistrict = []
            listDistrict.map(item => {
                if (data[i].district_id.indexOf(item.code) !== -1) {
                    newDistrict.push(mongoose.Types.ObjectId(item._id))
                }
            })
            data[i].district_id = newDistrict
            listPlanningCategory.map(item => {
                if (item.code === data[i].planning_category_id) {
                    data[i].planning_category_id = mongoose.Types.ObjectId(item._id)
                }
            })
            listResRoadCategory.map(item => {
                if (item.code === data[i].category_id) {
                    data[i].category_id = mongoose.Types.ObjectId(item._id)
                }
            })
            data[i].polygons = {
                type: 'LineString',
                coordinates: polygon(data[i].polygons)

            }
            data[i].points = {
                type: 'Point',
                coordinates: point(data[i].points)
            }
            let t = new resRoadModel(data[i])
            await t.save().then(document => {

            }).catch(e => {
                notifyError[1][`${i}`] = data[i].code
            })
        } catch (e) {
            notifyError[1][`${i}`] = e
        }
    }

    return res.send(listResRoadCategory)
})

router.get('/update_project_companyid', async (req, res) => {
    let wb = xlsx.readFile('D:\\import_data\\Duan_Hochiminh_28102019.xlsx')
    let ws = wb.Sheets['Du An']
    let data = xlsx.utils.sheet_to_json(ws)

    await data.map(async item => {
        if (item['47']) {
            let t = item['47'].split(',')
            if (t.length > 0) {
                var newResult = []
                for (let i = 0; i < t.length; i++) {
                    let investor = await resCompanyDemoModel.find({ code: t[i] }, { _id: 1, code: 1 })
                    newResult.push(mongoose.Types.ObjectId(investor[0]._id))
                }
                await projectModel.findOneAndUpdate({ code: item['2'] }, { $set: { investor_id: newResult } })
            }
        }
        if (item['48']) {
            let t = item['48'].split(',')
            if (t.length > 0) {
                var newResult = []
                for (let i = 0; i < t.length; i++) {
                    let contractor = await resCompanyDemoModel.find({ code: t[i] }, { _id: 1, code: 1 })
                    newResult.push(mongoose.Types.ObjectId(contractor[0]._id))
                }
                await projectModel.findOneAndUpdate({ code: item['2'] }, { $set: { contractor_id: newResult } })
            }
        }
        if (item['49']) {
            let t = item['49'].split(',')
            if (t.length > 0) {
                var newResult = []
                for (let i = 0; i < t.length; i++) {
                    let designer = await resCompanyDemoModel.find({ code: t[i] }, { _id: 1, code: 1 })
                    newResult.push(mongoose.Types.ObjectId(designer[0]._id))
                }
                await projectModel.findOneAndUpdate({ code: item['2'] }, { $set: { designer_id: newResult } })
            }
        }
        if (item['50']) {
            let t = item['50'].split(',')
            if (t.length > 0) {
                var newResult = []
                for (let i = 0; i < t.length; i++) {
                    let managet = await resCompanyDemoModel.find({ code: t[i] }, { _id: 1, code: 1 })
                    newResult.push(mongoose.Types.ObjectId(managet[0]._id))
                }

                let tho1 = await projectModel.findOneAndUpdate({ code: item['2'] }, { $set: { manager_id: newResult } })

            }
        }
    })

    return res.send('ok')
})


module.exports = router