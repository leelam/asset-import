var express = require('express')
var router = express.Router();
var parseJSON =require('../helpers/safelyParseJSON')
var postCategoryController = require('../controllers/post_category.controller')
var historyController = require('../controllers/history.controller')
var authMiddleware = require('../middlewares/authenticate.middleware')
var postController = require('../controllers/post.controller')
router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await postCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})

router.get('/category/list_tree',async(req,res)=>{
    return res.json(await postCategoryController.ListTree());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await postCategoryController.Count())
})
router.post('/category/add',authMiddleware,async(req,res)=>{
    var result = await postCategoryController.Add({...authMiddleware,create_uid:req.user._id})
 return res.json(result)

})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing Post Category ID')
    }
    var postDetail = await postCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body;
    return res.json(await postCategoryController.Delete(id))
})

router.put('/category/edit',authMiddleware,async(req,res)=>{
    return res.json(await postCategoryController.Edit(req.body))
})

router.post('/add',async(req,res)=>{
    return res.json(await postController.Add(req.body))
})

router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listPost = await postController.List(limit,page,filter);
    return res.json(listPost)
})

router.get('/count',async(req,res)=>{
    return res.json(await postController.Count(req.query.filter))
})
router.get('/detail',async(req,res)=>{
    return res.json(await postController.Detail(req.query.id))
})
router.delete('/delete',async(req,res)=>{
    return res.json(await postController.Delete(req.body.id))
})
router.put('/edit',authMiddleware,async(req,res)=>{
    return res.json(await postController.Edit({...req.body,write_uid:req.user._id}))
})
router.get('/history',async(req,res)=>{
    return res.json(await historyController.ListPostHistory(req.query.post_id,req.query.type,req.query.limit));
})
//---------
router.get('/test',async(req,res)=>{
    return res.json(await postController.Test())
})
router.get('/revert_history', authMiddleware, async(req, res) =>{
    return res.send(await postController.RevertHistory(req.query.id, req.user._id)) 
})
//-----------
module.exports = router;