var express = require('express');
var path = require('path');
var fs = require('fs');
var rimraf = require('rimraf');
var router = express.Router();
var multer = require('multer');
var DIRECTION = require('../configs/direction.config');

///change host model import
var postModel = require('../models/post.model');
///end of change host model import

var image_storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `./${DIRECTION.RESOURCE}/${req.query.direction || 'images'}`);
  },
  filename: (req, file, cb) => {
    var filename = path.parse(file.originalname).name;
    var ext = path.parse(file.originalname).ext;
    if (['.png', '.gif', '.jpeg', '.jpg'].includes(ext)) {
      cb(null, filename + Date.now() + ext);
    } else {
      cb(false, '');
    }
  },
  onError: (err, next) => {
    next(err);
  }
});
var document_storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `./${DIRECTION.RESOURCE}/${req.query.direction || 'documents'}`);
  },
  filename: (req, file, cb) => {
    var filename = path.parse(file.originalname).name;
    var ext = path.parse(file.originalname).ext;

    if (['.pdf'].includes(ext)) {
      cb(null, filename + Date.now() + ext);
    } else {
      cb(false, '');
    }
  },
  onError: (err, next) => {
    next(err);
  }
});

var upload_image = multer({ storage: image_storage });
var upload_document = multer({ storage: document_storage });
var fileController = require('../controllers/file.controller');
router.get('/list', async (req, res) => {
  return res.json(await fileController.ListImages(req.query.direction || ''));
});
router.get('/read_kmz', async (req, res) => {
  const directoryPath = path.join(__dirname, `../resources/QUAN3.kmz`);
  var file = fs.readFileSync('D:/QUAN3.kmz');
  return res.json(file);
});
router.post('/upload', upload_image.single('file'), async (req, res) => {
  return res.json([]);
});
router.post(
  '/upload_resource',
  upload_document.single('file'),
  async (req, res) => {
    return res.json([]);
  }
);
router.post('/create_folder', async (req, res) => {
  try {
    var type = req.body.type || 'IMAGE';
    if (!['IMAGE', 'DOCUMENT'].includes(type))
      return res.json({ status: false, message: 'type is not valid' });
    var directoryPath =
      `.${DIRECTION.RESOURCE}/${req.body.direction}` || 'images';
    var newPath = `${directoryPath}/${req.body.name}`;
    if (fs.existsSync(newPath)) {
      return res.json({ status: false, message: 'Thư mục đã tồn tại' });
    }
    fs.mkdirSync(newPath);
    return res.json({ status: true, message: 'Tạo mới thành công' });
  } catch (e) {
    return res.json({ status: false, message: e.toString() });
  }
});

router.delete('/delete_folder', async (req, res) => {
  try {
    if (req.body.name === 'images') {
      return res.json({
        status: false,
        message: 'Không thể xóa thư mục gốc (images)'
      });
    }
    const directoryPath = path.join(
      __dirname,
      `../${DIRECTION.IMAGE_SOURSE}/${req.body.direction || 'images'}`
    );
    newPath = `${directoryPath}/${req.body.name}`;
    rimraf.sync(newPath);
    return res.json({
      status: true,
      message: 'Xóa thành công'
    });
  } catch (err) {
    return res.json({
      status: false,
      message: err.toString()
    });
  }
});
router.get('/change_host', async (req, res) => {
  var listPost = await postModel.find();
  await listPost.map(async post => {
    await postModel.updateMany(
      { _id: post._id },
      {
        content: post.content.replace(
          /27.74.250.96:8386/g,
          '192.168.1.240:9000'
        )
      }
    );
  });
  return res.json('ok');
});
// const parseKMZ = require('parse-kmz');
var FILE = 'D:/demo.kmz';
router.get('/read_kml', async (req, res) => {
  parseKMZ
    .toJson(FILE)
    .then(data => {
      console.log(data);
      res.json(data.features.map(x => x.id));
    })
    .catch(err => console.log(err));
});
router.get('/test', async (req, res) => {
  let list = [];
  const stream = await getXlsxStream({
    filePath: __dirname + '\\bat dong san demo.xlsx',
    sheet: 0,
    ignoreEmpty: true,
    withHeader: false
  });
  // get ward id
  let wardName = await wardModel.find({}, { code: 1, province_id: 1 });
  //get product category
  let productCategoryName = await productCategoryModel.find({}, { code: 1 });
  let projectName = await projectDemoModel.find({}, { code: 1, _id: 1 });
  let notifyError = [{}, {}];
  let i = 0;
  stream.on('data', async x => {
    try {
      if (x.raw.obj.A >= 1) {
        let t = x.raw.obj;
        let item = {};
        item.code = t.B;
        if (t.C) {
          item.project_id = t.C;
        }
        item.points = {
          type: 'Point',
          coordinates: point(t.E)
        };
        item.polygons = {
          type: 'Polygon',
          coordinates: polygon(t.F)
        };
        wardName.map(newItem => {
          if (newItem.code === t.I.toString()) {
            item.ward_id = mongoose.Types.ObjectId(newItem._id);
          }
        });
        productCategoryName.map(newItem => {
          if (newItem.code === t.K) {
            item.category_id = mongoose.Types.ObjectId(newItem._id);
          }
        });
        item.name = t.L;
        item.land_area = t.R;
        item.land_area_unit_id = t.S;
        item.planning_area = t.V;
        item.planning_area_unit_id = t.W;
        item.lost_area = t.X;
        item.lost_area_unit_id = t.Y;
        item.location = t.B + ' - ' + t.L;
        item.number_of_sheet = t.AE;
        item.number_of_parcel = t.AF;
        item.investor_id = t.AG;
        item.handover_time = t.AH;
        item.cons_status = t.AI;
        item.planning_category_id = t.AK;
        item.type_id = mongoose.Types.ObjectId('5d6e57310b4d9fd73db02be9');
        let newProduct = new productDemoModel(item);
        newProduct._id = mongoose.Types.ObjectId();
        await newProduct
          .save()
          .then(async document => {
            // let search = new searchModel(
            //     {
            //         name: 'product',
            //         rel_id: document._id,
            //         title: document.name,
            //         search: `${document.name} ${parseKeyword(document.name)} ${document.address} ${parseKeyword(document.address)}`,
            //         rating: 1
            //     }
            // )
            // await search.save()
          })
          .catch(e => {
            console.log(e);
          });
      }
    } catch (e) {
      notifyError[1][`${i}`] = e;
    }
    i++;
  });

  return res.send(notifyError);
});
module.exports = router;
