var express = require("express");
var router = express.Router();
var _ = require('lodash')
var planningController = require("../controllers/planning.controller");
var parseJSON =require('../helpers/safelyParseJSON')
var planningCategoryController = require("../controllers/planning_category.controller");
router.get("/category/list", async (req, res) => {
  try {
    var limit = parseInt(req.query.limit) || 10;
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    return res.json(await planningCategoryController.List(limit, page, null));
  } catch (err) {
      return res.json({
          code: 1,
          message:err.toString()
      })
  }
});

router.get("/category/count",async(req,res)=>{
    return res.json(await planningCategoryController.Count(null))
})

router.get('/category/detail',async(req,res)=>{
  var planningCategory = await planningCategoryController.Detail(req.query.id);
  return res.json(planningCategory.length ? planningCategory[0] : {});
})

router.get('/count',async(req,res)=>{
  var filter = parseJSON(req.query.filter) || [];
 

  var total = await planningController.Count(filter);
  if (total[0] && total[0].total) {
    return res.json(total[0].total)
  }else {
    return res.json(0)
  }
 
})

router.get('/list',async(req,res)=>{
  try {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    return res.json(await planningController.List(limit, page, filter));
  } catch (err) {
      return res.json({
          code: 1,
          message:err.toString()
      })
  }
})

router.get('/detail',async(req,res)=>{
  var {planning_id} = req.query;
  if (planning_id) {
    return res.json(await planningController.Detail(planning_id))
  }else {
    return res.json({})
  }
})

module.exports = router;
