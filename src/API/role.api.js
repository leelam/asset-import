var express = require('express');
var router = express.Router();
var roleController = require('../controllers/role.controller')
const authMiddleware = require('../middlewares/authenticate.middleware')
const parseJSON = require('../helpers/safelyParseJSON')
router.get('/list_module_category', async (req, res) => {
    return res.json(await roleController.ListModuleCategory())
})

router.get('/list_module', async (req, res) => {
    return res.json(await roleController.ListModule(req.query.category_id))
})
router.get('/group_list', async (req, res) => {
    var listCategory = await roleController.ListGroup();
    return res.json(listCategory)
})



router.get('/group_count', async (req, res) => {
    return res.json(await roleController.CountGroup())
})
router.post('/group_add', async (req, res) => {
    var result = await roleController.AddGroup(req.body)
    return res.json(result)

})
router.get('/group_detail', async (req, res) => {
    var { id } = req.query;
    if (!id) {
        return res.json('Missing guide Category ID')
    }
    var groupDetail = await roleController.GroupDetail(id);
    return res.json(groupDetail)
})

router.delete('/group_delete', async (req, res) => {
    var { id } = req.body;
    return res.json(await roleController.Delete(id))
})
router.put('/group_edit', async (req, res) => {
    var result = await roleController.EditGroup(req.body)
    return res.json(result)
})


router.get('/user_company/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listUserCompany = await roleController.GetListUserCompany(page, limit, parseJSON(req.query.project), filter, parseJSON(req.query.sort))
    return res.json(listUserCompany)
})
router.get('/user_company/detail',async(req,res)=>{
    return res.json(await roleController.DetailUserCompany(req.query.id))
})
router.get('/user_company/count', async (req, res) => {
    var filter = parseJSON(req.query.filter);
    var countProject = await roleController.CountUserCompany(filter);
    return res.json(countProject.length ? countProject[0].total : 0)

})
router.post('/user_company/add', async (req, res) => {
    return res.json(await roleController.AddUserCompany(req.body))
})
router.delete('/user_company/delete', async (req, res) => {
    return res.json(await roleController.DeleteUserCompany(req.body.id))
})
router.post('/appoint/add', authMiddleware, async (req, res) => {
    const data = req.body;
    return res.json(await roleController.AddUserAgency(
        data, 
        req.user._id))
})
module.exports = router;