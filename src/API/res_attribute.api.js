var express =require('express');
var router = express.Router()
var resAttributeController = require('../controllers/res_atrribute.controller')
router.get('/list',async(req,res)=>{
    var listAttribute = await resAttributeController.List()
    return res.json(listAttribute)
})

router.get('/add',async(req,res)=>{
    
    return res.end(await resAttributeController.Add())
})

module.exports = router;