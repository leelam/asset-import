var express = require("express");
const  { get } = require('lodash')
var router = express.Router();
const mongoose = require('mongoose')
var parseJSON = require('../helpers/safelyParseJSON')
var productCategoryController = require("../controllers/product_category.controller");
var productTypeController = require('../controllers/product_type.controller')
var productController = require('../controllers/product.controller')
var productApproveController = require('../controllers/product_approve.controller')
const authMiddleWare = require('../middlewares/authenticate.middleware')
const { uploadPhotoProduct } = require('../middlewares/multer.middleware')
const {isPoint, isPolygon} = require('../helpers/geometry.helper')
const {omit} = require('lodash')
const {host} = require('../configs/database.config')
router.get('/demo',async(req,res)=>{

  return res.json(await productController.Demo())
})
router.get("/category/list", async (req, res) => {
  try {
    var limit = parseInt(req.query.limit) || 10;
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    return res.json(await productCategoryController.List(limit, page, null));
  } catch (err) {
    return res.json({
      code: 1,
      message: err.toString()
    })
  }
});
router.get("/category/count", async (req, res) => {
  return res.json(await productCategoryController.Count(null))
})

router.get('/category/detail', async (req, res) => {
  var productCategory = await productCategoryController.Detail(req.query.id);
  return res.json(productCategory.length ? productCategory[0] : {});
})

router.get('/test', async (req, res) => {
  var listResult = await productCategoryController.Test()
  return res.json(listResult)
})

router.put('/category/edit', async (req, res) => {
  return res.json(await productCategoryController.Edit(req.body))
})

router.delete('/category/delete', async (req, res) => {
  var { id } = req.body;
  return res.json(await productCategoryController.Delete(id))
})

router.post('/category/add', async (req, res) => {
  return res.json(await productCategoryController.Add(req.body))
})

// type api

router.get('/type/list', async (req, res) => {
  try {
    var limit = parseInt(req.query.limit) || 10;
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    return res.json(await productTypeController.List(limit, page, null));
  } catch (err) {
    return res.json({
      code: 1,
      message: err.toString()
    })
  }
})

router.get('/type/count', async (req, res) => {
  return res.json(await productTypeController.Count())
})

router.post('/type/add', async (req, res) => {
  return res.json(await productTypeController.Add(req.body))
})

router.get('/type/detail', async (req, res) => {
  return res.json(await productTypeController.Detail(req.query.ID))
})

router.put('/type/edit', async (req, res) => {
  return res.json(await productTypeController.Edit(req.body))
})

router.delete('/type/delete', async (req, res) => {
  var { id } = req.body;
  return res.json(await productTypeController.Delete(id))
})

// product
router.get('/list', async (req, res) => {
  var limit = parseInt(req.query.limit) || 10;
  var filter = parseJSON(req.query.filter) || [];
  limit = limit > 100 ? 100 : limit;
  var page = parseInt(req.query.page) || 1;
  var listCategory = await productController.List(
    limit,
    page,
    filter,
    parseJSON(req.query.project),
    parseJSON(req.query.sort)
  );
  return res.json(listCategory)
})

router.get('/count', async (req, res) => {
  var filter = parseJSON(req.query.filter);
  var countProduct = await productController.Count(filter);
  return res.json(countProduct.length ? countProduct[0].total : 0)
})

router.get('/detail', async (req, res) => {
  var product = await productController.Detail(req.query.id)
  return res.json(product.length ? product[0] : {})
})

 router.post('/add-product',
 authMiddleWare,
    (req,res,next) =>{
        req.idProduct = new mongoose.Types.ObjectId()
        req.idLegalInfo = new mongoose.Types.ObjectId()
        return next();
    },
    uploadPhotoProduct,
    async(req, res)=>{
        if(!req.files.image || !req.files.sub_image)
            return res.json({
                status: false,
                message: 'Thiếu hình ảnh'
            })
        const {idProduct} = req;
        const {_id} = req.user;
        const { general_info, legal_info, description, landlord_info } = req.body
        let cons_design_info = JSON.stringify({});
        if (req.body.cons_design_info) {
            cons_design_info = req.body.cons_design_info;
        }
        const legal_infoObj = JSON.parse(legal_info);
        const keys = Object.keys(legal_infoObj);
        const general_infoObj = JSON.parse(general_info)
        delete general_infoObj._id
        let shapeLegalInfo;
         keys.forEach((item, index) =>{
            shapeLegalInfo = {
               ...shapeLegalInfo,
               [item]: {
                   status: '',
                   file: null
               }
            }
                shapeLegalInfo[item].status = legal_infoObj[item]
                if(legal_infoObj[item] === 'Có' && req.files[`${item}_link`] ){
                    let pathLegal = req.files[`${item}_link`][0].path;
                    pathLegal = pathLegal.replace('resources','')
                    const link = `${host}${pathLegal}`
                    shapeLegalInfo[item].file = link
                }
        })
        let pathImage = req.files.image[0].path
            pathImage = pathImage.replace('resources','')
        const linkImage = `${host}${pathImage}`
        general_infoObj.image = linkImage
        if(req.files.document)
            general_infoObj.document = req.files.document.map(item => {
                let pathDocument = item.path;
                pathDocument = pathDocument.replace('resources','')
                const linkDocument = `${host}${pathDocument}`
                return linkDocument
            })
        general_infoObj.sub_image = req.files.sub_image.map(item=> {
            let pathSubImage = item.path;
            pathSubImage = pathSubImage.replace('resources','')
            const linkSubImage = `${host}${pathSubImage}`
            return linkSubImage
        })
        if( general_infoObj.points && !general_infoObj.points.type)
        general_infoObj.points = isPoint(general_infoObj.points) ? { type: 'Point', coordinates: general_infoObj.points } : null;
        if( general_infoObj.polygons && !general_infoObj.polygons.type)
        general_infoObj.polygons = isPolygon(general_infoObj.polygons) ? { type: 'Polygon', coordinates: [general_infoObj.polygons] } : null;
        const addGeneralInfoProduct = await productController.addProduct(
            _id,
            idProduct,
            general_infoObj,
            shapeLegalInfo,
            JSON.parse( description),
            JSON.parse(cons_design_info),
            JSON.parse(landlord_info))
        if(!addGeneralInfoProduct.status)
            return res.json({
                status: false,
                message: addGeneralInfoProduct.message
            })
        return res.json({
            status: true
        })
})

router.put('/edit', authMiddleWare,
(req,res,next) =>{
    req.idProduct = req.query.idProduct
    req.idLegalInfo = req.query.idLegalInfo
    return next();
},
    uploadPhotoProduct,
    async(req, res)=>{
      try{
    const {idProduct}= req;
    const {_id, role } = req.user;
    let { general_info, legal_info, description, cons_design_info, landlord_info } = req.body;
    let general_infoObj = JSON.parse(general_info)
    const legal_infoObj = JSON.parse(legal_info);
    const keys = Object.keys(legal_infoObj);
    let shapeLegalInfo;
    keys.forEach((item, index) =>{
      shapeLegalInfo = {
         ...shapeLegalInfo,
         [item]: {
             status: '',
             file: null
         }
      }
          shapeLegalInfo[item].status = legal_infoObj[item]
          shapeLegalInfo[item].file = legal_infoObj[`${item}_link`]
          if(legal_infoObj[item] === 'Có' && req.files[`${item}_link`] ){
            let pathLegal = req.files[`${item}_link`][0].path;
                    pathLegal = pathLegal.replace('resources','')
                    const link = `${host}${pathLegal}`
                    shapeLegalInfo[item].file = link
        }
  })
    general_infoObj = omit(general_infoObj, ['ward', 'district', 'province'])
    if( general_infoObj.points && !general_infoObj.points.type)
    general_infoObj.points = isPoint(general_infoObj.points) ? { type: 'Point', coordinates: general_infoObj.points } : null;
    if( general_infoObj.polygons && !general_infoObj.polygons.type)
    general_infoObj.polygons = isPolygon(general_infoObj.polygons) ? { type: 'Polygon', coordinates: [general_infoObj.polygons] } : null;
    let reqSubImage = []
    if(typeof general_infoObj.sub_image[0] ==='object'){
    general_infoObj.sub_image.map(item => {
        if(item.originURL)
            reqSubImage.push(item.originURL)
    })
    }
    let sub_image
    if(req.files.sub_image){
      sub_image  = req.files.sub_image.map(item=> {
        let pathSubImage = item.path;
        pathSubImage = pathSubImage.replace('resources', '')
        const linkSubImage = `${host}${pathSubImage}`
        return linkSubImage
    })
        reqSubImage.push(...sub_image)
    }
    
    let reqDocument = []
    if(typeof general_infoObj.document[0] ==='object'){
    general_infoObj.document.map(item => {
        if(item.originURL)
        reqDocument.push(item.originURL)
    })
    let document
    if(req.files.document){
      document  = req.files.document.map(item => {
        let pathDocument = item.path;
        pathDocument = pathDocument.replace('resources', '')
        const linkDocument = `${host}${pathDocument}`
        return linkDocument
    })
        reqDocument.push(...document)
    }
}
    if(req.files.image){
      let pathImage = req.files.image[0].path
      pathImage = pathImage.replace('resources','')
      const linkImage = `${host}${pathImage}`
      general_infoObj.image = linkImage
  }
    if(general_infoObj.sub_image && typeof general_infoObj.sub_image[0] ==='object')
        general_infoObj.sub_image = reqSubImage
        general_infoObj.document = reqDocument
    const cons_design_infoObj = cons_design_info !=='undefined'? JSON.parse(cons_design_info) : ''
    const land_lord_infoObj = landlord_info !=='undefined'? JSON.parse(landlord_info) : ''
    const product = await productApproveController.updateProductExisted(
        _id,
        idProduct,
        general_infoObj,
        shapeLegalInfo,
        JSON.parse(description),
        cons_design_infoObj,
        land_lord_infoObj,
        role.group._id,
        req.query.type
    )
    if(product)
        return res.json({
            status: true
        })
      } catch(e){
        console.log(e)
      }
    return res.json({
        status: false
    })
})

//approve

router.get('/approve/list', authMiddleWare, async(req, res) =>{
  var limit = parseInt(req.query.limit) || 10;
  var filter = parseJSON(req.query.filter) || [];
  limit = limit > 100 ? 100 : limit;
  var page = parseInt(req.query.page) || 1;
  const group = get(req.user, 'role.group');
  if(!group)
    return res.json({
      status: false,
      message: 'Bạn không có quyền xem danh sách'
    }) // chưa check role
  switch (group._id.toString()){
    case '5dbb9d6065bb02ebb0ec7fb4':
        return res.json(await productApproveController.ListApprovedDT());
    case '5db1294aa3a60236c439f115':
        return res.json(await productApproveController.ListApprovedRM());
    case '5db12ac3a3a60236c439f618':
        return res.json(await productApproveController.ListApprovedDT());
    default:
        return res.json({
          status: false,
          message: 'Bạn không có quyền duyệt'
        })
  }
})

router.get('/approve/count',authMiddleWare, async(req, res) =>{
  var filter = parseJSON(req.query.filter) || [];
  console.log(filter)
  const group = get(req.user, 'role.group');
  if(!group)
    return res.json({
      status: false,
      message: 'Bạn không có quyền xem danh sách'
    }) // chưa check role
  let role
  switch (group._id.toString()){
    case '5dbb9d6065bb02ebb0ec7fb4':
        role = 'DT'
        break;
    case '5db12ac3a3a60236c439f618':
        role = 'DT'
        break;
    case '5db12ac3a3a60236c439f618':
        role = 'DT'
        break;
    default:
        return res.json({
          status: false,
          message: 'Bạn không có quyền duyệt'
        })
  }
  var countProductApprove = await productApproveController.Count(filter)
  return res.json(countProductApprove)
})

router.get('/approve/detail', async(req, res) =>{
  return res.json(await productApproveController.Detail(req.query.id))
})

router.get('/approve/list_approved', authMiddleWare, async(req, res) =>{
  const group = get(req.user, 'role.group');
  if(!group)
    return res.json({
      status: false,
      message: 'Bạn không có quyền xem danh sách'
    }) // chưa check role
  switch (group._id.toString()){
    case '5dbb9d6065bb02ebb0ec7fb4':
        return res.json(await productApproveController.ListApprovedDT());
    case '5db1294aa3a60236c439f115':
        return res.json(await productApproveController.ListApprovedRM());
    case '5db12ac3a3a60236c439f618':
        return res.json(await productApproveController.ListApprovedDT());
    default:
        return res.json({
          status: false,
          message: 'Bạn không có quyền duyệt'
        })
  }
})

router.put('/approve-product', authMiddleWare,
(req,res,next) =>{
    req.idProduct = req.query.idProduct
    req.idLegalInfo = req.query.idLegalInfo
    return next();
},
    uploadPhotoProduct,
    async(req, res)=>{
    const {idProduct}= req;
    const {_id, role } = req.user;
    if(!role)
      return res.json({
        status: false,
        message: 'Dont have permission'
      })
    const check = await productApproveController.checkApprove(role.group._id, idProduct)
    if(!check)
        return res.json({
            status: false,
            message: 'Can not find approved request'
        })
    let { general_info, legal_info, description, cons_design_info, landlord_info } = req.body;
    let general_infoObj = JSON.parse(general_info)
    const legal_infoObj = JSON.parse(legal_info);
    const keys = Object.keys(legal_infoObj);
    let shapeLegalInfo;
    keys.forEach((item, index) =>{
      shapeLegalInfo = {
         ...shapeLegalInfo,
         [item]: {
             status: '',
             file: null
         }
      }
          shapeLegalInfo[item].status = legal_infoObj[item]
          shapeLegalInfo[item].file = legal_infoObj[`${item}_link`]
          if(legal_infoObj[item] === 'Có' && req.files[`${item}_link`] ){
            let pathLegal = req.files[`${item}_link`][0].path;
                    pathLegal = pathLegal.replace('resources','')
                    const link = `${host}${pathLegal}`
                    shapeLegalInfo[item].file = link
        }
  })
    general_infoObj = omit(general_infoObj, ['ward', 'district', 'province'])
    if( general_infoObj.points && !general_infoObj.points.type)
    general_infoObj.points = isPoint(general_infoObj.points) ? { type: 'Point', coordinates: general_infoObj.points } : null;
    if( general_infoObj.polygons && !general_infoObj.polygons.type)
    general_infoObj.polygons = isPolygon(general_infoObj.polygons) ? { type: 'Polygon', coordinates: [general_infoObj.polygons] } : null;
    let reqSubImage = []
    if(typeof general_infoObj.sub_image[0] ==='object'){
    general_infoObj.sub_image.map(item => {
        if(item.originURL)
            reqSubImage.push(item.originURL)
    })
    }
    let sub_image
    if(req.files.sub_image){
      sub_image  = req.files.sub_image.map(item=> {
        let pathSubImage = item.path;
        pathSubImage = pathSubImage.replace('resources', '')
        const linkSubImage = `${host}${pathSubImage}`
        return linkSubImage
    })
        reqSubImage.push(...sub_image)
    }
    
    let reqDocument = []
    if(typeof general_infoObj.document[0] ==='object'){
    general_infoObj.document.map(item => {
        if(item.originURL)
        reqDocument.push(item.originURL)
    })
    let document
    if(req.files.document){
      document  = req.files.document.map(item => {
        let pathDocument = item.path;
        pathDocument = pathDocument.replace('resources', '')
        const linkDocument = `${host}${pathDocument}`
        return linkDocument
    })
        reqDocument.push(...document)
    }
}
    if(req.files.image){
      let pathImage = req.files.image[0].path
      pathImage = pathImage.replace('resources','')
      const linkImage = `${host}${pathImage}`
      general_infoObj.image = linkImage
  }
    if(general_infoObj.sub_image && typeof general_infoObj.sub_image[0] ==='object')
        general_infoObj.sub_image = reqSubImage
    if(general_infoObj.document && typeof general_infoObj.document[0] ==='object')
        general_infoObj.document = reqDocument
    const cons_design_infoObj = cons_design_info !=='undefined'? JSON.parse(cons_design_info) : ''
    const land_lord_infoObj = landlord_info !=='undefined'? JSON.parse(landlord_info) : ''
    const product = await productApproveController.updateProduct(
        _id,
        idProduct,
        general_infoObj,
        shapeLegalInfo,
        JSON.parse(description),
        cons_design_infoObj,
        land_lord_infoObj,
        role.group._id,
        req.query.type
    )
    if(product)
        return res.json({
            status: true
        })
    return res.json({
        status: false
    })
})

router.get('/get-form-product',
authMiddleWare,
  async(req, res)=>{
    const {id, isExisted} = req.query;
    const result = await productApproveController.GetDetailProduct(id, isExisted);
    return res.json(result)
  }
)
router.get('/get-list-cons-status', async(req, res)=>{
  const listConStatus = await productController.getListConStatus();
  return res.json({
      status: true,
      data: listConStatus
  })
})
module.exports = router;
