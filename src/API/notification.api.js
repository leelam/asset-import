const express = require('express');
const authMiddleware = require('../middlewares/authenticate.middleware')
const notificationController = require('../controllers/notification.controller')
const parseJSON =require('../helpers/safelyParseJSON')
const router = express.Router();
router.put('/read_all',authMiddleware,async(req,res)=>{
    return res.json(await notificationController.ReadAll(req.user._id))
})
router.get('/list',authMiddleware,async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listNoti = await notificationController.List(limit,page,filter,req.user._id)

    return res.json(listNoti)
})
router.get('/count_unread',authMiddleware,async (req,res)=>{
    return res.json(await notificationController.CountUnread(req.user._id))
})
router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await notificationController.ListCategory(limit,page,filter);
    return res.json(listCategory)
})
router.get('/category/count',async(req,res)=>{
    return res.json(await notificationController.CountCategory())
})
router.post('/category/add',async(req,res)=>{
    var result = await notificationController.AddCategory(req.body)
 return res.json(result)

})
router.get('/category/detail',async(req,res)=>{

    var guideDetail = await notificationController.DetailCategory(req.query.id);
    return res.json(guideDetail.length? guideDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body
    return res.json(await notificationController.DeleteCategory(id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await notificationController.EditCategory(req.body))
})
module.exports = router;