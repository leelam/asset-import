var express = require('express');
var parseJSON =require('../helpers/safelyParseJSON')
var recruitCategoryController = require('../controllers/recruit_category.controller')
var recruitController = require('../controllers/recruit.controller')
var router = express.Router();
router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await recruitCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})
router.get('/category/list_tree',async(req,res)=>{
    return res.json(await recruitCategoryController.ListTree())
})
router.get('/category/all',async(req,res)=>{
    return res.json(await recruitCategoryController.List());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await recruitCategoryController.Count())
})
router.post('/category/add',async(req,res)=>{
    var result = await recruitCategoryController.Add(req.body)
    return res.json(result)
})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing Post Category ID')
    }
    var postDetail = await recruitCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body;
    return res.json(await recruitCategoryController.Delete(id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await recruitCategoryController.Edit(req.body))
})

router.post('/add',async(req,res)=>{
    return res.json(await recruitController.Add(req.body))
})

router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listPost = await recruitController.List(limit,page,filter);
    return res.json(listPost)
})

router.get('/count',async(req,res)=>{
    return res.json(await recruitController.Count(req.query.filter))
})
router.get('/detail',async(req,res)=>{
    return res.json(await recruitController.Detail(req.query.id))
})
router.delete('/delete',async(req,res)=>{
    return res.json(await recruitController.Delete(req.body.id))
})
router.put('/edit',async(req,res)=>{
    return res.json(await recruitController.Edit(req.body))
})

router.get('/all',async(req,res)=>{
    var listAll = await recruitCategoryController.AllItem();
    return res.json(listAll);
})

module.exports = router;