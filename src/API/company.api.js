const express = require('express');
const router = express.Router();
const parseJSON = require('../helpers/safelyParseJSON')
const companyCategoryController = require('../controllers/company_category.controller')
const _ =require('lodash')
const companyController = require('../controllers/company.controller')
router.get('/suggestion',async(req,res)=>{
    return res.json(await companyController.ListSugesstion(req.query.keyword || '', req.query.code))
})

router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCompany = await companyController.List(limit,page,filter);
    return res.json(listCompany)
})
router.post('/add',async(req,res)=>{
    return res.json(await companyController.Add(req.body))
})
router.put('/edit',async(req,res)=>{
    return res.json(await companyController.Edit(req.body))
})

router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await companyCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})

router.get('/category/list_tree',async(req,res)=>{
    return res.json(await companyCategoryController.ListTree());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await companyCategoryController.Count())
})
router.post('/category/add',async(req,res)=>{
    var result = await companyCategoryController.Add(req.body)
 return res.json(result)

})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing company category ID')
    }
    var postDetail = await companyCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body;
    return res.json(await companyCategoryController.Delete(id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await companyCategoryController.Edit(req.body))
})
router.get('/count',async(req,res)=>{
    
    let countResponse = await companyController.Count( parseJSON(req.query.filter) || []);
    return res.json(_.get(countResponse,'[0].total') || 0)
})
router.get('/detail',async(req,res)=>{
    return res.json(await companyController.Detail(req.query.id))
})
module.exports = router;