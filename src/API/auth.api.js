const express = require('express')
const router = express.Router();
const authBL = require('../controllers/auth.controller');
const authValid = require('../validate/auth.validate');
const fs = require('fs');
const _ = require('lodash')
const path = require('path');
const {CheckTokenServer} = require('../helpers/token.helper') 
const authConfig = require('../configs/auth.config')
const authenticateMiddleWare = require('../middlewares/authenticate.middleware')
const privateTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/privateToken.key`), 'utf8');
const privateRfTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/privateRfToken.key`), 'utf8');
const publicRfTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/publicRfToken.key`), 'utf8');
const privateRoleTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/privateRoleToken.key`), 'utf8');
const publicRoleTokenKey = fs.readFileSync(path.resolve(`${authConfig.tokenPath}/publicRoleToken.key`), 'utf8');
// Login
router.post('/login', async (req, res) => {
    var data = req.body;
    var checkType = await authBL.CheckTypeNumber(data);
    if (!checkType) {
        return res.json({message:'Số điện thoại nhập vào không đúng quy định!', isLogin:false});
    }
    var { error } = authValid.LoginValidation(data);

    if (error) return res.json({ message: error.details[0].message, isLogin: false });
    // checking phone
    var user = await authBL.Detail(null,data.phone)
    if (!user) {
        return res.json({ message: 'Số diện thoại không chính xác!', isLogin: false })
    };
    // checking password
    var validPass = await authBL.CheckPassword(data.password, user.password);
    if (!validPass) {
        return res.json({ message: 'Mật khẩu không chính xác!', isLogin:false })
    }
    
    var userGroup = await authBL.GetGroup(user._id)
    var userToken =  {
        last_name:user.last_name,
        first_name: user.first_name,
        phone: user.phone,
        id: user._id,
    }
    // Create and assign a token
    var token = authBL.GenToken(userToken, privateTokenKey, { expiresIn: 1000 * 60 * 60 * 24 * 365, algorithm: 'RS256' });
    var rfToken = authBL.GenToken(userToken, privateRfTokenKey, { expiresIn: 1000 * 60 * 60 * 24 * 365 * 2, algorithm: 'RS256' });
    var response = { message: "Đăng nhập thành công!",isLogin:true, status: true, token: `${token}`, refreshToken: rfToken, user:{...user,userGroup}  }
    var currentRole = {group_id:userGroup[0].group._id,company_id:userGroup[0].company._id}
    if(userGroup.length === 1) response. roleToken =authBL.GenToken(currentRole, privateRoleTokenKey, { expiresIn: 1000 * 60 * 60 * 24 * 365, algorithm: 'RS256' });
    return res.json(response)
})

router.get('/authenticate', authenticateMiddleWare, (req, res) => {
    res.json({message:'ok', status:true,isLogin:true})
});

router.post('/rfToken', async (req, res) => {
    var refreshToken = req.body.rfToken;

    if (refreshToken) {
        var check = await authBL.DecodedToken(refreshToken, publicRfTokenKey, { algorithms: ['RS256'] })
        if (check) {
            var payload = {
                id: check.id,
                role: check.role
            }
            var token = authBL.GenToken(payload, privateTokenKey, { expiresIn: 1000 * 30, algorithm: 'RS256' });
            res.json({ token: `JWT ${token}` })
        }
    }
})

router.get('/logout', async (req, res) => {
    res.json({isLogin:false});
})

router.post('/change_password',authenticateMiddleWare,async (req, res) =>{
    var data = req.body;
    var user =await authBL.CheckPhone(req.user);
    if (!user) {
        return res.json({ message: 'Số diện thoại không chính xác!', isLogin: false })
    };
    // checking password
    var validPass = await authBL.CheckPassword(data.oldPassword, user.password);
    if (!validPass) {
        return res.json({ message: 'Mật khẩu không chính xác!', isLogin:false })
    }
    var dataValidate = {newPassword: data.newPassword}
    var { error } = authValid.ChangePasswordValidate(dataValidate);
    if (error) return res.json({ message: error.details[0].message, status: false });
    await authBL.ChangePassword(data.newPassword, user.phone)
    return res.json({message: "Đổi mật khẩu thành công!", isLogin: false})
})



router.post('/check-login', authenticateMiddleWare, async(req, res) =>{
    const {_id} = req.user;
    
    const user = await authBL.Detail(_id,null)
    if(!user) return {status:false ,message:'User not found'}
    var userGroup = await authBL.GetGroup(user._id)
    const responseUser = {
            ...user,
            id: user._id,
            userGroup
    }
    res.json({ message: "Đăng nhập thành công!",isLogin:true, status: true, user: responseUser  })
})

router.post('/get-role-token', authenticateMiddleWare, async(req, res) =>{
    try {
    const { _id } = req.user;
    const {group_id,company_id} = req.body;
    if(!group_id)
        return {
            status: false,
            message: 'Không tìm thấy id user group'
        }
    var role = await authBL.GetGroup(_id,group_id,company_id);
    if(!role.length) return res.json({status:false,message:"Role not found!"})
    const token = await authBL.GenToken(req.body, privateRoleTokenKey,{ expiresIn: 1000 * 60 * 60 * 24 * 365, algorithm: 'RS256' });
    if(token)
        return res.json({
            status: true,
            token,
            role:role[0]
        })
    return res.json({
        status: false,
        message: 'Server lỗi'
    })
    }catch(err){
        return res.json({status:false,message:err.toString()})
    }
})
router.post('/get-role-active', authenticateMiddleWare,async(req, res)=>{
    const roleToken = req.cookies.roleToken || req.headers.roletoken;
    const dataToken = await CheckTokenServer(roleToken, publicRoleTokenKey);
    var role = await authBL.GetGroup(req.user._id,dataToken.group_id,dataToken.company_id);
    if(dataToken && role.length)
        return res.json({
            status: true,
            role: role[0]
        })
    return res.json({
        status: false
    })
})

router.post('/get_role',authenticateMiddleWare,async(req,res)=>{ 
    var listRole = await authBL.GetGroup(req.user._id);
    return res.json(listRole)
})
module.exports = router;