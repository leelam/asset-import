var express = require('express')
var router = express.Router();
var parseJSON = require('../helpers/safelyParseJSON')
var authMiddleware = require('../middlewares/authenticate.middleware')
var guideCategoryController = require('../controllers/guide_category.controller')
var guideController = require('../controllers/guide.controller');
var notificationSocket = require('../socket/notification.socket')
router.get('/category/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await guideCategoryController.List(limit, page, filter);
    return res.json(listCategory)
})

router.get('/category/list_tree', async (req, res) => {
    return res.json(await guideCategoryController.ListTree());
})

router.get('/category/count', async (req, res) => {
    return res.json(await guideCategoryController.Count())
})
router.post('/category/add', async (req, res) => {
    var result = await guideCategoryController.Add(req.body)
    return res.json(result)

})
router.get('/category/detail', async (req, res) => {
    var { id } = req.query;
    if (!id) {
        return res.json('Missing guide Category ID')
    }
    var guideDetail = await guideCategoryController.Detail(id);
    return res.json(guideDetail.length ? guideDetail[0] : {})
})

router.delete('/category/delete', async (req, res) => {
    var { id } = req.body;
    return res.json(await guideCategoryController.Delete(id))
})

router.put('/category/edit', authMiddleware, async (req, res) => {
    return res.json(await guideCategoryController.Edit({ ...req.body, write_uid: req.user._id }))
})

router.post('/add', authMiddleware, async (req, res) => {
    return res.json(await guideController.Add({ ...req.body, create_uid: req.user._id }))
})

router.get('/list', async (req, res) => {
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listguide = await guideController.List(limit, page, filter);
    return res.json(listguide)
})

router.get('/count', async (req, res) => {
    return res.json(await guideController.Count(req.query.filter))
})
router.get('/detail', async (req, res) => {
    return res.json(await guideController.Detail(req.query.id))
})
router.delete('/delete', async (req, res) => {
    return res.json(await guideController.Delete(req.body.id))
})
router.put('/edit', authMiddleware, async (req, res) => {
    const response = await guideController.Edit({ ...req.body, write_uid: req.user._id });
    notificationSocket.pushNotification(response,req.io)
    return res.json(response)
})

//---------
router.get('/test', async (req, res) => {
    return res.json(await guideController.Test())
})
//-----------
module.exports = router;