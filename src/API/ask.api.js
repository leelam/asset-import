var express = require('express');
var parseJSON =require('../helpers/safelyParseJSON')
var askCategoryController = require('../controllers/ask_category.controller')
var askController = require('../controllers/ask.controller')
var askCheckType = require('../helpers/askCheckType.helper')
var authMiddleware = require('../middlewares/authenticate.middleware')
var historyController = require('../controllers/history.controller')
var router = express.Router();
router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await askCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})

router.get('/category/list_tree',async(req,res)=>{
    return res.json(await askCategoryController.ListTree());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await askCategoryController.Count())
})
router.post('/category/add',async(req,res)=>{
    var result = await askCategoryController.Add(req.body)
    return res.json(result)
    
})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing Post Category ID')
    }
    var postDetail = await askCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})
router.delete('/category/delete',async(req,res)=>{
    var {_id } = req.body;
    return res.json(await askCategoryController.Delete(_id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await askCategoryController.Edit(req.body))
})
router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var type = askCheckType(req.query.type) ?req.query.type : "PENDING"
    var listAsk = await askController.List(limit,page,filter,null,type);
    return res.json(listAsk)
})

router.post('/add',authMiddleware,async(req,res)=>{
    return res.json(await askController.Add({...req.body,create_uid:req.user._id}))
})
router.put('/edit', authMiddleware, async(req,res)=>{
    return res.json(await askController.Edit(req.body, req.user._id))
})

router.get('/count',async(req,res)=>{
    return res.json(await askController.Count(parseJSON(req.query.filter),req.query.type))
})
router.get('/detail',async(req,res)=>{
    return res.json(await askController.Detail(req.query.id))
})
router.delete('/delete',async(req,res)=>{
    return res.json(await askController.Delete(req.body._id))
})

router.get('/history',async(req,res)=>{
    return res.json(await historyController.ListPostHistory(req.query.ask_id,req.query.type,req.query.limit));
})
module.exports = router;