var express = require("express");
var router = express.Router();
var commentController = require("../controllers/comment.controller");
const verifyTokenLogin  = require("../middlewares/authenticate.middleware");
router.get("/list", async (req, res) => {
  var limit = parseInt(req.query.limit) || 10;
  var offset = parseInt(req.query.offset) || 0;
  return res.json(
    await commentController.List(
      req.query.rel_id,
      req.query.create_date,
      req.query.parent_id,
      offset,
      limit
    )
  );
});

router.post("/add", verifyTokenLogin, async (req, res) => {
    return res.json(
      await commentController.Add(
        req.user,
        req.body.rel_id,
        req.body.title,
        req.body.parent_id
      )
    );
  });
router.get("/count_by_id", async (req, res) => {
  return res.json(await commentController.Count(req.query.rel_id));
});
module.exports = router;
