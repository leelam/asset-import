const express = require('express');
const router = express.Router();
const parseJSON = require('../helpers/safelyParseJSON')
const partnerCategoryController = require('../controllers/partner_category.controller')
const partnerController =require('../controllers/partner.controller')
router.get('/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listPartner = await partnerController.List(limit,page,filter);
    return res.json(listPartner)
})
router.get('/category/list',async(req,res)=>{
    var limit = parseInt(req.query.limit) || 10;
    var filter = parseJSON(req.query.filter) || [];
    limit = limit > 100 ? 100 : limit;
    var page = parseInt(req.query.page) || 1;
    var listCategory = await partnerCategoryController.List(limit,page,filter);
    return res.json(listCategory)
})

router.get('/category/list_tree',async(req,res)=>{
    return res.json(await partnerCategoryController.ListTree());
})

router.get('/category/count',async(req,res)=>{
    return res.json(await partnerCategoryController.Count())
})
router.post('/category/add',async(req,res)=>{
    var result = await partnerCategoryController.Add(req.body)
 return res.json(result)

})
router.get('/category/detail',async(req,res)=>{
    var {id}= req.query;
    if (!id) {
        return res.json('Missing company category ID')
    }
    var postDetail = await partnerCategoryController.Detail(id);
    return res.json(postDetail.length? postDetail[0] : {})
})

router.delete('/category/delete',async(req,res)=>{
    var {id } = req.body;
    return res.json(await partnerCategoryController.Delete(id))
})

router.put('/category/edit',async(req,res)=>{
    return res.json(await partnerCategoryController.Edit(req.body))
})


module.exports = router;